<?php

class Noty extends CWidget {
    /* defualt options of noty */

    private $defaultOptions = array(
        'layout' => 'center',
        'type' => 'alert',
        'text' => '',
        'dismissQueue' => true,
        'force' => true,
        'modal' => false,
        'maxVisible' => 5,
        'closeWith' => array('click'),
        'buttons' => false
    );
    /* template */
    private static $template = '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>';
    /* animation settings array */
    private static $animation = array(
        'open' => array('height' => 'toggle'),
        'close' => array('height' => 'toggle'),
        'easing' => 'swing',
        'speed' => 500,
    );

    /* callback array */
    private static $callback = array(
        'onShow' => array(),
        'afterShow' => array(),
        'onClose' => array(),
        'afterClose' => array(),
        'buttons' => false,
    );
    /* noty options */
    public $options = array();
    /* buttons array */
    private static $buttons_array = array();

    public function init() {
//        $this->publishAssets();
    }

    public function run() {

        $cs = Yii::app()->clientScript;
        $cs->registerScript(__CLASS__ . 'noty' . uniqid(), $this->getNoty() . ';', CClientScript::POS_READY);
    }

    private function publishAssets() {

        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);

        if (is_dir($assets)) {
            $cs = Yii::app()->clientScript;
            $cs->registerCoreScript('jquery');
            $cs->registerCssFile($baseUrl . '/buttons.css');

            $cs->registerScriptFile($baseUrl . '/jquery.noty.packaged.min.js', CClientScript::POS_HEAD);
        } else {
            throw new Exception('Noty  - Error: Couldn\'t find assets to publish.');
        }
    }

    public function getNoty() {
        $options = CJavaScript::encode(CMap::mergeArray($this->defaultOptions, $this->options));
        $js = "var n" . uniqid() . " = noty(" . $options . ")";
        return $js;
    }

}
