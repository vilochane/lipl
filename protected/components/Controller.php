<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/column1';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /* controller specific functions */
    /* delete confirmation message in views */

    public function deleteMeassage($column, $message = null) {
        return CommonSystem::deleteMeassage($column, $message);
    }

    public function encryptParam($param) {
        return CommonSystem::encryptParam($param);
    }

    public function decryptParam($param) {
        return CommonSystem::decryptParam($param);
    }

    /* noty success messages */

    public function getCreateSuccessMessage() {
        return "Record successfully added.";
    }

    public function getUpdateSuccessMessage() {
        return "Record successfully updated";
    }

    /* end of noty success messages */

    /**
     * @param string $getQuery get qury id
     * @param boolean $encrypted true retruns encrypted value else same value
     * comes handy with false to return same encrypted value and non encrypted values
     */
    public function getUrlQuery($getQuery, $encrypted = true) {
        $param = Yii::app()->request->getQuery($getQuery);
        if ($encrypted) {
            $param = $this->decryptParam(Yii::app()->request->getQuery($getQuery));
        }
        return $param;
    }

    /* This function prevents jquery conflicts in dialog popup */

    public function preventJscriptLoading() {
        /* preventing duplicating the script files this must for jquery dialog */
        Yii::app()->clientScript->corePackages = array();
        Yii::app()->clientScript->scriptMap['jquery-ui.min.js'] = false;
    }

    /**
     * if flash messages are set showing via noty
     */
    public function getNotyFlashMessage() {
        $flashKeyArray = array('alert', 'success', 'information', 'warning', 'error');
        foreach ($flashKeyArray as $key) {
            if (Yii::app()->user->hasFlash($key)) {
                $this->getNotyMessage(array('type' => $key, 'text' => Yii::app()->user->getFlash($key)));
            }
        }
    }

    public function setNumberFormat($value, $decimals = 3) {
        return number_format($value, $decimals);
    }

    /* end of controller specific functions */

    public function initFormatCurrency($optionsArray) {
        $optionsArray['clientOptions']['alias'] = 'decimal';
        $optionsArray['clientOptions']['groupSeparator'] = ',';
        $optionsArray['clientOptions']['autoGroup'] = true;
        $optionsArray['clientOptions']['rightAlign'] = false;
        $this->maskField($optionsArray);
    }

    /* initialize widgets */

    public function initTelephoneField($optionsArray) {
        if (!isset($optionsArray['clientOptions']['placeholder'])) {
            $optionsArray['clientOptions']['placeholder'] = "+## ### ### ###";
            $optionsArray['clientOptions']['removeMaskOnSubmit'] = false;
        }
        if (!isset($optionsArray["mask"])) {
            $optionsArray["mask"] = "+99 999 999 999";
        }
        $this->maskField($optionsArray);
    }

//    public function initInputMask($optionsArray) {
//        $this->widget('CMaskedTextField', array(
//            "model" => $optionsArray["model"],
//            "attribute" => $optionsArray["attribute"],
//            "mask" => $optionsArray["mask"],
//            "htmlOptions" => $optionsArray["htmlOptions"],
//        ));
//    }

    public function maskField($optionsArray) {
        $htmlOptions = array('class' => 'form-control', 'autocomplete' => 'off');
        $defaults = $clientOptions = array();
        $mask = '';
        if (isset($optionsArray['clientOptions'])) {
            $clientOptions = $optionsArray['clientOptions'];
        }
        if (isset($optionsArray['defaults'])) {
            $defaults = $optionsArray['defaults'];
        }
        if (isset($optionsArray['mask'])) {
            $mask = $optionsArray['mask'];
        }
        if (isset($optionsArray['htmlOptions'])) {
            $htmlOptions = array_merge($htmlOptions, $optionsArray['htmlOptions']);
        }
        $this->widget("ext.maskedInput.MaskedInput", array(
            "model" => $optionsArray["model"],
            "attribute" => $optionsArray["attribute"],
            "mask" => $mask,
            "clientOptions" => $clientOptions,
            "defaults" => $defaults,
            "htmlOptions" => $htmlOptions
        ));
    }

    /**
     * initialize the Cgrid view delete confinmation noty
     */
    public function getNotyMessage($optionsArray = array()) {
        $defaultOptionsArray = array(
            'layout' => 'top',
            'type' => 'success',
            'dismissQueue' => true,
            'force' => true,
            'modal' => false,
            'maxVisible' => 5,
            'buttons' => false,
            'timeout' => 3000
        );
        if (count($optionsArray) > 0) {
            $defaultOptionsArray = array_merge($defaultOptionsArray, $optionsArray);
        }
        $notyMessage = $this->widget('ext.noty.Noty', array(
            'options' => $defaultOptionsArray
        ));
        return $notyMessage;
    }

    /**
     * init select2
     *  ex array(
     *             'model' => $model,
     *               'attribute' => 'branch_ids',
     *               'data' => Common::getBranches(),
     *               'htmlOptions' => array(
     *              'multiple' => 'multiple',
     *               'placeholder' => 'Please select a branch',
      )
     * )
     */
    public function initSelect2($optionsArray) {
        $this->widget('ext.select2.ESelect2', $optionsArray);
    }

    /**
     * Cjui date pikcer
     */
    public function initDatePicker($optionsArray) {
        $defaultOptionsArray = array(
            'options' => array(
                'dateFormat' => 'yy-mm-dd',
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => '-100:+0',
                'showButtonPanel' => true,
                'maxDate' => 0,
            ),
            'htmlOptions' => array(
                'class' => 'form-control',
                'autocomplete' => 'off'
            )
        );
        if (isset($optionsArray['options']) && count($optionsArray['options']) > 0) {
            $optionsArray['options'] = array_merge($defaultOptionsArray['options'], $optionsArray['options']);
        }
        $defaultOptionsArray = array_merge($defaultOptionsArray, $optionsArray);
        $this->widget("zii.widgets.jui.CJuiDatePicker", $defaultOptionsArray
        );
    }

    /**
     * This function returns CJuiDialog
     * @param $optionsArray array(
     *  'ajaxLink' => 'controller/action',
     *  'dialogId' => 'SampleId',
     *  'dialogTitle' =>'Create New Item'
     *  'params'=>array('s2'=>'select2Id') // must
     * )
     */
    public function initDialog($optionsArray = array("ajaxLink" => "", "dialogId" => "", "dialogTitle" => "", "select2Id" => "", "params" => array())) {
        $defaultOptionsArray = array(
            'title' => $optionsArray['dialogTitle'],
            'position' => 'center',
            'width' => 'auto',
            'height' => 'auto',
            'autoOpen' => false,
            'resizable' => false,
            'stack' => false,
            'modal' => true,
        );
        //$defaultOptionsArray = array_merge($defaultOptionsArray, $optionsArray);
        $params = array();
        if (isset($optionsArray['params']) && count($optionsArray['params']) > 0) {
            $params = $optionsArray['params'];
        }
        echo CHtml::ajaxLink(Yii::app()->params['addNewButton'], Yii::app()->createAbsoluteUrl($optionsArray['ajaxLink']), array('data' => array_merge(array('s2' => $optionsArray["select2Id"], 'di' => $optionsArray["dialogId"]), $params), 'update' => '#' . $optionsArray['dialogId']), array('id' => $optionsArray['dialogId'] . 'AjaxLink' . uniqid(), 'onclick' => '$("#' . $optionsArray['dialogId'] . '").dialog("open")'));
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id' => $optionsArray['dialogId'],
            'options' => $defaultOptionsArray,
        ));
        $this->endWidget('zii.widgets.jui.CJuiDialog');

        //return array('ajaxLink' => $ajaxLink, 'dialog' => $dialog);
    }

    /** end of initialize widgets * */

    /**
     * This function sets model logging data
     * @param $m0del model object
     */
    public function setLoggingData($model) {
        $model->updated_by = $this->getUserId();
        $model->updated_date = $this->dateTimeOrTimeStamp();
    }

    /* user fucntions */
    /* this fucntion returns the user working branch */

    public function getUserWorkingBranchId() {
        return CommonUser::getUserWorkingBranchId();
    }

    /* this function updates the current user working branch */

    public function updateUserWorkingBranch($branchId) {
        $workingBranch = CmBranch::model()->findByPk($branchId);
        Yii::app()->user->setState("getUserWorkingBranchId", $branchId);
        Yii::app()->user->setState("getUserWorkingBranchName", $workingBranch->branch_name);
        Yii::app()->user->setFlash("information", "Working branch successfully selected.");
    }

    /* when employee profile updated updatin the user branches */

    public function setUserWorkingBranches($branchIds) {
        if (count($branchIds) > 0) {
            Yii::app()->user->setState("getUserBranchIds", $branchIds);
        }
    }

    public function dateTimeOrTimeStamp() {
        return Common::dateTimeOrTimeStamp(false, false, false);
    }

    /* User functions */

    public function getUserId() {
        return CommonUser::getUserId();
    }

}
