<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $user = SysUser::model()->find(new CDbCriteria(
                array(
            "together" => true,
            "with" => array(
                "mapEmployeeBranches" => array("joinType" => "INNER JOIN", "condition" => "employee_branch_status = '1' "),
                "peEmployee" => array("joinType" => "INNER JOIN")
            ),
            "condition" => "t.user_username = :username",
            "params" => array(":username" => $this->username)
        )));

        if (empty($user)) {
            /* ignored the ERROR_USERNAME_INVALID since the same error message displayed. */
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } elseif ($user->user_status === '0' || $user->user_status === '2') {
            $this->errorCode = self::ERROR_INACTIVE_USER;
        } elseif (CommonSystem::hashString($this->password) === $user->user_password) {
            $this->errorCode = self::ERROR_NONE;
            $userBranchIds = array();
            Yii::app()->user->setState("getUserId", $user->user_id);
            Yii::app()->user->setState("getUserRoleId", $user->user_role_id);
            Yii::app()->user->setState("getUserEmpId", $user->emp_id);
            Yii::app()->user->setState("getUserSuperUserStatus", $user->user_su_status);
            Yii::app()->user->setState("getUserDesignationId", $user->peEmployee->designation_id);
            if (!empty($user->mapEmployeeBranches)) {
                $empBranchMapping = $user->mapEmployeeBranches;
                foreach ($empBranchMapping as $key => $empBranchMapp) {
                    $userBranchIds[] = $empBranchMapp->branch_id;
                }
            }
            Yii::app()->user->setState("getUserBranchIds", $userBranchIds);
            $userWorkingBranchId = null;
            if (count($userBranchIds) === 1) {
                $userWorkingBranchId = $userBranchIds[0];
            }
            Yii::app()->user->setState("getUserWorkingBranchId", $userWorkingBranchId);
            $workingBranch = CmBranch::model()->findByPk($userWorkingBranchId);
            $workingBranchName = null;
            if(!empty($workingBranch)){
                $workingBranchName = $workingBranch->branch_name;
            }                    
            Yii::app()->user->setState("getUserWorkingBranchName", $workingBranchName);
        } else {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        return $this->errorCode;
    }

}
