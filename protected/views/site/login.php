<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array( 'Login',);
?>
<style type="text/css">
    div.form label {
        min-width: 10px;
    }
</style>
<h1>Login</h1>

<div class = "form col-xs-8 col-sm-8 col-md-5 col-lg-5">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <label class="required glyphicon glyphicon glyphicon-user" for="<?php echo CHtml::activeId($model, 'username') ?>"></label>           
            </span>
            <?php echo $form->textField($model, 'username', array('class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model, 'username'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <label class="required glyphicon glyphicon glyphicon glyphicon-lock" for="<?php echo CHtml::activeId($model, 'password') ?>"></label>      
            </span>
            <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->checkBox($model, 'rememberMe'); ?>                
            </span>
            <?php echo $form->label($model, 'rememberMe', array('class' => 'form-control')); ?>
        </div>
        <?php echo $form->error($model, 'rememberMe'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Login', array('class' => 'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
