<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name . ' - Select a Working Branch';
$this->breadcrumbs = array(
    'Select a Working Branch',
);
?>

<h1>Select a Working Branch</h1>


<p>
    Please select a working branch from below assigned branches.
</p>

<div class="form col-xs-12 col-sm-12 col-md-8 col-lg-8">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'select-branch-form',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'branch_id'); ?>
            </span>		
            <?php
            $branchListData = array();
            $branchIds = CommonUser::getUserBranchIds();
           // Common::displayResult(CommonUser::getUserWorkingBranchId());
            $ct = new CDbCriteria();
            if(!empty($branchIds)){
                foreach ($branchIds as $branchId) {
                $ct->compare("branch_id", $branchId, false, 'OR');
            }
            $branchListData = CHtml::listData(CmBranch::model()->findAll($ct), "branch_id", "branch_name");
            }                       
            $this->initSelect2(array(
                'model' => $model,
                'attribute' => 'branch_id',
                'data' => $branchListData,
                'options' => array(
                    'placeholder' => 'Please select a working branch.'
                )                
            ));
            ?>
        </div>
        <?php echo $form->error($model, 'branch_id'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit', array("class"=>"btn btn-default")); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
