<?php
/* @var $this PeCusGuarantorIncomeSourcesController */
/* @var $data PeCusGuarantorIncomeSources */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('income_source_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->income_source_id), array('view', 'id'=>$data->income_source_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_id')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('income_source_name')); ?>:</b>
	<?php echo CHtml::encode($data->income_source_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('income_source_income')); ?>:</b>
	<?php echo CHtml::encode($data->income_source_income); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('income_source_expense')); ?>:</b>
	<?php echo CHtml::encode($data->income_source_expense); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('income_source_net')); ?>:</b>
	<?php echo CHtml::encode($data->income_source_net); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>