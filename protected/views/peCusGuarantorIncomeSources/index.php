<?php
/* @var $this PeCusGuarantorIncomeSourcesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pe Cus Guarantor Income Sources',
);

$this->menu=array(
	array('label'=>'Create PeCusGuarantorIncomeSources', 'url'=>array('create')),
	array('label'=>'Manage PeCusGuarantorIncomeSources', 'url'=>array('admin')),
);
?>

<h1>Pe Cus Guarantor Income Sources</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
