<?php
/* @var $this PeCusGuarantorIncomeSourcesController */
/* @var $model PeCusGuarantorIncomeSources */

$this->breadcrumbs=array(
	'Pe Cus Guarantor Income Sources'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PeCusGuarantorIncomeSources', 'url'=>array('index')),
	array('label'=>'Manage PeCusGuarantorIncomeSources', 'url'=>array('admin')),
);
?>

<h1>Create PeCusGuarantorIncomeSources</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>