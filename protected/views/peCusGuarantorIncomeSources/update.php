<?php
/* @var $this PeCusGuarantorIncomeSourcesController */
/* @var $model PeCusGuarantorIncomeSources */

$this->breadcrumbs=array(
	'Pe Cus Guarantor Income Sources'=>array('index'),
	$model->income_source_id=>array('view','id'=>$this->encryptParam($model->income_source_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List PeCusGuarantorIncomeSources', 'url'=>array('index')),
	array('label'=>'Create PeCusGuarantorIncomeSources', 'url'=>array('create')),
	array('label'=>'View PeCusGuarantorIncomeSources', 'url'=>array('view', 'id'=>$model->income_source_id)),
	array('label'=>'Manage PeCusGuarantorIncomeSources', 'url'=>array('admin')),
);
?>

<h1>Update PeCusGuarantorIncomeSources <?php echo $model->income_source_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>