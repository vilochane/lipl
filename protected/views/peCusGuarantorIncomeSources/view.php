<?php
/* @var $this PeCusGuarantorIncomeSourcesController */
/* @var $model PeCusGuarantorIncomeSources */

$this->breadcrumbs=array(
	'Pe Cus Guarantor Income Sources'=>array('index'),
	$model->income_source_id,
);

$this->menu=array(
	array('label'=>'List PeCusGuarantorIncomeSources', 'url'=>array('index')),
	array('label'=>'Create PeCusGuarantorIncomeSources', 'url'=>array('create')),
	array('label'=>'Update PeCusGuarantorIncomeSources', 'url'=>array('update', 'id'=>$model->income_source_id)),
	array('label'=>'Delete PeCusGuarantorIncomeSources', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->income_source_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PeCusGuarantorIncomeSources', 'url'=>array('admin')),
);
?>

<h1>View PeCusGuarantorIncomeSources <?php echo $model->income_source_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'income_source_id',
		'guarantor_id',
		'income_source_name',
		'income_source_income',
		'income_source_expense',
		'income_source_net',
		'updated_by',
		'updated_date',
	),
)); ?>
