<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <?php
        $cs = Yii::app()->clientScript;
        /* css */
        $cs->registerCssFile(Yii::app()->request->baseUrl . '/bootstrap/css/bootstrap.min.css');
        $cs->registerCssFile(Yii::app()->request->baseUrl . '/bootstrap/css/bootstrap-theme.min.css');
        $cs->registerCssFile(Yii::app()->request->baseUrl . '/css/bootstrap-extended.css');
        /* jquery */
        $cs->registerCoreScript('jquery', CClientScript::POS_HEAD);
        $cs->registerCoreScript('jquery.ui', CClientScript::POS_HEAD);
        /* bootstrap */
        $cs->registerScriptFile(Yii::app()->request->baseUrl . '/bootstrap/js/bootstrap.min.js', CClientScript::POS_HEAD);
        /* noty */
        $cs->registerScriptFile(Yii::app()->request->baseUrl . "/js/jquery.noty.packaged.min.js", CClientScript::POS_HEAD);
        $cs->registerScriptFile(Yii::app()->request->baseUrl . "/js/system.js", CClientScript::POS_HEAD);
        /* if flash messages set */
        $this->getNotyFlashMessage();
        ?>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>
        <!--        loader-->
        <div id="overlay-loader">
            <div id="loader-wrapper">
                <div id="loader"></div> 
                <div class="loader-section section-left"></div>
                <div class="loader-section section-right"></div> 
            </div>
        </div>        
        <!-- end of loader-->


        <div id="page" class="container-fluid" >
            <div id="header">
                <nav class="navbar navbar-inverse" role="navigation">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo Yii::app()->createAbsoluteUrl("site/index") ?>"><?php echo CHtml::encode(Yii::app()->name); ?></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">                            
                            <?php
                            $this->widget('zii.widgets.CMainMenu', array(
                                'items' => SystemMainMenu::getSystemMainMenu(),
                            ));
                            ?>                                
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav><!-- mainmenu -->
            </div><!-- header -->

            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
            <?php endif ?>            
            <div class="container-fluid">
                <?php echo $content; ?>
            </div>
            <div class="clear"></div>

            <div id="footer">
                Copyright <?php echo date('Y'); ?> <?php echo CHtml::encode(Yii::app()->name); ?>
            </div><!-- footer -->
        </div><!-- page -->

    </body>
</html>
<?php
//if(!Yii::app()->user->isGuest){
//    // leaving with unsaved document confirmation
$cs->registerScript('main-document-ready', ''
        . 'var bodyChangeCount = 0;' //variable to check the content change count 
        . '$(document).ready(function(){'
        . 'removeOverlayLoader();'
        . '$("form").on("change", function(){ ++bodyChangeCount;});'
        . '$(".menu li a").click(function(event){
            if(bodyChangeCount > 0){    
                $(window).unbind("beforeunload");
                event.preventDefault();
                confirmationNoty($(this));                   
            }            
           });'
        . ''
        . '});'
//        . '$("input[type=\'submit\']").click(function(event){'
//        . 'console.log("form submission");'
//        . '$(window).unbind("beforeunload");'
//        . 'event.preventDefault();'
//        . 'noty({layout: "center", type: "warning", text: "Would like to submit the form?", buttons: [
//            {addClass: "btn btn-primary", text: "Ok", onClick: function($noty) {
//                    $noty.close();
//                    console.log($("form"));
//                    $("form")[0].submit(); // if there popup since the global selector those will too get submitted so to avoid only submitting buttons form
//                }},
//            {addClass: "btn btn-warning", text: "Cancel", onClick: function($noty) {
//                    $noty.close();
//                    noty({layout: "center", type: "information", timeout: 2500, text: "Operation cancelled"});
//                }}
//        ]});'
//        . ''
//        . '});'//on for msubmit inbindig the befroe unload
//        . '$(window).on("beforeunload", function(){
//                var message = "This should create a pop-up";
//                if(bodyChangeCount <= 0){ //if greater than o not letting to do so
//                    message = "";
//                }
//                return message;
//         });'
//        . ''
       . '', CClientScript::POS_READY);
//}
?>
