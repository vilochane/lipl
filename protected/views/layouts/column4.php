<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . "/css/bootstrap-extended.css"); ?>
<div class="container-fluid">
    <div class="row">
        <div id="col-sm-9 col-lg-6">
            <?php echo $content; ?>
        </div><!-- content -->
    </div> 
</div> <!-- page-->
