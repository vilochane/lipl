<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="sidebar" class="col-xs-12 col-sm-12 col-md-4 col-lg-3">   
        <?php
        $this->beginWidget('zii.widgets.CPortlet', array(
            'title' => 'Operations',
        ));
        $this->widget('zii.widgets.CMenu', array(
            'items' => $this->menu,
            'htmlOptions' => array('class' => 'list-group'),
        ));
        $this->endWidget();
        ?>
    </div>
<div id="content" class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
    <?php echo $content; ?>
</div>

<?php $this->endContent(); ?>