<td>
    <?php echo $form->hiddenField($model, "[$rowIndex]credit_ref_details_id"); ?>
    <?php echo $form->hiddenField($model, "[$rowIndex]cus_id"); ?>
    <?php echo $form->hiddenField($model, "[$rowIndex]modelScenario", array("value" => $model->getScenario())); ?>
    <?php echo $form->textField($model, "[$rowIndex]credit_ref_details_cmp_name", array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>   
    <?php echo $form->error($model, "[$rowIndex]credit_ref_details_cmp_name"); ?>
</td>

<td>
    <?php echo $form->textField($model, "[$rowIndex]credit_ref_details_cmp_tel", array('class' => 'form-control', 'size' => 15, 'maxlength' => 15, 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, "[$rowIndex]credit_ref_details_cmp_tel"); ?>
</td>


<td>
    <?php echo $form->textArea($model, "[$rowIndex]credit_ref_details_cmp_address", array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, "[$rowIndex]credit_ref_details_cmp_address"); ?>
</td>



