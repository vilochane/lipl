<?php
/* @var $this PeCustomerController */
/* @var $model PeCustomer */

$this->breadcrumbs=array(
	'Customers'=>array('index'),
	$model->cus_first_name=>array('view','id'=>$this->encryptParam($model->cus_id)),
	'Update',
);
$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array(
                'PeCustomerCreate' => array(),
                'PeCustomerView' => array('url' => array('id' => $model->cus_id)),
                'PeCustomerManage' => array(),
            )
        ));
?>

<h1>Update Customer <?php echo $model->cus_first_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, "childModels"=>$childModels)); ?>