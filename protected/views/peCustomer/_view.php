<?php
/* @var $this PeCustomerController */
/* @var $data PeCustomer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->cus_id), array('view', 'id'=>$data->cus_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_id')); ?>:</b>
	<?php echo CHtml::encode($data->branch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('divisional_secretariat_id')); ?>:</b>
	<?php echo CHtml::encode($data->divisional_secretariat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_first_name')); ?>:</b>
	<?php echo CHtml::encode($data->cus_first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_middle_name')); ?>:</b>
	<?php echo CHtml::encode($data->cus_middle_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_last_name')); ?>:</b>
	<?php echo CHtml::encode($data->cus_last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_gender')); ?>:</b>
	<?php echo CHtml::encode($data->cus_gender); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_marital_status')); ?>:</b>
	<?php echo CHtml::encode($data->cus_marital_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_nationality')); ?>:</b>
	<?php echo CHtml::encode($data->cus_nationality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_nic')); ?>:</b>
	<?php echo CHtml::encode($data->cus_nic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_slin')); ?>:</b>
	<?php echo CHtml::encode($data->cus_slin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_drivers_license')); ?>:</b>
	<?php echo CHtml::encode($data->cus_drivers_license); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_passport_no')); ?>:</b>
	<?php echo CHtml::encode($data->cus_passport_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_dob')); ?>:</b>
	<?php echo CHtml::encode($data->cus_dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_address')); ?>:</b>
	<?php echo CHtml::encode($data->cus_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_current_address')); ?>:</b>
	<?php echo CHtml::encode($data->cus_current_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_official_address')); ?>:</b>
	<?php echo CHtml::encode($data->cus_official_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_employer_address')); ?>:</b>
	<?php echo CHtml::encode($data->cus_employer_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_resident_duration')); ?>:</b>
	<?php echo CHtml::encode($data->cus_resident_duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_resident_info')); ?>:</b>
	<?php echo CHtml::encode($data->cus_resident_info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_profession')); ?>:</b>
	<?php echo CHtml::encode($data->cus_profession); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_organization')); ?>:</b>
	<?php echo CHtml::encode($data->cus_organization); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_telephone')); ?>:</b>
	<?php echo CHtml::encode($data->cus_telephone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_telephone2')); ?>:</b>
	<?php echo CHtml::encode($data->cus_telephone2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->cus_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_mobile2')); ?>:</b>
	<?php echo CHtml::encode($data->cus_mobile2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_email')); ?>:</b>
	<?php echo CHtml::encode($data->cus_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_email2')); ?>:</b>
	<?php echo CHtml::encode($data->cus_email2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_income_source')); ?>:</b>
	<?php echo CHtml::encode($data->cus_income_source); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_net_income')); ?>:</b>
	<?php echo CHtml::encode($data->cus_net_income); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_assets')); ?>:</b>
	<?php echo CHtml::encode($data->cus_assets); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_spouse_assets')); ?>:</b>
	<?php echo CHtml::encode($data->cus_spouse_assets); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_introducer_name')); ?>:</b>
	<?php echo CHtml::encode($data->cus_introducer_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_introducer_telephone')); ?>:</b>
	<?php echo CHtml::encode($data->cus_introducer_telephone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_introducer_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->cus_introducer_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_introducer_address')); ?>:</b>
	<?php echo CHtml::encode($data->cus_introducer_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_nic_scan_copy')); ?>:</b>
	<?php echo CHtml::encode($data->cus_nic_scan_copy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_image')); ?>:</b>
	<?php echo CHtml::encode($data->cus_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>