<?php
/* @var $this PeCustomerController */
/* @var $model PeCustomer */
/* @var $form CActiveForm */
$cs = Yii::app()->clientScript;
$cs->registerCoreScript('yiiactiveform');
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'pe-customer-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="panel panel-default">
        <div class="panel-heading"><div class="panel-title">For System Use</div></div>
        <div class="panel-body">
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'branch_id'); ?>
                    </span>		
                    <?php
                    $this->initSelect2(array(
                        'model' => $model,
                        'attribute' => 'branch_id',
                        'data' => CommonListData::getListDataBranches(),
                        'options' => array(
                            'placeholder' => 'Please select a branch.'
                        ),
                    ));
                    ?>
                </div>
                <?php echo $form->error($model, 'branch_id'); ?>
            </div>
        </div>
    </div>

    <!--         personal details   -->
    <div class="panel panel-default">
        <div class="panel-heading"><div class="panel-title">Personal Information</div></div>
        <div class="panel-body">
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'divisional_secretariat_id'); ?>
                    </span>		
                    <?php
                    $this->initSelect2(array(
                        'model' => $model,
                        'attribute' => 'divisional_secretariat_id',
                        'data' => CommonListData::getListDataDivisionalSecretariat(),
                        'options' => array(
                            'placeholder' => 'Please select a divisional secretariat.'
                        ),
                    ));
                    ?>            
                    <span class="input-group-addon">
                        <?php
                        $this->initDialog(array(
                            'ajaxLink' => 'CmDivisionalSecretariat/AjaxCreate',
                            'dialogId' => 'divisionalSecretariotDialog',
                            'dialogTitle' => 'Create a New Divisional Secretariot',
                            'select2Id' => 'PeCustomer_divisional_secretariat_id'
                        ));
                        ?>
                    </span>
                </div>
                <?php echo $form->error($model, 'divisional_secretariat_id'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_first_name'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_first_name', array('class' => 'form-control', 'size' => 100, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_first_name'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_middle_name'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_middle_name', array('class' => 'form-control', 'size' => 100, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_middle_name'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_last_name'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_last_name', array('class' => 'form-control', 'size' => 100, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_last_name'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_gender'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'cus_gender', CommonListData::getListDataGender(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_gender'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_marital_status'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'cus_marital_status', CommonListData::getListDataMartalStatus(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_marital_status'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_nationality'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'cus_nationality', CommonListData::getListDataNationality(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_nationality'); ?>
            </div>
            
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_dob'); ?>
                    </span>		
                    <?php
                    $this->initDatePicker(array(
                        "model" => $model,
                        "attribute" => "cus_dob"
                    ));
                    ?>            
                </div>
                <?php echo $form->error($model, 'cus_dob'); ?>
            </div>
            
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_nic'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_nic', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_nic'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_slin'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_slin', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_slin'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_drivers_license'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_drivers_license', array('class' => 'form-control', 'size' => 7, 'maxlength' => 7, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_drivers_license'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_passport_no'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_passport_no', array('class' => 'form-control', 'size' => 5, 'maxlength' => 5, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_passport_no'); ?>
            </div>            

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_address'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'cus_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_address'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_current_address'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'cus_current_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_current_address'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_official_address'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'cus_official_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_official_address'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_employer_address'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'cus_employer_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_employer_address'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'residence_duration_id'); ?>
                    </span>		
                    <?php
                    $this->initSelect2(array(
                        "model" => $model,
                        "attribute" => "residence_duration_id",
                        "data" => CommonListData::getListDataResidenceDuration(),
                        "options" => array('placeholder' => 'Please select a residence duration.')
                    ));
                    ?>
                    <span class="input-group-addon">
                        <?php
                        $this->initDialog(array(
                            'ajaxLink' => 'CmResidenceDuration/AjaxCreate',
                            'dialogId' => 'residenceDurationDialog',
                            'dialogTitle' => 'Create a New Residence Duration',
                            'select2Id' => 'PeCustomer_residence_duration_id'
                        ));
                        ?>
                    </span>
                </div>
                <?php echo $form->error($model, 'residence_duration_id'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_resident_info'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'cus_resident_info', CommonListData::getListDataResidenceInfo(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_resident_info'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'profession_id'); ?>
                    </span>		
                    <?php
                    $this->initSelect2(array(
                        "model" => $model,
                        "attribute" => "profession_id",
                        "data" => CommonListData::getListDataProfession(),
                        "htmlOptions" => array(
                            "placeholder" => "Please select a profession."
                        )
                    ));
                    ?>
                    <span class="input-group-addon">
                        <?php
                        $this->initDialog(array(
                            'ajaxLink' => 'CmProfession/AjaxCreate',
                            'dialogId' => 'professionDialog',
                            'dialogTitle' => 'Create a New Profession',
                            'select2Id' => 'PeCustomer_profession_id'
                        ));
                        ?>
                    </span>
                </div>
                <?php echo $form->error($model, 'profession_id'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_organization'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'cus_organization', CommonListData::getListDataCusOrganization(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_organization'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_telephone'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "cus_telephone")) ?>
                </div>
                <?php echo $form->error($model, 'cus_telephone'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_telephone2'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "cus_telephone2")) ?>
                </div>
                <?php echo $form->error($model, 'cus_telephone2'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_mobile'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "cus_mobile")) ?>
                </div>
                <?php echo $form->error($model, 'cus_mobile'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_mobile2'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "cus_mobile2")) ?>
                </div>
                <?php echo $form->error($model, 'cus_mobile2'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_email'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_email'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_email2'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_email2', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_email2'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_income_source'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'cus_income_source', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_income_source'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_net_income'); ?>
                    </span>		
                    <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "cus_net_income")) ?>
                </div>
                <?php echo $form->error($model, 'cus_net_income'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_assets'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'cus_assets', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_assets'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_spouse_assets'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'cus_spouse_assets', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_spouse_assets'); ?>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_nic_scan_copy'); ?>
                    </span>		
                    <?php echo $form->fileField($model, 'cus_nic_scan_copy', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
                    <span id="cus-nic-image-preview" class="input-group-addon">
                        <?php
                        if (!empty($model->cus_nic_scan_copy)) {
                            echo Common::getImage('images/cusImages/nicImages', $model->cus_nic_scan_copy, "thumb");
                        }
                        ?>
                    </span>
                </div>
                <?php echo $form->error($model, 'cus_nic_scan_copy'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_image'); ?>
                    </span>		
                    <?php echo $form->fileField($model, 'cus_image', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
                    <span id="cus-image-preview" class="input-group-addon">
                        <?php
                        if (!empty($model->cus_image)) {
                            echo Common::getImage('images/cusImages/cusImages', $model->cus_image, "thumb");
                        }
                        ?>
                    </span>
                </div>
                <?php echo $form->error($model, 'cus_image'); ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><div class="panel-title">Introducer Information</div></div>
        <div class="panel-body">
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_introducer_name'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'cus_introducer_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_introducer_name'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_introducer_telephone'); ?>
                    </span>		
                   <?php $this->initTelephoneField(array("model" => $model, "attribute" => "cus_introducer_telephone")) ?>
                </div>
                <?php echo $form->error($model, 'cus_introducer_telephone'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_introducer_mobile'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "cus_introducer_mobile")) ?>
                </div>
                <?php echo $form->error($model, 'cus_introducer_mobile'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'cus_introducer_address'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'cus_introducer_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'cus_introducer_address'); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <fieldset> <caption>Bankers Details</caption><?php $this->renderPartial("__from-bank-details-table", array("form" => $form, "childModels" => $childModels)); ?>
        </fieldset>
    </div>

    <div class="row">
        <fieldset> <caption>Credit Reference Details</caption><?php $this->renderPartial("__from-credit-ref-details-table", array("form" => $form, "childModels" => $childModels)); ?>
        </fieldset>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
$cs->registerScript("customer-form-script", ""
        . "$('#PeCustomer_cus_nic_scan_copy').change(function(){ imagePreview($(this)[0], 'cus-nic-image-preview'); });"
        . "$('#PeCustomer_cus_image').change(function(){ imagePreview($(this)[0], 'cus-image-preview'); });"
        . "var bankDetailsRow = new String(" . CJSON::encode($this->renderPartial("__from-bank-details-rows", array("rowIndex" => "count", "form" => $form, "model" => new PeCusBankDetails), true, false)) . ");"
        . "$('#add-bank-details-btn').click(function(){"
        . "var rowCount = $('#bank-details-table tbody tr').length;"
        . "var rowNo = ++rowCount;"
        . "$('#bank-details-table tbody').append('<tr>"
        . "<td><span>'+rowNo+'</span></td>"
        . "'+bankDetailsRow.replace(/count/g, rowCount)+'"
        . "<td>" . CommonSystem::getTabualrButtonLink("insert") . "</td>"
        . "</tr>');"
        . "});"
        . ""
        . "var creditRefDetailsRow = new String(" . CJSON::encode($this->renderPartial("__from-credit-ref-details-rows", array("rowIndex" => "count", "form" => $form, "model" => new PeCusCreditRefDetails), true, false)) . ");"
        . "$('#add-credit-ref-details-btn').click(function(){"
        . "var rowCount = $('#credit-ref-table tbody tr').length;"
        . "var rowNo = ++rowCount;"
        . "$('#credit-ref-table tbody').append('<tr>"
        . "<td><span>'+rowNo+'</span></td>"
        . "'+creditRefDetailsRow.replace(/count/g, rowCount)+'"
        . "<td>" . CommonSystem::getTabualrButtonLink("insert") . "</td>"
        . "</tr>');"
        . "});"
        /* disabling browser refresh */
        . "$(document).bind('keydown', disableBrowserRefresh);"
        . "", CClientScript::POS_READY);
?>