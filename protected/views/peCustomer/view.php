<?php
/* @var $this PeCustomerController */
/* @var $model PeCustomer */

$this->breadcrumbs = array(
    'Customer' => array('index'),
    $model->cus_first_name,
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array(
                'PeCustomerCreate' => array(),
                'PeCustomerUpdate' => array('url' => array('id' => $model->cus_id)),
                'PeCustomerDelete' => array('linkOptions' => array('submit' => array('id' => $model->cus_id), 'confirm' => CommonSystem::deleteMeassage("customer " . $model->cus_first_name) . '?')),
                'PeCustomerManage' => array(),                
            ),
            'Seperator'=>array('SeperatorLink' => array()),
            'PeCusGuarantor'=>array(
                'PeCusGuarantorSeperator' => array(),
                'PeCusGuarantorCreate' => array('url'=>array('c'=>$model->cus_id)),
                'PeCusGuarantorManage' => array('url'=>array('c'=>$model->cus_id)),
            ),
            'Seperator1'=>array('SeperatorLink1' => array()),
            'HipAgreement'=>array(
                'HipAgreementSeperator'=>array(),
                'HipAgreementCreate'=>array('url'=>array('c'=>$model->cus_id)),
                'HipAgreementManage'=>array('url'=>array('c'=>$model->cus_id))
            )
        ));
?>

<h1>View Customer <?php echo $model->cus_first_name; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array("name" => 'branch_id', "value" => $model->branch->branch_name),
        array("name" => 'divisional_secretariat_id', "value" => $model->divisionalSecretariat->divisional_secretariat_name),
        'cus_first_name',
        'cus_middle_name',
        'cus_last_name',
        array('name'=>'cus_gender', 'value'=>  CommonListData::getListDataGenderValue($model->cus_gender)),
        array('name'=>'cus_marital_status', 'value'=>  CommonListData::getListDataMartalStatusValue($model->cus_marital_status)),   
        array("name" => 'cus_nationality', "value" => CommonListData::getListDataNationalityValue($model->cus_nationality)),  
        'cus_nic',
        'cus_slin',
        'cus_drivers_license',
        'cus_passport_no',
        'cus_dob',
        'cus_address',
        'cus_current_address',
        'cus_official_address',
        'cus_employer_address',
        array("name" => 'residence_duration_id', "value" => $model->residenceDuration->residence_duration_name),
        array("name" => 'cus_resident_info', "value" => CommonListData::getListDataResidenceInfoValue($model->cus_resident_info)),
        array("name" => 'cus_profession', "value" => $model->profession->profession_name),
        array("name" => 'cus_organization', "value" => CommonListData::getListDataCusOrganizationValue($model->cus_organization)),
        'cus_telephone',
        'cus_telephone2',
        'cus_mobile',
        'cus_mobile2',
        'cus_email',
        'cus_email2',
        'cus_income_source',
        'cus_net_income',
        'cus_assets',
        'cus_spouse_assets',
        'cus_introducer_name',
        'cus_introducer_telephone',
        'cus_introducer_mobile',
        'cus_introducer_address',
        array("name" => 'cus_nic_scan_copy', "type" => "raw", "value" => Common::getImage('images/cusImages/nicImages', $model->cus_nic_scan_copy, 'thumb')),
        array("name" => 'cus_image', "type" => "raw", "value" => Common::getImage('images/cusImages/cusImages', $model->cus_image, 'thumb')),
    ),
));

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pe-cus-bank-details-grid',
	'dataProvider'=>$cusBankDetails->search(),
	'filter'=>$cusBankDetails,
	'columns'=>array(
		'bank_details_bank_name',
		'bank_details_bank_ac_no',
		'bank_details_bank_address',
		
	),
));

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'pe-cus-credit-ref-details-grid',
    'dataProvider' => $cusCreditDetails->search(),
    'filter'=>$cusCreditDetails,
    'columns' => array(
        'credit_ref_details_cmp_name',
        'credit_ref_details_cmp_address',
        'credit_ref_details_cmp_tel',
    ),
));
?>
