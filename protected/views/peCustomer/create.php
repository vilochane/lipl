<?php
/* @var $this PeCustomerController */
/* @var $model PeCustomer */

$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Create',
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array( 'PeCustomerManage' => array())));
?>

<h1>Create a Customer</h1>

<?php $this->renderPartial('_form', array('model'=>$model, "childModels"=>$childModels)); ?>