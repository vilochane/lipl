<table id="credit-ref-table" class="table-bordered">
    <thead>
        <tr>
            <th style="width: 2%;">#</th>
            <th style="width: 10%;">Company</th>
            <th style="width: 10%;">Telephone</th>
            <th style="width: 20%;">Address</th>
            <th style="width: 2%;"><button id="add-credit-ref-details-btn" title="Add a new row." type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span></button></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($childModels["PeCusCreditRefDetails"]) > 0) {
            foreach ($childModels["PeCusCreditRefDetails"] as $index => $modelObj) {
                ?>  
                <tr>
                    <td><span><?php echo $index + 1; ?></span></td>
                    <?php $this->renderPartial("__from-credit-ref-details-rows", array("rowIndex" => $index, "form" => $form, "model" => $modelObj)); ?>
                    <td><?php echo CommonSystem::getTabualrButtonLink($modelObj->getScenario(), "PeCusCreditRefDetails/delete", $modelObj->credit_ref_details_id); ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table> 