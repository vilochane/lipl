<?php
/* @var $this PeCustomerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pe Customers',
);

$this->menu=array(
	array('label'=>'Create PeCustomer', 'url'=>array('create')),
	array('label'=>'Manage PeCustomer', 'url'=>array('admin')),
);
?>

<h1>Pe Customers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
