<?php
/* @var $this PeCustomerController */
/* @var $model PeCustomer */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'divisional_secretariat_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'divisional_secretariat_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_first_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_first_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_middle_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_middle_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_last_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_last_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_gender'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_gender', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_marital_status'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_marital_status', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_nationality'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_nationality', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_nic'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_nic', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_slin'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_slin', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_drivers_license'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_drivers_license',array('class'=>'form-control', 'size'=>7,'maxlength'=>7, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_passport_no'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_passport_no',array('class'=>'form-control', 'size'=>5,'maxlength'=>5, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_dob'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_dob', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_address'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_current_address'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_current_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_official_address'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_official_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_employer_address'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_employer_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'residence_duration_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'residence_duration_id', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_resident_info'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_resident_info', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'profession_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'profession_id',array('class'=>'form-control', 'size'=>60,'maxlength'=>150, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_organization'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_organization', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_telephone'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_telephone', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_telephone2'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_telephone2', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_mobile'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_mobile', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_mobile2'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_mobile2', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_email'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_email',array('class'=>'form-control', 'size'=>60,'maxlength'=>150, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_email2'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_email2',array('class'=>'form-control', 'size'=>60,'maxlength'=>150, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_income_source'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_income_source',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_net_income'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_net_income',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_assets'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_assets',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_spouse_assets'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_spouse_assets',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_introducer_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_introducer_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_introducer_telephone'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_introducer_telephone', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_introducer_mobile'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_introducer_mobile', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_introducer_address'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_introducer_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_nic_scan_copy'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_nic_scan_copy',array('class'=>'form-control', 'size'=>60,'maxlength'=>100, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_image'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_image',array('class'=>'form-control', 'size'=>60,'maxlength'=>100, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_by'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
        <div class="row buttons">
        <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->