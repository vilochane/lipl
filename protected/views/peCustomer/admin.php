<?php
/* @var $this PeCustomerController */
/* @var $model PeCustomer */

$this->breadcrumbs=array(
	'Customers'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pe-customer-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Customers</h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    </span>
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['createANewRecord'], CController::createUrl('create'), array('class' => 'add-new-button')); ?>    </span>
</div>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pe-customer-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array("name"=>'branch_id', "value"=>'$data->branch->branch_name'),
		array("name"=>'divisional_secretariat_id', "value"=>'$data->divisionalSecretariat->divisional_secretariat_name'),
		'cus_first_name',
		'cus_middle_name',
		'cus_last_name',
		/*
		'cus_gender',
		'cus_marital_status',
		'cus_nationality',
		'cus_nic',
		'cus_slin',
		'cus_drivers_license',
		'cus_passport_no',
		'cus_dob',
		'cus_address',
		'cus_current_address',
		'cus_official_address',
		'cus_employer_address',
		'cus_resident_duration',
		'cus_resident_info',
		'cus_profession',
		'cus_organization',
		'cus_telephone',
		'cus_telephone2',
		'cus_mobile',
		'cus_mobile2',
		'cus_email',
		'cus_email2',
		'cus_income_source',
		'cus_net_income',
		'cus_assets',
		'cus_spouse_assets',
		'cus_introducer_name',
		'cus_introducer_telephone',
		'cus_introducer_mobile',
		'cus_introducer_address',
		'cus_nic_scan_copy',
		'cus_image',
		'updated_by',
		'updated_date',
		*/
		array(
			'class'=>'CButtonColumn',
                        'htmlOptions'=>array('class'=>'button-column', 'style'=>'width: 12%;'),
                        'template'=>'{view}{update}{delete}{createGuarantor}{manageGurantors}{createAgreement}{manageAgreements}',
                        'buttons'=>array(
                            'createGuarantor'=>array(
                                'url'=>'Yii::app()->createAbsoluteUrl("PeCusGuarantor/create", array("c"=>$this->encryptParam($data->cus_id)))',
                                'glyphIcon'=>'glyphicon glyphicon-plus',
                                'options'=>array('title'=>'Create a guarantor')
                            ),
                            'manageGurantors'=>array(
                                'url'=>'Yii::app()->createAbsoluteUrl("PeCusGuarantor/admin", array("c"=>$this->encryptParam($data->cus_id)))',
                                'glyphIcon'=>'glyphicon glyphicon-th',
                                'options'=>array('title'=>'Manage guarantors')
                            ),
                            'createAgreement'=>array(
                                'url'=>'Yii::app()->createAbsoluteUrl("HipAgreement/create", array("c"=>$this->encryptParam($data->cus_id)))',
                                'glyphIcon'=>'glyphicon glyphicon-plus',
                                'options'=>array('title'=>'Create an agreement')
                            ),
                            'manageAgreements'=>array(
                                'url'=>'Yii::app()->createAbsoluteUrl("HipAgreement/admin", array("c"=>$this->encryptParam($data->cus_id)))',
                                'glyphIcon'=>'glyphicon glyphicon-th',
                                'options'=>array('title'=>'Manage agreements')
                            ),
                        )
                        
		),
	),
)); ?>
