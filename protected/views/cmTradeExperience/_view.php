<?php
/* @var $this CmTradeExperienceController */
/* @var $data CmTradeExperience */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('trade_experience_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->trade_experience_id), array('view', 'id'=>$data->trade_experience_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trade_experience_name')); ?>:</b>
	<?php echo CHtml::encode($data->trade_experience_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>