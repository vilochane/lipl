<?php
/* @var $this CmTradeExperienceController */
/* @var $model CmTradeExperience */

$this->breadcrumbs=array(
	'Cm Trade Experiences'=>array('index'),
	$model->trade_experience_id=>array('view','id'=>$this->encryptParam($model->trade_experience_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmTradeExperience', 'url'=>array('index')),
	array('label'=>'Create CmTradeExperience', 'url'=>array('create')),
	array('label'=>'View CmTradeExperience', 'url'=>array('view', 'id'=>$model->trade_experience_id)),
	array('label'=>'Manage CmTradeExperience', 'url'=>array('admin')),
);
?>

<h1>Update CmTradeExperience <?php echo $model->trade_experience_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>