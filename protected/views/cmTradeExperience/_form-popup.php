<?php
/* @var $this CmTradeExperienceController */
/* @var $model CmTradeExperience */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-trade-experience-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'trade_experience_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'trade_experience_name', array('class' => 'form-control', 'size' => 20, 'maxlength' => 20, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'trade_experience_name'); ?>
    </div>
    <div class="row buttons">
        <?php echo CommonSystem::getAjaxSubmitButton(CController::createUrl("AjaxCreate"), $dialogId, $select2Id); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->