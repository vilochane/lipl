<?php
/* @var $this CmTradeExperienceController */
/* @var $model CmTradeExperience */

$this->breadcrumbs=array(
	'Cm Trade Experiences'=>array('index'),
	$model->trade_experience_id,
);

$this->menu=array(
	array('label'=>'List CmTradeExperience', 'url'=>array('index')),
	array('label'=>'Create CmTradeExperience', 'url'=>array('create')),
	array('label'=>'Update CmTradeExperience', 'url'=>array('update', 'id'=>$model->trade_experience_id)),
	array('label'=>'Delete CmTradeExperience', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->trade_experience_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmTradeExperience', 'url'=>array('admin')),
);
?>

<h1>View CmTradeExperience <?php echo $model->trade_experience_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'trade_experience_id',
		'trade_experience_name',
		'updated_by',
		'updated_date',
	),
)); ?>
