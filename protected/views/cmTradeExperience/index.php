<?php
/* @var $this CmTradeExperienceController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Trade Experiences',
);

$this->menu=array(
	array('label'=>'Create CmTradeExperience', 'url'=>array('create')),
	array('label'=>'Manage CmTradeExperience', 'url'=>array('admin')),
);
?>

<h1>Cm Trade Experiences</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
