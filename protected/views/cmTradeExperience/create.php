<?php
/* @var $this CmTradeExperienceController */
/* @var $model CmTradeExperience */

$this->breadcrumbs=array(
	'Cm Trade Experiences'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmTradeExperience', 'url'=>array('index')),
	array('label'=>'Manage CmTradeExperience', 'url'=>array('admin')),
);
?>

<h1>Create CmTradeExperience</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>