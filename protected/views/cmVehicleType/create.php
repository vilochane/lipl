<?php
/* @var $this CmVehicleTypeController */
/* @var $model CmVehicleType */

$this->breadcrumbs=array(
	'Cm Vehicle Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmVehicleType', 'url'=>array('index')),
	array('label'=>'Manage CmVehicleType', 'url'=>array('admin')),
);
?>

<h1>Create CmVehicleType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>