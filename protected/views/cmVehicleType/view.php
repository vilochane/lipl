<?php
/* @var $this CmVehicleTypeController */
/* @var $model CmVehicleType */

$this->breadcrumbs=array(
	'Cm Vehicle Types'=>array('index'),
	$model->vehicle_type_id,
);

$this->menu=array(
	array('label'=>'List CmVehicleType', 'url'=>array('index')),
	array('label'=>'Create CmVehicleType', 'url'=>array('create')),
	array('label'=>'Update CmVehicleType', 'url'=>array('update', 'id'=>$model->vehicle_type_id)),
	array('label'=>'Delete CmVehicleType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->vehicle_type_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmVehicleType', 'url'=>array('admin')),
);
?>

<h1>View CmVehicleType <?php echo $model->vehicle_type_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'vehicle_type_id',
		'vehicle_type_name',
		'updated_by',
		'updated_date',
	),
)); ?>
