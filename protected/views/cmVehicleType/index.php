<?php
/* @var $this CmVehicleTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Vehicle Types',
);

$this->menu=array(
	array('label'=>'Create CmVehicleType', 'url'=>array('create')),
	array('label'=>'Manage CmVehicleType', 'url'=>array('admin')),
);
?>

<h1>Cm Vehicle Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
