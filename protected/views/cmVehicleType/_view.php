<?php
/* @var $this CmVehicleTypeController */
/* @var $data CmVehicleType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_type_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->vehicle_type_id), array('view', 'id'=>$data->vehicle_type_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_type_name')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_type_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>