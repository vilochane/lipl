<?php
/* @var $this CmVehicleTypeController */
/* @var $model CmVehicleType */

$this->breadcrumbs=array(
	'Cm Vehicle Types'=>array('index'),
	$model->vehicle_type_id=>array('view','id'=>$this->encryptParam($model->vehicle_type_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmVehicleType', 'url'=>array('index')),
	array('label'=>'Create CmVehicleType', 'url'=>array('create')),
	array('label'=>'View CmVehicleType', 'url'=>array('view', 'id'=>$model->vehicle_type_id)),
	array('label'=>'Manage CmVehicleType', 'url'=>array('admin')),
);
?>

<h1>Update CmVehicleType <?php echo $model->vehicle_type_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>