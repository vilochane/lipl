<?php
/* @var $this CmVehicleTypeController */
/* @var $model CmVehicleType */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-vehicle-type-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <span class="show-error-summary"></span>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'vehicle_type_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'vehicle_type_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'vehicle_type_name'); ?>
    </div>
    <div class="row buttons">
        <?php CommonSystem::getAjaxSubmitButton(CController::createUrl('AjaxCreate'), $dialogId, $select2Id); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->