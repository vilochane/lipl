<?php
/* @var $this CmVehicleMakeController */
/* @var $model CmVehicleMake */

$this->breadcrumbs=array(
	'Cm Vehicle Makes'=>array('index'),
	$model->vehicle_make_id,
);

$this->menu=array(
	array('label'=>'List CmVehicleMake', 'url'=>array('index')),
	array('label'=>'Create CmVehicleMake', 'url'=>array('create')),
	array('label'=>'Update CmVehicleMake', 'url'=>array('update', 'id'=>$model->vehicle_make_id)),
	array('label'=>'Delete CmVehicleMake', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->vehicle_make_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmVehicleMake', 'url'=>array('admin')),
);
?>

<h1>View CmVehicleMake <?php echo $model->vehicle_make_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'vehicle_make_id',
		'vehicle_make_name',
		'vehicle_make_status',
		'updated_by',
		'updated_date',
	),
)); ?>
