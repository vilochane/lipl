<?php
/* @var $this CmVehicleMakeController */
/* @var $data CmVehicleMake */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_make_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->vehicle_make_id), array('view', 'id'=>$data->vehicle_make_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_make_name')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_make_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_make_status')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_make_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>