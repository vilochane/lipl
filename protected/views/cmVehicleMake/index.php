<?php
/* @var $this CmVehicleMakeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Vehicle Makes',
);

$this->menu=array(
	array('label'=>'Create CmVehicleMake', 'url'=>array('create')),
	array('label'=>'Manage CmVehicleMake', 'url'=>array('admin')),
);
?>

<h1>Cm Vehicle Makes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
