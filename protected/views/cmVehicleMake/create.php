<?php
/* @var $this CmVehicleMakeController */
/* @var $model CmVehicleMake */

$this->breadcrumbs=array(
	'Cm Vehicle Makes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmVehicleMake', 'url'=>array('index')),
	array('label'=>'Manage CmVehicleMake', 'url'=>array('admin')),
);
?>

<h1>Create CmVehicleMake</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>