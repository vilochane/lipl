<?php
/* @var $this CmVehicleMakeController */
/* @var $model CmVehicleMake */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-vehicle-make-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <span class="show-error-summary"></span>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'vehicle_make_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'vehicle_make_name', array('class' => 'form-control', 'size' => 20, 'maxlength' => 20, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'vehicle_make_name'); ?>
    </div>
    <div class="row buttons">
        <?php echo CommonSystem::getAjaxSubmitButton(CController::createUrl("AjaxCreate"), $dialogId, $select2Id); ?>    
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->