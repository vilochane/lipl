<?php
/* @var $this CmVehicleMakeController */
/* @var $model CmVehicleMake */

$this->breadcrumbs=array(
	'Cm Vehicle Makes'=>array('index'),
	$model->vehicle_make_id=>array('view','id'=>$this->encryptParam($model->vehicle_make_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmVehicleMake', 'url'=>array('index')),
	array('label'=>'Create CmVehicleMake', 'url'=>array('create')),
	array('label'=>'View CmVehicleMake', 'url'=>array('view', 'id'=>$model->vehicle_make_id)),
	array('label'=>'Manage CmVehicleMake', 'url'=>array('admin')),
);
?>

<h1>Update CmVehicleMake <?php echo $model->vehicle_make_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>