<?php
/* @var $this CmPeriodController */
/* @var $model CmPeriod */

$this->breadcrumbs=array(
	'Cm Periods'=>array('index'),
	$model->period_id,
);

$this->menu=array(
	array('label'=>'List CmPeriod', 'url'=>array('index')),
	array('label'=>'Create CmPeriod', 'url'=>array('create')),
	array('label'=>'Update CmPeriod', 'url'=>array('update', 'id'=>$model->period_id)),
	array('label'=>'Delete CmPeriod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->period_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmPeriod', 'url'=>array('admin')),
);
?>

<h1>View CmPeriod <?php echo $model->period_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'period_id',
		'period_name',
		'updated_by',
		'updated_date',
	),
)); ?>
