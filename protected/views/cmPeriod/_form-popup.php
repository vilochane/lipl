<?php
/* @var $this CmPeriodController */
/* @var $model CmPeriod */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-period-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <span class="show-error-summary"></span>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'period_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'period_name', array('class' => 'form-control', 'size' => 10, 'maxlength' => 10, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'period_name'); ?>
    </div>

    <div class="row buttons">
        <?php CommonSystem::getAjaxSubmitButton(CController::createUrl("AjaxCreate"), $dialogId, $select2Id); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
$cs= Yii::app()->clientScript;
$cs->registerScript("cmperiod-form", ""
        . "$('#CmPeriod_period_name').change(function(){"
        . "var period = $(this).val();"
        . "if(period.match(/^([0-9]{1})$/g)){"
        . "period = '0'+period;"
        . "$(this).val(period);"
        . "}"
        . "});"
        . "");
?>