<?php
/* @var $this CmPeriodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Periods',
);

$this->menu=array(
	array('label'=>'Create CmPeriod', 'url'=>array('create')),
	array('label'=>'Manage CmPeriod', 'url'=>array('admin')),
);
?>

<h1>Cm Periods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
