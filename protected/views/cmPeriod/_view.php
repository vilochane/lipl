<?php
/* @var $this CmPeriodController */
/* @var $data CmPeriod */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('period_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->period_id), array('view', 'id'=>$data->period_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('period_name')); ?>:</b>
	<?php echo CHtml::encode($data->period_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>