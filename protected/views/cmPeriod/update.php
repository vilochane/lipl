<?php
/* @var $this CmPeriodController */
/* @var $model CmPeriod */

$this->breadcrumbs=array(
	'Cm Periods'=>array('index'),
	$model->period_id=>array('view','id'=>$this->encryptParam($model->period_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmPeriod', 'url'=>array('index')),
	array('label'=>'Create CmPeriod', 'url'=>array('create')),
	array('label'=>'View CmPeriod', 'url'=>array('view', 'id'=>$model->period_id)),
	array('label'=>'Manage CmPeriod', 'url'=>array('admin')),
);
?>

<h1>Update CmPeriod <?php echo $model->period_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>