<?php
/* @var $this CmPeriodController */
/* @var $model CmPeriod */

$this->breadcrumbs=array(
	'Cm Periods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmPeriod', 'url'=>array('index')),
	array('label'=>'Manage CmPeriod', 'url'=>array('admin')),
);
?>

<h1>Create CmPeriod</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>