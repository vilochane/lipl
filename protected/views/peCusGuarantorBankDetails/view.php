<?php
/* @var $this PeCusGuarantorBankDetailsController */
/* @var $model PeCusGuarantorBankDetails */

$this->breadcrumbs=array(
	'Pe Cus Guarantor Bank Details'=>array('index'),
	$model->bank_details_id,
);

$this->menu=array(
	array('label'=>'List PeCusGuarantorBankDetails', 'url'=>array('index')),
	array('label'=>'Create PeCusGuarantorBankDetails', 'url'=>array('create')),
	array('label'=>'Update PeCusGuarantorBankDetails', 'url'=>array('update', 'id'=>$model->bank_details_id)),
	array('label'=>'Delete PeCusGuarantorBankDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->bank_details_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PeCusGuarantorBankDetails', 'url'=>array('admin')),
);
?>

<h1>View PeCusGuarantorBankDetails <?php echo $model->bank_details_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'bank_details_id',
		'guarantor_id',
		'bank_details_bank_name',
		'bank_details_bank_ac_no',
		'bank_details_bank_address',
		'updated_by',
		'updated_date',
	),
)); ?>
