<?php
/* @var $this PeCusGuarantorBankDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pe Cus Guarantor Bank Details',
);

$this->menu=array(
	array('label'=>'Create PeCusGuarantorBankDetails', 'url'=>array('create')),
	array('label'=>'Manage PeCusGuarantorBankDetails', 'url'=>array('admin')),
);
?>

<h1>Pe Cus Guarantor Bank Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
