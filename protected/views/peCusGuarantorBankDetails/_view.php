<?php
/* @var $this PeCusGuarantorBankDetailsController */
/* @var $data PeCusGuarantorBankDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank_details_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->bank_details_id), array('view', 'id'=>$data->bank_details_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_id')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank_details_bank_name')); ?>:</b>
	<?php echo CHtml::encode($data->bank_details_bank_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank_details_bank_ac_no')); ?>:</b>
	<?php echo CHtml::encode($data->bank_details_bank_ac_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bank_details_bank_address')); ?>:</b>
	<?php echo CHtml::encode($data->bank_details_bank_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>