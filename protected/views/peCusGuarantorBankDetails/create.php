<?php
/* @var $this PeCusGuarantorBankDetailsController */
/* @var $model PeCusGuarantorBankDetails */

$this->breadcrumbs=array(
	'Pe Cus Guarantor Bank Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PeCusGuarantorBankDetails', 'url'=>array('index')),
	array('label'=>'Manage PeCusGuarantorBankDetails', 'url'=>array('admin')),
);
?>

<h1>Create PeCusGuarantorBankDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>