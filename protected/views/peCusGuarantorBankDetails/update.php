<?php
/* @var $this PeCusGuarantorBankDetailsController */
/* @var $model PeCusGuarantorBankDetails */

$this->breadcrumbs=array(
	'Pe Cus Guarantor Bank Details'=>array('index'),
	$model->bank_details_id=>array('view','id'=>$this->encryptParam($model->bank_details_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List PeCusGuarantorBankDetails', 'url'=>array('index')),
	array('label'=>'Create PeCusGuarantorBankDetails', 'url'=>array('create')),
	array('label'=>'View PeCusGuarantorBankDetails', 'url'=>array('view', 'id'=>$model->bank_details_id)),
	array('label'=>'Manage PeCusGuarantorBankDetails', 'url'=>array('admin')),
);
?>

<h1>Update PeCusGuarantorBankDetails <?php echo $model->bank_details_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>