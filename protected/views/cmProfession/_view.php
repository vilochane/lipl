<?php
/* @var $this CmProfessionController */
/* @var $data CmProfession */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('profession_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->profession_id), array('view', 'id'=>$data->profession_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profession_name')); ?>:</b>
	<?php echo CHtml::encode($data->profession_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>