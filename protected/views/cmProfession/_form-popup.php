<?php
/* @var $this CmProfessionController */
/* @var $model CmProfession */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-profession-form',
        'enableAjaxValidation' => true,
    ));
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <span class="show-error-summary"></span>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'profession_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'profession_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'profession_name'); ?>
    </div>
    <div class="row buttons">
        <?php echo CommonSystem::getAjaxSubmitButton(CController::createUrl("AjaxCreate"), $dialogId, $select2Id); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->