<?php
/* @var $this CmProfessionController */
/* @var $model CmProfession */

$this->breadcrumbs=array(
	'Cm Professions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmProfession', 'url'=>array('index')),
	array('label'=>'Manage CmProfession', 'url'=>array('admin')),
);
?>

<h1>Create CmProfession</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>