<?php
/* @var $this CmProfessionController */
/* @var $model CmProfession */

$this->breadcrumbs=array(
	'Cm Professions'=>array('index'),
	$model->profession_id,
);

$this->menu=array(
	array('label'=>'List CmProfession', 'url'=>array('index')),
	array('label'=>'Create CmProfession', 'url'=>array('create')),
	array('label'=>'Update CmProfession', 'url'=>array('update', 'id'=>$model->profession_id)),
	array('label'=>'Delete CmProfession', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->profession_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmProfession', 'url'=>array('admin')),
);
?>

<h1>View CmProfession <?php echo $model->profession_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'profession_id',
		'profession_name',
		'updated_by',
		'updated_date',
	),
)); ?>
