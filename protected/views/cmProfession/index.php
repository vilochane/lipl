<?php
/* @var $this CmProfessionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Professions',
);

$this->menu=array(
	array('label'=>'Create CmProfession', 'url'=>array('create')),
	array('label'=>'Manage CmProfession', 'url'=>array('admin')),
);
?>

<h1>Cm Professions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
