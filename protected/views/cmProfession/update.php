<?php
/* @var $this CmProfessionController */
/* @var $model CmProfession */

$this->breadcrumbs=array(
	'Cm Professions'=>array('index'),
	$model->profession_id=>array('view','id'=>$this->encryptParam($model->profession_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmProfession', 'url'=>array('index')),
	array('label'=>'Create CmProfession', 'url'=>array('create')),
	array('label'=>'View CmProfession', 'url'=>array('view', 'id'=>$model->profession_id)),
	array('label'=>'Manage CmProfession', 'url'=>array('admin')),
);
?>

<h1>Update CmProfession <?php echo $model->profession_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>