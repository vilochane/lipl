<?php
/* @var $this HipPaymentsController */
/* @var $model HipPayments */
/* @var $form CActiveForm */
$cs = Yii::app()->clientScript;
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'hip-payments-form-popup',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>


    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'payment_payable'); ?>
            </span>		
            <?php echo $this->setNumberFormat($model->payment_payable); ?>
        </div>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'payment_penalty'); ?>
            </span>		
            <?php echo $this->setNumberFormat($model->payment_penalty); ?>
        </div>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'payment_deduct'); ?>
            </span>		
            <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'payment_deduct')); ?>
        </div>
        <?php echo $form->error($model, 'payment_deduct'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'payment_received'); ?>
            </span>		
            <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'payment_received')); ?>
        </div>
        <?php echo $form->error($model, 'payment_received'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'payment_received_date'); ?>
            </span>		
            <?php $this->initDatePicker(array('model' => $model, 'attribute' => 'payment_received_date')); ?>
        </div>
        <?php echo $form->error($model, 'payment_received_date'); ?>
    </div>

    <?php echo $form->hiddenField($model, 'payment_payable'); ?>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
$cs->registerScript("hip-payments-form", ""
        . "var payableDisplayObj = $('#HipPayments_payableDisplayVal');"
        . "var penaltyDisplayAmountObj = $('#HipPayments_penaltyAmountDisplayVal');"
        . "var payableObj = $('#HipPayments_payment_payable');"
        . "var penaltyIObj = $('#HipPayments_payment_p_i_rate');"
        . "var recievedObj = $('#HipPayments_payment_received');"
        . "var dueDisplayObj = $('#HipPayments_dueDisplayVal');"
        
        . "function updatePayableValue(){"
        . "var payableValTot = parseFloat(0);"
        . "var payableVal = parseFloat(payableObj.val());"
        . "var penaltyAmount = parseFloat(payableVal) * parseFloat(penaltyIObj.val());"
        . "payableValTot = parseFloat(payableVal) + parseFloat(penaltyAmount);"
        . "payableDisplayObj.val(payableValTot);"
        . "penaltyDisplayAmountObj.val(penaltyAmount);"
        
        . "}"
        . ""
        . "function setDueAmount(){" 
        . "var paymentReceived = parseFloat(recievedObj.val().replace(/,/g, ''));"
        . "var dueAmount = parseFloat(payableDisplayObj.val().replace(/,/g, '')) - paymentReceived;"
        . "dueDisplayObj.val(dueAmount);"
        . "}"
        . "penaltyIObj.change(function(){ updatePayableValue();});"
        . "recievedObj.change(function(){ setDueAmount();});"
        , CClientScript::POS_READY);
?>