<?php
/* @var $this HipPaymentsController */
/* @var $model HipPayments */
/* @var $form CActiveForm */
$cs = Yii::app()->clientScript;
?>
<div class="form-container">
    <ul class="nav nav-tabs">
        <li class="nav active"><a href="#form-content" data-toggle="tab">Form</a></li>
        <li class="nav"><a href="#pending-payments" data-toggle="tab">Pending Payments</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane fade in active" id="form-content">
            <div class="form">

                <?php
                $form = $this->beginWidget('CActiveForm', array(
                    'id' => 'hip-payments-form',
                    'enableAjaxValidation' => false,
                ));
                ?>
                <p class="note">Fields with <span class="required">*</span> are required.</p>
                <?php echo $form->errorSummary($model); ?>
                <div class="row">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'payment_p_last_date'); ?>
                        </span>
                        <?php echo $form->textField($model, 'payment_p_last_date', array('class' => 'form-control', 'readonly' => 'readonly')) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'payment_d_date'); ?>
                        </span>
                        <?php echo $form->textField($model, 'payment_d_date', array('class' => 'form-control', 'readonly' => 'readonly')) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'payableDisplayVal'); ?>
                        </span>
                        <?php
                        $this->initFormatCurrency(array('model' => $model, 'attribute' => 'payableDisplayVal', 'htmlOptions' => array('value' => $model->roundValue($model->payment_payable, 3), 'readonly' => 'readonly')));
                        echo $form->hiddenField($model, 'payment_payable');
                        echo $form->hiddenField($model, 'payment_penalty');
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'penaltyDisplayVal'); ?>
                        </span>
                        <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'penaltyDisplayVal', 'htmlOptions' => array('value' => $model->roundValue($model->payment_penalty, 3), 'readonly' => 'readonly'))); ?>
                    </div>
                </div>

                <div class="row">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'payment_received_date'); ?>
                        </span>		
                        <?php $this->initDatePicker(array('model' => $model, 'attribute' => 'payment_received_date')); ?>
                    </div>
                    <?php echo $form->error($model, 'payment_received_date'); ?>
                </div>

                <div class="row hide-field">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'payment_deduct'); ?>
                        </span>		
                        <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'payment_deduct', 'clientOptions' => array('digits' => 3))); ?>
                    </div>
                    <?php echo $form->error($model, 'payment_deduct'); ?>
                </div>
                <div class="row hide-field">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'payment_expense'); ?>
                        </span>		
                        <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'payment_expense', 'clientOptions' => array('digits' => 3))); ?>
                    </div>
                    <?php echo $form->error($model, 'payment_expense'); ?>
                </div>
                <div class="row hide-field">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'payment_received'); ?>
                        </span>		
                        <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'payment_received', 'clientOptions' => array('digits' => 3))); ?>
                    </div>
                    <?php echo $form->error($model, 'payment_received'); ?>
                </div>
                <div class="row hide-field">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <?php echo $form->labelEx($model, 'dueDisplayVal'); ?>
                        </span>		
                        <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'dueDisplayVal', 'clientOptions' => array('digits' => 3), 'htmlOptions' => array('value' => $model->roundValue($model->dueDisplayVal), 'readonly' => 'readonly'))); ?>
                    </div>
                    <?php echo $form->error($model, 'dueDisplayVal'); ?>
                </div>

                <?php
                echo $form->hiddenField($model, 'payment_payable');
                echo CHtml::hiddenField('ajax_request');
                ?>
                <div class="row buttons">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
                </div>

                <?php $this->endWidget(); ?>

            </div><!-- form -->

        </div>
        <div class="tab-pane fade" id="pending-payments">
            <?php echo $model->getCurrentDueTable(); ?>
        </div>
    </div>
</div>
<?php
$cs->registerScript('', ''
        . "var payableDisplayObj = $('#HipPayments_payableDisplayVal');"
        . "var penaltyDisplayObj = $('#HipPayments_penaltyDisplayVal');"
        . "var dueDisplayObj = $('#HipPayments_dueDisplayVal');"
        . "var payableObj = $('#HipPayments_payment_payable');"
        . "var penaltyObj = $('#HipPayments_payment_penalty');"
        . "var penaltyIObj = $('#HipPayments_payment_p_i_rate');"
        . "var recievedObj = $('#HipPayments_payment_received');"
        . "var deductObj = $('#HipPayments_payment_deduct');"
        . "var expenseObj = $('#HipPayments_payment_expense');"
        . "var paymentLastDateObj = $('#HipPayments_payment_p_last_date');"
        . "var paymentReceivedDateObj = $('#HipPayments_payment_received_date');"
        //
        . 'function updatePayableValue(){'
        . 'var payableValTot = roundValue(parseFloat(payableObj.val()));' 
        . 'var penaltyAmount = roundValue(parseFloat(penaltyObj.val()));'
        . 'var deductVal = roundValue(deductObj.val());'
        . 'var expenseVal = roundValue(expenseObj.val());'
        //. 'console.log(penaltyAmount);'
        //. 'console.log(deductVal);'
        // nan
        . 'if(isNaN(deductVal)){'
        . 'deductVal = 0;'
        . '}'
        . 'if(isNaN(expenseVal)){'
        . 'expenseVal = 0;'
        . '}'
        //expense value
        . 'payableValTot = payableValTot + expenseVal;' //if any expense adding it
        . 'console.log(payableValTot);'
        . 'if(deductVal <= penaltyAmount){'
        . 'penaltyAmount = roundValue(penaltyAmount - deductVal);'
        . 'payableValTot = roundValue(payableValTot - deductVal);'//if not empty
        . 'recievedObj.val(0);'
        . 'dueDisplayObj.val(payableValTot);'
        . '}else{'
        . "initNotyAlert('error','Deducting value : '+deductVal+' should be less than penalty value : '+penaltyAmount);"
        . 'deductObj.val(0);'
        . 'recievedObj.val(0);'
        . 'dueDisplayObj.val(payableObj.val());'
        . '}'
        . 'console.log(payableValTot);'
        . 'payableDisplayObj.val(payableValTot);'
        . 'penaltyDisplayObj.val(penaltyAmount);'
        . '}'
        . ''
        //
        . "function setDueAmount(){"
        . "var dueTot = roundValue(payableDisplayObj.val());"
        . "var paymentReceived = roundValue(recievedObj.val());"
        . "var dueAmount = roundValue(roundValue(payableDisplayObj.val()) - paymentReceived);"
        . 'dueDisplayObj.val(dueAmount);'
        . 'if(isNaN(dueAmount)){'
        . 'dueDisplayObj.val(dueTot);'
        . '}'
        . "}"
        . "", CClientScript::POS_END);
$cs->registerScript('hip-payments-form-ready', ''
        . 'penaltyIObj.change(function(){ updatePayableValue();});'
        . 'recievedObj.change(function(){ setDueAmount();});'
        . 'expenseObj.change(function(){ updatePayableValue(); });'
        //when deducting the payment
        . "deductObj.change(function(){ updatePayableValue();});"
        //hiding fields if not selected payment received date
        . "var paymentReceivedDate = paymentReceivedDateObj.val();"
        . "if(paymentReceivedDate == ''){"
        . "$('.hide-field').hide();"
        . "}"
        //payment received change function
        . 'paymentReceivedDateObj.change(function(){'
        . 'var lastPaymentDate = paymentLastDateObj.val();'
        . 'var receivedDate = paymentReceivedDateObj.val();'
        . '$("#ajax_request").val("hip-payments-form");' //before ajax request
        . 'if(lastPaymentDate < receivedDate){'
        . 'addOverlayLoader();'
        . 'deductObj.val(0);'
        . 'recievedObj.val(0);'
        . 'updatePayableValue();'
        . 'var url = "' . Yii::app()->createAbsoluteUrl('HipPayments/update', array('id' => $this->encryptParam($model->payment_id))) . '";'
        . 'var formData = $("#hip-payments-form").serialize();'
        . "$.post(url, formData, function(form){ $('.form-container').html(form); removeOverlayLoader();} );"
        . '}else{'
        . 'removeOverlayLoader();'
        . "initNotyAlert('error','Received date : '+lastPaymentDate+' should be greater than last payment date : '+receivedDate);"
        . '}'
        . '$("#ajax_request").val("");' //resetting ajax post var
        . '});' //end of date change function
        . 'updatePayableValue();' //updating payment record
        , CClientScript::POS_READY);
?>
</div>