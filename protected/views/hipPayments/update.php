<?php
/* @var $this HipPaymentsController */
/* @var $model HipPayments */

$this->breadcrumbs=array(
	'Hip Payments'=>array('index'),
	$model->payment_id=>array('view','id'=>$this->encryptParam($model->payment_id)),
	'Update',
);

$this->menu=array(array('label'=>'Manage Payments', 'url'=>array('admin', 'id'=>$this->encryptParam($model->payment_id))));
?>

<h1>Update Payments <?php echo Common::getRelatedModelValues($model, $model->agreement_id, "agreement", "agreement_code"); ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>