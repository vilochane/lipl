<?php
/* @var $this HipPaymentsController */
/* @var $model HipPayments */
?>

<h4>View Pending Payment <?php echo Common::getRelatedModelValues($model, $model->agreement_id, "agreement", "agreement_code"); ?></h4>

<?php
$decimalPoints = 3;
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'agreement_id', 'value' => Common::getRelatedModelValues($model, $model->agreement_id, "agreement", "agreement_code")),
        array('name'=>'Vehicle', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "vehicleDetails","vehicle_details_reg")),
        array('name'=>'Customer', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "cus","cus_first_name")),
        array('name'=>'Customer Mobile', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "cus","cus_mobile")),
        array('name'=>'Customer Address', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "cus","cus_address")),
        array('name'=>'Customer Current Address', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "cus","cus_current_address")),
        array('name' => 'payment_d_capital', 'value' => $model->setNumberFormat($model->payment_d_capital, null, 20, $decimalPoints)),
        array('name' => 'payment_d_interest', 'value' => $model->setNumberFormat($model->payment_d_interest, null, 20, $decimalPoints)),
        array('name' => 'payment_payable', 'value' => $model->setNumberFormat($model->payment_payable, null, 20, $decimalPoints)),
        array('name' => 'payment_penalty', 'value' => $model->setNumberFormat($model->payment_penalty, null, 20, $decimalPoints)),
        'payment_p_last_date',
        'payment_l_d_date',
        'payment_d_date',
        'payment_n_d_date',
        array('name'=>'payment_status', 'value'=>  Common::getPaymentStatus($model->payment_status)),
        array('name' => 'Dues', 'type'=>'raw', 'value' => $model->getCurrentDueTable()),
    ),
));
?>
