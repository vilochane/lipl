<?php
/* @var $this HipPaymentsController */
/* @var $data HipPayments */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->payment_id), array('view', 'id'=>$data->payment_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_id')); ?>:</b>
	<?php echo CHtml::encode($data->agreement_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_d_capital')); ?>:</b>
	<?php echo CHtml::encode($data->payment_d_capital); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_d_interest')); ?>:</b>
	<?php echo CHtml::encode($data->payment_d_interest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_amount')); ?>:</b>
	<?php echo CHtml::encode($data->payment_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_interest')); ?>:</b>
	<?php echo CHtml::encode($data->payment_interest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_p_i_rate')); ?>:</b>
	<?php echo CHtml::encode($data->payment_p_i_rate); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_p_amount')); ?>:</b>
	<?php echo CHtml::encode($data->payment_p_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_p_interest')); ?>:</b>
	<?php echo CHtml::encode($data->payment_p_interest); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_payable')); ?>:</b>
	<?php echo CHtml::encode($data->payment_payable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_received')); ?>:</b>
	<?php echo CHtml::encode($data->payment_received); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_due')); ?>:</b>
	<?php echo CHtml::encode($data->payment_due); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_d_date')); ?>:</b>
	<?php echo CHtml::encode($data->payment_d_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_p_late_count')); ?>:</b>
	<?php echo CHtml::encode($data->payment_p_late_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_status')); ?>:</b>
	<?php echo CHtml::encode($data->payment_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>