<?php
/* @var $this HipPaymentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hip Payments',
);

$this->menu=array(
	array('label'=>'Create HipPayments', 'url'=>array('create')),
	array('label'=>'Manage HipPayments', 'url'=>array('admin')),
);
?>

<h1>Hip Payments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
