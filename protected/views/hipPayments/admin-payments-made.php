<?php
/* @var $this HipPaymentsController */
/* @var $model HipPayments */
$decimalPoints = 3;
$this->breadcrumbs = array(
    'Hip Payments' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#hip-payments-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Payments</h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    </span>
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['createANewRecord'], CController::createUrl('create'), array('class' => 'add-new-button')); ?>    </span>
</div>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$js = "jQuery('#hip-payments-grid-dialog').dialog({'title':'Transshipment Rate','position':'center','width':'auto','height':'500','autoOpen':false,'resizable':true,'stack':false,'modal':true});";
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'hip-payments-grid',    
    'dataProvider' => $model->search(),
    //'rowCssClassExpression' => '$data->getGridRowColor()',
    'afterAjaxUpdate'=>  CommonSystem::initGridviewSelect2AfterAjax(array(),$js),
    'filter' => $model,
    'columns' => array(
        array('name' => 'agreement_id', 'value' => 'Common::getRelatedModelValues($data, $data->agreement_id, "agreement", "agreement_code")'),
        array('header'=>'Vehicle', 'value'=>'Common::getRelatedParentChildModelValues($data, $data->agreement_id, "agreement", "vehicleDetails","vehicle_details_reg")'),
        array('header'=>'Customer', 'value'=>'Common::getRelatedParentChildModelValues($data, $data->agreement_id, "agreement", "cus","cus_first_name")'),
        array('header'=>'Customer Mobile', 'value'=>'Common::getRelatedParentChildModelValues($data, $data->agreement_id, "agreement", "cus","cus_mobile")'),
        //array('header'=>'Customer Address', 'value'=>'Common::getRelatedParentChildModelValues($data, $data->agreement_id, "agreement", "cus","cus_address")'),
        //array('header'=>'Customer Current Address', 'value'=>'Common::getRelatedParentChildModelValues($data, $data->agreement_id, "agreement", "cus","cus_current_address")'),
        array('name' => 'payment_d_capital', 'value' => '$data->setNumberFormat($data->payment_d_capital, null, 20, ' . $decimalPoints . ')'),
        array('name' => 'payment_d_interest', 'value' => '$data->setNumberFormat($data->payment_d_interest, null, 20, ' . $decimalPoints . ')'),
        array('name' => 'payment_penalty', 'value' => '$data->setNumberFormat($data->payment_penalty, null, 20, ' . $decimalPoints . ')'),
        array('name' => 'payment_payable', 'value' => '$data->setNumberFormat($data->payment_payable, null, 20, ' . $decimalPoints . ')'),
        'payment_p_last_date',
        'payment_l_d_date',
        'payment_d_date',
        array('name'=>'payment_status', 'value'=>'Common::getPaymentStatus($data->payment_status)', 'filter'=>false),
        array(
            'class' => 'CButtonColumn',
            'gridViewDialogId'=>'hip-payments-made-grid-dialog',
            'dialogTitle' => 'Payment',
            'template' => '{view}',
            'buttons' => array(
                'view' => array(
                    'url' => 'Yii::app()->createAbsoluteUrl("HipPayments/AjaxView", array("id"=>$this->encryptParam($data->payment_id)))',
                    'options' => array('ajax' => true)
                ),
            )
        ),
    ),
));
?>
