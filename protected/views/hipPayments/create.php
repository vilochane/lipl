<?php
/* @var $this HipPaymentsController */
/* @var $model HipPayments */

$this->breadcrumbs=array(
	'Hip Payments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List HipPayments', 'url'=>array('index')),
	array('label'=>'Manage HipPayments', 'url'=>array('admin')),
);
?>

<h1>Create HipPayments</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>