<?php
/* @var $this HipPaymentsController */
/* @var $model HipPayments */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_d_capital'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_d_capital',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_d_interest'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_d_interest',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_p_amount_tot'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_p_amount_tot',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_p_interest_tot'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_p_interest_tot',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_p_d_amount'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_p_d_amount',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_p_d_interest'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_p_d_interest',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_rental'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_rental',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_rental_interest'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_rental_interest',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_p_excess'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_p_excess',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_p_late_count'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_p_late_count', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_l_d_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_l_d_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_d_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_d_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_n_d_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_n_d_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_p_i_rate'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_p_i_rate',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_penalty'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_penalty',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_payable'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_payable',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_deduct'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_deduct',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_received'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_received',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_due'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_due',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_received_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_received_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'payment_status'); ?>
                    </span>
                        <?php echo $form->textField($model,'payment_status', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_by'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
        <div class="row buttons">
        <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->