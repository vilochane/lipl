<?php
/* @var $this HipPaymentsController */
/* @var $model HipPayments */

$this->breadcrumbs = array(
    'Hip Payments' => array('index'),
    $model->payment_id,
);

$this->menu = array(
    array('label' => 'List HipPayments', 'url' => array('index')),
    array('label' => 'Create HipPayments', 'url' => array('create')),
    array('label' => 'Update HipPayments', 'url' => array('update', 'id' => $model->payment_id)),
    array('label' => 'Delete HipPayments', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->payment_id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage HipPayments', 'url' => array('admin')),
);
?>

<h4>View Pending Payment <?php echo Common::getRelatedModelValues($model, $model->agreement_id, "agreement", "agreement_code"); ?></h4>

<?php
$decimalPoints = 3;
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'agreement_id', 'value' => Common::getRelatedModelValues($model, $model->agreement_id, "agreement", "agreement_code")),
        array('name'=>'Vehicle', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "vehicleDetails","vehicle_details_reg")),
        array('name'=>'Customer', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "cus","cus_first_name")),
        array('name'=>'Customer Mobile', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "cus","cus_mobile")),
        array('name'=>'Customer Address', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "cus","cus_address")),
        array('name'=>'Customer Current Address', 'value'=>Common::getRelatedParentChildModelValues($model, $model->agreement_id, "agreement", "cus","cus_current_address")),
        array('name' => 'payment_d_capital', 'value' => $model->setNumberFormat($model->payment_d_capital, null, 20, $decimalPoints)),
        array('name' => 'payment_d_interest', 'value' => $model->setNumberFormat($model->payment_d_interest, null, 20, $decimalPoints)),
        array('name' => 'payment_amount', 'value' => $model->setNumberFormat($model->payment_amount, null, 20, $decimalPoints)),
        array('name' => 'payment_interest', 'value' => $model->setNumberFormat($model->payment_interest, null, 20, $decimalPoints)),
        array('name' => 'payment_p_amount', 'value' => $model->setNumberFormat($model->payment_p_amount, null, 20, $decimalPoints)),
        array('name' => 'payment_p_interest', 'value' => $model->setNumberFormat($model->payment_p_interest, null, 20, $decimalPoints)),
        array('name' => 'payment_penalty', 'value' => $model->setNumberFormat($model->payment_penalty, null, 20, $decimalPoints)),
        array('name' => 'payment_payable', 'value' => $model->setNumberFormat($model->payment_payable, null, 20, $decimalPoints)),
       // array('name'=>'payment_received', 'value'=>$model->setNumberFormat($model->payment_received, null, 20, $decimalPoints)),
        //array('name'=>'payment_due', 'value'=>$model->setNumberFormat($model->payment_due, null, 20, $decimalPoints)),
        'payment_last_date',
        'payment_d_date',
        'payment_p_late_days',
    //'payment_p_late_count',
    //'payment_status',
    ),
));
?>
