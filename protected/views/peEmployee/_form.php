<?php
/* @var $this PeEmployeeController */
/* @var $model PeEmployee */
/* @var $form CActiveForm */
$listStatus = CommonListData::getListDataStatus();
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'pe-employee-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'branchIds'); ?>
            </span>		
            <?php
            $this->initSelect2(array(
                'model' => $model,
                'attribute' => 'branchIds',
                'data' => CommonListData::getListDataBranches(),
                'options' => array(
                    'placeholder' => 'Please select a branch.'
                ),
                'htmlOptions' => array(
                    'multiple' => true
                )
            ));
            ?>
        </div>
        <?php echo $form->error($model, 'branchIds'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'designation_id'); ?>
            </span>		
            <?php
            $this->initSelect2(array(
                'model' => $model,
                'attribute' => 'designation_id',
                'data' => CommonListData::getListDataDesignations(),
                'options' => array(
                    'placeholder' => 'Please select a designation.'
                )
            ));
            ?>            
        </div>
        <?php echo $form->error($model, 'designation_id'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_first_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_first_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_first_name'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_middle_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_middle_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_middle_name'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_last_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_last_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_last_name'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_gender'); ?>
            </span>		
            <?php echo $form->radioButtonList($model, 'emp_gender', CommonListData::getListDataGender(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_gender'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_marital_status'); ?>
            </span>		
           <?php echo $form->radioButtonList($model, 'emp_marital_status', CommonListData::getListDataMartalStatus(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_marital_status'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_nic'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_nic', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_nic'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_slin'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_slin', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_slin'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_dob'); ?>
            </span>		
            <?php
            $this->initDatePicker(array(
                'model' => $model,
                'attribute' => 'emp_dob'
            ));
            ?>
        </div>
        <?php echo $form->error($model, 'emp_dob'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_address'); ?>
            </span>		
            <?php echo $form->textArea($model, 'emp_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_address'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_current_address'); ?>
            </span>		
            <?php echo $form->textArea($model, 'emp_current_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_current_address'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_telephone'); ?>
            </span>		
            <?php $this->initTelephoneField(array("model" => $model, "attribute" => "emp_telephone")) ?>
        </div>
        <?php echo $form->error($model, 'emp_telephone'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_mobile'); ?>
            </span>		
           <?php $this->initTelephoneField(array("model" => $model, "attribute" => "emp_mobile")) ?>
        </div>
        <?php echo $form->error($model, 'emp_mobile'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_email'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_email'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_etf_no'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_etf_no', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_etf_no'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_epf_no'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_epf_no', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_epf_no'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_date_of_employment'); ?>
            </span>		
            <?php
            $this->initDatePicker(array(
                'model' => $model,
                'attribute' => 'emp_date_of_employment',
            ));
            ?>
        </div>
        <?php echo $form->error($model, 'emp_date_of_employment'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_system_user'); ?>
            </span>		
            <?php echo $form->radioButtonList($model, 'emp_system_user', $listStatus); ?>                   
        </div>
        <?php echo $form->error($model, 'emp_system_user'); ?>
    </div>

    <div id="system-user-div">
        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($modelSystemUser, 'user_role_id'); ?>
                </span>		
                <?php
                $this->initSelect2(array(
                    'model' => $modelSystemUser,
                    'attribute' => 'user_role_id',
                    'data' => CommonListData::getListDataUserRoles(),
                    'options' => array(
                        'placeholder' => 'Please select a designation.'
                    )
                ));
                ?>
            </div>
            <?php echo $form->error($modelSystemUser, 'user_role_id'); ?>
        </div>
        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($modelSystemUser, 'user_username'); ?>
                </span>		
                <?php echo $form->textField($modelSystemUser, 'user_username', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($modelSystemUser, 'user_username'); ?>
        </div>
        <?php
        if ($modelSystemUser->getScenario() !== 'insert') {
            ?>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($modelSystemUser, 'oldPassword'); ?>
                    </span>		
                    <?php echo $form->passwordField($modelSystemUser, 'oldPassword', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($modelSystemUser, 'oldPassword'); ?>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($modelSystemUser, 'newPassword'); ?>
                    </span>		
                    <?php echo $form->passwordField($modelSystemUser, 'newPassword', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($modelSystemUser, 'newPassword'); ?>
            </div>
            <?php
        } else {
            ?>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($modelSystemUser, 'user_password'); ?>
                    </span>		
                    <?php echo $form->passwordField($modelSystemUser, 'user_password', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($modelSystemUser, 'user_password'); ?>
            </div>
            <?php
        }
        ?>
        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($modelSystemUser, 'passwordRepeat'); ?>
                </span>		
                <?php echo $form->passwordField($modelSystemUser, 'passwordRepeat', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($modelSystemUser, 'passwordRepeat'); ?>
        </div>
        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($modelSystemUser, 'user_status'); ?>
                </span>		
                <?php echo $form->radioButtonList($modelSystemUser, 'user_status', $listStatus, array('class' => 'form-control', 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($modelSystemUser, 'user_status'); ?>
        </div>
        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($modelSystemUser, 'user_su_status'); ?>
                </span>		
                <?php echo $form->radioButtonList($modelSystemUser, 'user_su_status', $listStatus, array('class' => 'form-control', 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($modelSystemUser, 'user_su_status'); ?>
        </div>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_nic_scan_copy'); ?>
            </span>
            <?php echo $form->fileField($model, 'emp_nic_scan_copy', array('autocomplete' => 'off')); ?>
            <span id="emp-nic-image-preview" class="img-rounded input-group-addon">
                <?php
                if (!empty($model->emp_nic_scan_copy)) {
                    echo Common::getImage('images/empImages/nicImages', $model->emp_nic_scan_copy, "thumb");
                }
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'emp_nic_scan_copy'); ?>

    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_image'); ?>
            </span>
            <?php echo $form->fileField($model, 'emp_image', array('autocomplete' => 'off')); ?>           
            <span id="emp-image-preview" class="img-rounded input-group-addon">
                <?php
                if (!empty($model->emp_image)) {
                    echo Common::getImage('images/empImages/empImages', $model->emp_image, "thumb");
                }
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'emp_image'); ?>

    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
$cs = Yii::app()->clientScript;
$cs->registerScript("emp_form", ""
        . "$('#PeEmployee_emp_nic_scan_copy').change(function(){ imagePreview($(this)[0], 'emp-nic-image-preview'); });"
        . "$('#PeEmployee_emp_image').change(function(){ imagePreview($(this)[0], 'emp-image-preview'); });"
        . "isSystemUser();"
        . "$('input:radio').click(function(){isSystemUser();});"
        . "function isSystemUser(){"
        . "$('#system-user-div').hide();"
        . "if($('#PeEmployee_emp_system_user_1').is(':checked')){"
        . "$('#system-user-div').show();"
        . "}"
        . "}"
        , CClientScript::POS_READY);
?>