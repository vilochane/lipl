<?php
/* @var $this PeEmployeeController */
/* @var $model PeEmployee */

$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'Create',
);

$this->menu=  SystemSubMenu::setMenuLinks(array(
    'PeEmployee'=>array(
        'PeEmployeeManage'=>array()
    )
));
?>

<h1>Create Employee</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'modelSystemUser'=>$modelSystemUser)); ?>