<?php
/* @var $this PeEmployeeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pe Employees',
);

$this->menu=array(
	array('label'=>'Create PeEmployee', 'url'=>array('create')),
	array('label'=>'Manage PeEmployee', 'url'=>array('admin')),
);
?>

<h1>Pe Employees</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
