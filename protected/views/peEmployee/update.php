<?php
/* @var $this PeEmployeeController */
/* @var $model PeEmployee */

$this->breadcrumbs=array(
	'Employees'=>array('index'),
	$model->emp_first_name=>array('view','id'=>$this->encryptParam($model->emp_id)),
	'Update',
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeEmployee' => array(
                'PeEmployeeCreate' => array(),
                'PeEmployeeView' => array('url' => array('id' => $model->emp_id)),
                'PeEmployeeManage' => array()
            )
        ));
?>

<h1>Update Employee <?php echo $model->emp_first_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'modelSystemUser'=>$modelSystemUser)); ?>