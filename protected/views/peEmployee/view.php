<?php
/* @var $this PeEmployeeController */
/* @var $model PeEmployee */

$this->breadcrumbs = array(
    'Employees' => array('index'),
    $model->emp_first_name,
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeEmployee' => array(
                'PeEmployeeCreate' => array(),
                'PeEmployeeUpdate' => array('url' => array('id' => $model->emp_id)),
                'PeEmployeeDelete' => array('linkOptions' => array('submit' => array('id' => $model->emp_id), 'confirm' => $this->deleteMeassage('employee') . $model->emp_first_name . '?')),
                'PeEmployeeManage' => array()
            )
        ));
?>

<h1>View Employee <?php echo $model->emp_first_name; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        // array('name' => 'branch_id', 'value' => $model->branch->branch_name),
        array('name' => 'designation_id', 'value' => $model->designation->designation_name),
        'emp_first_name',
        'emp_middle_name',
        'emp_last_name',
        array('name' => 'emp_gender', 'value' => CommonListData::getListDataGenderValue($model->emp_gender)),
        array('name' => 'emp_marital_status', 'value' => CommonListData::getListDataMartalStatusValue($model->emp_marital_status)),
        'emp_nic',
        'emp_slin',
        'emp_dob',
        'emp_address',
        'emp_current_address',
        'emp_telephone',
        'emp_mobile',
        'emp_email',
        'emp_etf_no',
        'emp_epf_no',
        array('name' => 'emp_system_user', 'value' => CommonListData::getListDataEmpUserStatusValue($model->emp_system_user)),
        'emp_date_of_employment',
        array('name' => 'emp_nic_scan_copy', 'type' => 'raw', 'value' => Common::getImage('images/empImages/nicImages', $model->emp_nic_scan_copy)),
        array('name' => 'emp_image', 'type' => 'raw', 'value' => Common::getImage('images/empImages/empImages', $model->emp_image)),
    ),
));
?>
