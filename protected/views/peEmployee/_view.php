<?php
/* @var $this PeEmployeeController */
/* @var $data PeEmployee */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->emp_id), array('view', 'id'=>$data->emp_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_id')); ?>:</b>
	<?php echo CHtml::encode($data->branch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('designation_id')); ?>:</b>
	<?php echo CHtml::encode($data->designation_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_first_name')); ?>:</b>
	<?php echo CHtml::encode($data->emp_first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_middle_name')); ?>:</b>
	<?php echo CHtml::encode($data->emp_middle_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_last_name')); ?>:</b>
	<?php echo CHtml::encode($data->emp_last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_gender')); ?>:</b>
	<?php echo CHtml::encode($data->emp_gender); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_marital_status')); ?>:</b>
	<?php echo CHtml::encode($data->emp_marital_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_nic')); ?>:</b>
	<?php echo CHtml::encode($data->emp_nic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_slin')); ?>:</b>
	<?php echo CHtml::encode($data->emp_slin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_dob')); ?>:</b>
	<?php echo CHtml::encode($data->emp_dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_address')); ?>:</b>
	<?php echo CHtml::encode($data->emp_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_current_address')); ?>:</b>
	<?php echo CHtml::encode($data->emp_current_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_telephone')); ?>:</b>
	<?php echo CHtml::encode($data->emp_telephone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->emp_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_email')); ?>:</b>
	<?php echo CHtml::encode($data->emp_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_etf_no')); ?>:</b>
	<?php echo CHtml::encode($data->emp_etf_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_epf_no')); ?>:</b>
	<?php echo CHtml::encode($data->emp_epf_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_system_user')); ?>:</b>
	<?php echo CHtml::encode($data->emp_system_user); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_date_of_employment')); ?>:</b>
	<?php echo CHtml::encode($data->emp_date_of_employment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_nic_scan_copy')); ?>:</b>
	<?php echo CHtml::encode($data->emp_nic_scan_copy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_image')); ?>:</b>
	<?php echo CHtml::encode($data->emp_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>