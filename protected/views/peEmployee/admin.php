<?php
/* @var $this PeEmployeeController */
/* @var $model PeEmployee */

$this->breadcrumbs=array(
	'Employees'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pe-employee-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Employees</h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    </span>
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['createANewRecord'], CController::createUrl('create'), array('class' => 'add-new-button')); ?>    </span>
</div>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pe-employee-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
	
		array(
                    "name"=>'designation_id',
                    "value"=>"\$data->designation->designation_name"
                ),
		'emp_first_name',
		'emp_middle_name',
		'emp_last_name',
		/*
		'emp_gender',
		'emp_marital_status',
		'emp_nic',
		'emp_slin',
		'emp_dob',
		'emp_address',
		'emp_current_address',
		'emp_telephone',
		'emp_mobile',
		'emp_email',
		'emp_etf_no',
		'emp_epf_no',
		'emp_system_user',
		'emp_date_of_employment',
		'emp_nic_scan_copy',
		'emp_image',

		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
