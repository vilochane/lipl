<?php
/* @var $this CmDesignationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Designations',
);

$this->menu=array(
	array('label'=>'Create CmDesignation', 'url'=>array('create')),
	array('label'=>'Manage CmDesignation', 'url'=>array('admin')),
);
?>

<h1>Cm Designations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
