<?php
/* @var $this CmDesignationController */
/* @var $model CmDesignation */

$this->breadcrumbs=array(
	'Designations'=>array('index'),
	$model->designation_name,
);

$this->menu=  SystemSubMenu::setMenuLinks(array(
    'CmDesignation'=>array(
        'CmDesignationCreate'=>array(),
        'CmDesignationUpdate'=>array('url'=>array('id'=>$model->designation_id)),
        'CmDesignationDelete' => array('linkOptions' => array('submit' => array('id'=>$model->designation_id), 'confirm' => $this->deleteMeassage('designation') . $model->designation_name . '?')),
        'CmDesignationManage'=>array()
    )
));
?>

<h1>View Designation <?php echo $model->designation_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'designation_name',
		'designation_remark',

	),
)); ?>
