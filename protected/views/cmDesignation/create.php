<?php
/* @var $this CmDesignationController */
/* @var $model CmDesignation */

$this->breadcrumbs=array(
	'Designations'=>array('index'),
	'Create',
);

$this->menu=  SystemSubMenu::setMenuLinks(array(
    'CmDesignation'=>array(
        'CmDesignationManage'=>array()
    )
));
?>

<h1>Create Designation</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>