<?php
/* @var $this CmDesignationController */
/* @var $model CmDesignation */

$this->breadcrumbs=array(
	'Designations'=>array('index'),
	$model->designation_name=>array('view','id'=>$this->encryptParam($model->designation_id)),
	'Update',
);

$this->menu=  SystemSubMenu::setMenuLinks(array(
    'CmDesignation'=>array(
        'CmDesignationCreate'=>array(),
        'CmDesignationView'=>array('url'=>array('id'=>$model->designation_id)),
        'CmDesignationManage'=>array()
    )
));
?>

<h1>Update Designation <?php echo $model->designation_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>