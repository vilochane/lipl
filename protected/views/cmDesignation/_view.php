<?php
/* @var $this CmDesignationController */
/* @var $data CmDesignation */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('designation_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->designation_id), array('view', 'id'=>$data->designation_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('designation_name')); ?>:</b>
	<?php echo CHtml::encode($data->designation_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('designation_remark')); ?>:</b>
	<?php echo CHtml::encode($data->designation_remark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>