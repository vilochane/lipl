<?php
/* @var $this HipVehichleDetailsController */
/* @var $data HipVehichleDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->vehicle_details_id), array('view', 'id'=>$data->vehicle_details_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_type_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_type_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_make_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_make_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_model_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_model_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_reg')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_reg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_color')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_color); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_yom')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_yom); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_con')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_con); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_ada')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_ada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_engine_cc')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_engine_cc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_engine_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_engine_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_chassis_no')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_chassis_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_price')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_status')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>