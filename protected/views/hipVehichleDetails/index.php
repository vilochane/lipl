<?php
/* @var $this HipVehichleDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hip Vehichle Details',
);

$this->menu=array(
	array('label'=>'Create HipVehichleDetails', 'url'=>array('create')),
	array('label'=>'Manage HipVehichleDetails', 'url'=>array('admin')),
);
?>

<h1>Hip Vehichle Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
