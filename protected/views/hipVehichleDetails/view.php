<?php
/* @var $this HipVehichleDetailsController */
/* @var $model HipVehichleDetails */

$this->breadcrumbs = array(
    'Vehicle Details' => array('index'),
    $model->vehicle_details_reg,
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'HipVehichleDetails' => array(
                'HipVehichleDetailsCreate' => array(),
                'HipVehichleDetailsUpdate' => array('url' => array('id' => $model->vehicle_details_id)),
                'HipVehichleDetailsDelete' => array('linkOptions' => array('submit' => array('id' => $model->vehicle_details_id), 'confirm' => CommonSystem::deleteMeassage('vehicle' . $model->vehicle_details_reg) . '?')),
                'HipVehichleDetailsManage' => array(),
            ),
        ));
?>

<h1>View Vehicle Details <?php echo $model->vehicle_details_reg; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name'=>'Vehicle Hire Status','value' => CommonListData::getVehicleHireStatus($model->hipAgreements)),    
        array('name' => 'vehicle_type_id', 'value' => $model->vehicleType->vehicle_type_name),
        array('name' => 'vehicle_make_id', 'value' => $model->vehicleMake->vehicle_make_name),
        array('name' => 'vehicle_model_id', 'value' => $model->vehicleModel->vehicle_model_name),
        'vehicle_details_reg',
        'vehicle_details_color',
        'vehicle_details_yom',
        array('name' => 'vehicle_details_con', 'value' => CommonListData::getVehicleConditionStatus($model->vehicle_details_con)),        
        'vehicle_details_ada',
        'vehicle_details_engine_cc',
        'vehicle_details_engine_no',
        'vehicle_details_chassis_no',
        array('name' => 'vehicle_details_price', 'value' => $model->setNumberFormat($model->vehicle_details_price)),
        array('name' => 'vehicle_details_price', 'value' => $model->setNumberFormat($model->vehicle_details_price)),
        array('name' => 'vehicle_details_status', 'value' => CommonListData::getListDataStatusValue($model->vehicle_details_status)),
          
    ),
));
?>
