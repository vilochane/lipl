<?php
/* @var $this HipVehichleDetailsController */
/* @var $model HipVehichleDetails */
/* @var $form CActiveForm */
$cs = Yii::app()->clientScript;
$cs->registerCoreScript('yiiactiveform');
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'hip-vehichle-details-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <span class="show-error-summary"></span>

    <div class="row">
        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_type_id'); ?>
                </span>		
                <?php
                $this->initSelect2(array(
                    "model" => $model,
                    "attribute" => "vehicle_type_id",
                    "data" => CommonListData::getListDataVehicleTypes(),
                    "htmlOptions" => array(
                        "placeholder" => "Please select a vehicle type."
                    )
                ));
                ?>
                <span class="input-group-addon">
                    <?php
                    $this->initDialog(array(
                        'ajaxLink' => 'CmVehicleType/AjaxCreate',
                        'dialogId' => 'vehicleTypeDialog',
                        'dialogTitle' => 'Create New Vehicle Type',
                        'select2Id' => 'HipVehichleDetails_vehicle_type_id',
                    ));
                    ?>
                </span>
            </div>
            <?php echo $form->error($model, 'vehicle_type_id'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_make_id'); ?>
                </span>		
                <?php
                $this->initSelect2(array(
                    "model" => $model,
                    "attribute" => "vehicle_make_id",
                    "data" => CommonListData::getListDataVehicleMake(),
                    "htmlOptions" => array(
                        "placeholder" => "Please select a make.",
                        'ajax' => array(
                            "url" => CController::createUrl('CmVehicleModel/AjaxGetOptionTags'),
                            "type" => "POST",
                            "data" => array("makeId" => "js: $(this).val()"),
                            "beforeSend" => "function (){ select2Loading('HipVehichleDetails_vehicle_model_id'); select2Empty('HipVehichleDetails_vehicle_model_id');}",
                            "success" => "js: function(data){ select2RemoveLoading('HipVehichleDetails_vehicle_model_id'); $('#HipVehichleDetails_vehicle_model_id').html(data)}"
                        )
                    )
                ));
                ?>
                <span class="input-group-addon">
                    <?php
                    $this->initDialog(array(
                        'ajaxLink' => 'CmVehicleMake/AjaxCreate',
                        'dialogId' => 'vehicleMakeDialog',
                        'dialogTitle' => 'Create New Vehicle Make',
                        'select2Id' => 'HipVehichleDetails_vehicle_make_id',
                    ));
                    ?>
                </span>
            </div>
            <?php echo $form->error($model, 'vehicle_make_id'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_model_id'); ?>
                </span>		
                <?php
                $this->initSelect2(array(
                    "model" => $model,
                    "attribute" => "vehicle_model_id",
                    "data" => CommonListData::getListDataVehicleModel(),
                    "htmlOptions" => array(
                        "placeholder" => "Please select a model.",
                        'ajax' => array(
                            "url" => CController::createUrl('CmVehicleModel/AjaxGetMakeId'),
                            "type" => "POST",
                            "data" => array("modelId" => "js: $(this).val()"),
                            "success" => "js: function(data){ select2Val('HipVehichleDetails_vehicle_make_id', data);}"
                        )
                    )
                ));
                ?>
                <span class="input-group-addon">
                    <?php
                    $this->initDialog(array(
                        'ajaxLink' => 'CmVehicleModel/AjaxCreate',
                        'dialogId' => 'vehicleModelDialog',
                        'dialogTitle' => 'Create a New Vehicle Model',
                        'select2Id' => 'HipVehichleDetails_vehicle_model_id',
                        'params' => array('ma' => "js: $('#HipVehichleDetails_vehicle_make_id').val()")
                    ));
                    ?>
                </span>
            </div>
            <?php echo $form->error($model, 'vehicle_model_id'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_reg'); ?>
                </span>		
                <?php echo $form->textField($model, 'vehicle_details_reg', array('class' => 'form-control', 'size' => 10, 'maxlength' => 10, 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_reg'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_color'); ?>
                </span>		
                <?php echo $form->textField($model, 'vehicle_details_color', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_color'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_yom'); ?>
                </span>		
                <?php
                $this->initDatePicker(array(
                    "model" => $model,
                    "attribute" => "vehicle_details_yom",
                ));
                ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_yom'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_con'); ?>
                </span>		
                <?php
                $this->initSelect2(array(
                    "model" => $model,
                    "attribute" => "vehicle_details_con",
                    "data" => CommonListData::getListDataVehicleCondition(),
                    "htmlOptions" => array(
                        "placeholder" => "Please select a condition."
                    )
                ));
                ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_con'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_engine_cc'); ?>
                </span>		
                <?php echo $form->textField($model, 'vehicle_details_engine_cc', array('class' => 'form-control', 'size' => 5, 'maxlength' => 5, 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_engine_cc'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_engine_no'); ?>
                </span>		
                <?php echo $form->textField($model, 'vehicle_details_engine_no', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_engine_no'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_chassis_no'); ?>
                </span>		
                <?php echo $form->textField($model, 'vehicle_details_chassis_no', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_chassis_no'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_price'); ?>
                </span>		
                <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "vehicle_details_price")); ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_price'); ?>
        </div>

        <div class="row">
            <div class="input-group">
                <span class="input-group-addon">
                    <?php echo $form->labelEx($model, 'vehicle_details_ada'); ?>
                </span>		
                <?php echo $form->textArea($model, 'vehicle_details_ada', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
            </div>
            <?php echo $form->error($model, 'vehicle_details_ada'); ?>
        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->