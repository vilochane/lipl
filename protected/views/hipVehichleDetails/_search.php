<?php
/* @var $this HipVehichleDetailsController */
/* @var $model HipVehichleDetails */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_type_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_type_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_make_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_make_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_model_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_model_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_reg'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_reg',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_color'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_color',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_yom'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_yom', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_con'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_con', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_ada'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_ada',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_engine_cc'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_engine_cc',array('class'=>'form-control', 'size'=>5,'maxlength'=>5, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_engine_no'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_engine_no',array('class'=>'form-control', 'size'=>60,'maxlength'=>100, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_chassis_no'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_chassis_no',array('class'=>'form-control', 'size'=>60,'maxlength'=>100, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_price'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_price',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_status'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_status', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_by'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
        <div class="row buttons">
        <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->