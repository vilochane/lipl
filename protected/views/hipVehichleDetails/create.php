<?php
/* @var $this HipVehichleDetailsController */
/* @var $model HipVehichleDetails */

$this->breadcrumbs = array(
    'Vehicle Details' => array('index'),
    'Create',
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'HipVehichleDetails' => array(
                'HipVehichleDetailsManage' => array(),
            ),
        ));
?>

<h1>Create Vehicle Details</h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>