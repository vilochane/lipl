<?php
/* @var $this HipVehichleDetailsController */
/* @var $model HipVehichleDetails */

$this->breadcrumbs = array(
    'Vehichle Details' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#hip-vehichle-details-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Vehicle Details</h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    
    </span>    
</div>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'hip-vehichle-details-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate'=>  CommonSystem::initGridviewSelect2AfterAjax(array('vehicle_type_id', 'vehicle_make_id', 'vehicle_model_id', 'vehicle_details_con', 'vehicle_details_status',)),
    'filter' => $model,
    'columns' => array(
        ///array('header' => 'Vehicle Hire Status', 'value' => 'CommonListData::getVehicleHireStatus($data->hipAgreements)'),
        array('name' => 'vehicle_type_id', 'value' => '$data->vehicleType->vehicle_type_name', 'filter'=>  CommonListData::getListDataVehicleTypes()),
        array('name' => 'vehicle_make_id', 'value' => '$data->vehicleMake->vehicle_make_name', 'filter'=>  CommonListData::getListDataVehicleMake()),
        array('name' => 'vehicle_model_id', 'value' => '$data->vehicleModel->vehicle_model_name', 'filter'=>  CommonListData::getListDataVehicleModel()),
        'vehicle_details_reg',
        'vehicle_details_color',
        'vehicle_details_yom',
        array('name' => 'vehicle_details_con', 'value' => 'CommonListData::getVehicleConditionStatus($data->vehicle_details_con)', 'filter'=>  CommonListData::getListDataVehicleCondition()),
//        'vehicle_details_ada',
        'vehicle_details_engine_cc',
//        'vehicle_details_engine_no',
//        'vehicle_details_chassis_no',
        array('name' => 'vehicle_details_price', 'value' => '$data->setNumberFormat($data->vehicle_details_price)'),
        array('name' => 'vehicle_details_price', 'value' => '$data->setNumberFormat($data->vehicle_details_price)'),
        array('name' => 'vehicle_details_status', 'value' => 'CommonListData::getListDataStatusValue($data->vehicle_details_status)', 'filter'=>  CommonListData::getListDataStatus()),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
