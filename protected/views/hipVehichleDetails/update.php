<?php
/* @var $this HipVehichleDetailsController */
/* @var $model HipVehichleDetails */

$this->breadcrumbs = array(
    'Vehicle Details' => array('index'),
    $model->vehicle_details_reg => array('view', 'id' => $this->encryptParam($model->vehicle_details_id)),
    'Update',
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'HipVehichleDetails' => array(
                'HipVehichleDetailsCreate' => array(),
                'HipVehichleDetailsView' => array('url' => array('id'=>$model->vehicle_details_id)),
                'HipVehichleDetailsManage' => array(),
            ),
        ));
?>

<h1>Update Vehicle Details <?php echo $model->vehicle_details_reg; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>