<?php
/* @var $this PeCusCreditRefDetailsController */
/* @var $model PeCusCreditRefDetails */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pe-cus-credit-ref-details-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

      <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'cus_id'); ?>
                    </span>		
                        <?php echo $form->textField($model,'cus_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'cus_id'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'credit_ref_details_cmp_name'); ?>
                    </span>		
                        <?php echo $form->textField($model,'credit_ref_details_cmp_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'credit_ref_details_cmp_name'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'credit_ref_details_cmp_address'); ?>
                    </span>		
                        <?php echo $form->textField($model,'credit_ref_details_cmp_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'credit_ref_details_cmp_address'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'credit_ref_details_cmp_tel'); ?>
                    </span>		
                        <?php echo $form->textField($model,'credit_ref_details_cmp_tel',array('class'=>'form-control', 'size'=>15,'maxlength'=>15, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'credit_ref_details_cmp_tel'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'updated_by'); ?>
                    </span>		
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'updated_by'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'updated_date'); ?>
                    </span>		
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'updated_date'); ?>
          </div>

            <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save' , array('class' =>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->