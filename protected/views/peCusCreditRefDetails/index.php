<?php
/* @var $this PeCusCreditRefDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pe Cus Credit Ref Details',
);

$this->menu=array(
	array('label'=>'Create PeCusCreditRefDetails', 'url'=>array('create')),
	array('label'=>'Manage PeCusCreditRefDetails', 'url'=>array('admin')),
);
?>

<h1>Pe Cus Credit Ref Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
