<?php
/* @var $this PeCusCreditRefDetailsController */
/* @var $model PeCusCreditRefDetails */

$this->breadcrumbs=array(
	'Pe Cus Credit Ref Details'=>array('index'),
	$model->credit_ref_details_id,
);

$this->menu=array(
	array('label'=>'List PeCusCreditRefDetails', 'url'=>array('index')),
	array('label'=>'Create PeCusCreditRefDetails', 'url'=>array('create')),
	array('label'=>'Update PeCusCreditRefDetails', 'url'=>array('update', 'id'=>$model->credit_ref_details_id)),
	array('label'=>'Delete PeCusCreditRefDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->credit_ref_details_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PeCusCreditRefDetails', 'url'=>array('admin')),
);
?>

<h1>View PeCusCreditRefDetails <?php echo $model->credit_ref_details_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'credit_ref_details_id',
		'cus_id',
		'credit_ref_details_cmp_name',
		'credit_ref_details_cmp_address',
		'credit_ref_details_cmp_tel',
		'updated_by',
		'updated_date',
	),
)); ?>
