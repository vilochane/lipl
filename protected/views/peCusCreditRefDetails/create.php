<?php
/* @var $this PeCusCreditRefDetailsController */
/* @var $model PeCusCreditRefDetails */

$this->breadcrumbs=array(
	'Pe Cus Credit Ref Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PeCusCreditRefDetails', 'url'=>array('index')),
	array('label'=>'Manage PeCusCreditRefDetails', 'url'=>array('admin')),
);
?>

<h1>Create PeCusCreditRefDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>