<?php
/* @var $this PeCusCreditRefDetailsController */
/* @var $data PeCusCreditRefDetails */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('credit_ref_details_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->credit_ref_details_id), array('view', 'id'=>$data->credit_ref_details_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_id')); ?>:</b>
	<?php echo CHtml::encode($data->cus_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('credit_ref_details_cmp_name')); ?>:</b>
	<?php echo CHtml::encode($data->credit_ref_details_cmp_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('credit_ref_details_cmp_address')); ?>:</b>
	<?php echo CHtml::encode($data->credit_ref_details_cmp_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('credit_ref_details_cmp_tel')); ?>:</b>
	<?php echo CHtml::encode($data->credit_ref_details_cmp_tel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>