<?php
/* @var $this PeCusCreditRefDetailsController */
/* @var $model PeCusCreditRefDetails */

$this->breadcrumbs=array(
	'Pe Cus Credit Ref Details'=>array('index'),
	$model->credit_ref_details_id=>array('view','id'=>$this->encryptParam($model->credit_ref_details_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List PeCusCreditRefDetails', 'url'=>array('index')),
	array('label'=>'Create PeCusCreditRefDetails', 'url'=>array('create')),
	array('label'=>'View PeCusCreditRefDetails', 'url'=>array('view', 'id'=>$model->credit_ref_details_id)),
	array('label'=>'Manage PeCusCreditRefDetails', 'url'=>array('admin')),
);
?>

<h1>Update PeCusCreditRefDetails <?php echo $model->credit_ref_details_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>