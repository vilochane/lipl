<?php
/* @var $this CmOperationPeriodController */
/* @var $model CmOperationPeriod */

$this->breadcrumbs=array(
	'Cm Operation Periods'=>array('index'),
	$model->operation_period_id=>array('view','id'=>$this->encryptParam($model->operation_period_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmOperationPeriod', 'url'=>array('index')),
	array('label'=>'Create CmOperationPeriod', 'url'=>array('create')),
	array('label'=>'View CmOperationPeriod', 'url'=>array('view', 'id'=>$model->operation_period_id)),
	array('label'=>'Manage CmOperationPeriod', 'url'=>array('admin')),
);
?>

<h1>Update CmOperationPeriod <?php echo $model->operation_period_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>