<?php
/* @var $this CmOperationPeriodController */
/* @var $model CmOperationPeriod */

$this->breadcrumbs=array(
	'Cm Operation Periods'=>array('index'),
	$model->operation_period_id,
);

$this->menu=array(
	array('label'=>'List CmOperationPeriod', 'url'=>array('index')),
	array('label'=>'Create CmOperationPeriod', 'url'=>array('create')),
	array('label'=>'Update CmOperationPeriod', 'url'=>array('update', 'id'=>$model->operation_period_id)),
	array('label'=>'Delete CmOperationPeriod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->operation_period_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmOperationPeriod', 'url'=>array('admin')),
);
?>

<h1>View CmOperationPeriod <?php echo $model->operation_period_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'operation_period_id',
		'operation_period_name',
		'updated_by',
		'updated_date',
	),
)); ?>
