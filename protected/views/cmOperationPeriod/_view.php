<?php
/* @var $this CmOperationPeriodController */
/* @var $data CmOperationPeriod */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('operation_period_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->operation_period_id), array('view', 'id'=>$data->operation_period_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('operation_period_name')); ?>:</b>
	<?php echo CHtml::encode($data->operation_period_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>