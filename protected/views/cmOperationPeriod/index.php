<?php
/* @var $this CmOperationPeriodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Operation Periods',
);

$this->menu=array(
	array('label'=>'Create CmOperationPeriod', 'url'=>array('create')),
	array('label'=>'Manage CmOperationPeriod', 'url'=>array('admin')),
);
?>

<h1>Cm Operation Periods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
