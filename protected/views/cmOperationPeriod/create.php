<?php
/* @var $this CmOperationPeriodController */
/* @var $model CmOperationPeriod */

$this->breadcrumbs=array(
	'Cm Operation Periods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmOperationPeriod', 'url'=>array('index')),
	array('label'=>'Manage CmOperationPeriod', 'url'=>array('admin')),
);
?>

<h1>Create CmOperationPeriod</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>