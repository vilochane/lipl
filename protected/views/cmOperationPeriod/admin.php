<?php
/* @var $this CmOperationPeriodController */
/* @var $model CmOperationPeriod */

$this->breadcrumbs=array(
	'Cm Operation Periods'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cm-operation-period-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Cm Operation Periods</h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    </span>
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['createANewRecord'], CController::createUrl('create'), array('class' => 'add-new-button')); ?>    </span>
</div>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'cm-operation-period-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'operation_period_id',
		'operation_period_name',
		'updated_by',
		'updated_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
