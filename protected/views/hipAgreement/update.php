<?php
/* @var $this HipAgreementController */
/* @var $model HipAgreement */

$this->breadcrumbs=array(
	'Agreement'=>array('admin', 'c'=>$this->getUrlQuery('c', false)),
	$model->agreement_code=>array('view','id'=>$this->encryptParam($model->agreement_id), 'c'=>$this->getUrlQuery('c', false)),
	'Update',
);
$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array('PeCustomerManage' => array()),
            'HipAgreement' => array(
                'HipAgreementSeperator' => array(), 
                'HipAgreementCreate' => array('url'=>array('c'=>$this->getUrlQuery('c'))),
                'HipAgreementView' => array('url' => array('id' => $model->agreement_id, 'c'=>$this->getUrlQuery('c'))),
                'HipAgreementManage' => array('url'=>array('c'=>$this->getUrlQuery('c'))),
            )));
?>

<h1>Update Agreement <?php echo $model->agreement_code; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>