<?php
/* @var $this HipAgreementController */
/* @var $model HipAgreement */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'vehicle_details_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'vehicle_details_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'period_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'period_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'due_date_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'due_date_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'prefix_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'prefix_name',array('class'=>'form-control', 'size'=>2,'maxlength'=>2, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_no'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_no',array('class'=>'form-control', 'size'=>6,'maxlength'=>6, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_code'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_code',array('class'=>'form-control', 'size'=>8,'maxlength'=>8, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_credit_value'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_credit_value',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_rental'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_rental',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_d_payment'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_d_payment',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_start_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_start_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'agreement_end_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'agreement_end_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_by'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
        <div class="row buttons">
        <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->