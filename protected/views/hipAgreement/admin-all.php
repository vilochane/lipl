<?php
/* @var $this HipAgreementController */
/* @var $model HipAgreement */

$this->breadcrumbs = array(
    'Agreements' => array('adminAll'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#hip-agreement-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Agreements <?php echo CommonListData::getListDataCustomerName($this->getUrlQuery('c')) ?></h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    
    </span>    
</div>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'hip-agreement-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'afterAjaxUpdate' => CommonSystem::initGridviewSelect2AfterAjax(array('cus_id', 'vehicle_details_id', 'period_id', 'due_date_id','agreement_closed_status')),
    'columns' => array(
        'agreement_code',
        array("name" => 'cus_id', "value" => '$data->cus->cus_first_name', 'filter' => CommonListData::getListDataCustomer()),
        array("name" => 'vehicle_details_id', "value" => '$data->vehicleDetails->vehicle_details_reg', 'filter' => CommonListData::getListDataVehicles(true)),
        array("name" => 'period_id', "value" => '$data->period->period_name', 'filter' => CommonListData::getListDataAgreementPeriod()),
        array("name" => 'due_date_id', "value" => '$data->dueDate->due_date_date', 'filter' => CommonListData::getListDataDueDate()),
        array("name" => 'agreement_credit_value', "value" => '$data->setNumberFormat($data->agreement_credit_value)'),
        array("name" => 'agreement_rental', "value" => '$data->setNumberFormat($data->agreement_rental)'),
        array("name" => 'agreement_d_payment', "value" => '$data->setNumberFormat($data->agreement_d_payment)'),
        'agreement_start_date',
        'agreement_end_date',
        array('name' => 'agreement_closed_status', 'filter'=>  CommonListData::getListDataAgreementInsuranceStatus(),'value' => 'CommonListData::getListDataAgreementInsuranceStatusVal($data->agreement_closed_status)'),        
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{update}{delete}',
            'buttons' => array(
                'view' => array(
                    'url' => 'Yii::app()->createAbsoluteUrl("HipAgreement/viewAll", array("id"=>$this->encryptParam($data->agreement_id)))'
                ),
                'update' => array(
                    'url' => 'Yii::app()->createAbsoluteUrl("HipAgreement/updateAll", array("id"=>$this->encryptParam($data->agreement_id)))'
                ),                
            )
        ),
    ),
));
?>
