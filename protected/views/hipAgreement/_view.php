<?php
/* @var $this HipAgreementController */
/* @var $data HipAgreement */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->agreement_id), array('view', 'id'=>$data->agreement_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_id')); ?>:</b>
	<?php echo CHtml::encode($data->cus_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_details_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_details_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('period_id')); ?>:</b>
	<?php echo CHtml::encode($data->period_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('due_date_id')); ?>:</b>
	<?php echo CHtml::encode($data->due_date_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prefix_name')); ?>:</b>
	<?php echo CHtml::encode($data->prefix_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_no')); ?>:</b>
	<?php echo CHtml::encode($data->agreement_no); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_code')); ?>:</b>
	<?php echo CHtml::encode($data->agreement_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_credit_value')); ?>:</b>
	<?php echo CHtml::encode($data->agreement_credit_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_rental')); ?>:</b>
	<?php echo CHtml::encode($data->agreement_rental); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_d_payment')); ?>:</b>
	<?php echo CHtml::encode($data->agreement_d_payment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_start_date')); ?>:</b>
	<?php echo CHtml::encode($data->agreement_start_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agreement_end_date')); ?>:</b>
	<?php echo CHtml::encode($data->agreement_end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>