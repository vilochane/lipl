<?php
/* @var $this HipAgreementController */
/* @var $model HipAgreement */

$this->breadcrumbs=array(
	'Agreement'=>array('adminAll'),
	$model->agreement_code=>array('viewAll','id'=>$this->encryptParam($model->agreement_id)),
	'Update',
);
$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array('PeCustomerManage' => array()),
            'HipAgreement' => array(
                'HipAgreementSeperator' => array(), 
                'HipAgreementViewAll' => array('url' => array('id' => $model->agreement_id)),
                'HipAgreementManageAll' => array(),
            )));
?>

<h1>Update Agreement <?php echo $model->agreement_code; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>