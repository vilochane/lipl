<?php
/* @var $this HipAgreementController */
/* @var $model HipAgreement */

$this->breadcrumbs=array(
	'Agreement'=>array('admin', 'c'=>$this->getUrlQuery('c', false)),
	'Create',
);
$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array('PeCustomerManage' => array()),
            'HipAgreement' => array(
                'HipAgreementSeperator' => array(), 
                'HipAgreementManage' => array('url'=>array('c'=>$this->getUrlQuery('c'))),
            )));
?>

<h1>Create Agreement <?php echo CommonListData::getListDataCustomerName($this->getUrlQuery('c'))?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>