<?php
/* @var $this HipAgreementController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Hip Agreements',
);

$this->menu=array(
	array('label'=>'Create HipAgreement', 'url'=>array('create')),
	array('label'=>'Manage HipAgreement', 'url'=>array('admin')),
);
?>

<h1>Hip Agreements</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
