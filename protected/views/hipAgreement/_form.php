<?php
/* @var $this HipAgreementController */
/* @var $model HipAgreement */
/* @var $form CActiveForm */
$cs = Yii::app()->clientScript;
$cs->registerCoreScript('yiiactiveform');
$hireSatus = true;
if ($model->scenario === 'insert') {
    $hireSatus = false;
}
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'hip-agreement-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'vehicle_details_id'); ?>
            </span>
            <?php
            $this->initSelect2(array(
                "model" => $model,
                "attribute" => "vehicle_details_id",
                "data" => CommonListData::getListDataVehicles($hireSatus),
                "htmlOptions" => array(
                    "placeholder" => "Please select a vehicle."
                )
            ));
            ?>
            <span class="input-group-addon">
                <?php
                $this->initDialog(array(
                    'ajaxLink' => 'HipVehichleDetails/AjaxCreate',
                    'dialogId' => 'vehicleDetailsDialog',
                    'dialogTitle' => 'Create a New Vehicle',
                    'select2Id' => 'HipAgreement_vehicle_details_id',
                ));
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'vehicle_details_id'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'period_id'); ?>
            </span>		
            <?php
            $this->initSelect2(array(
                "model" => $model,
                "attribute" => "period_id",
                "data" => CommonListData::getListDataAgreementPeriod(),
                "htmlOptions" => array(
                    "placeholder" => "Please select a period."
                )
            ));
            ?>
            <span class="input-group-addon">
                <?php
                $this->initDialog(array(
                    "ajaxLink" => "CmPeriod/AjaxCreate",
                    "dialogId" => "periodDialog",
                    "dialogTitle" => "Create a New Period",
                    'select2Id' => 'HipAgreement_period_id',
                ));
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'agreement_period'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_no'); ?>
            </span>		
            <span class="input-group-addon" style="width: 10%;">
                <?php
                $this->initSelect2(array(
                    "model" => $model,
                    "attribute" => "prefix_name",
                    "data" => CommonListData::getListDataPrefix(),
                ));
                ?>
            </span>
            <span class="input-group-addon" style="width: 50%;">
                <?php echo $form->textField($model, 'agreement_no', array('class' => 'form-control', 'size' => 8, 'maxlength' => 8, 'autocomplete' => 'off'));
                ?>
            </span>
            <span class="input-group-addon">
                <?php
                $this->initDialog(array(
                    "ajaxLink" => "CmPrefix/AjaxCreate",
                    "dialogId" => "prefixDialog",
                    "dialogTitle" => "Create a New Prefix",
                    "select2Id" => "HipAgreement_prefix_name",
                ));
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'agreement_code'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_credit_value'); ?>
            </span>		
            <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "agreement_credit_value")); ?>
        </div>
        <?php echo $form->error($model, 'agreement_credit_value'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_rental'); ?>
            </span>		
            <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "agreement_rental")); ?>
        </div>
        <?php echo $form->error($model, 'agreement_rental'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_d_payment'); ?>
            </span>
            <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "agreement_d_payment")); ?>
        </div>
        <?php echo $form->error($model, 'agreement_d_payment'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_i_rate'); ?>
            </span>
            <?php echo $form->textField($model, 'agreement_i_rate', array('class' => 'form-control', 'size' => 5, 'maxlength' => 5, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'agreement_i_rate'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'rentalTotDisplay'); ?>
            </span>
            <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "rentalTotDisplay")); ?>
        </div>
        <?php echo $form->error($model, 'rentalTotDisplay'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'due_date_id'); ?>
            </span>		
            <?php
            $this->initSelect2(array(
                "model" => $model,
                "attribute" => "due_date_id",
                "data" => CommonListData::getListDataDueDate(),
                "htmlOptions" => array(
                    "placeholder" => "Please select a due date."
                )
            ));
            ?>
            <span class="input-group-addon">
                <?php
                $this->initDialog(array(
                    "ajaxLink" => "CmDueDate/AjaxCreate",
                    "dialogId" => "dueDateDialog",
                    "dialogTitle" => "Create a New Due Date",
                    "select2Id" => "HipAgreement_due_date_id",
                ));
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'due_date_id'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_start_date'); ?>
            </span>		
            <?php
            $this->initDatePicker(array("model" => $model, "attribute" => "agreement_start_date",
                "options" => array(
                    'maxDate' => "90",
                )
            ));
            ?>
        </div>
        <?php echo $form->error($model, 'agreement_start_date'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_end_date'); ?>
            </span>		
            <?php
            $this->initDatePicker(array("model" => $model, "attribute" => "agreement_end_date",
                "options" => array("minDate" => 0, "maxDate" => null, "yearRange" => "-0:+100")));
            ?>
        </div>
        <?php echo $form->error($model, 'agreement_end_date'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_ins_status'); ?>
            </span>		
            <?php echo $form->radioButtonList($model, "agreement_ins_status", CommonListData::getListDataAgreementInsuranceStatus()); ?>
        </div>
        <?php echo $form->error($model, 'agreement_ins_status'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_c_collected'); ?>
            </span>		
            <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "agreement_c_collected")); ?>
        </div>
        <?php echo $form->error($model, 'agreement_c_collected'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_p_collected'); ?>
            </span>		
            <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "agreement_p_collected")); ?>
        </div>
        <?php echo $form->error($model, 'agreement_p_collected'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_c_due'); ?>
            </span>		
            <?php $this->initFormatCurrency(array("model" => $model, "attribute" => "agreement_c_due")); ?>
        </div>
        <?php echo $form->error($model, 'agreement_c_due'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_p_last_date'); ?>
            </span>		
            <?php $this->initDatePicker(array(
                'model'=>$model,
                'attribute'=>'agreement_p_last_date'
            )); ?>
        </div>
        <?php echo $form->error($model, 'agreement_p_last_date'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'agreement_c_p_collected_status'); ?>
            </span>		
            <?php echo $form->radioButtonList($model, 'agreement_c_p_collected_status', CommonListData::getPaymentCollectedStatus()); ?>
        </div>
        <?php echo $form->error($model, 'agreement_c_p_collected_status'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<?php
$cs->registerScript("hip=agreement-form-functions", ""

        /* init period error message */
        . "function initSelectPeriodMsg(){"
        . "initNotyAlert('error','Please select a  Period (in Months)');"
        . "setFocus('HipAgreement_period_id');"
        . "}"
        . "var periodObj = $('#HipAgreement_period_id');"
        . "var creditObj = $('#HipAgreement_agreement_credit_value');"
        . "var rentalObj = $('#HipAgreement_agreement_rental');"
        . "var downPaymentObj = $('#HipAgreement_agreement_d_payment');"
        . "var dueDateObj = $('#HipAgreement_due_date_id');"
        . "var startDateObj = $('#HipAgreement_agreement_start_date');"
        . "var endDateObj = $('#HipAgreement_agreement_end_date');"
        . "var iRateObj = $('#HipAgreement_agreement_i_rate');"
        . "var rentalPayableObj = $('#HipAgreement_rentalTotDisplay');"
        . "downPaymentObj.val(0);"
        /* setting the rental */
        . "function setRental(){"
        . "var creditVal = parseFloat(creditObj.val().replace(/,/g,''));"
        . "var period = parseInt(periodObj.children('option:selected').text());"
        . "if(!isNaN(period)){"
        . "var rental = creditVal/period;"
        . "rentalObj.val(rental);"
        . "}else{"
        . "initSelectPeriodMsg();"
        . ""
        . "}"
        . "}"
        /* setting rental payable */
        . "function setRentalPayable(){"
        . "var creditVal = parseFloat(creditObj.val().replace(/,/g,''));"
        . "var periodVal = parseInt(periodObj.children('option:selected').text());"
        . "var iRateVal = parseFloat(iRateObj.val().replace(/,/g,''));"
        . "var interestVal = parseFloat((creditVal * iRateVal)/100)/periodVal; "
        . "var rental = parseFloat(rentalObj.val().replace(/,/g,''));"
        . "var rentalTot = rental + interestVal;"
        . "if(!isNaN(periodVal) && !isNaN(iRateVal)){"
        . "rentalPayableObj.val(rentalTot);"
        . "}"        
        . "}"
        /* setting end date */
        . "function setEndDate(){"
        . "if(periodObj.val() !== ''){"
        . "if(startDateObj.val() !== ''){"
        . "var endDate = addMonth(startDateObj.val(), startDateObj,  periodObj.children('option:selected').text());"
        . "endDateObj.datepicker({dateFormat:'yy-mm-dd'});"
        . "endDateObj.datepicker('setDate', endDate);"
        . "disableDatePicker('HipAgreement_agreement_end_date');"
        . "}"
        . ""
        . "}else{"
        . "initSelectPeriodMsg();"
        . "}"
        . "}"
        /* empties due date and start and end date */
        . "function setValuesEmpty(){"
        . "dueDateObj.val('');"
        . "startDateObj.val('');"
        . "endDateObj.val('');"
        . "}"
        /* set start date */
        . "function setStartDate(){"
        // . "console.log('here');"
        . "if(dueDateObj.val() === ''){"
        . "initNotyAlert('error','Please select a due date');"
        . "startDateObj.val('');"
        /* auto setting the start date to current 1 month later */
        . "}else if(startDateObj.val() === ''){"
        . "console.log(getCurrentDate(1));"
        . "var startDate = getCurrentDate(1);"
        . "var startDate = getDate(startDate, dueDateObj.children('option:selected').text());"
        . "startDateObj.datepicker('setDate', startDate);"
        . "setEndDate();"
        . "}else{"
        . "var startDate = getDate(startDateObj.val(), dueDateObj.children('option:selected').text());"
        . "startDateObj.datepicker('setDate', startDate);"
        . "console.log(startDate);"
        . "setEndDate();"
        . "}"
        . "}"
        , CClientScript::POS_END);
$cs->registerScript("hip-agreement-form", ""
        . "$('#HipAgreement_agreement_credit_value').change(function(){ setRental(); setRentalPayable();});"
        . "$('#HipAgreement_agreement_i_rate').change(function(){ setRentalPayable();});"
        . "$('#HipAgreement_period_id').change(function(){ setValuesEmpty(); setRental(); setRentalPayable();});"
        . "$('#HipAgreement_due_date_id').change(function(){ setStartDate();});"
        . "$('#HipAgreement_agreement_start_date').change(function(){ setStartDate();});"
        . "$('#HipAgreement_agreement_end_date').change(function(){ setStartDate();});"
        . ""
        . "", CClientScript::POS_READY);
?>