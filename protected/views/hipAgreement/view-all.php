<?php
/* @var $this HipAgreementController */
/* @var $model HipAgreement */

$this->breadcrumbs = array(
    'Agreement' => array('adminAll'),
    $model->agreement_code,
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array('PeCustomerManage' => array()),
            'HipAgreement' => array(
                'HipAgreementSeperator' => array(),
                'HipAgreementUpdateAll' => array('url' => array('id' => $model->agreement_id)),
                'HipAgreementDelete' => array('linkOptions' => array('submit' => array('id' => $model->agreement_id), 'confirm' => CommonSystem::deleteMeassage('agreement ' . $model->agreement_code) . '?')),
                'HipAgreementManageAll' => array(),
        )));
?>

<h1>View Agreement <?php echo $model->agreement_code; ?> <?php echo CommonListData::getListDataCustomerName($this->getUrlQuery('c')) ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array("name" => 'cus_id', "value" => $model->cus->cus_first_name),
        array("name" => 'vehicle_details_id', "value" => $model->vehicleDetails->vehicle_details_reg),
        array("name" => 'period_id', "value" => $model->period->period_name),
        array("name" => 'due_date_id', "value" => $model->dueDate->due_date_date),
        array("name" => 'agreement_credit_value', "value" => $model->setNumberFormat($model->agreement_credit_value)),
        array("name" => 'agreement_rental', "value" => $model->setNumberFormat($model->agreement_rental)),
        array("name" => 'agreement_rental_interest', "value" => $model->setNumberFormat($model->agreement_rental_interest)),
        array("name" => 'agreement_rental_tot', "value" => $model->setNumberFormat($model->agreement_rental_tot)),
        array("name" => 'agreement_interest_tot', "value" => $model->setNumberFormat($model->agreement_interest_tot)),
        array("name" => 'agreement_rental_rate', "value" => $model->setNumberFormat($model->agreement_rental_rate)),
        array("name" => 'agreement_r_interest_rate', "value" => $model->setNumberFormat($model->agreement_r_interest_rate)),
        array("name" => 'agreement_i_rate', "value" => $model->setNumberFormat($model->agreement_i_rate)),
        array("name" => 'agreement_d_payment', "value" => $model->setNumberFormat($model->agreement_d_payment)),
        array("name" => 'agreement_d_capital', "value" => $model->setNumberFormat($model->agreement_d_capital)),
        array("name" => 'agreement_d_interest', "value" => $model->setNumberFormat($model->agreement_d_interest)),
        array("name" => 'agreement_p_amount_tot', "value" => $model->setNumberFormat($model->agreement_p_amount_tot)),
        array("name" => 'agreement_p_interest_tot', "value" => $model->setNumberFormat($model->agreement_p_interest_tot)),
        array("name" => 'agreement_p_d_amount', "value" => $model->setNumberFormat($model->agreement_p_d_amount)),
        array("name" => 'agreement_p_d_interest', "value" => $model->setNumberFormat($model->agreement_p_d_interest)),
        'agreement_p_late_count',
        'agreement_start_date',
        'agreement_end_date',
        'agreement_l_d_date',
        'agreement_n_d_date',
        'agreement_p_last_date',
        array('name' => 'agreement_closed_status', 'value' => CommonListData::getListDataAgreementInsuranceStatusVal($model->agreement_closed_status)),
        array('name' => 'agreement_ins_status', 'value' => CommonListData::getListDataAgreementInsuranceStatusVal($model->agreement_ins_status))
    ),
));
?>
