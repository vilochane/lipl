<?php
/* @var $this CmCompanyProfileController */
/* @var $data CmCompanyProfile */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->company_profile_id), array('view', 'id'=>$data->company_profile_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_name')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_reg_no')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_reg_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_address')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_telephone')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_telephone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_telephone2')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_telephone2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_fax')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_fax); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_fax2')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_fax2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_email')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_email2')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_email2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_web_site')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_web_site); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_profile_logo')); ?>:</b>
	<?php echo CHtml::encode($data->company_profile_logo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>