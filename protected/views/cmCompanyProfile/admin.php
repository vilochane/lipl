<?php
/* @var $this CmCompanyProfileController */
/* @var $model CmCompanyProfile */

$this->breadcrumbs = array(
    'Company Profiles' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List CmCompanyProfile', 'url' => array('index')),
    array('label' => 'Create CmCompanyProfile', 'url' => array('create')),
);

?>

<h1>Manage Company Profile</h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['createANewRecord'], CController::createUrl('create'), array('class' => 'add-new-button')); ?>    </span>
</div>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'cm-company-profile-grid',
    'dataProvider' => $model->search(),
    //'filter' => $model,
    'columns' => array(
        'company_profile_name',
        'company_profile_reg_no',
        'company_profile_address',
        'company_profile_telephone',
        'company_profile_telephone2',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
