<?php
/* @var $this CmCompanyProfileController */
/* @var $model CmCompanyProfile */

$this->breadcrumbs=array(
	'Company Profiles'=>array('index'),
	'Create',
);

$this->menu=  SystemSubMenu::setMenuLinks(array(
    'CmCompanyProfile'=>array(
        'CmCompanyProfileManage'=>array()
    )
));
?>

<h1>Create Company Profile</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>