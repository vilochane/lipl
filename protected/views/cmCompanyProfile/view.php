<?php
/* @var $this CmCompanyProfileController */
/* @var $model CmCompanyProfile */

$this->breadcrumbs=array(
	'Company Profiles'=>array('index'),
	$model->company_profile_name,
);

$this->menu=  SystemSubMenu::setMenuLinks(array(
    'CmCompanyProfile'=>array(
        'CmCompanyProfileCreate'=>array(),
        'CmCompanyProfileUpdate'=>array('url'=>array('id'=>$model->company_profile_id)),
        'CmCompanyProfileDelete'=>array('linkOptions'=>array('submit'=>array('id'=>$model->company_profile_id), 'confirm' => $this->deleteMeassage('company profile') . $model->company_profile_name . '?')),
        'CmCompanyProfileManage'=>array()
    )
));
?>

<h1>View Company Profile <?php echo $model->company_profile_name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'company_profile_name',
		'company_profile_reg_no',
		'company_profile_address',
		'company_profile_telephone',
		'company_profile_telephone2',
		'company_profile_fax',
		'company_profile_fax2',
		'company_profile_email',
		'company_profile_email2',
		'company_profile_web_site',
		array('name'=>'company_profile_logo', 'type'=>'raw', 'value'=> Common::getImage('images/companyImages',$model->company_profile_logo, 'thumb'))

	),
)); ?>
