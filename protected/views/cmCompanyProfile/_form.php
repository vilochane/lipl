<?php
/* @var $this CmCompanyProfileController */
/* @var $model CmCompanyProfile */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-company-profile-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'company_profile_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'company_profile_name'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_reg_no'); ?>
            </span>		
            <?php echo $form->textField($model, 'company_profile_reg_no', array('class' => 'form-control', 'size' => 10, 'maxlength' => 10, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'company_profile_reg_no'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_address'); ?>
            </span>		
            <?php echo $form->textArea($model, 'company_profile_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'company_profile_address'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_telephone'); ?>
            </span>		
            <?php $this->initTelephoneField(array("model" => $model, "attribute" => "company_profile_telephone")) ?>
        </div>
        <?php echo $form->error($model, 'company_profile_telephone'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_telephone2'); ?>
            </span>		
            <?php $this->initTelephoneField(array("model" => $model, "attribute" => "company_profile_telephone2")) ?>
        </div>
        <?php echo $form->error($model, 'company_profile_telephone2'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_fax'); ?>
            </span>		
            <?php $this->initTelephoneField(array("model" => $model, "attribute" => "company_profile_fax")) ?>
        </div>
        <?php echo $form->error($model, 'company_profile_fax'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_fax2'); ?>
            </span>		
            <?php $this->initTelephoneField(array("model" => $model, "attribute" => "company_profile_fax2")) ?>
        </div>
        <?php echo $form->error($model, 'company_profile_fax2'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_email'); ?>
            </span>		
            <?php echo $form->textField($model, 'company_profile_email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'company_profile_email'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_email2'); ?>
            </span>		
            <?php echo $form->textField($model, 'company_profile_email2', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'company_profile_email2'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_web_site'); ?>
            </span>		
            <?php echo $form->textField($model, 'company_profile_web_site', array('placeholder'=>'http://lipl.com','class' => 'form-control', 'size' => 60, 'maxlength' => 150, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'company_profile_web_site'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'company_profile_logo'); ?>
            </span>		
            <?php echo $form->fileField($model, 'company_profile_logo', array('class' => 'form-control','autocomplete' => 'off')); ?>
            <span id="cmp-image-preview" class="img-rounded input-group-addon">
                <?php
                if (!empty($model->company_profile_logo)) {
                    echo Common::getImage('images/companyImages', $model->company_profile_logo, "thumb");
                }
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'company_profile_logo'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
<?php
$cs = Yii::app()->clientScript;
$cs->registerScript("emp_form", ""
        . "$('#CmCompanyProfile_company_profile_logo').change(function(){ imagePreview($(this)[0], 'cmp-image-preview'); });"
        , CClientScript::POS_READY);
?>