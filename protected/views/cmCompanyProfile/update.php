<?php
/* @var $this CmCompanyProfileController */
/* @var $model CmCompanyProfile */

$this->breadcrumbs=array(
	'Company Profiles'=>array('index'),
	$model->company_profile_name=>array('view','id'=>$this->encryptParam($model->company_profile_id)),
	'Update',
);

$this->menu=  SystemSubMenu::setMenuLinks(array(
    'CmCompanyProfile'=>array(
        'CmCompanyProfileCreate'=>array(),
        'CmCompanyProfileView'=>array('url'=>array('id'=>$model->company_profile_id)),
        'CmCompanyProfileManage'=>array()
    )
));
?>

<h1>Update Company Profile <?php echo $model->company_profile_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>