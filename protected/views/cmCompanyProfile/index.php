<?php
/* @var $this CmCompanyProfileController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Company Profiles',
);

$this->menu=array(
	array('label'=>'Create CmCompanyProfile', 'url'=>array('create')),
	array('label'=>'Manage CmCompanyProfile', 'url'=>array('admin')),
);
?>

<h1>Cm Company Profiles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
