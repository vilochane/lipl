<?php
/* @var $this CmEmpPeriodController */
/* @var $model CmEmpPeriod */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-emp-period-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <span class="show-error-summary"></span>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_period_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'emp_period_name', array('class' => 'form-control', 'size' => 20, 'maxlength' => 20, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'emp_period_name'); ?>
    </div>
    <div class="row buttons">
        <?php CommonSystem::getAjaxSubmitButton(CController::createUrl("AjaxCreate"), $dialogId, $select2Id); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->