<?php
/* @var $this CmEmpPeriodController */
/* @var $model CmEmpPeriod */

$this->breadcrumbs=array(
	'Cm Emp Periods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmEmpPeriod', 'url'=>array('index')),
	array('label'=>'Manage CmEmpPeriod', 'url'=>array('admin')),
);
?>

<h1>Create CmEmpPeriod</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>