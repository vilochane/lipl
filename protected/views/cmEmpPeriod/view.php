<?php
/* @var $this CmEmpPeriodController */
/* @var $model CmEmpPeriod */

$this->breadcrumbs=array(
	'Cm Emp Periods'=>array('index'),
	$model->emp_period_id,
);

$this->menu=array(
	array('label'=>'List CmEmpPeriod', 'url'=>array('index')),
	array('label'=>'Create CmEmpPeriod', 'url'=>array('create')),
	array('label'=>'Update CmEmpPeriod', 'url'=>array('update', 'id'=>$model->emp_period_id)),
	array('label'=>'Delete CmEmpPeriod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->emp_period_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmEmpPeriod', 'url'=>array('admin')),
);
?>

<h1>View CmEmpPeriod <?php echo $model->emp_period_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'emp_period_id',
		'emp_period_name',
		'updated_by',
		'updated_date',
	),
)); ?>
