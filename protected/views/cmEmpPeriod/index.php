<?php
/* @var $this CmEmpPeriodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Emp Periods',
);

$this->menu=array(
	array('label'=>'Create CmEmpPeriod', 'url'=>array('create')),
	array('label'=>'Manage CmEmpPeriod', 'url'=>array('admin')),
);
?>

<h1>Cm Emp Periods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
