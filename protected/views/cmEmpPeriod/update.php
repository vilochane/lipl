<?php
/* @var $this CmEmpPeriodController */
/* @var $model CmEmpPeriod */

$this->breadcrumbs=array(
	'Cm Emp Periods'=>array('index'),
	$model->emp_period_id=>array('view','id'=>$this->encryptParam($model->emp_period_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmEmpPeriod', 'url'=>array('index')),
	array('label'=>'Create CmEmpPeriod', 'url'=>array('create')),
	array('label'=>'View CmEmpPeriod', 'url'=>array('view', 'id'=>$model->emp_period_id)),
	array('label'=>'Manage CmEmpPeriod', 'url'=>array('admin')),
);
?>

<h1>Update CmEmpPeriod <?php echo $model->emp_period_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>