<?php
/* @var $this CmBranchController */
/* @var $model CmBranch */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-branch-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'branch_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'branch_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50)); ?>
        </div>
        <?php echo $form->error($model, 'branch_name'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'emp_id'); ?>
            </span>		
            <?php
            $optionsArray = array(
                'model' => $model,
                'attribute' => 'emp_id',
                'data' => CommonListData::getListDataEmployee(true, 1),
                'htmlOptions' => array(
                    'placeholder' => 'Please select a employee',
                )
            );
            $this->initSelect2($optionsArray);
            
            ?>
        </div>
        <?php echo $form->error($model, 'emp_id'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'branch_address'); ?>
            </span>		
            <?php echo $form->textArea($model, 'branch_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200)); ?>
        </div>
        <?php echo $form->error($model, 'branch_address'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'branch_telephone'); ?>
            </span>		
            <?php $this->initTelephoneField(array("model" => $model, "attribute" => "branch_telephone")) ?>
        </div>
        <?php echo $form->error($model, 'branch_telephone'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'branch_telephone2'); ?>
            </span>		
            <?php $this->initTelephoneField(array("model" => $model, "attribute" => "branch_telephone2")) ?>
        </div>
        <?php echo $form->error($model, 'branch_telephone2'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'branch_fax'); ?>
            </span>		
            <?php $this->initTelephoneField(array("model" => $model, "attribute" => "branch_fax")) ?>
        </div>
        <?php echo $form->error($model, 'branch_fax'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'branch_fax2'); ?>
            </span>		
             <?php $this->initTelephoneField(array("model" => $model, "attribute" => "branch_fax2")) ?>
        </div>
        <?php echo $form->error($model, 'branch_fax2'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->