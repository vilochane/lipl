<?php
/* @var $this CmBranchController */
/* @var $model CmBranch */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10)); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50)); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'emp_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'emp_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10)); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_address'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200)); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_telephone'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_telephone', array('class'=>'form-control')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_telephone2'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_telephone2', array('class'=>'form-control')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_fax'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_fax', array('class'=>'form-control')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_fax2'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_fax2', array('class'=>'form-control')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_by'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10)); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control')); ?>
                </div>
            </div>
        <div class="row buttons">
        <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->