<?php
/* @var $this CmBranchController */
/* @var $model CmBranch */

$this->breadcrumbs = array(
    'Cm Branches' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List CmBranch', 'url' => array('index')),
    array('label' => 'Create CmBranch', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cm-branch-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Branches</h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    </span>
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['createANewRecord'], CController::createUrl('create'), array('class' => 'add-new-button')); ?>    </span>
</div>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'cm-branch-grid',
    'afterAjaxUpdate' => CommonSystem::initGridviewSelect2AfterAjax(array('emp_id')),
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'branch_name',
        array(
            "name" => 'emp_id',
            "value" => 'empty($data->emp_id) ? null : $data->manager->emp_first_name',
            "filter" => CommonListData::getListDataBranchManager()
        ),
        'branch_address',
        'branch_telephone',
        'branch_telephone2',
        /*
          'branch_fax',
          'branch_fax2',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
