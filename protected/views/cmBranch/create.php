<?php
/* @var $this CmBranchController */
/* @var $model CmBranch */

$this->breadcrumbs=array(
	'Branches'=>array('index'),
	'Create',
);

$this->menu= SystemSubMenu::setMenuLinks(array(
    'CmBranch'=>array('CmBranchManage'=>array())
));
?>

<h1>Create Branch</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>