<?php
/* @var $this CmBranchController */
/* @var $model CmBranch */

$this->breadcrumbs = array(
    'Branches' => array('index'),
    $model->branch_name,
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'CmBranch' => array(
                'CmBranchCreate' => array(),
                'CmBranchUpdate' => array('url' => array('id' => $model->branch_id)),
                'CmBranchDelete'=>array('linkOptions'=>array('submit'=>array('id'=>$model->branch_id), 'confirm' => $this->deleteMeassage('branch') . $model->branch_name . '?')),
                'CmBranchManage' => array())
        ));
?>

<h1>View Branch <?php echo $model->branch_name; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'branch_name',
        array(
            "name" => 'emp_id',
            "value" => empty($model->emp_id) ? null : $model->manager->emp_first_name,            
        ),
        'branch_address',
        'branch_telephone',
        'branch_telephone2',
        'branch_fax',
        'branch_fax2',
    ),
));
?>
