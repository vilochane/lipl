<?php
/* @var $this CmBranchController */
/* @var $model CmBranch */

$this->breadcrumbs=array(
	'Branches'=>array('index'),
	$model->branch_name=>array('view','id'=>$this->encryptParam($model->branch_id)),
	'Update',
);

$this->menu=  SystemSubMenu::setMenuLinks(array(
            'CmBranch' => array(
                'CmBranchCreate' => array(),
                'CmBranchView' => array('url' => array('id' => $model->branch_id)),
                'CmBranchManage' => array())
        ));
?>

<h1>Update Branch <?php echo $model->branch_name; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>