<?php
/* @var $this CmBranchController */
/* @var $data CmBranch */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->branch_id), array('view', 'id'=>$data->branch_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_name')); ?>:</b>
	<?php echo CHtml::encode($data->branch_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_address')); ?>:</b>
	<?php echo CHtml::encode($data->branch_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_telephone')); ?>:</b>
	<?php echo CHtml::encode($data->branch_telephone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_telephone2')); ?>:</b>
	<?php echo CHtml::encode($data->branch_telephone2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_fax')); ?>:</b>
	<?php echo CHtml::encode($data->branch_fax); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_fax2')); ?>:</b>
	<?php echo CHtml::encode($data->branch_fax2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>