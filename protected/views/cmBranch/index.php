<?php
/* @var $this CmBranchController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Branches',
);

$this->menu=array(
	array('label'=>'Create CmBranch', 'url'=>array('create')),
	array('label'=>'Manage CmBranch', 'url'=>array('admin')),
);
?>

<h1>Cm Branches</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
