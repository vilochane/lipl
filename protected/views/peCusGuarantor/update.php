<?php
/* @var $this PeCusGuarantorController */
/* @var $model PeCusGuarantor */

$this->breadcrumbs=array(
	'Guarantors'=> array('admin', 'c'=>$this->getUrlQuery('c', false)),
	$model->guarantor_first_name=>array('view','id'=>$this->encryptParam($model->guarantor_id), 'c'=>$this->getUrlQuery('c', false)),
	'Update',
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array('PeCustomerManage' => array()),
            'PeCusGuarantor' => array(
                'PeCusGuarantorSeperator' => array(),
                'PeCusGuarantorCreate' => array('url' => array('c' => $this->getUrlQuery('c'))),
                'PeCusGuarantorView' => array('url' => array('id' => $model->guarantor_id, 'c' => $this->getUrlQuery('c'))),
                'PeCusGuarantorDelete' => array('linkOptions' => array('submit' => array('id' => $model->guarantor_id), 'confirm' => CommonSystem::deleteMeassage("guarantor " . $model->guarantor_first_name) . '?')),
                'PeCusGuarantorManage' => array('url' => array('c' => $this->getUrlQuery('c'))),
            )
        ));
?>

<h1>Update Guarantor <?php echo $model->guarantor_first_name; ?> <?php echo CommonListData::getListDataCustomerName($this->getUrlQuery('c'))?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'childModels'=>$childModels)); ?>