<?php
/* @var $this PeCusGuarantorController */
/* @var $model PeCusGuarantor */

$this->breadcrumbs = array(
    'Guarantors' => array('admin', 'c'=>$this->getUrlQuery('c', false)),
    $model->guarantor_first_name,
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array('PeCustomerManage' => array()),
            'PeCusGuarantor' => array(
                'PeCusGuarantorSeperator' => array(),
                'PeCusGuarantorCreate' => array('url' => array('c' => $this->getUrlQuery('c'))),
                'PeCusGuarantorUpdate' => array('url' => array('id' => $model->guarantor_id, 'c' => $this->getUrlQuery('c'))),
                'PeCusGuarantorDelete' => array('linkOptions' => array('submit' => array('id' => $model->guarantor_id), 'confirm' => CommonSystem::deleteMeassage("guarantor " . $model->guarantor_first_name) . '?')),
                'PeCusGuarantorManage' => array('url' => array('c' => $this->getUrlQuery('c'))),
            )
        ));
?>

<h1>View Guarantor <?php echo $model->guarantor_first_name; ?> <?php echo CommonListData::getListDataCustomerName($this->getUrlQuery('c'))?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        array('name' => 'branch_id', 'value' => $model->branch->branch_name),
        array('name' => 'cus_id', 'value' => $model->cus->cus_first_name),
        'guarantor_first_name',
        'guarantor_middle_name',
        'guarantor_last_name',
        'guarantor_initials_name',
        'guarantor_nic',
        'guarantor_slin',
        'guarantor_drivers_license',
        'guarantor_passport_no',
        'guarantor_address',
        'guarantor_official_address',
        array('name' => 'residence_duration_id', 'value' => $model->trade_experience_id === "" ? Null : $model->residenceDuration->residence_duration_name),
        array('name' => 'guarantor_resident_info', 'value' => CommonListData::getListDataResidenceInfoValue($model->guarantor_resident_info)),
        'guarantor_telephone',
        'guarantor_telephone2',
        'guarantor_office_tp',
        'guarantor_office_fax',
        'guarantor_mobile',
        'guarantor_mobile2',
        'guarantor_email',
        'guarantor_qualification',
        array("name" => 'guarantor_organization', "value" => CommonListData::getListDataCusOrganizationValue($model->guarantor_organization)),
        array('name' => 'guarantor_gender', 'value' => CommonListData::getListDataGenderValue($model->guarantor_gender)),
        array('name' => 'guarantor_marital_status', 'value' => CommonListData::getListDataMartalStatusValue($model->guarantor_marital_status)),
        array("name" => 'guarantor_nationality', "value" => CommonListData::getListDataNationalityValue($model->guarantor_nationality)),
        //array('name' => 'trade_experience_id', 'value' => $model->tradeExperience->trade_experience_name),
        //array('name' => 'operation_period_id', 'value' => $model->operationPeriod->operation_period_name),
        'guarantor_bis_reg',
        'guarantor_bis_reg_date',
        'guarantor_bis_nat',
        'guarantor_bis_add',
        array("name" => 'guarantor_bis_prem', "value" => CommonListData::getListDataPremisOwnerShipValue($model->guarantor_organization)),
        array("name" => 'guarantor_bis_emp_no', "value" => $model->setNumberFormat($model->guarantor_bis_emp_no, 'decimal')),
        array("name" => 'guarantor_income', "value" => $model->setNumberFormat($model->guarantor_income)),
    ),
));
?>
<div class="panel panel-default">
    <!--    employment info-->
    <div class="panel-heading">
        <div class="panel-title">Employment Information</div>
    </div>
    <table class="table-bordered">
        <thead>
        <th>Information</th>
        <th>Guarantor</th>
        <th>Spouse</th>        
        </thead>
        <tbody>
            <tr>
                <td>Company Name</td>
                <td>
                    <?php echo $model->guarantor_emp_name; ?>                        
                </td>
                <td>
                    <?php echo $model->guarantor_semp_name; ?>                       
                </td>
            </tr>
            <tr> 
                <td>Address</td>
                <td>
                    <?php echo $model->guarantor_emp_place; ?>   
                </td>
                <td>
                    <?php echo $model->guarantor_semp_place; ?>                       
                </td>
            </tr>
            <tr>
                <td>Designation</td>
                <td>
                    <?php echo CommonListData::getListDataProfessionValue($model->guarantor_emp_pro); ?>                       
                </td>
                <td>
                    <?php echo CommonListData::getListDataProfessionValue($model->guarantor_semp_pro); ?>                                      
                </td>
            </tr>
            <tr>
                <td>Period</td>
                <td>
                    <?php echo CommonListData::getListDataProfessionValue($model->guarantor_emp_period_id); ?> 
                </td>
                <td>
                    <?php echo CommonListData::getListDataProfessionValue($model->guarantor_semp_period_id); ?>            
                </td>
            </tr>
            <tr>
                <td>Status</td>
                <td>
                    <?php echo CommonListData::getListDataEmploymentStatusValue($model->guarantor_emp_status) ?>
                </td>
                <td>
                    <?php echo CommonListData::getListDataEmploymentStatusValue($model->guarantor_semp_status) ?>
                </td>
            </tr>
            <tr>
                <td>Salary</td>
                <td>
                    <?php echo $model->setNumberFormat($model->guarantor_emp_sal); ?>
                </td>
                <td>
                    <?php echo $model->setNumberFormat($model->guarantor_semp_sal); ?>
                </td>
            </tr>            
        </tbody>
    </table>
</div>

<div class="panel panel-default">
    <div class="panel panel-heading">
        <div class="panel-title">Bankers Details</div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'pe-cus-guarantor-bank-details-grid',
        'dataProvider' => $modelBankDetails->search(),
        'filter' => $modelBankDetails,
        'columns' => array(
            'bank_details_bank_name',
            'bank_details_bank_ac_no',
            'bank_details_bank_address',
        ),
    ));
    ?>
</div>

<div class="panel panel-default">
    <div class="panel panel-heading">
        <div class="panel-title">Credit Reference Details</div>
    </div>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'pe-cus-guarantor-income-sources-grid',
        'dataProvider' => $modelIncomeSources->search(),
        'filter' => $modelIncomeSources,
        'columns' => array(
            'income_source_name',
            'income_source_income',
            'income_source_expense',
            'income_source_net',
        ),
    ));
    ?>
</div>
