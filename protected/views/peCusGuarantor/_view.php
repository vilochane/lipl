<?php
/* @var $this PeCusGuarantorController */
/* @var $data PeCusGuarantor */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->guarantor_id), array('view', 'id'=>$data->guarantor_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branch_id')); ?>:</b>
	<?php echo CHtml::encode($data->branch_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cus_id')); ?>:</b>
	<?php echo CHtml::encode($data->cus_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('residence_duration_id')); ?>:</b>
	<?php echo CHtml::encode($data->residence_duration_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_emp_pro')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_emp_pro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_emp_period_id')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_emp_period_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_semp_pro')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_semp_pro); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_semp_period_id')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_semp_period_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trade_experience_id')); ?>:</b>
	<?php echo CHtml::encode($data->trade_experience_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('operation_period_id')); ?>:</b>
	<?php echo CHtml::encode($data->operation_period_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_first_name')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_middle_name')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_middle_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_last_name')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_initials_name')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_initials_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_gender')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_gender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_marital_status')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_marital_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_nationality')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_nationality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_nic')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_nic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_slin')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_slin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_drivers_license')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_drivers_license); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_passport_no')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_passport_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_address')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_official_address')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_official_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_resident_info')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_resident_info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_telephone')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_telephone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_telephone2')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_telephone2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_office_tp')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_office_tp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_office_fax')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_office_fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_mobile2')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_mobile2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_email')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_qualification')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_qualification); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_emp_name')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_emp_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_emp_place')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_emp_place); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_emp_status')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_emp_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_emp_sal')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_emp_sal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_semp_name')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_semp_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_semp_place')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_semp_place); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_semp_status')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_semp_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_semp_sal')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_semp_sal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_organization')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_organization); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_bis_reg')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_bis_reg); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_bis_reg_date')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_bis_reg_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_bis_nat')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_bis_nat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_bis_add')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_bis_add); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_bis_prem')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_bis_prem); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_bis_emp_no')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_bis_emp_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('guarantor_income')); ?>:</b>
	<?php echo CHtml::encode($data->guarantor_income); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>