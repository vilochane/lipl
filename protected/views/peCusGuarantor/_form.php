<?php
/* @var $this PeCusGuarantorController */
/* @var $model PeCusGuarantor */
/* @var $form CActiveForm */
$cs = Yii::app()->clientScript;
$cs->registerCoreScript('yiiactiveform');
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'pe-cus-guarantor-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>
    <div class="panel panel-default">
        <div class="panel-heading"><div class="panel-title">For System Use</div></div>
        <div class="panel-body">
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'branch_id'); ?>
                    </span>		
                    <?php
                    $this->initSelect2(array(
                        'model' => $model,
                        'attribute' => 'branch_id',
                        'data' => CommonListData::getListDataBranches(),
                        'options' => array(
                            'placeholder' => 'Please select a branch.'
                        ),
                    ));
                    ?>
                </div>
                <?php echo $form->error($model, 'branch_id'); ?>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><div class="panel-title">Personal Information</div></div>
        <div class="panel-body">

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_first_name'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_first_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_first_name'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_middle_name'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_middle_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_middle_name'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_last_name'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_last_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_last_name'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_initials_name'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_initials_name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_initials_name'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_gender'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'guarantor_gender', CommonListData::getListDataGender(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_gender'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_marital_status'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'guarantor_marital_status', CommonListData::getListDataMartalStatus(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_marital_status'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_nationality'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'guarantor_nationality', CommonListData::getListDataNationality(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_nationality'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_nic'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_nic', array('class' => 'form-control', 'size' => 10, 'maxlength' => 10, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_nic'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_slin'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_slin', array('class' => 'form-control', 'size' => 13, 'maxlength' => 13, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_slin'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_drivers_license'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_drivers_license', array('class' => 'form-control', 'size' => 7, 'maxlength' => 7, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_drivers_license'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_passport_no'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_passport_no', array('class' => 'form-control', 'size' => 5, 'maxlength' => 5, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_passport_no'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_address'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'guarantor_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_address'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_official_address'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'guarantor_official_address', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_official_address'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'residence_duration_id'); ?>
                    </span>		
                    <?php
                    $this->initSelect2(array(
                        "model" => $model,
                        "attribute" => "residence_duration_id",
                        "data" => CommonListData::getListDataResidenceDuration(),
                        "options" => array('placeholder' => 'Please select a residence duration.')
                    ));
                    ?>
                    <span class="input-group-addon">
                        <?php
                        $this->initDialog(array(
                            'ajaxLink' => 'CmResidenceDuration/AjaxCreate',
                            'dialogId' => 'residenceDurationDialog',
                            'dialogTitle' => 'Create a New Residence Duration',
                            'select2Id' => 'PeCusGuarantor_residence_duration_id'
                        ));
                        ?>
                    </span>
                </div>
                <?php echo $form->error($model, 'residence_duration_id'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_resident_info'); ?>
                    </span>		
                    <?php echo $form->radioButtonList($model, 'guarantor_resident_info', CommonListData::getListDataResidenceInfo(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_resident_info'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_telephone'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "guarantor_telephone")) ?>
                </div>
                <?php echo $form->error($model, 'guarantor_telephone'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_telephone2'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "guarantor_telephone2")) ?>
                </div>
                <?php echo $form->error($model, 'guarantor_telephone2'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_office_tp'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "guarantor_office_tp")) ?>
                </div>
                <?php echo $form->error($model, 'guarantor_office_tp'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_office_fax'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "guarantor_office_fax")) ?>
                </div>
                <?php echo $form->error($model, 'guarantor_office_fax'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_mobile'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "guarantor_mobile")) ?>
                </div>
                <?php echo $form->error($model, 'guarantor_mobile'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_mobile2'); ?>
                    </span>		
                    <?php $this->initTelephoneField(array("model" => $model, "attribute" => "guarantor_mobile2")) ?>
                </div>
                <?php echo $form->error($model, 'guarantor_mobile2'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_email'); ?>
                    </span>		
                    <?php echo $form->textField($model, 'guarantor_email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_email'); ?>
            </div>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model, 'guarantor_qualification'); ?>
                    </span>		
                    <?php echo $form->textArea($model, 'guarantor_qualification', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                </div>
                <?php echo $form->error($model, 'guarantor_qualification'); ?>
            </div>

        </div>
    </div>

    <!--    employment info-->
    <fieldset> <caption>Employment Information</caption>
        <table class="table-bordered">
            <thead>
            <th>Information</th>
            <th>Guarantor</th>
            <th>Spouse</th>        
            </thead>
            <tbody>
                <tr>
                    <td>Company Name</td>
                    <td>
                        <?php echo $form->textField($model, 'guarantor_emp_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'guarantor_emp_name'); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($model, 'guarantor_semp_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'guarantor_semp_name'); ?>
                    </td>
                </tr>
                <tr> 
                    <td>Address</td>
                    <td>
                        <?php echo $form->textArea($model, 'guarantor_emp_place', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>                        
                        <?php echo $form->error($model, 'guarantor_emp_place'); ?>
                    </td>
                    <td>
                        <?php echo $form->textArea($model, 'guarantor_semp_place', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'guarantor_semp_place'); ?>
                    </td>
                </tr>
                <tr>
                    <td>Designation</td>
                    <td>
                        <div class="row">
                            <div class="input-group">            		
                                <?php
                                $this->initSelect2(array(
                                    "model" => $model,
                                    "attribute" => "guarantor_emp_pro",
                                    "data" => CommonListData::getListDataProfession(),
                                    "htmlOptions" => array(
                                        "placeholder" => "Please select a profession."
                                    )
                                ));
                                ?>
                                <span class="input-group-addon">
                                    <?php
                                    $this->initDialog(array(
                                        'ajaxLink' => 'CmProfession/AjaxCreate',
                                        'dialogId' => 'empProfessionDialog',
                                        'dialogTitle' => 'Create a New Profession',
                                        'select2Id' => 'PeCusGuarantor_guarantor_emp_pro'
                                    ));
                                    ?>
                                </span>
                            </div>
                            <?php echo $form->error($model, 'guarantor_emp_pro'); ?>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="input-group">
                                <?php
                                $this->initSelect2(array(
                                    "model" => $model,
                                    "attribute" => "guarantor_semp_pro",
                                    "data" => CommonListData::getListDataProfession(),
                                    "htmlOptions" => array(
                                        "placeholder" => "Please select a profession."
                                    )
                                ));
                                ?>
                                <span class="input-group-addon">
                                    <?php
                                    $this->initDialog(array(
                                        'ajaxLink' => 'CmProfession/AjaxCreate',
                                        'dialogId' => 'sempProfessionDialog',
                                        'dialogTitle' => 'Create a New Profession',
                                        'select2Id' => 'PeCusGuarantor_guarantor_semp_pro'
                                    ));
                                    ?>
                                </span>
                            </div>
                            <?php echo $form->error($model, 'guarantor_semp_pro'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Period</td>
                    <td>
                        <div class="row">
                            <div class="input-group">
                                <?php
                                $this->initSelect2(array(
                                    "model" => $model,
                                    "attribute" => "guarantor_emp_period_id",
                                    "data" => CommonListData::getListDataEmpPeriod(),
                                    "htmlOptions" => array(
                                        "placeholder" => "Please select a period."
                                    )
                                ));
                                ?>
                                <span class="input-group-addon">
                                    <?php
                                    $this->initDialog(array(
                                        'ajaxLink' => 'CmEmpPeriod/AjaxCreate',
                                        'dialogId' => 'empPeriodDialog',
                                        'dialogTitle' => 'Create a New Period',
                                        'select2Id' => 'PeCusGuarantor_guarantor_emp_period_id'
                                    ));
                                    ?>
                                </span>
                            </div>
                            <?php echo $form->error($model, 'guarantor_emp_period_id'); ?>
                        </div>
                    </td>
                    <td>
                        <div class="row">
                            <div class="input-group">
                                <?php
                                $this->initSelect2(array(
                                    "model" => $model,
                                    "attribute" => "guarantor_semp_period_id",
                                    "data" => CommonListData::getListDataEmpPeriod(),
                                    "htmlOptions" => array(
                                        "placeholder" => "Please select a period."
                                    )
                                ));
                                ?>
                                <span class="input-group-addon">
                                    <?php
                                    $this->initDialog(array(
                                        'ajaxLink' => 'CmEmpPeriod/AjaxCreate',
                                        'dialogId' => 'sempPeriodDialog',
                                        'dialogTitle' => 'Create a New Period',
                                        'select2Id' => 'PeCusGuarantor_guarantor_semp_period_id'
                                    ));
                                    ?>
                                </span>
                            </div>
                            <?php echo $form->error($model, 'guarantor_semp_period_id'); ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                        <?php echo $form->radioButtonList($model, 'guarantor_emp_status', CommonListData::getListDataEmploymentStatus(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'guarantor_emp_status'); ?>
                    </td>
                    <td>
                        <?php echo $form->radioButtonList($model, 'guarantor_semp_status', CommonListData::getListDataEmploymentStatus(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
                        <?php echo $form->error($model, 'guarantor_semp_status'); ?>
                    </td>
                </tr>
                <tr>
                    <td>Salary</td>
                    <td>
                        <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'guarantor_emp_sal')); ?>    
                        <?php echo $form->error($model, 'guarantor_emp_sal'); ?>   
                    </td>
                    <td>
                        <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'guarantor_semp_sal')); ?>
                        <?php echo $form->error($model, 'guarantor_semp_sal'); ?>
                    </td>
                </tr>            
            </tbody>
        </table>
    </fieldset>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'trade_experience_id'); ?>
            </span>		
            <?php
            $this->initSelect2(array(
                "model" => $model,
                "attribute" => "trade_experience_id",
                "data" => CommonListData::getListDataTradeExperience(),
                "htmlOptions" => array(
                    "placeholder" => "Please select a trade experience."
                )
            ));
            ?>
            <span class="input-group-addon">
                <?php
                $this->initDialog(array(
                    'ajaxLink' => 'CmTradeExperience/AjaxCreate',
                    'dialogId' => 'tradeExperienceDialog',
                    'dialogTitle' => 'Create a New Trade Experience',
                    'select2Id' => 'PeCusGuarantor_trade_experience_id'
                ));
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'trade_experience_id'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'guarantor_organization'); ?>
            </span>		
            <?php echo $form->radioButtonList($model, 'guarantor_organization', CommonListData::getListDataCusOrganization(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'guarantor_organization'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'guarantor_bis_reg'); ?>
            </span>		
            <?php echo $form->textField($model, 'guarantor_bis_reg', array('class' => 'form-control', 'size' => 10, 'maxlength' => 10, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'guarantor_bis_reg'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'guarantor_bis_reg_date'); ?>
            </span>		
            <?php
            $this->initDatePicker(array(
                "model" => $model,
                "attribute" => "guarantor_bis_reg_date"
            ));
            ?>  
        </div>
        <?php echo $form->error($model, 'guarantor_bis_reg_date'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'guarantor_bis_nat'); ?>
            </span>		
            <?php echo $form->textArea($model, 'guarantor_bis_nat', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'guarantor_bis_nat'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'guarantor_bis_add'); ?>
            </span>		
            <?php echo $form->textArea($model, 'guarantor_bis_add', array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'guarantor_bis_add'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'operation_period_id'); ?>
            </span>		
            <?php
            $this->initSelect2(array(
                "model" => $model,
                "attribute" => "operation_period_id",
                "data" => CommonListData::getListDataOperationPeriod(),
                "htmlOptions" => array(
                    "placeholder" => "Please select a operation period."
                )
            ));
            ?>
            <span class="input-group-addon">
                <?php
                $this->initDialog(array(
                    'ajaxLink' => 'CmOperationPeriod/AjaxCreate',
                    'dialogId' => 'operationDialog',
                    'dialogTitle' => 'Create a New Opetation Period',
                    'select2Id' => 'PeCusGuarantor_operation_period_id'
                ));
                ?>
            </span>
        </div>
        <?php echo $form->error($model, 'operation_period_id'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'guarantor_bis_prem'); ?>
            </span>		
            <?php echo $form->radioButtonList($model, 'guarantor_organization', CommonListData::getListDataPremisOwnerShip(), array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'guarantor_bis_prem'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'guarantor_bis_emp_no'); ?>
            </span>		
            <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'guarantor_bis_emp_no')); ?>
        </div>
        <?php echo $form->error($model, 'guarantor_bis_emp_no'); ?>
    </div>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'guarantor_income'); ?>
            </span>		
            <?php $this->initFormatCurrency(array('model' => $model, 'attribute' => 'guarantor_income')); ?>
        </div>
        <?php echo $form->error($model, 'guarantor_income'); ?>
    </div>
    <div class="row">
        <fieldset> <caption>Bankers Details</caption><?php $this->renderPartial("__from-bank-details-table", array("form" => $form, "childModels" => $childModels)); ?>
        </fieldset>
    </div>

    <div class="row">
        <fieldset> <caption>Credit Reference Details</caption><?php $this->renderPartial("__from-income-sources-table", array("form" => $form, "childModels" => $childModels)); ?>
        </fieldset>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-default')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div><!-- form -->
<?php
$cs->registerScript("guarantor-form-script", ""        
        . "var bankDetailsRow = new String(" . CJSON::encode($this->renderPartial("__from-bank-details-rows", array("rowIndex" => "count", "form" => $form, "model" => new PeCusGuarantorBankDetails), true, false)) . ");"
        . "$('#add-bank-details-btn').click(function(){"
        . "var rowCount = $('#bank-details-table tbody tr').length;"
        . "var rowNo = ++rowCount;"
        . "$('#bank-details-table tbody').append('<tr>"
        . "<td><span>'+rowNo+'</span></td>"
        . "'+bankDetailsRow.replace(/count/g, rowCount)+'"
        . "<td>" . CommonSystem::getTabualrButtonLink("insert") . "</td>"
        . "</tr>');"
        . "});"
        . ""
        . "var creditRefDetailsRow = new String(" . CJSON::encode($this->renderPartial("__from-income-sources-rows", array("rowIndex" => "count", "form" => $form, "model" => new PeCusGuarantorIncomeSources), true, false)) . ");"
        . "$('#add-income-sources-btn').click(function(){"
        . "var rowCount = $('#income-sources-table tbody tr').length;"
        . "var rowNo = ++rowCount;"
        . "$('#income-sources-table tbody').append('<tr>"
        . "<td><span>'+rowNo+'</span></td>"
        . "'+creditRefDetailsRow.replace(/count/g, rowCount)+'"
        . "<td>" . CommonSystem::getTabualrButtonLink("insert") . "</td>"
        . "</tr>');"
        . "});"
        /* disabling browser refresh */
        . "$(document).bind('keydown', disableBrowserRefresh);"
        . "", CClientScript::POS_READY);
?>