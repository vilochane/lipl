<?php
/* @var $this PeCusGuarantorController */
/* @var $model PeCusGuarantor */

$this->breadcrumbs = array(
    'Guarantors' => array('admin', 'c'=>$this->getUrlQuery('c', false)),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pe-cus-guarantor-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Guarantors <?php echo CommonListData::getListDataCustomerName($this->getUrlQuery('c'))?></h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    </span>
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['createANewRecord'], CController::createUrl('create', array('c' => $this->getUrlQuery('c', false))), array('class' => 'add-new-button')); ?>    </span>
</div>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'pe-cus-guarantor-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array('name' => 'branch_id', 'value' => '$data->branch->branch_name'),
//        array('name' => 'cus_id', 'value' => '$data->cus->cus_first_name'),        
        'residence_duration_id',
        'guarantor_first_name',
        'guarantor_middle_name',
        'guarantor_last_name',
        'guarantor_initials_name',
        'guarantor_nic',
        'guarantor_slin',
        'guarantor_drivers_license',
        'guarantor_passport_no',
        'guarantor_address',
        'guarantor_official_address',
        /*
          'guarantor_semp_pro',
          'guarantor_semp_period_id',
          'trade_experience_id',
          'operation_period_id',
          'guarantor_first_name',
          'guarantor_middle_name',
          'guarantor_last_name',
          'guarantor_initials_name',
          'guarantor_gender',
          'guarantor_marital_status',
          'guarantor_nationality',
          'guarantor_nic',
          'guarantor_slin',
          'guarantor_drivers_license',
          'guarantor_passport_no',
          'guarantor_address',
          'guarantor_official_address',
          'guarantor_resident_info',
          'guarantor_telephone',
          'guarantor_telephone2',
          'guarantor_office_tp',
          'guarantor_office_fax',
          'guarantor_mobile',
          'guarantor_mobile2',
          'guarantor_email',
          'guarantor_qualification',
          'guarantor_emp_name',
          'guarantor_emp_place',
          'guarantor_emp_status',
          'guarantor_emp_sal',
          'guarantor_semp_name',
          'guarantor_semp_place',
          'guarantor_semp_status',
          'guarantor_semp_sal',
          'guarantor_organization',
          'guarantor_bis_reg',
          'guarantor_bis_reg_date',
          'guarantor_bis_nat',
          'guarantor_bis_add',
          'guarantor_bis_prem',
          'guarantor_bis_emp_no',
          'guarantor_income',
          'updated_by',
          'updated_date',
         */
        array(
            'class' => 'CButtonColumn',
            'template' => "{view}{update}{delete}",
            'buttons' => array(
                'view' => array(
                    'url' => 'Yii::app()->createUrl("PeCusGuarantor/view", array("id"=>$data->encryptParam($data->guarantor_id), "c"=>$this->getUrlQuery("c",false)))'
                ),
                'update' => array(
                    'url' => 'Yii::app()->createUrl("PeCusGuarantor/update", array("id"=>$data->encryptParam($data->guarantor_id), "c"=>$this->getUrlQuery("c",false)))'
                )
            )
        ),
    ),
));
?>
