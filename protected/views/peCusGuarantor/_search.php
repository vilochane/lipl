<?php
/* @var $this PeCusGuarantorController */
/* @var $model PeCusGuarantor */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'branch_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'branch_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'cus_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'cus_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'residence_duration_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'residence_duration_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_emp_pro'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_emp_pro',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_emp_period_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_emp_period_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_semp_pro'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_semp_pro',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_semp_period_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_semp_period_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'trade_experience_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'trade_experience_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'operation_period_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'operation_period_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_first_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_first_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_middle_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_middle_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_last_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_last_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_initials_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_initials_name',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_gender'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_gender', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_marital_status'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_marital_status', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_nationality'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_nationality', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_nic'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_nic',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_slin'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_slin',array('class'=>'form-control', 'size'=>13,'maxlength'=>13, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_drivers_license'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_drivers_license',array('class'=>'form-control', 'size'=>7,'maxlength'=>7, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_passport_no'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_passport_no',array('class'=>'form-control', 'size'=>5,'maxlength'=>5, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_address'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_official_address'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_official_address',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_resident_info'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_resident_info', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_telephone'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_telephone',array('class'=>'form-control', 'size'=>15,'maxlength'=>15, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_telephone2'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_telephone2',array('class'=>'form-control', 'size'=>15,'maxlength'=>15, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_office_tp'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_office_tp',array('class'=>'form-control', 'size'=>15,'maxlength'=>15, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_office_fax'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_office_fax',array('class'=>'form-control', 'size'=>15,'maxlength'=>15, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_mobile'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_mobile',array('class'=>'form-control', 'size'=>15,'maxlength'=>15, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_mobile2'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_mobile2',array('class'=>'form-control', 'size'=>15,'maxlength'=>15, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_email'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_email',array('class'=>'form-control', 'size'=>60,'maxlength'=>150, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_qualification'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_qualification',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_emp_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_emp_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_emp_place'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_emp_place',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_emp_status'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_emp_status', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_emp_sal'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_emp_sal',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_semp_name'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_semp_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_semp_place'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_semp_place',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_semp_status'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_semp_status', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_semp_sal'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_semp_sal',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_organization'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_organization', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_bis_reg'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_bis_reg',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_bis_reg_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_bis_reg_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_bis_nat'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_bis_nat',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_bis_add'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_bis_add',array('class'=>'form-control', 'size'=>60,'maxlength'=>200, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_bis_prem'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_bis_prem', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_bis_emp_no'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_bis_emp_no', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'guarantor_income'); ?>
                    </span>
                        <?php echo $form->textField($model,'guarantor_income',array('class'=>'form-control', 'size'=>20,'maxlength'=>20, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_by'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
        <div class="row buttons">
        <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->