<table id="income-sources-table" class="table-bordered">
    <thead>
        <tr>
            <th style="width: 2%;">#</th>
            <th style="width: 20%;">Income Source</th>
            <th style="width: 10%;">Income</th>
            <th style="width: 10%;">Expenses</th>
            <th style="width: 10%;">Net Income</th>
            <th style="width: 2%;"><button id="add-income-sources-btn" title="Add a new row." type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span></button></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($childModels["PeCusGuarantorIncomeSources"]) > 0) {
            foreach ($childModels["PeCusGuarantorIncomeSources"] as $index => $modelObj) {
                ?>  
                <tr>
                    <td><span><?php echo $index + 1; ?></span></td>
                    <?php $this->renderPartial("__from-income-sources-rows", array("rowIndex" => $index, "form" => $form, "model" => $modelObj)); ?>
                    <td><?php echo CommonSystem::getTabualrButtonLink($modelObj->getScenario(), "PeCusGuarantorIncomeSources/delete", $modelObj->income_source_id); ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table> 