<table id="bank-details-table" class="table-bordered">
    <thead>
        <tr>
            <th style="width: 2%;">#</th>
            <th style="width: 10%;">Bank</th>
            <th style="width: 10%;">Account No.</th>
            <th style="width: 20%;">Address</th>
            <th style="width: 2%;"><button id="add-bank-details-btn" title="Add a new row." type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span></button></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (count($childModels["PeCusGuarantorBankDetails"]) > 0) {
            foreach ($childModels["PeCusGuarantorBankDetails"] as $index => $modelObj) {
                ?>  
                <tr>
                    <td><span><?php echo $index + 1; ?></span></td>
                    <?php $this->renderPartial("__from-bank-details-rows", array("rowIndex" => $index, "form" => $form, "model" => $modelObj)); ?>
                    <td><?php echo CommonSystem::getTabualrButtonLink($modelObj->getScenario(), "PeCusGuarantorBankDetails/delete", $modelObj->bank_details_id); ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table> 