<td>
    <?php echo $form->hiddenField($model, "[$rowIndex]income_source_id"); ?>
    <?php echo $form->hiddenField($model, "[$rowIndex]guarantor_id"); ?>
    <?php echo $form->hiddenField($model, "[$rowIndex]modelScenario", array("value" => $model->getScenario())); ?>
    <?php echo $form->textField($model, "[$rowIndex]income_source_name", array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>   
    <?php echo $form->error($model, "[$rowIndex]income_source_name"); ?>
</td>

<td>
    <?php echo $form->textField($model, "[$rowIndex]income_source_income", array('class' => 'form-control', 'size' => 15, 'maxlength' => 15, 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, "[$rowIndex]income_source_income"); ?>
</td>


<td>
    <?php echo $form->textField($model, "[$rowIndex]income_source_expense", array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, "[$rowIndex]income_source_expense"); ?>
</td>
<td>
    <?php echo $form->textField($model, "[$rowIndex]income_source_net", array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, "[$rowIndex]income_source_net"); ?>
</td>


