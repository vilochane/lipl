<?php
/* @var $this PeCusGuarantorController */
/* @var $model PeCusGuarantor */

$this->breadcrumbs = array(
    'Guarantors' =>  array('admin', 'c'=>$this->getUrlQuery('c', false)),
    'Create',
);

$this->menu = SystemSubMenu::setMenuLinks(array(
            'PeCustomer' => array('PeCustomerManage' => array()),
            'PeCusGuarantor' => array('PeCusGuarantorSeperator' => array(), 'PeCusGuarantorManage' => array('url' => array('c' => $this->getUrlQuery('c')))),
        ));
?>

<h1>Create Guarantor <?php echo CommonListData::getListDataCustomerName($this->getUrlQuery('c')) ?></h1>

<?php $this->renderPartial('_form', array('model' => $model, 'childModels' => $childModels)); ?>