<td>
    <?php echo $form->hiddenField($model, "[$rowIndex]bank_details_id"); ?>
    <?php echo $form->hiddenField($model, "[$rowIndex]guarantor_id"); ?>
    <?php echo $form->hiddenField($model, "[$rowIndex]modelScenario", array("value" => $model->getScenario())); ?>
    <?php echo $form->textField($model, "[$rowIndex]bank_details_bank_name", array('class' => 'form-control', 'maxlength' => 50, 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, "[$rowIndex]bank_details_bank_name"); ?>    
</td>

<td>
    <?php echo $form->textField($model, "[$rowIndex]bank_details_bank_ac_no", array('class' => 'form-control', 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, "[$rowIndex]bank_details_bank_ac_no"); ?>
</td>
<td>
    <?php echo $form->textArea($model, "[$rowIndex]bank_details_bank_address", array('class' => 'form-control', 'size' => 60, 'maxlength' => 200, 'autocomplete' => 'off')); ?>
    <?php echo $form->error($model, "[$rowIndex]bank_details_bank_address"); ?>
</td>
