<?php
/* @var $this PeCusGuarantorController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pe Cus Guarantors',
);

$this->menu=array(
	array('label'=>'Create PeCusGuarantor', 'url'=>array('create')),
	array('label'=>'Manage PeCusGuarantor', 'url'=>array('admin')),
);
?>

<h1>Pe Cus Guarantors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
