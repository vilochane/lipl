<?php
/* @var $this PeCusBankDetailsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pe Cus Bank Details',
);

$this->menu=array(
	array('label'=>'Create PeCusBankDetails', 'url'=>array('create')),
	array('label'=>'Manage PeCusBankDetails', 'url'=>array('admin')),
);
?>

<h1>Pe Cus Bank Details</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
