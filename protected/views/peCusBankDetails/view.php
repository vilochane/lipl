<?php
/* @var $this PeCusBankDetailsController */
/* @var $model PeCusBankDetails */

$this->breadcrumbs=array(
	'Pe Cus Bank Details'=>array('index'),
	$model->bank_details_id,
);

$this->menu=array(
	array('label'=>'List PeCusBankDetails', 'url'=>array('index')),
	array('label'=>'Create PeCusBankDetails', 'url'=>array('create')),
	array('label'=>'Update PeCusBankDetails', 'url'=>array('update', 'id'=>$model->bank_details_id)),
	array('label'=>'Delete PeCusBankDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->bank_details_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PeCusBankDetails', 'url'=>array('admin')),
);
?>

<h1>View PeCusBankDetails <?php echo $model->bank_details_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'bank_details_id',
		'cus_id',
		'bank_details_bank_name',
		'bank_details_bank_account_no',
		'bank_details_bank_address',
		'updated_by',
		'updated_date',
	),
)); ?>
