<?php
/* @var $this PeCusBankDetailsController */
/* @var $model PeCusBankDetails */

$this->breadcrumbs=array(
	'Pe Cus Bank Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PeCusBankDetails', 'url'=>array('index')),
	array('label'=>'Manage PeCusBankDetails', 'url'=>array('admin')),
);
?>

<h1>Create PeCusBankDetails</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>