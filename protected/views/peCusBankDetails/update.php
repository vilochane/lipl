<?php
/* @var $this PeCusBankDetailsController */
/* @var $model PeCusBankDetails */

$this->breadcrumbs=array(
	'Pe Cus Bank Details'=>array('index'),
	$model->bank_details_id=>array('view','id'=>$this->encryptParam($model->bank_details_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List PeCusBankDetails', 'url'=>array('index')),
	array('label'=>'Create PeCusBankDetails', 'url'=>array('create')),
	array('label'=>'View PeCusBankDetails', 'url'=>array('view', 'id'=>$model->bank_details_id)),
	array('label'=>'Manage PeCusBankDetails', 'url'=>array('admin')),
);
?>

<h1>Update PeCusBankDetails <?php echo $model->bank_details_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>