<?php
/* @var $this CmVehicleModelController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Vehicle Models',
);

$this->menu=array(
	array('label'=>'Create CmVehicleModel', 'url'=>array('create')),
	array('label'=>'Manage CmVehicleModel', 'url'=>array('admin')),
);
?>

<h1>Cm Vehicle Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
