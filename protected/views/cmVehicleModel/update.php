<?php
/* @var $this CmVehicleModelController */
/* @var $model CmVehicleModel */

$this->breadcrumbs=array(
	'Cm Vehicle Models'=>array('index'),
	$model->vehicle_model_id=>array('view','id'=>$this->encryptParam($model->vehicle_model_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmVehicleModel', 'url'=>array('index')),
	array('label'=>'Create CmVehicleModel', 'url'=>array('create')),
	array('label'=>'View CmVehicleModel', 'url'=>array('view', 'id'=>$model->vehicle_model_id)),
	array('label'=>'Manage CmVehicleModel', 'url'=>array('admin')),
);
?>

<h1>Update CmVehicleModel <?php echo $model->vehicle_model_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>