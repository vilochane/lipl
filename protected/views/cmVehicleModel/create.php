<?php
/* @var $this CmVehicleModelController */
/* @var $model CmVehicleModel */

$this->breadcrumbs=array(
	'Cm Vehicle Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmVehicleModel', 'url'=>array('index')),
	array('label'=>'Manage CmVehicleModel', 'url'=>array('admin')),
);
?>

<h1>Create CmVehicleModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>