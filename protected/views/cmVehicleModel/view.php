<?php
/* @var $this CmVehicleModelController */
/* @var $model CmVehicleModel */

$this->breadcrumbs=array(
	'Cm Vehicle Models'=>array('index'),
	$model->vehicle_model_id,
);

$this->menu=array(
	array('label'=>'List CmVehicleModel', 'url'=>array('index')),
	array('label'=>'Create CmVehicleModel', 'url'=>array('create')),
	array('label'=>'Update CmVehicleModel', 'url'=>array('update', 'id'=>$model->vehicle_model_id)),
	array('label'=>'Delete CmVehicleModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->vehicle_model_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmVehicleModel', 'url'=>array('admin')),
);
?>

<h1>View CmVehicleModel <?php echo $model->vehicle_model_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'vehicle_model_id',
		'vehicle_make_id',
		'vehicle_model_name',
		'vehicle_model_status',
		'updated_by',
		'updated_date',
	),
)); ?>
