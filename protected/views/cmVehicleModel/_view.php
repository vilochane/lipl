<?php
/* @var $this CmVehicleModelController */
/* @var $data CmVehicleModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_model_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->vehicle_model_id), array('view', 'id'=>$data->vehicle_model_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_make_id')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_make_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_model_name')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_model_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('vehicle_model_status')); ?>:</b>
	<?php echo CHtml::encode($data->vehicle_model_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>