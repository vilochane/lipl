<?php
/* @var $this CmVehicleModelController */
/* @var $model CmVehicleModel */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-vehicle-model-form',
        'enableAjaxValidation' => true,
    ));
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <span class="show-error-summary"></span>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'vehicle_make_id'); ?>
            </span>		
            <?php
            $this->initSelect2(array(
                "model" => $model,
                "attribute" => "vehicle_make_id",
                "data" => CommonListData::getListDataVehicleMake(),
                "htmlOptions" => array(
                    "placeholder" => "Please select a make."
                )
            ));
            ?>
        </div>
        <?php echo $form->error($model, 'vehicle_make_id'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'vehicle_model_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'vehicle_model_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'vehicle_model_name'); ?>
    </div>
    <div class="row buttons">
        <?php 
        $other = 'if(response.makeId !== ""){ $("#HipVehichleDetails_vehicle_make_id").select2().select2("val", response.makeId); }';
        CommonSystem::getAjaxSubmitButton(CController::createUrl('AjaxCreate'), $dialogId, $select2Id, $other); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->