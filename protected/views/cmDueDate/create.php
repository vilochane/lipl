<?php
/* @var $this CmDueDateController */
/* @var $model CmDueDate */

$this->breadcrumbs=array(
	'Due Dates'=>array('index'),
	'Create',
);

$this->menu = SystemSubMenu::setMenuLinks(
                array(
                    'CmDueDate' => array(
                        'CmDueDateManage' => array(),
                    )
        ));
?>

<h1>Create Due Date</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>