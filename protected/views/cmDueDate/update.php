<?php
/* @var $this CmDueDateController */
/* @var $model CmDueDate */

$this->breadcrumbs = array(
    'Due Dates' => array('index'),
    $model->due_date_date => array('view', 'id' => $this->encryptParam($model->due_date_id)),
    'Update',
);

$this->menu = SystemSubMenu::setMenuLinks(
                array(
                    'CmDueDate' => array(
                        'CmDueDateCreate' => array('label' => 'Create a Due Date', 'url' => array('create')),
                        'CmDueDateUpdate' => array('label' => 'Update Due Date', 'url' => array('id' => $model->due_date_id)),
                        'CmDueDateDelete' => array('linkOptions' => array('submit' => array('id' => $model->due_date_id), 'confirm' => $this->deleteMeassage('due date') . $model->due_date_date . '?')),
                        'CmDueDateManage' => array(),
                    )
        ));
?>

<h1>Update Due Date <?php echo $model->due_date_date; ?></h1>

<?php $this->renderPartial('_form', array('model' => $model)); ?>