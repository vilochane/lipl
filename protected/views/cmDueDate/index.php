<?php
/* @var $this CmDueDateController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Due Dates',
);

$this->menu=array(
	array('label'=>'Create CmDueDate', 'url'=>array('create')),
	array('label'=>'Manage CmDueDate', 'url'=>array('admin')),
);
?>

<h1>Cm Due Dates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
