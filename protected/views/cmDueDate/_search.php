<?php
/* @var $this CmDueDateController */
/* @var $model CmDueDate */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'due_date_id'); ?>
                    </span>
                        <?php echo $form->textField($model,'due_date_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'due_date_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'due_date_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'due_date_remark'); ?>
                    </span>
                        <?php echo $form->textField($model,'due_date_remark',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_by'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            </div>
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->label($model,'updated_date'); ?>
                    </span>
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            </div>
        <div class="row buttons">
        <?php echo CHtml::submitButton('Search', array('class'=>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->