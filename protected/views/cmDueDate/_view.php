<?php
/* @var $this CmDueDateController */
/* @var $data CmDueDate */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('due_date_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->due_date_id), array('view', 'id'=>$data->due_date_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('due_date_date')); ?>:</b>
	<?php echo CHtml::encode($data->due_date_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('due_date_remark')); ?>:</b>
	<?php echo CHtml::encode($data->due_date_remark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>