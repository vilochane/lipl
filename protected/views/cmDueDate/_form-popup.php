<?php
/* @var $this CmDueDateController */
/* @var $model CmDueDate */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-due-date-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <span class="show-error-summary"></span>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'due_date_date'); ?>
            </span>		
            <?php echo $form->textField($model, 'due_date_date', array('class' => 'form-control', 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'due_date_date'); ?>
    </div>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'due_date_remark'); ?>
            </span>		
            <?php echo $form->textArea($model, 'due_date_remark', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'due_date_remark'); ?>
    </div>
    <div class="row buttons">
        <?php CommonSystem::getAjaxSubmitButton(CController::createUrl("AjaxCreate"), $dialogId, $select2Id); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->