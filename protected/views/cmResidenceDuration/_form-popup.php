<?php
/* @var $this CmResidenceDurationController */
/* @var $model CmResidenceDuration */
/* @var $form CActiveForm */
?>
<div class="form">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-residence-duration-form',
        'enableAjaxValidation' => true,
    ));
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <span class="show-error-summary"></span>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'residence_duration_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'residence_duration_name', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'residence_duration_name'); ?>
    </div>
    <div class="row buttons">
        <?php echo CommonSystem::getAjaxSubmitButton(CController::createUrl("AjaxCreate"), $dialogId, $select2Id); ?>    
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->