<?php
/* @var $this CmResidenceDurationController */
/* @var $model CmResidenceDuration */

$this->breadcrumbs=array(
	'Cm Residence Durations'=>array('index'),
	$model->residence_duration_id=>array('view','id'=>$this->encryptParam($model->residence_duration_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmResidenceDuration', 'url'=>array('index')),
	array('label'=>'Create CmResidenceDuration', 'url'=>array('create')),
	array('label'=>'View CmResidenceDuration', 'url'=>array('view', 'id'=>$model->residence_duration_id)),
	array('label'=>'Manage CmResidenceDuration', 'url'=>array('admin')),
);
?>

<h1>Update CmResidenceDuration <?php echo $model->residence_duration_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>