<?php
/* @var $this CmResidenceDurationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Residence Durations',
);

$this->menu=array(
	array('label'=>'Create CmResidenceDuration', 'url'=>array('create')),
	array('label'=>'Manage CmResidenceDuration', 'url'=>array('admin')),
);
?>

<h1>Cm Residence Durations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
