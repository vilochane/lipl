<?php
/* @var $this CmResidenceDurationController */
/* @var $model CmResidenceDuration */

$this->breadcrumbs=array(
	'Cm Residence Durations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmResidenceDuration', 'url'=>array('index')),
	array('label'=>'Manage CmResidenceDuration', 'url'=>array('admin')),
);
?>

<h1>Create CmResidenceDuration</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>