<?php
/* @var $this CmResidenceDurationController */
/* @var $model CmResidenceDuration */

$this->breadcrumbs=array(
	'Cm Residence Durations'=>array('index'),
	$model->residence_duration_id,
);

$this->menu=array(
	array('label'=>'List CmResidenceDuration', 'url'=>array('index')),
	array('label'=>'Create CmResidenceDuration', 'url'=>array('create')),
	array('label'=>'Update CmResidenceDuration', 'url'=>array('update', 'id'=>$model->residence_duration_id)),
	array('label'=>'Delete CmResidenceDuration', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->residence_duration_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmResidenceDuration', 'url'=>array('admin')),
);
?>

<h1>View CmResidenceDuration <?php echo $model->residence_duration_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'residence_duration_id',
		'residence_duration_name',
		'updated_by',
		'updated_date',
	),
)); ?>
