<?php
/* @var $this CmResidenceDurationController */
/* @var $data CmResidenceDuration */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('residence_duration_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->residence_duration_id), array('view', 'id'=>$data->residence_duration_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('residence_duration_name')); ?>:</b>
	<?php echo CHtml::encode($data->residence_duration_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>