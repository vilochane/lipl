<?php
/* @var $this CmResidenceDurationController */
/* @var $model CmResidenceDuration */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cm-residence-duration-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

      <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'residence_duration_name'); ?>
                    </span>		
                        <?php echo $form->textField($model,'residence_duration_name',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'residence_duration_name'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'updated_by'); ?>
                    </span>		
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'updated_by'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'updated_date'); ?>
                    </span>		
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'updated_date'); ?>
          </div>

            <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save' , array('class' =>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->