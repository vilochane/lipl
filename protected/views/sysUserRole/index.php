<?php
/* @var $this SysUserRoleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sys User Roles',
);

$this->menu=array(
	array('label'=>'Create SysUserRole', 'url'=>array('create')),
	array('label'=>'Manage SysUserRole', 'url'=>array('admin')),
);
?>

<h1>Sys User Roles</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
