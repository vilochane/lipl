<?php
/* @var $this SysUserRoleController */
/* @var $model SysUserRole */

$this->breadcrumbs=array(
	'Sys User Roles'=>array('index'),
	$model->user_role_id=>array('view','id'=>$this->encryptParam($model->user_role_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List SysUserRole', 'url'=>array('index')),
	array('label'=>'Create SysUserRole', 'url'=>array('create')),
	array('label'=>'View SysUserRole', 'url'=>array('view', 'id'=>$model->user_role_id)),
	array('label'=>'Manage SysUserRole', 'url'=>array('admin')),
);
?>

<h1>Update SysUserRole <?php echo $model->user_role_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>