<?php
/* @var $this SysUserRoleController */
/* @var $model SysUserRole */

$this->breadcrumbs=array(
	'Sys User Roles'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SysUserRole', 'url'=>array('index')),
	array('label'=>'Manage SysUserRole', 'url'=>array('admin')),
);
?>

<h1>Create SysUserRole</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>