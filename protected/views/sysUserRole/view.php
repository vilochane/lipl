<?php
/* @var $this SysUserRoleController */
/* @var $model SysUserRole */

$this->breadcrumbs=array(
	'Sys User Roles'=>array('index'),
	$model->user_role_id,
);

$this->menu=array(
	array('label'=>'List SysUserRole', 'url'=>array('index')),
	array('label'=>'Create SysUserRole', 'url'=>array('create')),
	array('label'=>'Update SysUserRole', 'url'=>array('update', 'id'=>$model->user_role_id)),
	array('label'=>'Delete SysUserRole', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->user_role_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SysUserRole', 'url'=>array('admin')),
);
?>

<h1>View SysUserRole <?php echo $model->user_role_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'user_role_id',
		'user_role_name',
		'user_remark',
		'user_role_status',
		'updated_by',
		'updated_date',
	),
)); ?>
