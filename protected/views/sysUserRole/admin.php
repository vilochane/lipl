<?php
/* @var $this SysUserRoleController */
/* @var $model SysUserRole */

$this->breadcrumbs=array(
	'Sys User Roles'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sys-user-role-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
        $('.search-form').toggle();
	return false;
});
");
?>

<h1>Manage Sys User Roles</h1>

<div class="operation-buttons">
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['searchButton'], '#', array('class' => 'search-button')); ?>    </span>
    <span class="operation-button">
        <?php echo CHtml::link(Yii::app()->params['createANewRecord'], CController::createUrl('create'), array('class' => 'add-new-button')); ?>    </span>
</div>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sys-user-role-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'user_role_id',
		'user_role_name',
		'user_remark',
		'user_role_status',
		'updated_by',
		'updated_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
