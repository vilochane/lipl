<?php
/* @var $this SysUserRoleController */
/* @var $data SysUserRole */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_role_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->user_role_id), array('view', 'id'=>$data->user_role_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_role_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_role_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_remark')); ?>:</b>
	<?php echo CHtml::encode($data->user_remark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_role_status')); ?>:</b>
	<?php echo CHtml::encode($data->user_role_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>