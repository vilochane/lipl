<?php
/* @var $this CmPrefixController */
/* @var $model CmPrefix */

$this->breadcrumbs=array(
	'Cm Prefixes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmPrefix', 'url'=>array('index')),
	array('label'=>'Manage CmPrefix', 'url'=>array('admin')),
);
?>

<h1>Create CmPrefix</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>