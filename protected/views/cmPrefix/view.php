<?php
/* @var $this CmPrefixController */
/* @var $model CmPrefix */

$this->breadcrumbs=array(
	'Cm Prefixes'=>array('index'),
	$model->prefix_id,
);

$this->menu=array(
	array('label'=>'List CmPrefix', 'url'=>array('index')),
	array('label'=>'Create CmPrefix', 'url'=>array('create')),
	array('label'=>'Update CmPrefix', 'url'=>array('update', 'id'=>$model->prefix_id)),
	array('label'=>'Delete CmPrefix', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->prefix_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmPrefix', 'url'=>array('admin')),
);
?>

<h1>View CmPrefix <?php echo $model->prefix_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'prefix_id',
		'prefix_name',
		'updated_by',
		'updated_date',
	),
)); ?>
