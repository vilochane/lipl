<?php
/* @var $this CmPrefixController */
/* @var $model CmPrefix */

$this->breadcrumbs=array(
	'Cm Prefixes'=>array('index'),
	$model->prefix_id=>array('view','id'=>$this->encryptParam($model->prefix_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmPrefix', 'url'=>array('index')),
	array('label'=>'Create CmPrefix', 'url'=>array('create')),
	array('label'=>'View CmPrefix', 'url'=>array('view', 'id'=>$model->prefix_id)),
	array('label'=>'Manage CmPrefix', 'url'=>array('admin')),
);
?>

<h1>Update CmPrefix <?php echo $model->prefix_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>