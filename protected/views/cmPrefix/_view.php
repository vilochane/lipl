<?php
/* @var $this CmPrefixController */
/* @var $data CmPrefix */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('prefix_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->prefix_id), array('view', 'id'=>$data->prefix_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prefix_name')); ?>:</b>
	<?php echo CHtml::encode($data->prefix_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>