<?php
/* @var $this CmPrefixController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Prefixes',
);

$this->menu=array(
	array('label'=>'Create CmPrefix', 'url'=>array('create')),
	array('label'=>'Manage CmPrefix', 'url'=>array('admin')),
);
?>

<h1>Cm Prefixes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
