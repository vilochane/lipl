<?php
/* @var $this CmPrefixController */
/* @var $model CmPrefix */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-prefix-form',
        'enableAjaxValidation' => true,
    ));
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <span class="show-error-summary"></span>
    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'prefix_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'prefix_name', array('class' => 'form-control', 'size' => 2, 'maxlength' => 2, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'prefix_name'); ?>
    </div>
    <div class="row buttons">
        <?php CommonSystem::getAjaxSubmitButton(CController::createUrl("AjaxCreate"), $dialogId, $select2Id); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->