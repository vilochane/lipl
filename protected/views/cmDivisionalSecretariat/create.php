<?php
/* @var $this CmDivisionalSecretariatController */
/* @var $model CmDivisionalSecretariat */

$this->breadcrumbs=array(
	'Cm Divisional Secretariats'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CmDivisionalSecretariat', 'url'=>array('index')),
	array('label'=>'Manage CmDivisionalSecretariat', 'url'=>array('admin')),
);
?>

<h1>Create CmDivisionalSecretariat</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>