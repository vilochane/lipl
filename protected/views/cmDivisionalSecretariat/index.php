<?php
/* @var $this CmDivisionalSecretariatController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Cm Divisional Secretariats',
);

$this->menu=array(
	array('label'=>'Create CmDivisionalSecretariat', 'url'=>array('create')),
	array('label'=>'Manage CmDivisionalSecretariat', 'url'=>array('admin')),
);
?>

<h1>Cm Divisional Secretariats</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
