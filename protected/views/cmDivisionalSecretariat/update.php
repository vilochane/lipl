<?php
/* @var $this CmDivisionalSecretariatController */
/* @var $model CmDivisionalSecretariat */

$this->breadcrumbs=array(
	'Cm Divisional Secretariats'=>array('index'),
	$model->divisional_secretariat_id=>array('view','id'=>$this->encryptParam($model->divisional_secretariat_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List CmDivisionalSecretariat', 'url'=>array('index')),
	array('label'=>'Create CmDivisionalSecretariat', 'url'=>array('create')),
	array('label'=>'View CmDivisionalSecretariat', 'url'=>array('view', 'id'=>$model->divisional_secretariat_id)),
	array('label'=>'Manage CmDivisionalSecretariat', 'url'=>array('admin')),
);
?>

<h1>Update CmDivisionalSecretariat <?php echo $model->divisional_secretariat_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>