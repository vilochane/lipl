<?php
/* @var $this CmDivisionalSecretariatController */
/* @var $model CmDivisionalSecretariat */

$this->breadcrumbs=array(
	'Cm Divisional Secretariats'=>array('index'),
	$model->divisional_secretariat_id,
);

$this->menu=array(
	array('label'=>'List CmDivisionalSecretariat', 'url'=>array('index')),
	array('label'=>'Create CmDivisionalSecretariat', 'url'=>array('create')),
	array('label'=>'Update CmDivisionalSecretariat', 'url'=>array('update', 'id'=>$model->divisional_secretariat_id)),
	array('label'=>'Delete CmDivisionalSecretariat', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->divisional_secretariat_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CmDivisionalSecretariat', 'url'=>array('admin')),
);
?>

<h1>View CmDivisionalSecretariat <?php echo $model->divisional_secretariat_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'divisional_secretariat_id',
		'divisional_secretariat_name',
		'updated_by',
		'updated_date',
	),
)); ?>
