<?php
/* @var $this CmDivisionalSecretariatController */
/* @var $model CmDivisionalSecretariat */
/* @var $form CActiveForm */
?>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'cm-divisional-secretariat-form',
        'enableAjaxValidation' => true,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <span class="show-error-summary"></span>

    <div class="row">
        <div class="input-group">
            <span class="input-group-addon">
                <?php echo $form->labelEx($model, 'divisional_secretariat_name'); ?>
            </span>		
            <?php echo $form->textField($model, 'divisional_secretariat_name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 100, 'autocomplete' => 'off')); ?>
        </div>
        <?php echo $form->error($model, 'divisional_secretariat_name'); ?>
    </div>

    <div class="row buttons">
        <?php
        CommonSystem::getAjaxSubmitButton(CController::createUrl('AjaxCreate'), $dialogId, $select2Id);
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->