<?php
/* @var $this CmDivisionalSecretariatController */
/* @var $data CmDivisionalSecretariat */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('divisional_secretariat_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->divisional_secretariat_id), array('view', 'id'=>$data->divisional_secretariat_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('divisional_secretariat_name')); ?>:</b>
	<?php echo CHtml::encode($data->divisional_secretariat_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />


</div>