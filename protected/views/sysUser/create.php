<?php
/* @var $this SysUserController */
/* @var $model SysUser */

$this->breadcrumbs=array(
	'Sys Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SysUser', 'url'=>array('index')),
	array('label'=>'Manage SysUser', 'url'=>array('admin')),
);
?>

<h1>Create SysUser</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>