<?php
/* @var $this SysUserController */
/* @var $model SysUser */

$this->breadcrumbs=array(
	'Sys Users'=>array('index'),
	$model->user_id,
);

$this->menu=array(
	array('label'=>'List SysUser', 'url'=>array('index')),
	array('label'=>'Create SysUser', 'url'=>array('create')),
	array('label'=>'Update SysUser', 'url'=>array('update', 'id'=>$model->user_id)),
	array('label'=>'Delete SysUser', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->user_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SysUser', 'url'=>array('admin')),
);
?>

<h1>View SysUser <?php echo $model->user_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'user_role_id',
		'emp_id',
		'user_username',
		'user_password',
		'user_status',
		'user_su_status',
		'updated_by',
		'updated_date',
	),
)); ?>
