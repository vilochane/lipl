<?php
/* @var $this SysUserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Sys Users',
);

$this->menu=array(
	array('label'=>'Create SysUser', 'url'=>array('create')),
	array('label'=>'Manage SysUser', 'url'=>array('admin')),
);
?>

<h1>Sys Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
