<?php
/* @var $this SysUserController */
/* @var $model SysUser */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sys-user-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

      <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'user_role_id'); ?>
                    </span>		
                        <?php echo $form->textField($model,'user_role_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'user_role_id'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'emp_id'); ?>
                    </span>		
                        <?php echo $form->textField($model,'emp_id',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'emp_id'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'user_username'); ?>
                    </span>		
                        <?php echo $form->textField($model,'user_username',array('class'=>'form-control', 'size'=>50,'maxlength'=>50, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'user_username'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'user_password'); ?>
                    </span>		
                        <?php echo $form->textField($model,'user_password',array('class'=>'form-control', 'size'=>60,'maxlength'=>100, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'user_password'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'user_status'); ?>
                    </span>		
                        <?php echo $form->textField($model,'user_status', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'user_status'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'user_su_status'); ?>
                    </span>		
                        <?php echo $form->textField($model,'user_su_status', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'user_su_status'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'updated_by'); ?>
                    </span>		
                        <?php echo $form->textField($model,'updated_by',array('class'=>'form-control', 'size'=>10,'maxlength'=>10, 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'updated_by'); ?>
          </div>

          <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <?php echo $form->labelEx($model,'updated_date'); ?>
                    </span>		
                        <?php echo $form->textField($model,'updated_date', array('class'=>'form-control', 'autocomplete' => 'off')); ?>
                </div>
            <?php echo $form->error($model,'updated_date'); ?>
          </div>

            <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save' , array('class' =>'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->