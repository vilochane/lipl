<?php
/* @var $this SysUserController */
/* @var $model SysUser */

$this->breadcrumbs=array(
	'Sys Users'=>array('index'),
	$model->user_id=>array('view','id'=>$this->encryptParam($model->user_id)),
	'Update',
);

$this->menu=array(
	array('label'=>'List SysUser', 'url'=>array('index')),
	array('label'=>'Create SysUser', 'url'=>array('create')),
	array('label'=>'View SysUser', 'url'=>array('view', 'id'=>$model->user_id)),
	array('label'=>'Manage SysUser', 'url'=>array('admin')),
);
?>

<h1>Update SysUser <?php echo $model->user_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>