<?php

/*
 * This class contains the list data related functions for each model
 * prefix with getListData
 */

final class CommonListData {
    
    public static function getPaymentCollectedStatus(){
        return array('0'=>'No', '1'=>'Yes');
    }


    public static function getPendingPaymentAgreements(){
        $date = date('Y-m-d');
        $ct = new CDbCriteria();
        $ct->condition = "agreement_status !='2' and agreement_start_date > $date";
        return HipAgreement::model()->findAll($ct);
    }


    public static function getListDataOperationPeriod($order = 'ASC') {
        return CHtml::listData(CmOperationPeriod::model()->findAll(new CDbCriteria(array("order" => "operation_period_name $order"))), 'operation_period_id', 'operation_period_name');
    }

    public static function getListDataTradeExperience($order = 'ASC') {
        return CHtml::listData(CmTradeExperience::model()->findAll(new CDbCriteria(array("order" => "trade_experience_name $order"))), 'trade_experience_id', 'trade_experience_name');
    }

    public static function getListDataEmpPeriod($order = 'ASC') {
        return CHtml::listData(CmEmpPeriod::model()->findAll(new CDbCriteria(array("order" => "emp_period_name $order"))), 'emp_period_id', 'emp_period_name');
    }

    public static function getListDataEmpPeriodValue($val) {
        $empPeriodName = null;
        $empPeriod = CmEmpPeriod::model()->findByPk($val);
        if (!empty($empPeriod)) {
            $empPeriodName = $empPeriod->emp_period_name;
        }
        return $empPeriodName;
    }

    public static function getListDataCustomer($branch = false, $divisionalSec = false, $profession = false, $inactive = false, $deleted = false, $order = 'ASC') {
        $ct = new CDbCriteria();
        if ($branch) {
            $ct->compare("branch_id", CommonUser::getUserWorkingBranchId());
        }
        if ($divisionalSec) {
            $ct->compare("divisional_secretariat_id", $divisionalSec);
        }
        if ($profession) {
            $ct->compare("profession_id", $profession);
        }
        if ($deleted) {
            $ct->compare("cus_status", array('2'));
        } elseif ($inactive && !$deleted) {
            $ct->compare("cus_status", array('0'));
        } else {
            $ct->compare("cus_status", array('0', '1'));
        }

        return CHtml::listData(PeCustomer::model()->findAll($ct), "cus_id", "cus_first_name");
    }

    public static function getListDataCustomerName($val) {
        $customerName = null;
        $customer = PeCustomer::model()->findByPk($val);
        if (!empty($customer)) {
            $customerName = "of Customer " . $customer->cus_first_name;
        }
        return $customerName;
    }

    public static function getListDataDueDate() {
        return CHtml::listData(CmDueDate::model()->findAll(new CDbCriteria(array("order" => "due_date_date ASC"))), "due_date_id", "due_date_date");
    }

    public static function getListDataPrefix() {
        return CHtml::listData(CmPrefix::model()->findAll(new CDbCriteria(array("order" => "prefix_name ASC"))), "prefix_name", "prefix_name");
    }

    /* this function returns the hire status by checking the model relation */

    public static function getVehicleHireStatus($relationObj) {
        $hired = "Not Hired";
        if (is_array($relationObj) && count($relationObj) > 0) {
            $hired = "Hired";
        }
        return $hired;
    }

    public static function getListDataVehicleCondition() {
        return array("0" => "First Hand", "1" => "Second Hand", "3" => "More");
    }

    public static function getVehicleConditionStatus($val) {
        $listArray = array("0" => "First Hand", "1" => "Second Hand", "3" => "More");
        return $listArray[$val];
    }

    public static function getListDataAgreementPeriod($order = 'ASC') {
        return CHtml::listData(CmPeriod::model()->findAll(new CDbCriteria(array("order" => "period_name $order"))), "period_id", "period_name");
    }

    public static function getListDataVehicleModel($makeId = null, $order = 'ASC') {
        $ct = new CDbCriteria();
        $ct->condition = "vehicle_model_status != '2'";
        $ct->compare('vehicle_make_id', $makeId);
        return CHtml::listData(CmVehicleModel::model()->findAll($ct), "vehicle_model_id", "vehicle_model_name");
    }

    public static function getListDataVehicleMake($order = 'ASC') {
        return CHtml::listData(CmVehicleMake::model()->findAll(new CDbCriteria(array("condition" => "vehicle_make_status !='2'", "order" => "vehicle_make_name $order"))), "vehicle_make_id", "vehicle_make_name");
    }

    public static function getListDataVehicleTypes($order = 'ASC') {
        return CHtml::listData(CmVehicleType::model()->findAll(new CDbCriteria(array("order" => "vehicle_type_name $order"))), "vehicle_type_id", "vehicle_type_name");
    }

    /** hired true only hired vehicles false none hired vehicles if all including deleted vehicles 
     */
    public static function getListDataVehicles($hired = false, $order = 'ASC') {
        $ct = new CDbCriteria();
        if (!$hired) {
            $ct->compare('vehicle_details_status', '0');
        } elseif ($hired === 'all') {
            $ct->compare('vehicle_details_status', array('0', '1', '2'));
        } elseif ($hired === true) {
            $ct->compare('vehicle_details_status', '1');
        }
        $ct->order = "vehicle_details_reg $order";
        return CHtml::listData(HipVehichleDetails::model()->findAll($ct), 'vehicle_details_id', 'vehicle_details_reg');
    }

    /**
     * This function returns the agreement insurance status flags
     */
    public static function getListDataAgreementInsuranceStatusVal($val) {
        $list = array('0' => 'No', '1' => 'Yes');
        return $list[$val];
    }
    public static function getListDataAgreementInsuranceStatus() {
        return array('0' => 'No', '1' => 'Yes');
    }

    public static function getListDataEmploymentStatus() {
        return array("1" => "Yes", "0" => "No");
    }

    public static function getListDataEmpUserStatusValue($val) {
        $userStatus = array("1" => "Yes", "0" => "No");
        return $userStatus[$val];
    }

    public static function getListDataEmploymentStatusValue($val) {
        if ($val !== null) {
            $empStatus = array("1" => "Yes", "0" => "No");
            return $empStatus[$val];
        }
        return null;
    }

    public static function getListDataPremisOwnerShip() {
        return array("0" => "Rented", "1" => "Owned", "2" => "N/A");
    }

    public static function getListDataPremisOwnerShipValue($val) {
        if ($val !== null) {
            $premises = array("0" => "Rented", "1" => "Owned", "2" => "N/A");
            return $premises[$val];
        }
        return null;
    }

    public static function getListDataCusOrganization() {
        return array("0" => "Sole Proprietorship", "1" => "Partnership", "2" => "Limited Company", "3" => "N/A");
    }

    public static function getListDataCusOrganizationValue($val) {
        if ($val !== null) {
            $organization = array("0" => "Sole Proprietorship", "1" => "Partnership", "2" => "Limited Company", "3" => "N/A");
            return $organization[$val];
        }
        return null;
    }

    public static function getListDataResidenceInfo() {
        return array("0" => "Owner", "1" => "Tenant", "2" => "Boader");
    }

    public static function getListDataResidenceInfoValue($val) {
        if ($val !== null) {
            $residence = array("0" => "Owner", "1" => "Tenant", "2" => "Boader");
            return $residence[$val];
        }
        return null;
    }

    public static function getListDataProfession($order = "ASC") {
        return CHtml::listData(CmProfession::model()->findAll(new CDbCriteria(array("order" => "profession_id $order"))), "profession_id", "profession_name");
    }

    public static function getListDataProfessionValue($val) {
        $professionName = null;
        $profession = CmProfession::model()->findByPk($val);
        if (!empty($profession)) {
            $professionName = $profession->profession_name;
        }
        return $professionName;
    }

    public static function getListDataResidenceDuration($order = "ASC") {
        return CHtml::listData(CmResidenceDuration::model()->findAll(new CDbCriteria(array("order" => "residence_duration_name $order"))), "residence_duration_id", "residence_duration_name");
    }

    public static function getListDataNationality() {
        return array("1" => "Sri Lankan", "0" => "Foriegn");
    }

    public static function getListDataNationalityValue($val) {
        if ($val !== null) {
            $nationality = array("1" => "Sri Lankan", "0" => "Foriegn");
            return $nationality[$val];
        }
        return null;
    }

    public static function getListDataMartalStatus() {
        return array("0" => "Single", "1" => "Married");
    }

    public static function getListDataMartalStatusValue($val) {
        if ($val !== null) {
            $maritalStatus = array("0" => "Single", "1" => "Married");
            return $maritalStatus[$val];
        }
        return null;
    }

    public static function getListDataGender() {
        return array("1" => "Male", "0" => "Female");
    }

    public static function getListDataGenderValue($val) {
        if ($val !== null) {
            $gender = array("1" => "Male", "0" => "Female");
            return $gender[$val];
        }
        return null;
    }

    public static function getListDataDivisionalSecretariat($order = "ASC") {
        return CHtml::listData(CmDivisionalSecretariat::model()->findAll(new CDbCriteria(array("order" => "divisional_secretariat_name $order"))), "divisional_secretariat_id", "divisional_secretariat_name");
    }

    public static function getListDataUserRoles() {
        return CHtml::listData(SysUserRole::model()->findAll(new CDbCriteria(array('order' => 'user_role_name ASC'))), 'user_role_id', 'user_role_name');
    }

    /**
     * This function returns the status list
     */
    public static function getListDataStatus() {
        return array(0 => 'Inactive', 1 => 'Active');
    }

    public static function getListDataStatusValue($val) {
        $list = array(0 => 'Inactive', 1 => 'Active');
        return $list[$val];
    }

    /**
     * This function returns employees
     * @param string $branchId bracg id
     * @param boolean $isWithDesignation
     */
    public static function getListDataEmployee($isWithDesignation = false, $branchId = null, $order = 'ASC') {

        if ($isWithDesignation && !empty($branchId)) {
            $withArray = array(
                "designation" => array("joinType" => "INNER JOIN", 'alias' => 'cd'),
                "mapEmployeeBranches" => array('joinType' => "INNER JOIN", "select" => false, "condition" => "branch_id = :branchId", "params" => array(":branchId" => $branchId)),
            );
            $select = array("t.emp_id", "CONCAT('Designation :- ', cd.designation_name, ' | Name :- ', t.emp_first_name) as emp_first_name");
        } elseif ($isWithDesignation && empty($branchId)) {
            $withArray = array(
                "designation" => array("joinType" => "INNER JOIN", 'alias' => 'cd'),
            );
            $select = array("t.emp_id", "CONCAT('Designation :- ', cd.designation_name, ' | Name :- ', t.emp_first_name) as emp_first_name");
        } elseif (!$isWithDesignation && !empty($branchId)) {
            $withArray = array(
                "mapEmployeeBranches" => array('joinType' => "INNER JOIN", "select" => false, "condition" => "branch_id = :branchId", "params" => array(":branchId" => $branchId)),
            );
            $select = "t.emp_id, t.emp_first_name";
        } else {
            $withArray = array();
            $select = "t.emp_id, t.emp_first_name";
        }
        return CHtml::listData(PeEmployee::model()->findAll(new CDbCriteria(array("with" => $withArray, "select" => $select, "order" => "emp_first_name $order"))), 'emp_id', 'emp_first_name');
    }

    /**
     * This function returns the branches
     */
    public static function getListDataBranches() {
        return CHtml::listData(CmBranch::model()->findAll(), 'branch_id', 'branch_name');
    }

    /* This function returns the designations */

    public static function getListDataDesignations() {
        return CHtml::listData(CmDesignation::model()->findAll(), 'designation_id', 'designation_name');
    }

    /* end of list data */

    public static function getListDataBranchManager() {
        $branches = CmBranch::model()->findAll(new CDbCriteria(array("select" => "emp_id", "condition" => "emp_id != 'null'")));
        $ct = new CDbCriteria();
        if (!empty($branches)) {
            foreach ($branches as $branch) {
                $ct->compare('emp_id', $branch->emp_id);
            }
            return CHtml::listData(PeEmployee::model()->findAll($ct), "emp_id", "emp_first_name");
        } else {
            return array();
        }
    }

}
