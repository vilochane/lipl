<?php

/*
 * This class handles the tabular inputs with or without transaction
 * multiple or single child models 
 * @param $childModelPosts post array of the child model indexed with model class name
 * @param $_childModelParamsArray additional parameters of the child model to set
 * @param $parentModelObject parent model object insert or update
 * @param $childAttributesArray resulting child model attributes if multiple models resulting array indexed with model name
 */

final class CommonTabularInput {

    public  $childAttributesArray = array();
    private $_childClass;
    private $_childModelParamsArray = null;
    private $_childModelObjArray = array();
    private $_childModelErrorCount = 0;
    private $_isChildModelValid = false; /* a counter of errors 0 means no errors */
    private $_isSaveChildModels;

    public function setModels(array $childModelsPostsArray, array $childModelParamsArray = array(), array $childModelsPkCol, $saveChildModel = true, $parentModelObject = null) {
        $this->_isSaveChildModels = $saveChildModel;
        /* when new objects passed setting resetting the error count */
        $this->_childModelErrorCount = 0;
        /* empty parent model means no transaction */
        if (empty($parentModelObject)) {
            if (count($childModelsPostsArray) > 0) {
                foreach ($childModelsPostsArray as $childClass => $childModelPostArray) {
                    $this->_childModelParamsArray = $childModelParamsArray[$childClass];
                    $this->childAttributesArray[$childClass] = $this->setModelAttributes($childClass, $childModelPostArray, $childModelsPkCol[$childClass]);
                }
            }
        } else {
            $this->_isSaveChildModels = false;
            /* setting transaction items */
            if (count($childModelsPostsArray) > 0) {
                foreach ($childModelsPostsArray as $childClass => $childModelPostArray) {
                    if (count($childModelPostArray) > 0) {
                        $this->_childModelParamsArray = $childModelParamsArray[$childClass];
                        $this->childAttributesArray[$childClass] = $this->setModelAttributes($childClass, $childModelPostArray, $childModelsPkCol[$childClass]);
                    }else{
                        $this->childAttributesArray[$childClass] = array();
                    }
                }
            }
            $this->setTransactionModels($parentModelObject);
        }
        return $this->childAttributesArray;
    }

    private function setTransactionModels($parentModelObject) {
        $parentPk = $parentModelObject->getTableSchema()->primaryKey;
        if ($this->validateChildModels() === 0) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($parentModelObject->save() && count($this->childAttributesArray) > 0) {
                    foreach ($this->childAttributesArray as $childClass => $childModelObjArray) {
                        foreach ($childModelObjArray as $index => $modelObj) {
                            $modelObj->$parentPk = $parentModelObject->$parentPk;
                            $modelObj->save();
                        }
                    }
                }$transaction->commit();return true;                
            } catch (CDbException $ce) {
                $transaction->rollBack();
                Yii::app()->user->setFlash('error', "{$ce->getMessage()}");
                return false;
            }
        }
        return false;
    }

    private function setModelAttributes($childClass, $childModelPostArray, $childModelPkCol) {
        $this->_childModelObjArray = array();
        if (count($childModelPostArray) > 0) {
            foreach ($childModelPostArray as $attributes) {
                if ($attributes['modelScenario'] === 'insert') {
                    $childModelObj = $this->createChildModel($attributes, $childClass, $childModelPkCol);
                    if ($this->_isSaveChildModels) {
                        $childModelObj->save();
                    }
                    array_push($this->_childModelObjArray, $childModelObj);
                } else {
                    $childModelObj = $this->updateChildModel($attributes, $childClass, $childModelPkCol);
                    if ($this->_isSaveChildModels) {
                        $childModelObj->save();
                    }
                    array_push($this->_childModelObjArray, $childModelObj);
                }
            }
            return $this->_childModelObjArray;
        }
    }

    private function createChildModel($childModelPost, $childClass, $childModelPkCol) {
        $childModelPost[$childModelPkCol] = '';
        $childModelObj = new $childClass;
        $childModelObj->attributes = $childModelPost;
        Yii::app()->getController()->setLoggingData($childModelObj);
        $this->setModelAttributeParams($childModelObj);
        /* validating child model */
        $this->validateChildModel($childModelObj);
        return $childModelObj;
    }

    private function updateChildModel($childModelPost, $childClass, $childModelPkCol) {
        $childModelObj = $childClass::model()->findByPk($childModelPost[$childModelPkCol]);
        $childModelObj->attributes = $childModelPost;
        Yii::app()->getController()->setLoggingData($childModelObj);
        $this->setModelAttributeParams($childModelObj);
        /* validating child model */
        $this->validateChildModel($childModelObj);
        return $childModelObj;
    }

    /* mainly used for before transaction */
    private function validateChildModels() {
        $childModelErrorCount = 0;
        foreach ($this->childAttributesArray as $childClass => $childModelObjArray) {
            foreach ($childModelObjArray as $index => $modelObj) {
                $this->validateChildModel($modelObj);
                $childModelErrorCount = $this->_childModelErrorCount;
            }
        }
        return $childModelErrorCount;
    }

    private function validateChildModel($modelObj) {
        if (!$modelObj->validate()) {
            $this->_childModelErrorCount++;
        }
    }

    /* this function returns the error count of child models if no errors it's 0 */

    public function isChildModelsValid() {
        if ($this->_childModelErrorCount === 0) {
            $this->_isChildModelValid = true;
        }
        return $this->_isChildModelValid;
    }

    /* This function sets the model attribute params */

    private function setModelAttributeParams($modelObj) {
        if (count($this->_childModelParamsArray) > 0) {
            foreach ($this->_childModelParamsArray as $column => $value) {
                $modelObj->$column = $value;
            }
        }
    }

}
