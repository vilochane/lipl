<?php

/**
 * This is the model class for table "hip_payments".
 *
 * The followings are the available columns in table 'hip_payments':
 * @property string $payment_id
 * @property string $agreement_id
 * @property string $payment_d_capital
 * @property string $payment_d_interest
 * @property string $payment_p_amount_tot
 * @property string $payment_p_interest_tot
 * @property string $payment_p_d_amount
 * @property string $payment_p_d_interest
 * @property string $payment_rental
 * @property string $payment_rental_interest
 * @property string $payment_p_excess
 * @property integer $payment_p_late_count
 * @property integer $payment_term_late_count
 * @property string $payment_p_last_date
 * @property string $payment_l_d_date
 * @property string $payment_d_date
 * @property string $payment_n_d_date
 * @property string $payment_p_i_rate
 * @property string $payment_c_due_payment
 * @property string $payment_penalty
 * @property string $payment_payable
 * @property string $payment_deduct
 * @property string $payment_expense
 * @property string $payment_received
 * @property string $payment_due
 * @property string $payment_excess
 * @property string $payment_late_days_count
 * @property string $payment_received_date
 * @property integer $payment_status
 * @property integer $payment_agreement_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property HipAgreement $agreement
 */
class HipPayments extends CActiveRecord {
    /* for penalty */

    public $dailyPenaltyRate;
    public $gracePeriod;
    /* end of for penalty */
    public $payableDisplayVal;
    public $penaltyDisplayVal;
    public $penaltyArrayDisplay;
    public $dueDisplayVal;
    public $monthlyRental;

    /* dates */
    public $today;
    public $currentMonthDueDate;
    public $dueDate; // the due date of the agreement
    /* counters */
    public $pLateMonthsCount = 0; /* Month diffrence between the due date and today same as payment late count */
    /* end of counters */
    private $_isInitialRecord; /* is this parent initial record */
    private $_isDueDateExceeded; /* flag to check parent setted due date exceeded against the current month due date */
    private $_isDueDateFutureDate; /* if due date is a future date */
    private $_previousDues = 0;
    private $_currentDue = 0; /* due up to current date */
    private $_currentPenalty = 0;
    private $_previousPenalty = 0; /* previous due penalty */
    private $_dueTotal = 0;
    private $_dateDiffObj = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hip_payments';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('agreement_id, payment_d_capital, payment_d_interest, payment_rental, payment_rental_interest, payment_term_late_count, payment_p_last_date, payment_l_d_date, payment_d_date, payment_n_d_date, payment_late_days_count, payment_payable, payment_received, payment_due, payment_received_date, updated_by, updated_date', 'required'),
            array('payment_p_late_count, payment_term_late_count, payment_status, payment_agreement_status', 'numerical', 'integerOnly' => true),
            array('agreement_id, payment_p_excess, payment_deduct, updated_by', 'length', 'max' => 10),
            //array('payment_d_capital, payment_d_interest, payment_p_amount_tot, payment_p_interest_tot, payment_p_d_amount, payment_p_d_interest, payment_rental, payment_rental_interest, payment_p_i_rate, payment_penalty, payment_payable, payment_received, payment_due, payment_excess', 'length', 'max' => 22),
            array('payment_l_d_date, payment_expense, payableDisplayVal, penaltyDisplayVal, dueDisplayVal', 'safe'),
            //custom rules
            // array('payment_deduct', 'checkDeductValue'),
            //end of custom rules
            array('payment_id, agreement_id, payment_d_capital, payment_d_interest, payment_p_amount_tot, payment_p_interest_tot, payment_p_d_amount, payment_p_d_interest, payment_rental, payment_rental_interest, payment_p_excess, payment_p_late_count, payment_l_d_date, payment_d_date, payment_n_d_date, payment_p_i_rate, payment_penalty, payment_payable, payment_deduct, payment_expense, payment_received, payment_due, payment_received_date, payment_status, payment_agreement_status, updated_by, updated_date', 'safe', 'on' => 'search'),
        );
    }

//    function checkDeductValue(){
//        $deductVal = (double)$this->payment_deduct;
//        $payment_penalty = (double)$this->payment_penalty;
//        if($deductVal < $payment_penalty){
//                            
////            $this->payment_penalty = $this->payment_penalty - $this->payment_deduct;
////            $this->payment_payable = $this->payment_payable -  $this->payment_deduct;       
//        }  else {
//            $this->addError('payment_deduct', "Deducting value : $this->payment_deduct should be less than penalty value : $this->payment_penalty");
//        }
//    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'agreement' => array(self::BELONGS_TO, 'HipAgreement', 'agreement_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'payment_id' => 'Payment',
            'agreement_id' => 'Agreement',
            'payment_d_capital' => 'Due Capital',
            'payment_d_interest' => 'Due Interest',
            'payment_p_amount_tot' => 'Penalty Amount Tot',
            'payment_p_interest_tot' => 'Penalty Interest Tot',
            'payment_p_d_amount' => 'Previous Due Amount',
            'payment_p_d_interest' => 'Previous Due Interest',
            'payment_rental' => 'Rental',
            'payment_rental_interest' => 'Interest',
            'payment_p_excess' => 'Previous Excess',
            'payment_p_late_count' => 'Late Payment(s) Count',
            'payment_p_last_date' => 'Last Payment Made Date',
            'payment_l_d_date' => 'Last Due Date',
            'payment_d_date' => 'Current Due Date',
            'payment_n_d_date' => 'Next Due Date',
            'payment_p_i_rate' => 'Interest Rate',
            'payment_penalty' => 'Total Penalty',
            'payment_payable' => 'Total Payable',
            'payment_deduct' => 'Deduct',
            'payment_expense' => 'Expense',
            'payment_received' => 'Received',
            'payment_due' => 'Current Payment Due',
            'payment_excess' => 'Current Payment Excess',
            'payment_received_date' => 'Payment Received Date',
            'payment_status' => 'Status',
            'payment_agreement_status' => 'Agreement Closed status',
            'payableDisplayVal' => 'Total Pyable',
            'penaltyDisplayVal' => 'Total Penalty',
            'dueDisplayVal' => 'Current Payment Due',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria = new CDbCriteria;
        if (!isset($_GET['HipPayments_sort'])) {
            $criteria->order = 'payment_id DESC';
        }
        $criteria->compare('payment_id', $this->payment_id, true);
        $criteria->compare('agreement_id', $this->agreement_id);
        $criteria->compare('payment_d_capital', $this->payment_d_capital, true);
        $criteria->compare('payment_d_interest', $this->payment_d_interest, true);
        $criteria->compare('payment_p_amount_tot', $this->payment_p_amount_tot, true);
        $criteria->compare('payment_p_interest_tot', $this->payment_p_interest_tot, true);
        $criteria->compare('payment_p_d_amount', $this->payment_p_d_amount, true);
        $criteria->compare('payment_p_d_interest', $this->payment_p_d_interest, true);
        $criteria->compare('payment_rental', $this->payment_rental, true);
        $criteria->compare('payment_rental_interest', $this->payment_rental_interest, true);
        $criteria->compare('payment_p_excess', $this->payment_p_excess, true);
        $criteria->compare('payment_p_late_count', $this->payment_p_late_count);
        $criteria->compare('payment_l_d_date', $this->payment_l_d_date, true);
        $criteria->compare('payment_d_date', $this->payment_d_date, true);
        $criteria->compare('payment_n_d_date', $this->payment_n_d_date, true);
        $criteria->compare('payment_p_i_rate', $this->payment_p_i_rate, true);
        $criteria->compare('payment_penalty', $this->payment_penalty, true);
        $criteria->compare('payment_payable', $this->payment_payable, true);
        $criteria->compare('payment_deduct', $this->payment_deduct, true);
        $criteria->compare('payment_expense', $this->payment_expense, true);
        $criteria->compare('payment_received', $this->payment_received, true);
        $criteria->compare('payment_due', $this->payment_due, true);
        $criteria->compare('payment_received_date', $this->payment_received_date, true);
        $criteria->compare('payment_status', $this->payment_status);
        $criteria->compare('payment_agreement_status', $this->payment_agreement_status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return HipPayments the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function reversePayment() {
        $agreementModel = $this->getAgreementModel($this->agreement_id);
        $agreementModel->agreement_d_capital = $this->payment_d_capital;
        $agreementModel->agreement_d_interest = $this->payment_d_interest;
        $agreementModel->agreement_p_amount_tot = $this->payment_p_amount_tot;
        $agreementModel->agreement_p_interest_tot = $this->payment_p_interest_tot;
        $agreementModel->agreement_p_d_amount = $this->payment_p_d_amount;
        $agreementModel->agreement_p_d_interest = $this->payment_p_d_interest;
        $agreementModel->agreement_p_late_count = $this->payment_p_late_count;
        $agreementModel->agreement_l_d_date = $this->payment_l_d_date;
        $agreementModel->agreement_p_last_date = $this->payment_p_last_date;
        $agreementModel->agreement_p_excess = $this->payment_excess;
        $agreementModel->agreement_closed_status = '0';
        $agreementModel->agreement_status = '1';
        $this->payment_status = '2';
        Yii::app()->controller->setLoggingData($agreementModel);
        Yii::app()->controller->setLoggingData($this);
        $agreementModel->save();
        $this->save();
        $this->deletePaymentsAfterReversePayment();
    }

    //Deletes all the agreement payments after the reverse payment 
    private function deletePaymentsAfterReversePayment() {
        $sql = "UPDATE `hip_payments` SET payment_status = '2' WHERE agreement_id = '$this->agreement_id' AND `payment_id` > '$this->payment_id' AND payment_status = '1'"; //ONLY PAID PAYMENTS ARE DELETED
        Yii::app()->db->createCommand($sql)->execute();
    }

    //update the reccord when payment received
    public function updateReceivedPayment() {
        $isEmptyReceviedDate = true;
        $isEmptyReceviedPayment = true;

        $receivedDate = $this->payment_received_date;
        $this->dueDisplayVal = $this->payment_payable;        
        $agreementModel = $this->getAgreementModel($this->agreement_id);
        $paymentReceived = (double) $this->payment_received;
        $penalty = (double) $this->penaltyDisplayVal;
        $paymentExcludingPenalty = $paymentReceived - $penalty;
        $dueAmount = (double) $this->payableDisplayVal - $paymentReceived;
        $this->updateAgreementDueAmounts($agreementModel, $paymentExcludingPenalty);
        $this->updateAgreementPenaltyTotals($agreementModel, $penalty);
        $this->updatePreviousDueAmounts($agreementModel, $dueAmount);
        $this->updateAgreementAttributes($agreementModel);
        if (!empty($receivedDate)) {
            $isEmptyReceviedDate = false;
        } else {
            $this->addError('payment_received_date', "{$this->getAttributeLabel('payment_received_date')} cannot be blank.");
        }
        $this->payment_status = 1;
        $this->validate();
        //var_dump($paymentReceived > (double)0);
        if (!empty($paymentReceived) && ($paymentReceived > (double) 0)) {
            $isEmptyReceviedPayment = false;
        } else {
            $this->addError('payment_received', "{$this->getAttributeLabel('payment_received')} cannot be blank.");
        }
        if ((!$isEmptyReceviedDate && !$isEmptyReceviedPayment) && ($agreementModel->save() && $this->save())) {
            Yii::app()->controller->redirect(array('admin'));
        }
    }

    public function beforeSave() {
        if ((double) $this->penaltyDisplayVal > (double) 0) {
            $this->payment_penalty = (double) $this->penaltyDisplayVal;
        }
        if ((double) $this->penaltyDisplayVal > (double) 0) {
            $this->payment_payable = (double) $this->payableDisplayVal;
        }
        if ((double) $this->penaltyDisplayVal > (double) 0) {
            $this->payment_due = (double) $this->dueDisplayVal;
        }
        return true;
    }

    private function updateAgreementAttributes($agreementModel) {
        $duDate = $agreementModel->dueDate->due_date_date;
        $paymentMadeDate = $this->payment_received_date;
        //var_dump(substr($paymentMadeDate, 0,8));
        $paymentMadeDueDate = substr($paymentMadeDate, 0, 8) . $duDate;
        $agreementModel->agreement_p_late_count = $this->payment_term_late_count;
        if (($agreementModel->agreement_p_d_amount <= (double) 0) && ($agreementModel->agreement_p_d_interest <= (double) 0) && ($agreementModel->agreement_d_capital <= (double) 0) && ($agreementModel->agreement_d_interest <= (double) 0)) {
            $agreementModel->agreement_closed_status = 1;
            $this->payment_agreement_status = 1; //if the agreemtn is closed setting the payment status to be closed
            $sql = "UPDATE `hip_payments` SET payment_agreement_status = '1' WHERE agreement_id = '$this->agreement_id' AND payment_status != '2'";
            Yii::app()->db->createCommand($sql)->execute(); // updating all the payment records since wont display on payments made
        }
        $agreementModel->agreement_l_d_date = $paymentMadeDueDate;
        $agreementModel->agreement_n_d_date = Common::addDate($paymentMadeDueDate, 0, 1);
        $agreementModel->agreement_p_last_date = $paymentMadeDate;
    }

    private function updatePreviousDueAmounts($agreementModel, $dueAmount) {
        $agreementModel->agreement_p_excess = 0;
        $agreementModel->agreement_p_d_amount = ((double) $dueAmount * (double) $agreementModel->agreement_rental_rate) / 100;
        $agreementModel->agreement_p_d_interest = ((double) $dueAmount * (double) $agreementModel->agreement_r_interest_rate) / 100;
        if ($dueAmount < (double) 0) {
            $this->payment_excess = $agreementModel->agreement_p_excess = abs($dueAmount);
        }
    }

    private function updateAgreementPenaltyTotals($agreementModel, $penalty) {
        $agreementModel->agreement_p_amount_tot += ((double) $penalty * (double) $agreementModel->agreement_rental_rate) / 100;
        $agreementModel->agreement_p_interest_tot += ((double) $penalty * (double) $agreementModel->agreement_r_interest_rate) / 100;
    }

    private function updateAgreementDueAmounts($agreementModel, $paymentExcludingPenalty) {
        $agreementModel->agreement_d_capital -= ((double) $paymentExcludingPenalty * (double) $agreementModel->agreement_rental_rate) / 100;
        $agreementModel->agreement_d_interest -= ((double) $paymentExcludingPenalty * (double) $agreementModel->agreement_r_interest_rate) / 100;
    }

    private function getAgreementModel($agreementId) {
        $model = HipAgreement::model()->findByPk($agreementId);
        if ($model === null)
            throw new CHttpException(404, 'Agreement model does not exists.');
        return $model;
    }

    /*     * ___________________________________________________________________________________________________* */

    /** when a fresh payment records adds 
     * Depends on controller setPaymentRecord
     */
    public function setModelValues($agreementModel, $today) {
        $this->updateRecordFormAgreement($agreementModel, $today);
        $this->save(false);
    }

    /**
     * This function update the record attributes from parent model
     * @param object $agreementModel
     */
    private function updateRecordFormAgreement($agreementModel, $today) {
        $this->setMonthlyRental($agreementModel);
        $this->setPaymentDates($agreementModel, $today);
        $this->setDateFlags($agreementModel);
        $this->setPaymentLateCount($agreementModel);
        $this->setPaymentExcess($agreementModel);
        $this->setPreviousDues($agreementModel); // where when the record added or record updated values plus if there is a ;ate count currently so to that amount dues
        $this->setCurrentDue();
        $this->agreement_id = $agreementModel->agreement_id;
        /* parent record data incase of reverse record */
        $this->payment_p_i_rate = $this->dailyPenaltyRate;
        $this->payment_p_late_count = $agreementModel->agreement_p_late_count;
        $this->payment_d_capital = $agreementModel->agreement_d_capital;
        $this->payment_d_interest = $agreementModel->agreement_d_interest;
        $this->payment_p_amount_tot = $agreementModel->agreement_p_amount_tot;
        $this->payment_p_interest_tot = $agreementModel->agreement_p_interest_tot;
        $this->payment_p_d_amount = $agreementModel->agreement_p_d_amount; /* previous due amount */
        $this->payment_p_d_interest = $agreementModel->agreement_p_d_interest; /* previous interest due amount */
        $this->payment_rental = $agreementModel->agreement_rental;
        $this->payment_rental_interest = $agreementModel->agreement_rental_interest;
        /* end of parent record data incase of reverse record */
        Yii::app()->controller->setLoggingData($this);
    }

    /**
     * Sets setMonthly rental + interest
     * Depends on updateRecordFormAgreement
     */
    private function setMonthlyRental($agreementModel) {
        $this->monthlyRental = (double) $agreementModel->agreement_rental_tot;
    }

    /**
     * Set dates
     */
    private function setPaymentDates($agreementModel, $today) {
        $this->payment_p_last_date = $agreementModel->agreement_p_last_date;
        $this->payment_l_d_date = $agreementModel->agreement_l_d_date;
        $this->today = $today; /* setting dates up to this day penalty calculated */
        $this->dueDate = $agreementModel->dueDate->due_date_date;
        $this->currentMonthDueDate = $agreementModel->currentMonthDueDate = substr($today, 0, 8) . $this->dueDate; /* set by controller setPaymentRecord */
        $this->payment_d_date = $this->currentMonthDueDate;
        $this->payment_n_d_date = Common::addDate($this->payment_d_date, 0, 1);
    }

    /** date flags */
    private function setDateFlags($agreementModel) {
        $this->_isInitialRecord = $agreementModel->isInitialRecord; /* set from controller setPaymentRecord */
        $this->_isDueDateExceeded = $this->payment_d_date < $this->today ? true : false; /* parent setted due date against today */
        $this->_isDueDateFutureDate = $this->payment_d_date > $this->today ? true : false; /* if the due date is a future date */
    }

    /**
     * Set payment late count
     */
    private function setPaymentLateCount() { // intially getting the late count since last payment made due date
        $lastDueDate = $this->payment_l_d_date; // when adding the records or when payment recorded the last due date added if initila the lat payment made date moth due date added
        if (empty($lastDueDate)) { // if a newrocrd last payment date is the next due date, exsisting if the collected values types this will be reseteed to user to prompt to enter the lat payment date
            throw new CDbException('Last payment due date cannot be null');
        }
        // late count calulated for an initial record with the last payment made date uo today
        // if a partial payment done this value wont get updated on agreement, it'll remain to calculate the late count correctly
        if ($this->payment_p_last_date > $this->today) {
            throw new CHttpException(401, 'Wrong system date and time detected please correct it.');
        }
        $this->_dateDiffObj = $this->getDateDifference($lastDueDate, $this->today); // if crrent month due date is today it'll add current monght also
        $lateYears = (int) $this->_dateDiffObj->y;
        $this->pLateMonthsCount = (int) $this->_dateDiffObj->m;
        $this->payment_late_days_count = $this->getLateDays($this->_dateDiffObj);
        //$this->payment_term_late_count = (int) ($this->payment_late_days_count / 31); /* the late terms, use to multiply the rental */
        $this->payment_term_late_count = ($lateYears * 12) + $this->pLateMonthsCount; /* the late terms, use to multiply the rental */
    }

    /**
     * Late days with the grace period
     */
    private function getLateDays($dateDiffObj) {
        $lateDays = $dateDiffObj->days;
        if ($dateDiffObj->invert === 1) {
            $lateDays = (int) -$dateDiffObj->days; /* if due date is greater than today inversion required */
        }
        if ($dateDiffObj->m < 1) { /* if the payment is late less than one month grace period applied */
            $lateDays = $lateDays - $this->gracePeriod;
        }
        if ($lateDays < 0) {
            $lateDays = 0;
        }
        return $lateDays;
    }

    /**
     * Setting excess
     */
    private function setPaymentExcess($agreementModel) {
        $this->payment_p_excess = (double) $agreementModel->agreement_p_excess;
    }

    /**
     * Sets the previous dues
     * Depends on updateRecordFormAgreement
     */
    private function setPreviousDues($agreementModel) {
        $this->_previousDues = (double) $agreementModel->agreement_p_d_amount + (double) $agreementModel->agreement_p_d_interest;
        if ($this->_previousDues > 0) { //if not an minus value if it's an excess
            $this->_previousPenalty = $this->calculatePenalty($this->_previousDues, $this->payment_late_days_count);
        }
    }

    /**
     * Sets the current due the dynamic due after the previous due it depends on the last payment made date to current day
     */
    private function setCurrentDue() {
        $duePaymentArray = array();
        if ($this->_previousDues > (double) 0) {
            $duePaymentArray[] = array(
                'dueDate' => $this->payment_l_d_date,
                'amount' => $this->_previousDues,
                'excess' => 0.00,
                'penalty' => $this->_previousPenalty,
                'payable' => $this->_previousDues + $this->_previousPenalty,
                'lateDays' => $this->payment_late_days_count,
                'delay' => "{$this->_dateDiffObj->y} Year(s) {$this->_dateDiffObj->m} Month(s) {$this->_dateDiffObj->d} day(s)");
        } //if no previous dues        
        $lateTermCount = $this->payment_term_late_count - 1;
        //if($this->_isDueDateFutureDate){ $lateTermCount = $this->payment_term_late_count; } // if it's a future date reducing the count by 1        
        for ($i = $lateTermCount; $i >= 0; $i--) { //calculates the penalty accrodingly starting from the earliest month due date
            $rMonths = $i;
            $excess = 0.00;
            if ($this->_isDueDateFutureDate) { //if due date is a future date to avoid adding this month not letting the count to be 0 so adding 1 to avoid it
                $rMonths +=1;
            } // if the current due date is a future date that rental not added but below rentals with the due is added
            $rentalDis = $rental = $this->monthlyRental;            
            if ($i === $lateTermCount) { //for the initial payment if any excesses reducing it
                $rentalDis = (double) $rentalDis - (double) $this->payment_p_excess;
                $excess = (double) $this->payment_p_excess;
            }
            $dPaymentDueDate = Common::reduceDate($this->currentMonthDueDate, 0, $rMonths); //previous payments due date
            $dateDifferenceObj = $this->getDateDifference($dPaymentDueDate, $this->today);
            $daysCount = (int) $this->getLateDays($dateDifferenceObj);
            $dueRentalPenalty = $this->calculatePenalty($rentalDis, $daysCount); //penalty shoud be calculated do the deducted rental below if - panalty it's deducted from the total
            $this->_currentDue +=$rentalDis;
            $this->_currentPenalty +=$dueRentalPenalty;
            //var_dump($this->_currentDue);
            //var_dump($this->_currentPenalty);
            $duePaymentArray[] = array(
                'dueDate' => $dPaymentDueDate,
                'amount' => $rentalDis,
                'excess' => $excess,
                'penalty' => $dueRentalPenalty,
                'payable' => ($rentalDis + $dueRentalPenalty),
                'lateDays' => $daysCount,
                'delay' => "{$dateDifferenceObj->y} Year(s) {$dateDifferenceObj->m} Month(s) {$dateDifferenceObj->d} day(s)");
        }
        $this->payment_c_due_payment = CJSON::encode($duePaymentArray); //converting to string
//        var_dump($this->_previousDues);
//        var_dump($this->payment_p_excess);
        $this->_dueTotal = $this->_currentDue + $this->_previousDues + $this->payment_p_excess; //previous dues either - or + value excess will remove it
        $this->penaltyDisplayVal = $this->payment_penalty = $this->_currentPenalty + $this->_previousPenalty; //penalty total
        $this->payableDisplayVal = $this->payment_payable = $this->_dueTotal + $this->payment_penalty;
//        var_dump($this->payment_penalty);
//        var_dump($this->_previousDues);
    }

    /**
     * This function returns the date difference of the two dates
     */
    private function getDateDifference($initDate, $secondDate) {
        $initDateObj = date_create($initDate);
        $secondDateObj = date_create($secondDate);
        return date_diff($initDateObj, $secondDateObj);
    }

    /**
     * This function calculates the penalty
     */
    private function calculatePenalty($rental, $lateDays) {
        $penalty = 0;
        if ($lateDays > 0) {
            $penalty = (double) $rental * ((int) $lateDays * (double) ($this->dailyPenaltyRate));
        }
        return $penalty;
    }

    public function roundValue($value, $precision = 5) {
        $value = (double) $value;
        return round($value, $precision, PHP_ROUND_HALF_ODD);
    }

    public function getCurrentDueTable() {
        $paymentCurrentDue = $this->payment_c_due_payment;
        if (is_string($paymentCurrentDue)) {
            $paymentCurrentDue = CJSON::decode($paymentCurrentDue);
        }
        $this->penaltyArrayDisplay = "<table><tbody>";
        foreach ($paymentCurrentDue as $index => $payment) {
            //var_dump($payment);
            $this->penaltyArrayDisplay .= ""
                    . "<tr><th>Due Date</th><td>{$payment['dueDate']}</td></tr>"
                    . "<tr><th title='Rental payable (If any excess deducted)'>Amount</th><td>{$this->formatValue($payment['amount'])}</td></tr>"
                    . "<tr><th title='Excess'>Excess</th><td>{$this->formatValue($payment['excess'])}</td></tr>"
                    . "<tr><th>Penalty</th><td>{$this->formatValue($payment['penalty'])}</td></tr>"
                    . "<tr><th>Payable</th><td>{$this->formatValue($payment['payable'])}</td></tr>"
                    . "<tr><th title='With grace period applied'>Late Days</th><td>{$payment['lateDays']}</td></tr>"
                    . "<th title='Total delayed days'>Delay</th><td>{$payment['delay']}</td></tr>"
                    . '<tr><th>____________</th><td>_________________________________</td></tr>';
        }
        $this->penaltyArrayDisplay .="</tbody></table>";
        return $this->penaltyArrayDisplay;
    }

    /**
     * This function returns the row color for Cgrid View
     */
    public function getGridRowColor($gracePeriod = 5) {
        $lateDays = 0;
        $paymentCurrentDue = $this->payment_c_due_payment;
        if (is_string($paymentCurrentDue)) {
            $paymentCurrentDue = CJSON::decode($paymentCurrentDue);
        }
        if ($this->payment_p_d_amount !== (double) 0 && isset($paymentCurrentDue[0]['lateDays'])) {
            $lateDays = $paymentCurrentDue[0]['lateDays'];
        } elseif (isset($paymentCurrentDue[1]['lateDays'])) {
            $lateDays = $paymentCurrentDue[1]['lateDays'];
        }
        $color = 'normal';
        if ($lateDays <= 0) {
            $color = 'no-delays';
        } elseif ($lateDays <= $gracePeriod) {
            $color = 'grace-delay';
        } elseif ($lateDays > $gracePeriod) {
            $color = 'very-delay';
        }
        return $color;
    }

    private function formatValue($value, $precision = 3){
        return Common::formatNumber($value, $precision);
    }
}
