<?php
/**
 * This is the model class for table "cm_period".
 *
 * The followings are the available columns in table 'cm_period':
 * @property string $period_id
 * @property string $period_name
 * @property integer $period_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property HipAgreement[] $hipAgreements
 */
class CmPeriod extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_period';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('period_name, updated_by', 'required'),
                        array('period_name, period_status', 'numerical', 'integerOnly'=>true),
			array('period_name, updated_by', 'length', 'max'=>10),
                        array('period_name', 'unique'),
                        array('period_name', 'padWithZero'),
                        
			array('period_id, period_name, period_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'period_id' => 'Period',
			'period_name' => 'Period Name',
                        'period_status' => 'Period Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('period_id',$this->period_id,true);
		$criteria->compare('period_name',$this->period_name,true);
		$criteria->compare('period_status',$this->period_status);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmPeriod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function padWithZero(){
            if(strlen($this->period_name)=== 1){
                $this->period_name = str_pad($this->period_name, 1, '0', STR_PAD_LEFT);
                return true;
            }
        }
}
