<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * This function mapps the model data
 */

final class CommonModelMapping {
    /* set Mapping models */

    /**
     * This function used in controllers where mapping is required
     * performs contrller specific required functions
     * @param String $modelClass Mapping model class
     * @param array  $selectedArray selected options id array of select input
     * @param string $attribute parent column name of the Mapping table
     * @param string $attributeId parent column value of the Mapping table
     * @param string $dynamicAttribute dynamic column name of the Mapping table (the mapped table id)
     * @param string $statusColumn Mapping table status column name
     * @param array $paramsArray additional attributes indexed as column name
     */
    public static function setModelMapping($modelClass, $selectedArray, $attribute, $attributeId, $dynamicAttribute, $statusColumn, $paramsArray = array()) {
        if ($selectedArray === NULL) {
            $selectedArray = array();
        }
        $allArray = array();
        $undeletedArray = array();
        $paramsCount = count($paramsArray);

        $modelAllRecords = self::getAllModelRecords($modelClass, $attribute, $attributeId);

        foreach ($modelAllRecords as $record) {
            if ($record->$statusColumn !== '2') {
                array_push($undeletedArray, $record->$dynamicAttribute);
            }
            array_push($allArray, $record->$dynamicAttribute);
        }

        $newRecords = array_diff($selectedArray, $allArray);
        $recordsToActive = array_diff($selectedArray, $undeletedArray, $newRecords);
        $recordsToDelete = array_diff($undeletedArray, $selectedArray);

        /* for not compulsary attribute nothing selected at all or all removed current links will be deleted */
        if (count($selectedArray) === 0) {
            $recordsToDelete = $undeletedArray;
        }
        
        if (count($newRecords) > 0) {
            foreach ($newRecords as $id) {
                $mappingModel = new $modelClass();
                $mappingModel->$attribute = $attributeId;
                $mappingModel->$dynamicAttribute = $id;
                $mappingModel->$statusColumn = 1;
                self::setModelLogginData($mappingModel);
                $mappingModel->save();
            }            
        }

        if (count($recordsToActive) > 0) {
            foreach ($recordsToActive as $id) {
                $mappingModel = $modelClass::model()->find(new CDbCriteria(array("condition" => $attribute . "= :attributeId AND " . $dynamicAttribute . " = :dynamicAttribute", "params" => array(":attributeId" => $attributeId, ":dynamicAttribute" => $id))));
                $mappingModel->$statusColumn = 1;
                self::setModelLogginData($mappingModel);
                $mappingModel->save();
            }
        }

        /* this function searches for the dynamic attribute column not the consistent column 
         * ex item supplier mapping
         * item_id consistent
         * supp_id dynamic
         */
        if (count($recordsToDelete) > 0) {
            foreach ($recordsToDelete as $id) {
                $mappingModel = $modelClass::model()->find(new CDbCriteria(array("condition" => $attribute . "= :attributeId AND " . $dynamicAttribute . " = :dynamicAttribute", "params" => array(":attributeId" => $attributeId, ":dynamicAttribute" => $id))));
                $mappingModel->$statusColumn = 2;
                self::setModelLogginData($mappingModel);
                $mappingModel->save();
            }
        }

        if ($paramsCount > 0) {
            $modelAllRecords = self::getAllModelRecords($modelClass, $attribute, $attributeId);
            foreach ($modelAllRecords as $modelRecord) {
                self::setModelMappingParams($paramsArray, $modelRecord);
                $modelRecord->save();
            }
        }
    }

    private static function setModelMappingParams($paramsArray, $mappingModel) {
        foreach ($paramsArray as $column => $value) {
            $mappingModel->$column = $value;
        }
    }

    private static function setModelLogginData($mappingModel) {
        $userId = CommonUser::getUserId();
        $mappingModel->updated_by = $userId;
        $mappingModel->updated_date = null;
    }

    /* end of Mapping models */

    public static function getEmployeeBranchesMapping($empId, $isWithDeleted = false) {
        $condition = 'emp_id = :empId AND  employee_branch_status !=\'2\'';
        if ($isWithDeleted) {
            $condition = 'emp_id = :empId';
        }
        return MapEmployeeBranch::model()->findAll(new CdbCriteria(array('condition' => $condition, 'params' => array(':empId' => $empId))));
    }

    public static function getAllModelRecords($modelClass, $attribute, $attributeId) {
        return $modelClass::model()->findAll(new CDbCriteria(array("condition" => $attribute . " = :attributeId", "params" => array(":attributeId" => $attributeId))));
    }

}
