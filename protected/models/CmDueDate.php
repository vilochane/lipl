<?php
/**
 * This is the model class for table "cm_due_date".
 *
 * The followings are the available columns in table 'cm_due_date':
 * @property string $due_date_id
 * @property integer $due_date_date
 * @property string $due_date_remark
 * @property integer $due_date_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property HipAgreement[] $hipAgreements
 */
class CmDueDate extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_due_date';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('due_date_date, updated_by', 'required'),                    
			array('due_date_date, due_date_status', 'numerical', 'integerOnly'=>true),
                        array('due_date_date',  'length', 'max'=>2),
			array('due_date_remark', 'length', 'max'=>50),
			array('updated_by', 'length', 'max'=>10),
                        array('due_date_date',  'match', 'pattern'=>'/^(([0]{1}[1-9]{1})|([1-2]{1}[0-9]{1})|([3]{1}[0-1]{1}))$/'),
                        array('due_date_remark', 'filter', 'filter'=>'ucwords'),
                        array('due_date_id, due_date_date, due_date_remark, due_date_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'due_date_id' => 'Date',
			'due_date_date' => 'Due Date',
			'due_date_remark' => 'Date Remark',
                        'due_date_status' => 'Due Date Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('due_date_id',$this->due_date_id,true);
		$criteria->compare('due_date_date',$this->due_date_date);
		$criteria->compare('due_date_remark',$this->due_date_remark,true);
                $criteria->compare('due_date_status',$this->due_date_status);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmDueDate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
