<?php

class HUploadedFile {

    public $model;
    public $attributeName;
    private $_path;
    private $_imageName;
    private $_oldImageName;
    private $_fileObject;

    public function __construct($path, $model, $attributeName) {
        $this->_path = trim($path);
        $this->model = $model;
        $this->attributeName = $attributeName;
        $this->_oldImageName = $model->$attributeName;
    }

    public function setFile() {
        $model = $this->model;
        $attributeName = $this->attributeName;
        $fileObject = $this->_fileObject = CUploadedFile::getInstance($model, $attributeName);
        if (!empty($fileObject) && is_object($fileObject)) {
            $rnd = mt_rand();
            $model->$attributeName = "{$rnd}_{$fileObject}";
        } else {
            $model->$attributeName = $this->_oldImageName;
        }
    }

    public function saveFile() {
        $fileObject = $this->_fileObject;
        $model = $this->model;
        $attributeName = $this->attributeName;
        $this->deleteIfChanged();
        if (!empty($fileObject) && is_object($fileObject)) {
            $fileObject->saveAs(Yii::app()->basePath . '/../' . $this->_path . '/' . $model->$attributeName);
        }
    }

    public function deleteIfChanged() {
        $model = $this->model;
        $attributeName = $this->attributeName;
        if (!empty($this->_oldImageName) && !empty($model->$attributeName) && ($this->_oldImageName !== $model->$attributeName)) {
            if (file_exists(Yii::app()->basePath . '/../' . $this->_path . '/' . $this->_oldImageName)) {
                unlink(Yii::app()->basePath . '/../' . $this->_path . '/' . $this->_oldImageName);
            }
        }
    }

}
