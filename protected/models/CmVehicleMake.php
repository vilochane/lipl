<?php

/**
 * This is the model class for table "cm_vehicle_make".
 *
 * The followings are the available columns in table 'cm_vehicle_make':
 * @property string $vehicle_make_id
 * @property string $vehicle_make_name
 * @property integer $vehicle_make_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property CmVehicleModel[] $cmVehicleModels
 * @property HipVehichleDetails[] $hipVehichleDetails
 */
class CmVehicleMake extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_vehicle_make';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('vehicle_make_name, vehicle_make_status, updated_by', 'required'),
			array('vehicle_make_status', 'numerical', 'integerOnly'=>true),
			array('vehicle_make_name', 'length', 'max'=>20),
			array('updated_by', 'length', 'max'=>10),
                        array('vehicle_make_name', 'filter', 'filter'=>'ucwords'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('vehicle_make_id, vehicle_make_name, vehicle_make_status, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cmVehicleModels' => array(self::HAS_MANY, 'CmVehicleModel', 'vehicle_make_id'),
			'hipVehichleDetails' => array(self::HAS_MANY, 'HipVehichleDetails', 'make_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'vehicle_make_id' => 'Vehicle Make',
			'vehicle_make_name' => 'Vehicle Make Name',
			'vehicle_make_status' => 'Vehicle Make Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('vehicle_make_id',$this->vehicle_make_id,true);
		$criteria->compare('vehicle_make_name',$this->vehicle_make_name,true);
		$criteria->compare('vehicle_make_status',$this->vehicle_make_status);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmVehicleMake the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
