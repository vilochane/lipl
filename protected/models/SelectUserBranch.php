<?php

/*
 * This class for branch select form
 */

class SelectUserBranch extends CFormModel{

    public $branch_id;

    public function rules() {

        return array(
            array('branch_id', 'required')
        );
    }

    public function attributeLabels() {
        return array(
            'branch_id' => 'Working Branch'
        );
    }

}
