<?php
/**
 * This is the model class for table "cm_residence_duration".
 *
 * The followings are the available columns in table 'cm_residence_duration':
 * @property string $residence_duration_id
 * @property string $residence_duration_name
 * @property integer $residence_duration_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeCusGuarantor[] $peCusGuarantors
 * @property PeCustomer[] $peCustomers
 */
class CmResidenceDuration extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_residence_duration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('residence_duration_name, updated_by', 'required'),
                        array('residence_duration_status', 'numerical', 'integerOnly'=>true),
			array('residence_duration_name', 'length', 'max'=>50),
			array('updated_by', 'length', 'max'=>10),
                        array('residence_duration_name', 'unique'),
                        array('residence_duration_name', 'filter', 'filter'=>'ucwords'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('residence_duration_id, residence_duration_name, residence_duration_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'residence_duration_id' => 'Residence Duration',
			'residence_duration_name' => 'Residence Duration Name',
                        'residence_duration_status' => 'Residence Duration Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('residence_duration_id',$this->residence_duration_id,true);
		$criteria->compare('residence_duration_name',$this->residence_duration_name,true);
		$criteria->compare('residence_duration_status',$this->residence_duration_status);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmResidenceDuration the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
