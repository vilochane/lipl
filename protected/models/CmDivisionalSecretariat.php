<?php
/**
 * This is the model class for table "cm_divisional_secretariat".
 *
 * The followings are the available columns in table 'cm_divisional_secretariat':
 * @property string $divisional_secretariat_id
 * @property string $divisional_secretariat_name
 * @property integer $divisional_secretariat_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeCustomer[] $peCustomers
 */
class CmDivisionalSecretariat extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_divisional_secretariat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('divisional_secretariat_name, updated_by', 'required'),
                        array('divisional_secretariat_status', 'numerical', 'integerOnly'=>true),
			array('divisional_secretariat_name', 'length', 'max'=>100),
			array('updated_by', 'length', 'max'=>10),
                        array('divisional_secretariat_name', 'filter', 'filter'=>'ucwords'),
			array('divisional_secretariat_id, divisional_secretariat_name, divisional_secretariat_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'peCustomers' => array(self::HAS_MANY, 'PeCustomer', 'divisional_secretariat_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'divisional_secretariat_id' => 'Divisional Secretariat',
			'divisional_secretariat_name' => 'Divisional Secretariat Name',
                        'divisional_secretariat_status' => 'Divisional Secretariat Status', 
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('divisional_secretariat_id',$this->divisional_secretariat_id,true);
		$criteria->compare('divisional_secretariat_name',$this->divisional_secretariat_name,true);
                $criteria->compare('divisional_secretariat_status',$this->divisional_secretariat_status);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmDivisionalSecretariat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
