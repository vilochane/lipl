<?php

/**
 * This is the model class for table "sys_user".
 *
 * The followings are the available columns in table 'sys_user':
 * @property string $user_id
 * @property string $user_role_id
 * @property string $emp_id
 * @property string $user_username
 * @property string $user_password
 * @property integer $user_status
 * @property integer $user_su_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property MapEmployeeBranch[] $mapEmployeeBranches
 * @property SysUserRole $userRole
 * @property PeEmployee $emp
 */
class SysUser extends CActiveRecord {

    public $passwordRepeat;
    public $oldPassword;
    public $newPassword;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'sys_user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_role_id, user_username, updated_by', 'required'),
            array('user_username', 'unique'),
            array('user_status, user_su_status', 'numerical', 'integerOnly' => true),
            array('user_role_id, emp_id, updated_by', 'length', 'max' => 10),
            array('user_username', 'length', 'max' => 50),
            array('user_password , newPassword', 'length', 'min' => 6, 'max' => 100),
            array('user_password', 'required', 'on' => 'insert'),
            array('passwordRepeat', 'compare', 'on' => 'insert', 'compareAttribute' => 'user_password', 'message' => 'Confirm password must be repeated exactly.'),
            array('oldPassword', 'setNewPassword', 'on' => 'update'),
            array('oldPassword, newPassword, passwordRepeat', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('user_id, user_role_id, emp_id, user_username, user_password, user_status, user_su_status, updated_by, updated_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'mapEmployeeBranches' => array(self::HAS_MANY, 'MapEmployeeBranch', 'user_id'),
            'userRole' => array(self::BELONGS_TO, 'SysUserRole', 'user_role_id'),
            'peEmployee' => array(self::BELONGS_TO, 'PeEmployee', 'emp_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'user_id' => 'User',
            'user_role_id' => 'Role',
            'emp_id' => 'Emp',
            'user_username' => 'Username',
            'user_password' => 'Password',
            'passwordRepeat' => 'Confirm Password',
            'oldPassword' => 'Current Password',
            'newPassword' => 'New Password',
            'user_status' => 'User Status',
            'user_su_status' => 'User Su Status',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('user_role_id', $this->user_role_id, true);
        $criteria->compare('emp_id', $this->emp_id, true);
        $criteria->compare('user_username', $this->user_username, true);
        $criteria->compare('user_password', $this->user_password, true);
        $criteria->compare('user_status', $this->user_status);
        $criteria->compare('user_su_status', $this->user_su_status);
        $criteria->compare('updated_by', $this->updated_by, true);
        $criteria->compare('updated_date', $this->updated_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SysUser the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function setNewPassword() {
        if (!empty($this->oldPassword)) {
            if (CommonSystem::hashString($this->oldPassword) !== $this->user_password) {
                $this->addError('oldPassword', 'Incorrect current password.');
                return false;
            } else {
                $this->isNewPassword();
            }
        } else {
            return true;
        }
    }

    public function isNewPassword() {
        if (empty($this->newPassword)) {
            $this->addError('newPassword', 'New password cannot be blank.');
            return false;
        } elseif (empty($this->passwordRepeat)) {
            $this->addError('passwordRepeat', 'Confirm password cannot be blank.');
            return false;
        } elseif ($this->newPassword === $this->oldPassword) {
            $this->addError('newPassword', 'New password cannot be current password.');
            return false;
        } elseif ($this->newPassword !== $this->passwordRepeat) {
            $this->addError('passwordRepeat', 'Confirm password must be repeated exactly.');
            return false;
        } else {
            return true;
        }
    }

    public function hashUserPassword() {
        $this->user_password = CommonSystem::hashString($this->user_password);
    }

    public function beforeSave() {
        if ($this->isNewRecord) {
            $this->hashUserPassword();
        } elseif (!empty($this->newPassword)) {
            $this->user_password = $this->newPassword;
            $this->hashUserPassword();
        }
        return true;
    }

}
