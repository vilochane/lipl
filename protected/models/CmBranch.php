<?php

/**
 * This is the model class for table "cm_branch".
 *
 * The followings are the available columns in table 'cm_branch':
 * @property string $branch_id
 * @property string $branch_name
 * @property string $emp_id
 * @property string $branch_address
 * @property integer $branch_telephone
 * @property integer $branch_telephone2
 * @property integer $branch_fax
 * @property integer $branch_fax2
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeEmployee[] $peEmployees
 */
class CmBranch extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_branch';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('branch_name, branch_address, branch_telephone, updated_by', 'required'),
			array('branch_telephone, branch_telephone2, branch_fax, branch_fax2', 'numerical', 'integerOnly'=>true),
			array('branch_name', 'length', 'max'=>50),
			array('emp_id, updated_by', 'length', 'max'=>10),
			array('branch_address', 'length', 'max'=>200),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('branch_id, branch_name, emp_id, branch_address, branch_telephone, branch_telephone2, branch_fax, branch_fax2, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manager' => array(self::HAS_ONE, 'PeEmployee', 'emp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'branch_id' => 'Branch',
			'branch_name' => 'Name',
			'emp_id' => 'Manager',
			'branch_address' => 'Address',
			'branch_telephone' => 'Telephone',
			'branch_telephone2' => 'Telephone Secondary',
			'branch_fax' => 'Fax',
			'branch_fax2' => 'Fax Secondary',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('branch_id',$this->branch_id,true);
		$criteria->compare('branch_name',$this->branch_name,true);
		$criteria->compare('emp_id',$this->emp_id,true);
		$criteria->compare('branch_address',$this->branch_address,true);
		$criteria->compare('branch_telephone',$this->branch_telephone);
		$criteria->compare('branch_telephone2',$this->branch_telephone2);
		$criteria->compare('branch_fax',$this->branch_fax);
		$criteria->compare('branch_fax2',$this->branch_fax2);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmBranch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
