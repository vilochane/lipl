<?php

/**
 * This is the model class for table "cm_trade_experience".
 *
 * The followings are the available columns in table 'cm_trade_experience':
 * @property string $trade_experience_id
 * @property string $trade_experience_name
 * @property integer $trade_experience_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeCusGuarantor[] $peCusGuarantors
 */
class CmTradeExperience extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_trade_experience';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trade_experience_name, updated_by, updated_date', 'required'),
                        array('trade_experience_status', 'numerical', 'integerOnly'=>true),
			array('trade_experience_name', 'length', 'max'=>20),
			array('updated_by', 'length', 'max'=>10),
                        array('trade_experience_name', 'filter', 'filter'=>'ucwords'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('trade_experience_id, trade_experience_name, trade_experience_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'peCusGuarantors' => array(self::HAS_MANY, 'PeCusGuarantor', 'trade_experience_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'trade_experience_id' => 'Trade Experience',
			'trade_experience_name' => 'Trade Experience Name',
                        'trade_experience_status' => 'Trade Experience Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('trade_experience_id',$this->trade_experience_id,true);
		$criteria->compare('trade_experience_name',$this->trade_experience_name,true);
		$criteria->compare('trade_experience_status',$this->trade_experience_status);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmTradeExperience the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
