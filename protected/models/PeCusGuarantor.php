<?php

/**
 * This is the model class for table "pe_cus_guarantor".
 *
 * The followings are the available columns in table 'pe_cus_guarantor':
 * @property string $guarantor_id
 * @property string $branch_id
 * @property string $cus_id
 * @property string $residence_duration_id
 * @property string $guarantor_emp_pro
 * @property string $guarantor_emp_period_id
 * @property string $guarantor_semp_pro
 * @property string $guarantor_semp_period_id
 * @property string $trade_experience_id
 * @property string $operation_period_id
 * @property string $guarantor_first_name
 * @property string $guarantor_middle_name
 * @property string $guarantor_last_name
 * @property string $guarantor_initials_name
 * @property integer $guarantor_gender
 * @property integer $guarantor_marital_status
 * @property integer $guarantor_nationality
 * @property string $guarantor_dob
 * @property string $guarantor_nic
 * @property string $guarantor_slin
 * @property string $guarantor_drivers_license
 * @property string $guarantor_passport_no
 * @property string $guarantor_address
 * @property string $guarantor_official_address
 * @property integer $guarantor_resident_info
 * @property string $guarantor_telephone
 * @property string $guarantor_telephone2
 * @property string $guarantor_office_tp
 * @property string $guarantor_office_fax
 * @property string $guarantor_mobile
 * @property string $guarantor_mobile2
 * @property string $guarantor_email
 * @property string $guarantor_qualification
 * @property string $guarantor_emp_name
 * @property string $guarantor_emp_place
 * @property integer $guarantor_emp_status
 * @property string $guarantor_emp_sal
 * @property string $guarantor_semp_name
 * @property string $guarantor_semp_place
 * @property integer $guarantor_semp_status
 * @property string $guarantor_semp_sal
 * @property integer $guarantor_organization
 * @property string $guarantor_bis_reg
 * @property string $guarantor_bis_reg_date
 * @property string $guarantor_bis_nat
 * @property string $guarantor_bis_add
 * @property integer $guarantor_bis_prem
 * @property integer $guarantor_bis_emp_no
 * @property string $guarantor_income
 * @property integer $guarantor_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property CmBranch $branch
 * @property PeCustomer $cus
 * @property CmResidenceDuration $residenceDuration
 * @property CmProfession $guarantorEmpPro
 * @property CmEmpPeriod $guarantorEmpPeriod
 * @property CmProfession $guarantorSempPro
 * @property CmEmpPeriod $guarantorSempPeriod
 * @property CmTradeExperience $tradeExperience
 * @property CmOperationPeriod $operationPeriod
 * @property PeCusGuarantorBankDetails[] $peCusGuarantorBankDetails
 */
class PeCusGuarantor extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pe_cus_guarantor';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('branch_id, cus_id, residence_duration_id, guarantor_first_name, guarantor_initials_name, guarantor_gender, guarantor_marital_status, guarantor_nationality, guarantor_nic, guarantor_address, guarantor_resident_info, guarantor_mobile, guarantor_income, updated_by, updated_date', 'required'),
            array('guarantor_gender, guarantor_marital_status, guarantor_nationality, guarantor_resident_info, guarantor_emp_status, guarantor_semp_status, guarantor_organization, guarantor_bis_prem, guarantor_bis_emp_no, guarantor_status', 'numerical', 'integerOnly' => true),
            array('branch_id, cus_id, residence_duration_id, guarantor_emp_pro, guarantor_emp_period_id, guarantor_semp_pro, guarantor_semp_period_id, trade_experience_id, operation_period_id, guarantor_nic, guarantor_bis_reg, updated_by', 'length', 'max' => 10),
            array('guarantor_first_name, guarantor_middle_name, guarantor_last_name, guarantor_emp_name, guarantor_semp_name', 'length', 'max' => 50),
            array('guarantor_initials_name, guarantor_address, guarantor_official_address, guarantor_qualification, guarantor_emp_place, guarantor_semp_place, guarantor_bis_nat, guarantor_bis_add', 'length', 'max' => 200),
            array('guarantor_slin', 'length', 'max' => 13),
            array('guarantor_drivers_license', 'length', 'max' => 7),
            array('guarantor_passport_no', 'length', 'max' => 5),
            array('guarantor_telephone, guarantor_telephone2, guarantor_office_tp, guarantor_office_fax, guarantor_mobile, guarantor_mobile2', 'length', 'max' => 15),
            array('guarantor_email', 'length', 'max' => 150),
            array('guarantor_emp_sal, guarantor_semp_sal, guarantor_income', 'length', 'max' => 20),
            array('guarantor_middle_name, guarantor_last_name, guarantor_dob, guarantor_bis_reg_date', 'safe'),
            array('guarantor_email', 'email'),            
            array('guarantor_nic, guarantor_slin, guarantor_drivers_license, guarantor_passport_no, guarantor_bis_reg', 'filter', 'filter'=>'strtoupper'),
            array('guarantor_nic', 'match', 'pattern'=>'/^([0-9]{9}[X|V])$/'),
            array('guarantor_first_name, guarantor_middle_name, guarantor_last_name, guarantor_initials_name, guarantor_address, guarantor_official_address'
                . ', guarantor_qualification, guarantor_bis_nat, guarantor_bis_add', 'filter', 'filter'=>'ucwords'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('guarantor_id, branch_id, cus_id, residence_duration_id, guarantor_emp_pro, guarantor_emp_period_id, guarantor_semp_pro, guarantor_semp_period_id, trade_experience_id, operation_period_id, guarantor_first_name, guarantor_middle_name, guarantor_last_name, guarantor_initials_name, guarantor_gender, guarantor_marital_status, guarantor_nationality, guarantor_dob, guarantor_nic, guarantor_slin, guarantor_drivers_license, guarantor_passport_no, guarantor_address, guarantor_official_address, guarantor_resident_info, guarantor_telephone, guarantor_telephone2, guarantor_office_tp, guarantor_office_fax, guarantor_mobile, guarantor_mobile2, guarantor_email, guarantor_qualification, guarantor_emp_name, guarantor_emp_place, guarantor_emp_status, guarantor_emp_sal, guarantor_semp_name, guarantor_semp_place, guarantor_semp_status, guarantor_semp_sal, guarantor_organization, guarantor_bis_reg, guarantor_bis_reg_date, guarantor_bis_nat, guarantor_bis_add, guarantor_bis_prem, guarantor_bis_emp_no, guarantor_income, guarantor_status, updated_by, updated_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'branch' => array(self::BELONGS_TO, 'CmBranch', 'branch_id'),
            'cus' => array(self::BELONGS_TO, 'PeCustomer', 'cus_id'),
            'residenceDuration' => array(self::BELONGS_TO, 'CmResidenceDuration', 'residence_duration_id'),
            'guarantorEmpPro' => array(self::BELONGS_TO, 'CmProfession', 'guarantor_emp_pro'),
            'guarantorEmpPeriod' => array(self::BELONGS_TO, 'CmEmpPeriod', 'guarantor_emp_period_id'),
            'guarantorSempPro' => array(self::BELONGS_TO, 'CmProfession', 'guarantor_semp_pro'),
            'guarantorSempPeriod' => array(self::BELONGS_TO, 'CmEmpPeriod', 'guarantor_semp_period_id'),
            'tradeExperience' => array(self::BELONGS_TO, 'CmTradeExperience', 'trade_experience_id'),
            'operationPeriod' => array(self::BELONGS_TO, 'CmOperationPeriod', 'operation_period_id'),
            'peCusGuarantorBankDetails' => array(self::HAS_MANY, 'PeCusGuarantorBankDetails', 'guarantor_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'guarantor_id' => 'Guarantor',
            'branch_id' => 'Branch',
            'cus_id' => 'Customer',
            'residence_duration_id' => 'Residence Duration',
            'guarantor_emp_pro' => 'Guarantor Profession',
            'guarantor_emp_period_id' => 'Guarantor Employment Period',
            'guarantor_semp_pro' => 'Spouse Profession',
            'guarantor_semp_period_id' => 'Spouse Employment Period',
            'trade_experience_id' => 'Experience in Trade',
            'operation_period_id' => 'Operation Period',
            'guarantor_first_name' => 'First Name',
            'guarantor_middle_name' => 'Middle Name',
            'guarantor_last_name' => 'Last Name',
            'guarantor_initials_name' => 'Name with Initials',
            'guarantor_gender' => 'Gender',
            'guarantor_marital_status' => 'Marital Status',
            'guarantor_nationality' => 'Nationality',
            'guarantor_dob' => 'D.O.B.',
            'guarantor_nic' => 'N.I.C.',
            'guarantor_slin' => 'S.L.I.N.',
            'guarantor_drivers_license' => 'Drivers License No.',
            'guarantor_passport_no' => 'Guarantor Passport No.',
            'guarantor_address' => 'Address',
            'guarantor_official_address' => 'Official Address',
            'guarantor_resident_info' => 'Resident Info',
            'guarantor_telephone' => 'Telephone',
            'guarantor_telephone2' => 'Telephone Secondary',
            'guarantor_office_tp' => 'Office Telephone',
            'guarantor_office_fax' => 'Office Fax',
            'guarantor_mobile' => 'Mobile',
            'guarantor_mobile2' => 'Mobile Secondary',
            'guarantor_email' => 'Email',
            'guarantor_qualification' => 'Qualification',
            'guarantor_emp_name' => 'Company Name',
            'guarantor_emp_place' => 'Company Address',
            'guarantor_emp_status' => 'Employment Status',
            'guarantor_emp_sal' => 'Employment Salary',
            'guarantor_semp_name' => 'Company Name',
            'guarantor_semp_place' => 'Company Address',
            'guarantor_semp_status' => 'Employment Status',
            'guarantor_semp_sal' => 'Employment Salary',
            'guarantor_organization' => 'Type of guarantor',
            'guarantor_bis_reg' => 'Business Registration No.',
            'guarantor_bis_reg_date' => 'Business Registration Date',
            'guarantor_bis_nat' => 'Nature of Business',
            'guarantor_bis_add' => 'Additional Business Activities',
            'guarantor_bis_prem' => 'Ownership of Business Premises',
            'guarantor_bis_emp_no' => 'No of employees other than immidiate family members',
            'guarantor_income' => 'Total Monthly Income',
            'guarantor_status' => 'Guarantor Status',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $cusId = Yii::app()->controller->getUrlQuery('c');
        if (!empty($cusId)) {
            $this->cus_id = $cusId;
        }
        $criteria = new CDbCriteria;

        $criteria->compare('guarantor_id', $this->guarantor_id);
        $criteria->compare('branch_id', $this->branch_id);
        $criteria->compare('cus_id', $this->cus_id);
        $criteria->compare('residence_duration_id', $this->residence_duration_id);
        $criteria->compare('guarantor_emp_pro', $this->guarantor_emp_pro);
        $criteria->compare('guarantor_emp_period_id', $this->guarantor_emp_period_id);
        $criteria->compare('guarantor_semp_pro', $this->guarantor_semp_pro);
        $criteria->compare('guarantor_semp_period_id', $this->guarantor_semp_period_id);
        $criteria->compare('trade_experience_id', $this->trade_experience_id);
        $criteria->compare('operation_period_id', $this->operation_period_id);
        $criteria->compare('guarantor_first_name', $this->guarantor_first_name, true);
        $criteria->compare('guarantor_middle_name', $this->guarantor_middle_name, true);
        $criteria->compare('guarantor_last_name', $this->guarantor_last_name, true);
        $criteria->compare('guarantor_initials_name', $this->guarantor_initials_name, true);
        $criteria->compare('guarantor_gender', $this->guarantor_gender);
        $criteria->compare('guarantor_marital_status', $this->guarantor_marital_status);
        $criteria->compare('guarantor_nationality', $this->guarantor_nationality);
        $criteria->compare('guarantor_dob', $this->guarantor_dob, true);
        $criteria->compare('guarantor_nic', $this->guarantor_nic, true);
        $criteria->compare('guarantor_slin', $this->guarantor_slin, true);
        $criteria->compare('guarantor_drivers_license', $this->guarantor_drivers_license, true);
        $criteria->compare('guarantor_passport_no', $this->guarantor_passport_no, true);
        $criteria->compare('guarantor_address', $this->guarantor_address, true);
        $criteria->compare('guarantor_official_address', $this->guarantor_official_address, true);
        $criteria->compare('guarantor_resident_info', $this->guarantor_resident_info);
        $criteria->compare('guarantor_telephone', $this->guarantor_telephone, true);
        $criteria->compare('guarantor_telephone2', $this->guarantor_telephone2, true);
        $criteria->compare('guarantor_office_tp', $this->guarantor_office_tp, true);
        $criteria->compare('guarantor_office_fax', $this->guarantor_office_fax, true);
        $criteria->compare('guarantor_mobile', $this->guarantor_mobile, true);
        $criteria->compare('guarantor_mobile2', $this->guarantor_mobile2, true);
        $criteria->compare('guarantor_email', $this->guarantor_email, true);
        $criteria->compare('guarantor_qualification', $this->guarantor_qualification, true);
        $criteria->compare('guarantor_emp_name', $this->guarantor_emp_name, true);
        $criteria->compare('guarantor_emp_place', $this->guarantor_emp_place, true);
        $criteria->compare('guarantor_emp_status', $this->guarantor_emp_status);
        $criteria->compare('guarantor_emp_sal', $this->guarantor_emp_sal, true);
        $criteria->compare('guarantor_semp_name', $this->guarantor_semp_name, true);
        $criteria->compare('guarantor_semp_place', $this->guarantor_semp_place, true);
        $criteria->compare('guarantor_semp_status', $this->guarantor_semp_status);
        $criteria->compare('guarantor_semp_sal', $this->guarantor_semp_sal, true);
        $criteria->compare('guarantor_organization', $this->guarantor_organization);
        $criteria->compare('guarantor_bis_reg', $this->guarantor_bis_reg, true);
        $criteria->compare('guarantor_bis_reg_date', $this->guarantor_bis_reg_date, true);
        $criteria->compare('guarantor_bis_nat', $this->guarantor_bis_nat, true);
        $criteria->compare('guarantor_bis_add', $this->guarantor_bis_add, true);
        $criteria->compare('guarantor_bis_prem', $this->guarantor_bis_prem);
        $criteria->compare('guarantor_bis_emp_no', $this->guarantor_bis_emp_no);
        $criteria->compare('guarantor_income', $this->guarantor_income, true);
        $criteria->compare('guarantor_status', $this->guarantor_status);
        $criteria->compare('updated_by', $this->updated_by, true);
        $criteria->compare('updated_date', $this->updated_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PeCusGuarantor the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
	
	public function beforeSave(){
		$tradeExperiance = $this->trade_experience_id;
		$empPro = $this->guarantor_emp_pro;
		$opPeriod = $this->operation_period_id;
		$empPeriod = $this->guarantor_emp_period_id;
		$sEmpPro = $this->guarantor_semp_pro;
		$sEmpPeriod = $this->guarantor_semp_period_id;
		
		if(empty($tradeExperiance)){
			$this->trade_experience_id = null;
		}
		if(empty($empPro)){
			$this->guarantor_emp_pro = null;
		}
		if(empty($opPeriod)){
			$this->operation_period_id = null;
		}
		if(empty($empPeriod)){
			$this->guarantor_emp_period_id = null;
		}
		if(empty($sEmpPro)){
			$this->guarantor_semp_pro = null;
		}
		if(empty($sEmpPeriod)){
			$this->guarantor_semp_period_id = null;
		}
		return true;
		
	}

}
