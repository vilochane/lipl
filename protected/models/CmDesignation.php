<?php
/**
 * This is the model class for table "cm_designation".
 *
 * The followings are the available columns in table 'cm_designation':
 * @property string $designation_id
 * @property string $designation_name
 * @property string $designation_remark
 * @property integer $designation_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeEmployee[] $peEmployees
 */
class CmDesignation extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_designation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('designation_name, updated_by', 'required'),
                        array('designation_status', 'numerical', 'integerOnly'=>true),
			array('designation_name', 'length', 'max'=>50),
			array('designation_remark', 'length', 'max'=>100),
			array('updated_by', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('designation_id, designation_name, designation_remark, designation_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'peEmployees' => array(self::HAS_MANY, 'PeEmployee', 'designation_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'designation_id' => 'Designation',
			'designation_name' => 'Designation Name',
			'designation_remark' => 'Designation Remark',
                        'designation_status' => 'Designation Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('designation_id',$this->designation_id,true);
		$criteria->compare('designation_name',$this->designation_name,true);
		$criteria->compare('designation_remark',$this->designation_remark,true);
                $criteria->compare('designation_status',$this->designation_status);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmDesignation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
