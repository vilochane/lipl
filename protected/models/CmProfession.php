<?php
/**
 * This is the model class for table "cm_profession".
 *
 * The followings are the available columns in table 'cm_profession':
 * @property string $profession_id
 * @property string $profession_name
 * @property integer $profession_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeCusGuarantor[] $peCusGuarantors
 * @property PeCusGuarantor[] $peCusGuarantors1
 * @property PeCustomer[] $peCustomers
 */
class CmProfession extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_profession';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profession_name, updated_by', 'required'),
                        array('profession_status', 'numerical', 'integerOnly'=>true),
			array('profession_name', 'length', 'max'=>50),
			array('updated_by', 'length', 'max'=>10),
                        array('profession_name', 'unique'),
                        array('profession_name', 'filter', 'filter'=>'ucwords'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('profession_id, profession_name, profession_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'profession_id' => 'Profession',
			'profession_name' => 'Profession Name',
                        'profession_status' => 'Profession Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('profession_id',$this->profession_id,true);
		$criteria->compare('profession_name',$this->profession_name,true);
		$criteria->compare('profession_status',$this->profession_status);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmProfession the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
