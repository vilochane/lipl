<?php

/**
 * This is the model class for table "pe_customer".
 *
 * The followings are the available columns in table 'pe_customer':
 * @property string $cus_id
 * @property string $branch_id
 * @property string $divisional_secretariat_id
 * @property string $residence_duration_id
 * @property string $profession_id
 * @property string $cus_first_name
 * @property string $cus_middle_name
 * @property string $cus_last_name
 * @property integer $cus_gender
 * @property integer $cus_marital_status
 * @property integer $cus_nationality
 * @property string $cus_nic
 * @property string $cus_slin
 * @property string $cus_drivers_license
 * @property string $cus_passport_no
 * @property string $cus_dob
 * @property string $cus_address
 * @property string $cus_current_address
 * @property string $cus_official_address
 * @property string $cus_employer_address
 * @property integer $cus_resident_info
 * @property integer $cus_organization
 * @property string $cus_telephone
 * @property string $cus_telephone2
 * @property string $cus_mobile
 * @property string $cus_mobile2
 * @property string $cus_email
 * @property string $cus_email2
 * @property string $cus_income_source
 * @property string $cus_net_income
 * @property string $cus_assets
 * @property string $cus_spouse_assets
 * @property string $cus_introducer_name
 * @property string $cus_introducer_telephone
 * @property string $cus_introducer_mobile
 * @property string $cus_introducer_address
 * @property string $cus_nic_scan_copy
 * @property string $cus_image
 * @property integer $cus_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property HipAgreement[] $hipAgreements
 * @property PeCusBankDetails[] $peCusBankDetails
 * @property PeCusCreditRefDetails[] $peCusCreditRefDetails
 * @property CmBranch $branch
 * @property CmDivisionalSecretariat $divisionalSecretariat
 * @property CmResidenceDuration $residenceDuration
 * @property CmProfession $profession
 */
class PeCustomer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('branch_id, divisional_secretariat_id, residence_duration_id, profession_id, cus_first_name, cus_gender, cus_nic, cus_address, cus_resident_info, cus_mobile, cus_income_source, cus_net_income, updated_by, updated_date', 'required'),
			array('cus_gender, cus_marital_status, cus_nationality, cus_resident_info, cus_organization, cus_status', 'numerical', 'integerOnly'=>true),
			array('branch_id, divisional_secretariat_id, residence_duration_id, profession_id, cus_nic, updated_by', 'length', 'max'=>10),
			array('cus_first_name, cus_middle_name, cus_last_name, cus_nic_scan_copy, cus_image', 'length', 'max'=>100),
			array('cus_slin', 'length', 'max'=>13),
			array('cus_drivers_license', 'length', 'max'=>7),
			array('cus_passport_no', 'length', 'max'=>5),
			array('cus_address, cus_current_address, cus_official_address, cus_employer_address, cus_income_source, cus_assets, cus_spouse_assets, cus_introducer_address', 'length', 'max'=>200),
			array('cus_telephone, cus_telephone2, cus_mobile, cus_mobile2, cus_introducer_telephone, cus_introducer_mobile', 'length', 'max'=>15),
			array('cus_email, cus_email2', 'length', 'max'=>150),
			array('cus_net_income', 'length', 'max'=>20),
			array('cus_introducer_name', 'length', 'max'=>50),
			array('cus_middle_name, cus_last_name, cus_dob, cus_employer_address', 'safe'),
                        array('cus_email, cus_email2', 'email'),
                        array('cus_nic', 'match', 'pattern'=>'/^([0-9]{9}[X|V])$/'),
                        array('cus_nic, cus_drivers_license, cus_passport_no', 'filter', 'filter'=>'strtoupper'),
                        array('cus_first_name, cus_middle_name, cus_last_name, cus_address, cus_current_address, cus_official_address, cus_employer_address, '
                            . ', cus_income_source, cus_assets, cus_spouse_assets, cus_introducer_name, cus_introducer_address', 'filter', 'filter'=>'ucwords'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cus_id, branch_id, divisional_secretariat_id, residence_duration_id, profession_id, cus_first_name, cus_middle_name, cus_last_name, cus_gender, cus_marital_status, cus_nationality, cus_nic, cus_slin, cus_drivers_license, cus_passport_no, cus_dob, cus_address, cus_current_address, cus_official_address, cus_employer_address, cus_resident_info, cus_organization, cus_telephone, cus_telephone2, cus_mobile, cus_mobile2, cus_email, cus_email2, cus_income_source, cus_net_income, cus_assets, cus_spouse_assets, cus_introducer_name, cus_introducer_telephone, cus_introducer_mobile, cus_introducer_address, cus_nic_scan_copy, cus_image, cus_status, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hipAgreements' => array(self::HAS_MANY, 'HipAgreement', 'cus_id'),
			'peCusBankDetails' => array(self::HAS_MANY, 'PeCusBankDetails', 'cus_id'),
			'peCusCreditRefDetails' => array(self::HAS_MANY, 'PeCusCreditRefDetails', 'cus_id'),
			'branch' => array(self::BELONGS_TO, 'CmBranch', 'branch_id'),
			'divisionalSecretariat' => array(self::BELONGS_TO, 'CmDivisionalSecretariat', 'divisional_secretariat_id'),
			'residenceDuration' => array(self::BELONGS_TO, 'CmResidenceDuration', 'residence_duration_id'),
			'profession' => array(self::BELONGS_TO, 'CmProfession', 'profession_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cus_id' => 'Cus',
			'branch_id' => 'Branch',
			'divisional_secretariat_id' => 'Divisional Secretariat',
			'residence_duration_id' => 'Residence Duration',
			'profession_id' => 'Profession',
			'cus_first_name' => 'First Name',
			'cus_middle_name' => 'Middle Name',
			'cus_last_name' => 'Last Name',
			'cus_gender' => 'Gender',
			'cus_marital_status' => 'Marital Status',
			'cus_nationality' => 'Nationality',
			'cus_nic' => 'N.I.C.',
			'cus_slin' => 'S.L.I.N.',
			'cus_drivers_license' => 'Drivers License No.',
			'cus_passport_no' => 'Passport No.',
			'cus_dob' => 'D.O.B.',
			'cus_address' => 'Address',
			'cus_current_address' => 'Current Address',
			'cus_official_address' => 'Official Address',
			'cus_employer_address' => 'Employer Address',
			'cus_resident_info' => 'Resident Info',
			'cus_organization' => 'If an Organization',
			'cus_telephone' => 'Telephone',
			'cus_telephone2' => 'Telephone Secondary',
			'cus_mobile' => 'Mobile',
			'cus_mobile2' => 'Mobile Secondary',
			'cus_email' => 'Email',
			'cus_email2' => 'Email Secondary',
			'cus_income_source' => 'Income Source',
			'cus_net_income' => 'Net Income',
			'cus_assets' => 'Assets',
			'cus_spouse_assets' => 'Spouse Assets',
			'cus_introducer_name' => 'Introducer Name',
			'cus_introducer_telephone' => 'Introducer Telephone',
			'cus_introducer_mobile' => 'Introducer Mobile',
			'cus_introducer_address' => 'Introducer Address',
			'cus_nic_scan_copy' => 'NIC Scan Copy',
			'cus_image' => 'Route Image',
			'cus_status' => 'Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cus_id',$this->cus_id,true);
		$criteria->compare('branch_id',$this->branch_id,true);
		$criteria->compare('divisional_secretariat_id',$this->divisional_secretariat_id,true);
		$criteria->compare('residence_duration_id',$this->residence_duration_id,true);
		$criteria->compare('profession_id',$this->profession_id,true);
		$criteria->compare('cus_first_name',$this->cus_first_name,true);
		$criteria->compare('cus_middle_name',$this->cus_middle_name,true);
		$criteria->compare('cus_last_name',$this->cus_last_name,true);
		$criteria->compare('cus_gender',$this->cus_gender);
		$criteria->compare('cus_marital_status',$this->cus_marital_status);
		$criteria->compare('cus_nationality',$this->cus_nationality);
		$criteria->compare('cus_nic',$this->cus_nic,true);
		$criteria->compare('cus_slin',$this->cus_slin,true);
		$criteria->compare('cus_drivers_license',$this->cus_drivers_license,true);
		$criteria->compare('cus_passport_no',$this->cus_passport_no,true);
		$criteria->compare('cus_dob',$this->cus_dob,true);
		$criteria->compare('cus_address',$this->cus_address,true);
		$criteria->compare('cus_current_address',$this->cus_current_address,true);
		$criteria->compare('cus_official_address',$this->cus_official_address,true);
		$criteria->compare('cus_employer_address',$this->cus_employer_address,true);
		$criteria->compare('cus_resident_info',$this->cus_resident_info);
		$criteria->compare('cus_organization',$this->cus_organization);
		$criteria->compare('cus_telephone',$this->cus_telephone,true);
		$criteria->compare('cus_telephone2',$this->cus_telephone2,true);
		$criteria->compare('cus_mobile',$this->cus_mobile,true);
		$criteria->compare('cus_mobile2',$this->cus_mobile2,true);
		$criteria->compare('cus_email',$this->cus_email,true);
		$criteria->compare('cus_email2',$this->cus_email2,true);
		$criteria->compare('cus_income_source',$this->cus_income_source,true);
		$criteria->compare('cus_net_income',$this->cus_net_income,true);
		$criteria->compare('cus_assets',$this->cus_assets,true);
		$criteria->compare('cus_spouse_assets',$this->cus_spouse_assets,true);
		$criteria->compare('cus_introducer_name',$this->cus_introducer_name,true);
		$criteria->compare('cus_introducer_telephone',$this->cus_introducer_telephone,true);
		$criteria->compare('cus_introducer_mobile',$this->cus_introducer_mobile,true);
		$criteria->compare('cus_introducer_address',$this->cus_introducer_address,true);
		$criteria->compare('cus_nic_scan_copy',$this->cus_nic_scan_copy,true);
		$criteria->compare('cus_image',$this->cus_image,true);
		$criteria->compare('cus_status',$this->cus_status);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PeCustomer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
