<?php

/*
 * This class contains system related functions
 */

final class CommonSystem {
    /* parameter encryption */

    /**
     * this function encrypts paramter
     */
    public static function encryptParam($param) {
        return base64_encode($param);
    }

    /**
     * This function decrypts parameter
     */
    public static function decryptParam($param) {
        return base64_decode($param);
    }

    /* end of parameter encryption */

    /* grid view */

    /**
     *  Cgrid view functions 
     * this function returns the grid view button column with required options
     * @param string $controller controller name
     * @param array $delete_confirm delete confirmation options 
     * eg - array('column'=>'branch', 'column_no' => '2')
     * @param array $template button template if no template given button will not be generated
     * eg - array('view', 'update', 'delete')
     * advanced
     * eg-  array('controller'=>array('action'=>array('label'=>'image or glype')))
     * sublink with very own get paramaters
     * 'StSerial'=>array('admin'=>array('label'=>'<span class="glyphicon glyphicon-th"></span>', 'params'=>array('stb'=>'$data->stock_ba_id')))
     * @param array $get_query get query params indexed with the query id
     * eg - array('id'=>'')
     * @param boolean $html_options to set the column with of grid
     */
    public static function getGridViewButtonColumn($controller, $delete_confirm = array(), $template = array(), $get_query = array(), $html_options = TRUE) {
        /* noty customization options
         * $deleteMessageType, $responseMessageOkType, $responseMessageCancelType, $responseMessageOkText, $responseMessageCancelText, $deleteMessageLayout, $deleteMessageModel, $btnPrimaryCss, $btnCancelCss
         * $responseMessageTimeout
         * 
         * ex
         * array(
         *  'class'=>'CButtonColumn',
         * same for the response message types
         *  'deleteMessageType' => 'warning', types 'alert, information, error, warning, notification, success'
         *  'deleteMessageLayout'=>'top, topCenter, topLeft, topRight, center, centerLeft, centerRight, bottom, bottomCenter, bottomLeft, bottomRight'
         *  'deleteMessageModel' => false
         *  'btnPrimaryCss'=>'btn btn-primary',
         *  'btnCancelCss'=>'btn btn-danger'
         * )
         */
        /* checks to wheather to inlcude id in another controller action */
        $get_query_array_string = $get_sub_link_query_array_string = '';
        $isIncludeIdInSubLink = true;
        $cBtutton_col_array = array('class' => 'CButtonColumn');

        if ($html_options) {
            $html_options_array = array('htmlOptions' => array('style' => self::gridViewButtonColumn()));
            $cBtutton_col_array = array_merge($cBtutton_col_array, $html_options_array);
        }

        if (count($delete_confirm) > 0) {
            $column = $delete_confirm['column'];
            $column_no = $delete_confirm['column_no'];
            $delete_confirmation_array = array('deleteConfirmation' => self::deleteConfirmationCGridView($column, $column_no));
            $cBtutton_col_array = array_merge($cBtutton_col_array, $delete_confirmation_array);
        }

        if (count($get_query) > 0) {
            foreach ($get_query as $get_param => $value) {
                $get_query_array_string .= '"' . $get_param . '"=>$data->encryptParam(' . $value . '),';
            }
        }

        if (count($template) > 0) {
            $url_label = '';
            $template_string = '';
            $buttons_array = array();
            /* $key can be controller or numric index $value can be action with options or just action */
            foreach ($template as $key => $value) {
                /* default controller */
                $url_controller = $controller;
                $action = $value;
                if (is_string($key)) {
                    $url_controller = $key;
                }

                if (is_array($value)) {
                    foreach ($value as $c_action => $options) {
                        $action = $c_action;
                        $url_label = $url_controller . $action;
                        if (isset($options['label']) && !empty($options['label'])) {
                            $url_label = $options['label'];
                        }
                        if (isset($options['params']) && is_array($options['params'])) {
                            $isIncludeIdInSubLink = false;
                            foreach ($options['params'] as $queryId => $param) {
                                $get_sub_link_query_array_string .= '"' . $queryId . '"=>$data->encryptParam(' . $param . '),';
                            }
                        }
                    }
                    $button_index = $url_controller . $action;
                    $template_string.='{' . $url_controller . $action . '}';
                } else {
                    $template_string.='{' . $action . '}';
                    $button_index = $value;
                }

                $url_action = '"' . $url_controller . '/' . $action . '"';
                $url = 'Yii::app()->createAbsoluteUrl(' . $url_action . ')';
                if (!empty($get_query_array_string) && $isIncludeIdInSubLink) {
                    $url = 'Yii::app()->createAbsoluteUrl(' . $url_action . ', array(' . $get_query_array_string . '))';
                } elseif (!empty($get_sub_link_query_array_string) && !$isIncludeIdInSubLink) {
                    $url = 'Yii::app()->createAbsoluteUrl(' . $url_action . ', array(' . $get_sub_link_query_array_string . '))';
                }
                $buttons_array[$button_index] = array('url' => $url);
                if (!empty($url_label)) {
                    $buttons_array[$button_index] = array('label' => $url_label, 'url' => $url);
                }
            }
            $cBtutton_col_array = array_merge($cBtutton_col_array, array('template' => $template_string, 'buttons' => $buttons_array));
        }
        return $cBtutton_col_array;
    }

    /* this function returns the common message format for grid view */

    public static function deleteConfirmationCGridView($column = null, $column_no = null) {
        return "js:'" . self::deleteMeassage($column) . "'+$(this).parent().parent().children(':nth-child($column_no)').text()+'?'";
    }

    /* common message format for gerid view and sub menus delete links */

    public static function deleteMeassage($column = null, $message = null) {
        if (empty($message)) {
            $message = 'Do you really want to delete ' . $column . ' ';
        }
        return $message;
    }

    public static function gridViewButtonColumnStyle($style_options = null) {
        $width = '10%';
        if (empty($style_options)) {
            $style_options = "width : $width";
        }
        return $style_options;
    }

    public static function initGridviewSelect2AfterAjax($select2_obj_array = array(), $js = null) {
        $jQuery_string = 'js: function(){';
        foreach ($select2_obj_array as $select2_obj) {
            $jQuery_string.= '$("#' . $select2_obj . '").select2({allowClear:true});';
        }
        $jQuery_string .= $js;
        $jQuery_string .= '}';
        return $jQuery_string;
    }

    /* end of grid view */

    /* password hashing */

    public static function hashString($string) {
        $salt1 = Yii::app()->params['sString1'];
        $salt2 = Yii::app()->params['sString2'];
        $password = $salt1 . $string . $salt2;
//        $cost = 15;
//        $result = password_hash($password, PASSWORD_BCRYPT, ['cost' => $cost]);
        $result = hash("sha256", $password);
        return $result;
    }

    /** Generating option tags for ajax functions
     * must index with the id and value
     * @param array $option_array array('value'=>'text')
     */
    public static function getAjaxOptionTags($model, $idColumn = null, $textColumn = null) {
        if (is_array($model)) {
            $option_array = $model;
        } else {
            $option_array = CHtml::listData($model::model()->findAll(), $idColumn, $textColumn);
        }
        $tags = CHtml::tag('option', array('value' => ''), '', TRUE);
        if (count($option_array) > 0) {
            foreach ($option_array as $id => $value) {
                $tags .= CHtml::tag('option', array('value' => $id), CHtml::encode($value), TRUE);
            }
        }
        return $tags;
    }

    /* dialog popup form ajax submit button whic appends the option tags to select and selects the new options so on */

    public static function getAjaxSubmitButton($url, $dialogId, $select2Id, $other = null) {
        $ajaxOptions = array(
            'success' => 'js:function(data){'
            /* converting data to json object */
            . 'var response = $.parseJSON(data);'
            /* if errors applying to the div */
            . '$(".show-error-summary").html(response.errorSummary);'
            /* appending the options */
            . 'if(response.optionString !== ""){$("#' . $select2Id . '").html(response.optionString);}'
            . "$other"
            /* setting new option */
            . 'if(response.newOption !== ""){$("#' . $select2Id . '").select2().select2("val", response.newOption);}'
            . 'if(response.errorSummary === ""){ $("#' . $dialogId . '").dialog("close"); '
            . '$( "#' . $select2Id . '" ).focus();'
            . '$( "#' . $select2Id . '" ).trigger("change");'
            . '}'
            . '}'
        );
        echo CHtml::ajaxSubmitButton('Save', $url, $ajaxOptions, array("id" => $dialogId . "ajaxButton" . uniqid(), 'class' => 'btn btn-default'));
    }

    /**
     * This function return the ajax remove or delete links for tabular inputs  depending on the scenario 
     * @param string $ajaxDeleteAction delete url
     * @param string $primaryKey encoded primary key input fields
     * 
     */
    public static function getTabualrButtonLink($scenario, $ajaxDeleteAction = null, $primaryKey = null) {
        $ajaxDeleteUrl = Yii::app()->createAbsoluteUrl($ajaxDeleteAction, array("id" => self::encryptParam($primaryKey)));
        $deleteMessage = self::deleteMeassage('this item');
        $buttonLink = CHtml::link(Yii::app()->params["removeNewRecord"], "", array("class" => "remove-item-serial", "onclick" => "js: removeTableRow($(this))"));
        if ($scenario !== "insert") {
            $buttonLink = CHtml::link(Yii::app()->params["deleteRecord"], "", array("class" => "delete-item-serial", "onclick" => "js: deleteTableRow($(this), '$ajaxDeleteUrl', '$deleteMessage')"));
        }
        return $buttonLink;
    }

    /**
     * This fucntion is useful when the javascript is dynamic
     */
    public static function getPopupNormalAjaxSubmitButton($url, $paramsArray = array(), $js, $isEncrypt = true) {
        $urlQueryArray = array();
        if (count($paramsArray) > 0) {
            foreach ($paramsArray as $key => $value) {
                $urlQueryArray[$key] = $value;
                if ($isEncrypt) {
                    $urlQueryArray[$key] = CommonSystem::encryptParam($value);
                }
            }
        }
        $btnUrl = Yii::app()->createAbsoluteUrl($url);
        if (count($urlQueryArray) > 0) {
            $btnUrl = Yii::app()->createAbsoluteUrl($url, $urlQueryArray);
        }
        self::getPopupAjaxSubmitButton($btnUrl, $js);
    }

    /**
     * This function returns complete ajax submit button for popups
     */
    public static function getPopupAjaxSubmitButton($url, $js) {
        echo CHtml::ajaxSubmitButton('Save', $url, array(
            "success" => "js: function(data){"
            . 'var response = $.parseJSON(data);'
            . '$(".show-error-summary").html(response.errorSummary);'
            . "$js"
            . "}"
                ), array('id' => 'ajaxBtn-' . uniqid(), 'class' => 'btn btn-default'));
    }

}
