<?php

/**
 * This is the model class for table "cm_prefix".
 *
 * The followings are the available columns in table 'cm_prefix':
 * @property string $prefix_id
 * @property string $prefix_name
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property SeqAgreement[] $seqAgreements
 */
class CmPrefix extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_prefix';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('prefix_name, updated_by', 'required'),
			array('prefix_name', 'length', 'max'=>2),
			array('updated_by', 'length', 'max'=>10),
                        array('prefix_name', 'filter', 'filter'=>'strtoupper'),
                        array('prefix_name', 'unique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prefix_id, prefix_name, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'seqAgreements' => array(self::HAS_MANY, 'SeqAgreement', 'prefix_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prefix_id' => 'Prefix',
			'prefix_name' => 'Prefix Name',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('prefix_id',$this->prefix_id,true);
		$criteria->compare('prefix_name',$this->prefix_name,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmPrefix the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
