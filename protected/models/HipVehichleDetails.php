<?php

/**
 * This is the model class for table "hip_vehichle_details".
 *
 * The followings are the available columns in table 'hip_vehichle_details':
 * @property string $vehicle_details_id
 * @property string $vehicle_type_id
 * @property string $vehicle_make_id
 * @property string $vehicle_model_id
 * @property string $vehicle_details_reg
 * @property string $vehicle_details_color
 * @property string $vehicle_details_yom
 * @property integer $vehicle_details_con
 * @property string $vehicle_details_ada
 * @property string $vehicle_details_engine_cc
 * @property string $vehicle_details_engine_no
 * @property string $vehicle_details_chassis_no
 * @property string $vehicle_details_price
 * @property integer $vehicle_details_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property HipAgreement[] $hipAgreements
 * @property CmVehicleType $vehicleType
 * @property CmVehicleMake $vehicleMake
 * @property CmVehicleModel $vehicleModel
 */
class HipVehichleDetails extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hip_vehichle_details';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('vehicle_type_id, vehicle_make_id, vehicle_model_id, vehicle_details_reg, vehicle_details_yom, vehicle_details_con, vehicle_details_engine_cc, vehicle_details_engine_no, vehicle_details_chassis_no, vehicle_details_price, updated_by, updated_date', 'required'),
            array('vehicle_details_con, vehicle_details_status', 'numerical', 'integerOnly' => true),
            array('vehicle_type_id, vehicle_make_id, vehicle_model_id, vehicle_details_reg, updated_by', 'length', 'max' => 10),
            array('vehicle_details_color', 'length', 'max' => 50),
            array('vehicle_details_ada', 'length', 'max' => 200),
            array('vehicle_details_engine_cc', 'length', 'max' => 5),
            array('vehicle_details_engine_no, vehicle_details_chassis_no', 'length', 'max' => 100),
            array('vehicle_details_price', 'length', 'max' => 20),
            array('vehicle_details_reg', 'unique'),
            array('vehicle_details_color, vehicle_details_engine_no, vehicle_details_chassis_no, vehicle_details_ada', 'filter', 'filter' => 'ucwords'),
            array('vehicle_details_reg', 'filter', 'filter' => 'strtoupper'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('vehicle_details_id, vehicle_type_id, vehicle_make_id, vehicle_model_id, vehicle_details_reg, vehicle_details_color, vehicle_details_yom, vehicle_details_con, vehicle_details_ada, vehicle_details_engine_cc, vehicle_details_engine_no, vehicle_details_chassis_no, vehicle_details_price, vehicle_details_status, updated_by, updated_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'hipAgreements' => array(self::HAS_MANY, 'HipAgreement', 'vehicle_details_id'),
            'vehicleType' => array(self::BELONGS_TO, 'CmVehicleType', 'vehicle_type_id'),
            'vehicleMake' => array(self::BELONGS_TO, 'CmVehicleMake', 'vehicle_make_id'),
            'vehicleModel' => array(self::BELONGS_TO, 'CmVehicleModel', 'vehicle_model_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'vehicle_details_id' => 'Vehicle Details',
            'vehicle_type_id' => 'Type',
            'vehicle_make_id' => 'Make',
            'vehicle_model_id' => 'Model',
            'vehicle_details_reg' => 'Registration No',
            'vehicle_details_color' => 'Color',
            'vehicle_details_yom' => 'Y.O.M',
            'vehicle_details_con' => 'Condition',
            'vehicle_details_ada' => 'Additional Accessaries',
            'vehicle_details_engine_cc' => 'Engine Capacity (cc)',
            'vehicle_details_engine_no' => 'Engine No',
            'vehicle_details_chassis_no' => 'Chassis No',
            'vehicle_details_price' => 'Price',
            'vehicle_details_status' => 'Status',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('vehicle_details_id', $this->vehicle_details_id, true);
        $criteria->compare('vehicle_type_id', $this->vehicle_type_id, true);
        $criteria->compare('vehicle_make_id', $this->vehicle_make_id, true);
        $criteria->compare('vehicle_model_id', $this->vehicle_model_id, true);
        $criteria->compare('vehicle_details_reg', $this->vehicle_details_reg, true);
        $criteria->compare('vehicle_details_color', $this->vehicle_details_color, true);
        $criteria->compare('vehicle_details_yom', $this->vehicle_details_yom, true);
        $criteria->compare('vehicle_details_con', $this->vehicle_details_con);
        $criteria->compare('vehicle_details_ada', $this->vehicle_details_ada, true);
        $criteria->compare('vehicle_details_engine_cc', $this->vehicle_details_engine_cc, true);
        $criteria->compare('vehicle_details_engine_no', $this->vehicle_details_engine_no, true);
        $criteria->compare('vehicle_details_chassis_no', $this->vehicle_details_chassis_no, true);
        $criteria->compare('vehicle_details_price', $this->vehicle_details_price, true);
        $criteria->compare('vehicle_details_status', $this->vehicle_details_status);
        $criteria->compare('updated_by', $this->updated_by, true);
        $criteria->compare('updated_date', $this->updated_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return HipVehichleDetails the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
