<?php

/*
 * This class contains all the user related functionality
 */

final class CommonUser {

    public static function getUserId() {
        return Yii::app()->user->getState("getUserId");
    }

    public static function getUserRoleId() {
        return Yii::app()->user->getState("getUserRoleId");
    }

    public static function getUserEmpId() {
        return Yii::app()->user->getState("getUserEmpId");
    }

    public static function getUserSuperUserStatus() {
        return Yii::app()->user->getState("getUserSuperUserStatus");
    }

    public static function getUserDesignationId() {
        return Yii::app()->user->getState("getUserDesignationId");
    }

    public static function getUserBranchIds() {
        return Yii::app()->user->getState("getUserBranchIds");
    }

    public static function getUserWorkingBranchId() {
        return Yii::app()->user->getState("getUserWorkingBranchId");
    }

    public static function getUserWorkingBranchName() {
        return Yii::app()->user->getState("getUserWorkingBranchName");
    }

    /* This function returns the user model of an employee1 */

    public static function getEmployeeUserModel($empId) {
        return SysUser::model()->find(new CDbCriteria(array("condition" => "emp_id = :empId", "params" => array(":empId" => $empId))));
    }

}
