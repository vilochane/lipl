<?php

/**
 * This is the model class for table "hip_agreement".
 *
 * The followings are the available columns in table 'hip_agreement':
 * @property string $agreement_id
 * @property string $cus_id
 * @property string $vehicle_details_id
 * @property string $period_id
 * @property string $due_date_id
 * @property string $prefix_name
 * @property string $agreement_no
 * @property string $agreement_code
 * @property string $agreement_credit_value
 * @property string $agreement_rental
 * @property string $agreement_rental_interest
 * @property string $agreement_rental_tot
 * @property string $agreement_interest_tot
 * @property string $agreement_rental_rate
 * @property string $agreement_r_interest_rate
 * @property string $agreement_i_rate
 * @property string $agreement_d_payment
 * @property string $agreement_start_date
 * @property string $agreement_end_date
 * @property string $agreement_d_capital
 * @property string $agreement_d_interest
 * @property string $agreement_p_amount_tot
 * @property string $agreement_p_interest_tot
 * @property string $agreement_p_d_amount
 * @property string $agreement_p_d_interest
 * @property integer $agreement_p_late_count
 * @property integer $agreement_ins_status
 * @property integer $agreement_closed_status
 * @property string $agreement_l_d_date
 * @property string $agreement_n_d_date
 * @property string $agreement_p_last_date
 * @property string $agreement_c_collected
 * @property string $agreement_p_collected
 * @property string $agreement_c_due
 * @property string $agreement_p_excess
 * @property integer $agreement_c_p_collected_status
 * @property integer $agreement_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeCustomer $cus
 * @property HipVehichleDetails $vehicleDetails
 * @property CmPeriod $period
 * @property CmDueDate $dueDate
 * @property HipPayments[] $hipPayments
 */
class HipAgreement extends CActiveRecord {

    public $currentMonthDueDate; //used in HipPayments
    public $rentalTotDisplay; //to dispaly the total rental
    public $isInitialRecord = false;

    /**
     * @return string the associated database table name
     */

    public function tableName() {
        return 'hip_agreement';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('cus_id, vehicle_details_id, period_id, due_date_id, prefix_name, agreement_no, agreement_code, agreement_credit_value, agreement_rental, agreement_rental_interest, agreement_rental_tot, agreement_interest_tot, agreement_rental_rate, agreement_r_interest_rate, agreement_i_rate, agreement_d_payment, agreement_start_date, agreement_end_date, agreement_d_capital, agreement_d_interest, agreement_ins_status, agreement_n_d_date, agreement_c_p_collected_status, updated_by, updated_date', 'required'),
            array('agreement_p_late_count, agreement_ins_status, agreement_closed_status, agreement_c_p_collected_status, agreement_status', 'numerical', 'integerOnly' => true),
            array('cus_id, vehicle_details_id, period_id, due_date_id, updated_by', 'length', 'max' => 10),
            array('prefix_name', 'length', 'max' => 2),
            array('agreement_no', 'length', 'max' => 6),
            array('agreement_code', 'length', 'max' => 8),
            array('agreement_credit_value, agreement_rental, agreement_rental_interest, agreement_rental_tot, agreement_interest_tot, agreement_rental_rate, agreement_r_interest_rate, agreement_i_rate, agreement_d_payment, agreement_d_capital, agreement_d_interest, agreement_p_amount_tot, agreement_p_interest_tot, agreement_p_d_amount, agreement_p_d_interest, agreement_c_collected, agreement_p_collected, agreement_c_due', 'length', 'max' => 30),
            array('agreement_l_d_date, agreement_p_excess, agreement_p_last_date', 'safe'),
            array('agreement_code', 'unique'),
            array('agreement_id, cus_id, vehicle_details_id, period_id, due_date_id, prefix_name, agreement_no, agreement_code, agreement_credit_value, agreement_rental, agreement_rental_interest, agreement_rental_tot, agreement_interest_tot, agreement_rental_rate, agreement_r_interest_rate, agreement_i_rate, agreement_d_payment, agreement_start_date, agreement_end_date, agreement_d_capital, agreement_d_interest, agreement_p_amount_tot, agreement_p_interest_tot, agreement_p_d_amount, agreement_p_d_interest, agreement_p_late_count, agreement_ins_status, agreement_closed_status, agreement_l_d_date, agreement_n_d_date, agreement_p_last_date, agreement_c_collected, agreement_p_collected, agreement_c_due, agreement_p_excess, agreement_c_p_collected_status, agreement_status, updated_by, updated_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cus' => array(self::BELONGS_TO, 'PeCustomer', 'cus_id'),
            'vehicleDetails' => array(self::BELONGS_TO, 'HipVehichleDetails', 'vehicle_details_id'),
            'period' => array(self::BELONGS_TO, 'CmPeriod', 'period_id'),
            'dueDate' => array(self::BELONGS_TO, 'CmDueDate', 'due_date_id'),
            'hipPayments' => array(self::HAS_MANY, 'HipPayments', 'agreement_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'agreement_id' => 'Agreement',
            'cus_id' => 'Cus',
            'vehicle_details_id' => 'Vehicle',
            'period_id' => 'Period',
            'due_date_id' => 'Due Date',
            'prefix_name' => 'Prefix Name',
            'agreement_no' => 'No',
            'agreement_code' => 'Code',
            'agreement_credit_value' => 'Credit Value',
            'agreement_rental' => 'Rental Amount',
            'agreement_rental_interest' => 'Rental Interest',
            'agreement_rental_tot' => 'Rental Total',
            'rentalTotDisplay' => 'Rental Payable',
            'agreement_interest_tot' => 'Interest Total',
            'agreement_rental_rate' => 'Rental Rate',
            'agreement_r_interest_rate' => 'Rental Interest Rate',
            'agreement_i_rate' => 'Interest(%)',
            'agreement_d_payment' => 'Down Payment',
            'agreement_start_date' => 'Start Date',
            'agreement_end_date' => 'End Date',
            'agreement_d_capital' => 'Due Capital',
            'agreement_d_interest' => 'Due Interest',
            'agreement_p_amount_tot' => 'Total Penalty',
            'agreement_p_interest_tot' => 'Total Penalty Interest',
            'agreement_p_d_amount' => 'Due Amount',
            'agreement_p_d_interest' => 'Due Interest',
            'agreement_p_late_count' => 'Payment Late Count',
            'agreement_ins_status' => 'Insurance Status',
            'agreement_closed_status' => 'Closed Status',
            'agreement_l_d_date' => 'Last Due Date',
            'agreement_n_d_date' => 'Next Due Date',
            'agreement_p_last_date' => 'Last Payment Made Date',
            'agreement_c_collected' => 'Capital Collected',
            'agreement_p_collected' => 'Penalty Collected',
            'agreement_c_due' => 'Current Arrears',
            'agreement_p_excess' => 'Agreement P Excess',
            'agreement_c_p_collected_status' => 'This Months Payment Collected',
            'agreement_status' => 'Status',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        $this->cus_id = Yii::app()->controller->getUrlQuery('c');
        $criteria = new CDbCriteria;
        if (!isset($_GET['HipAgreement_sort'])) {
            $criteria->order = 'agreement_id DESC';
        }
        $criteria->compare('agreement_id', $this->agreement_id);
        $criteria->compare('cus_id', $this->cus_id, true);
        $criteria->compare('vehicle_details_id', $this->vehicle_details_id, true);
        $criteria->compare('period_id', $this->period_id, true);
        $criteria->compare('due_date_id', $this->due_date_id, true);
        $criteria->compare('prefix_name', $this->prefix_name, true);
        $criteria->compare('agreement_no', $this->agreement_no, true);
        $criteria->compare('agreement_code', $this->agreement_code, true);
        $criteria->compare('agreement_credit_value', $this->agreement_credit_value, true);
        $criteria->compare('agreement_rental', $this->agreement_rental, true);
        $criteria->compare('agreement_rental_interest', $this->agreement_rental_interest, true);
        $criteria->compare('agreement_rental_tot', $this->agreement_rental_tot, true);
        $criteria->compare('agreement_interest_tot', $this->agreement_interest_tot, true);
        $criteria->compare('agreement_rental_rate', $this->agreement_rental_rate, true);
        $criteria->compare('agreement_r_interest_rate', $this->agreement_r_interest_rate, true);
        $criteria->compare('agreement_i_rate', $this->agreement_i_rate, true);
        $criteria->compare('agreement_d_payment', $this->agreement_d_payment, true);
        $criteria->compare('agreement_start_date', $this->agreement_start_date, true);
        $criteria->compare('agreement_end_date', $this->agreement_end_date, true);
        $criteria->compare('agreement_d_capital', $this->agreement_d_capital, true);
        $criteria->compare('agreement_d_interest', $this->agreement_d_interest, true);
        $criteria->compare('agreement_p_amount_tot', $this->agreement_p_amount_tot, true);
        $criteria->compare('agreement_p_interest_tot', $this->agreement_p_interest_tot, true);
        $criteria->compare('agreement_p_d_amount', $this->agreement_p_d_amount, true);
        $criteria->compare('agreement_p_d_interest', $this->agreement_p_d_interest, true);
        $criteria->compare('agreement_p_late_count', $this->agreement_p_late_count);
        $criteria->compare('agreement_ins_status', $this->agreement_ins_status);
        $criteria->compare('agreement_closed_status', $this->agreement_closed_status);
        $criteria->compare('agreement_l_d_date', $this->agreement_l_d_date, true);
        $criteria->compare('agreement_n_d_date', $this->agreement_n_d_date, true);
        $criteria->compare('agreement_p_last_date', $this->agreement_p_last_date, true);
        $criteria->compare('agreement_c_collected', $this->agreement_c_collected, true);
        $criteria->compare('agreement_p_collected', $this->agreement_p_collected, true);
        $criteria->compare('agreement_c_due', $this->agreement_c_due, true);
        $criteria->compare('agreement_p_excess',$this->agreement_p_excess,true);
        $criteria->compare('agreement_c_p_collected_status', $this->agreement_c_p_collected_status);
        $criteria->compare('agreement_status', $this->agreement_status);
        $criteria->compare('updated_by', $this->updated_by, true);
        $criteria->compare('updated_date', $this->updated_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return HipAgreement the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * This function checks period set or not
     */
    private function isPeriod() {
        $periodId = $this->period_id;
        $result = false;
        if (!empty($periodId)) {
            $result = true;
        }
        return $result;
    }

    /**
     * This funciton sets the rental
     */
    public function setRental() {
        if ($this->isPeriod()) {
            $this->agreement_rental = (double) $this->agreement_credit_value / (int) $this->period->period_name;
        }
    }

    /**
     * This fucntion sets the rental interest and inerest total and due interest
     */
    public function setRentalInterestAndInterestTotal() {
        if ($this->isPeriod()) {
            $this->agreement_d_interest = $this->agreement_interest_tot = (((double) $this->agreement_credit_value * (double) $this->agreement_i_rate) / 100);
            $this->agreement_rental_interest = (double) $this->agreement_interest_tot / (int) $this->period->period_name;
        }
    }

    /**
     * This funciton sets the total payable rental + interest
     */
    public function setRentalTotal() {
        $this->agreement_rental_tot = (double) $this->agreement_rental + (double) $this->agreement_rental_interest;
    }

    /**
     * This fucntion sets the rental fraction rate from rental tot
     */
    public function setRentalRate() {
        if ($this->agreement_rental_tot !== (double) 0) {
            $this->agreement_rental_rate = ((double) $this->agreement_rental / (double) $this->agreement_rental_tot) * 100;
        }
    }

    /**
     * This function sets the interest fraction rate from rental tot
     */
    public function setRentalInterestRate() {
        if ($this->agreement_rental_tot !== (double) 0) {
            $this->agreement_r_interest_rate = ((double) $this->agreement_rental_interest / (double) $this->agreement_rental_tot) * 100;
        }
    }

    /**
     * This function sets the due capital
     */
    public function setDueCapital() {
        $this->agreement_d_capital = (double) $this->agreement_credit_value - (double) $this->agreement_d_payment;
    }

    /**
     * This fucntion sets the current due against the collected values
     */
    public function setCurrentDueCapitalAndInterest() {
        $collectedCapital = ((double) $this->agreement_c_collected * (double) $this->agreement_rental_rate) / 100;
        $collectedInterest = ((double) $this->agreement_c_collected * (double) $this->agreement_r_interest_rate) / 100;
        $this->agreement_d_capital = (double) $this->agreement_d_capital - $collectedCapital;
        $this->agreement_d_interest = (double) $this->agreement_d_interest - $collectedInterest;
    }

    /**
     * This function sets the collected due penalty and interest
     */
    public function setCollectedPenaltyAmountTotals() {
        $this->agreement_p_amount_tot = ((double) $this->agreement_p_collected * (double) $this->agreement_rental_rate) / 100;
        $this->agreement_p_interest_tot = ((double) $this->agreement_p_collected * (double) $this->agreement_r_interest_rate) / 100;
    }

    /**
     * This function sets the current due amounts
     */
    public function setCurrentDue() {
        $this->agreement_p_d_amount = ((double) $this->agreement_c_due * (double) $this->agreement_rental_rate) / 100;
        $this->agreement_p_d_interest = ((double) $this->agreement_c_due * (double) $this->agreement_r_interest_rate) / 100;
    }

    /**
     * When adding the record initially setting the last due date and next due date
     */
    public function setDueDates() {
        $dueDateModel = CmDueDate::model()->findByPk($this->due_date_id);
        $dueDate = empty($dueDateModel) ? null : $dueDateModel->due_date_date;
        $lastPaymentMadeDate = $this->agreement_p_last_date;
        $startDate = $this->agreement_start_date;
        $this->agreement_l_d_date = $startDate;
        if(!empty($lastPaymentMadeDate)){
            $this->agreement_l_d_date = substr($lastPaymentMadeDate, 0, 8).$dueDate;
        }
        $this->agreement_n_d_date = Common::addDate($this->agreement_l_d_date, 0, 1);
        //$isCurrentMonthPaymentCollected = false;
//        if ($this->agreement_c_p_collected_status === '1') {
//            $isCurrentMonthPaymentCollected = true;
//        }
//        if (!empty($startDate)) {
//            $dueDateModel = CmDueDate::model()->findByPk($this->due_date_id);
//            $dueDate = empty($dueDateModel) ? null : $dueDateModel->due_date_date;
//            $cMonthDueDate = date('Y-m-') . $dueDate; /* if start date is a past date */
//            if ($startDate > $today) {/* if start date is a future date */
//                $cMonthDueDate = $startDate;
//            }
//            $nMonthDueDate = Common::addDate($cMonthDueDate, 0, 1); 
//            if (!$isCurrentMonthPaymentCollected) { /* if the this months due not collected */
//                $this->agreement_l_d_date = null; /* means no payment records added */
//                $this->agreement_n_d_date = $cMonthDueDate;
//            } elseif ($isCurrentMonthPaymentCollected) { /* if the this months due collected pushed to next month */
//                $this->agreement_l_d_date = null; /* means no payment records added */
//                $this->agreement_n_d_date = $nMonthDueDate;
//            } else { /* if the start date is greater than today */
//                $this->agreement_l_d_date = null; /* means no payment records added */
//                $this->agreement_n_d_date = null;
//            }
            //Common::displayResult($this->agreement_n_d_date,false);
//        }
    }
    
    public static function notClosedAgreements(){
        return HipAgreement::model()->findAll(array('order' => 'agreement_start_date ASC', "condition" => "agreement_start_date <= CURDATE() AND agreement_closed_status = '0' AND agreement_status = '1'"));
    }

    public static function notClosedAgreementIds(){
        $agreementIds = array();
        $agreementModels = self::notClosedAgreements();
        if(count($agreementModels) > 0){
            foreach ($agreementModels as $model) {
                $agreementIds[] = $model->agreement_id;
            }
        }
        return $agreementIds;
    }
    
}
