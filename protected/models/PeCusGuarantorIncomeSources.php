<?php

/**
 * This is the model class for table "pe_cus_guarantor_income_sources".
 *
 * The followings are the available columns in table 'pe_cus_guarantor_income_sources':
 * @property string $income_source_id
 * @property string $guarantor_id
 * @property string $income_source_name
 * @property string $income_source_income
 * @property string $income_source_expense
 * @property string $income_source_net
 * @property string $updated_by
 * @property string $updated_date
 */
class PeCusGuarantorIncomeSources extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_cus_guarantor_income_sources';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('income_source_name, income_source_income, income_source_expense, income_source_net, updated_by', 'required'),
			array('guarantor_id, updated_by', 'length', 'max'=>10),
			array('income_source_name', 'length', 'max'=>50),
			array('income_source_income, income_source_expense, income_source_net', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('income_source_id, guarantor_id, income_source_name, income_source_income, income_source_expense, income_source_net, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'income_source_id' => 'Income Source',
			'guarantor_id' => 'Guarantor',
			'income_source_name' => 'Income Source Name',
			'income_source_income' => 'Income Source Income',
			'income_source_expense' => 'Income Source Expense',
			'income_source_net' => 'Income Source Net',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('income_source_id',$this->income_source_id,true);
		$criteria->compare('guarantor_id',$this->guarantor_id,true);
		$criteria->compare('income_source_name',$this->income_source_name,true);
		$criteria->compare('income_source_income',$this->income_source_income,true);
		$criteria->compare('income_source_expense',$this->income_source_expense,true);
		$criteria->compare('income_source_net',$this->income_source_net,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PeCusGuarantorIncomeSources the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
