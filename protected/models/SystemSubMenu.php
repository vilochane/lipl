<?php

/**
 * this class returns the sub menus according to the view
 * and controller  
 * @param array $menu[0] controller names
 * @param array $menu[0][controller] links
 * @param array $menu[0][controller][links] link options
 * @param array $menu[0][controller][links][] link params
 * 
 * parameter nesting order [controller][l]
 */
final class SystemSubMenu {

    static $menu_array = array();
    private static $skip_action_array = array('submit', 'confirm');
    private static $sort = SORT_ASC;

    public static function setMenuLinks(array $menu, $sort = 'asc') {
        $controller_menu = NULL;
        $menu_link_array = array();
        if ($sort !== 'asc') {
            self::$sort = SORT_DESC;
        }

        if (count($menu) > 0) {
            foreach ($menu as $controller_menu => $links) {
                self::$menu_array = self::setMenuArray($controller_menu, $links);
                foreach (self::$menu_array as $action => $link) {
                    $menu_link_array[$action] = $link;
                }
            }
        }
        self::$menu_array = $menu_link_array;
        return self::$menu_array;
    }

    /**
     * this function sets and filters and returns the needed links
     */
    private static function setMenuArray($controller_menu, $view_links) {
        $menu_array_all = self::menuAllDefaultLinks($controller_menu);
        /* filtering only with the needed links */
        self::$menu_array = array_intersect_key($menu_array_all, $view_links);
        /* looping through the filteresd link */
        foreach (self::$menu_array as $viewAction => $options) {
            /* looping througth the default options of the array */
            foreach ($options as $option => $params) {
                /* checks looping option exists in the  needed link 
                 * else default options will not be overwritten
                 */
                if (array_key_exists($option, $view_links[$viewAction])) {
                    /* replacing option lables which is string */
                    if (is_string($view_links[$viewAction][$option])) {
                        self::$menu_array[$viewAction][$option] = $view_links[$viewAction][$option];
                    } else {
                        /* looping through the found options */
                        foreach ($view_links[$viewAction][$option] as $option_key => $value) {
                            /* encryption url parameters if not in skip array */
                            if (!in_array($option_key, self::$skip_action_array)) {
                                $value = CommonSystem::encryptParam($value);
                            } elseif (is_array($value)) {
                                /* setting the default option if link option action not set */
                                if (!array_key_exists(0, $value)) {
                                    $value[0] = self::$menu_array[$viewAction][$option][$option_key][0];
                                    array_multisort($value, SORT_DESC);
                                }
                                /* looping through indetail arrays such as link options submit */
                                foreach ($value as $sub_option_key => $sub_value) {//                                    
                                    /* encryption url parameters if not in skip array */
                                    if (!in_array($sub_option_key, self::$skip_action_array)) {
                                        $sub_value = CommonSystem::encryptParam($sub_value);
                                    }
                                    /* replacing encrypted indetail values in $value such as in link options */
                                    $value[$sub_option_key] = $sub_value;
                                }
                            }
                            /* replacing the options params of the found needed option */
                            self::$menu_array[$viewAction][$option][$option_key] = $value;
                        }
                    }
                }
            }
        }
        array_multisort(self::$menu_array, self::$sort);
        return self::$menu_array;
    }

    /* this function contains the all the menus of the system 
     * indexed by the controller name
     * will return specific controller links
     */

    private static function menuAllDefaultLinks($controller_menu) {

        $menu_links_all_array = array(
            'Seperator' => array(
                'SeperatorLink' => array('label' => '')
            ),
            'Seperator1' => array(
                'SeperatorLink1' => array('label' => '')
            ),
            'Seperator2' => array(
                'SeperatorLink2' => array('label' => '')
            ),
            'CmCompanyProfile' => array(
                'CmCompanyProfileCreate' => array('label' => 'Create Company Profile', 'url' => array('create')),
                'CmCompanyProfileUpdate' => array('label' => 'Update Company Profile', 'url' => array('update')),
                'CmCompanyProfileView' => array('label' => 'View Company Profile', 'url' => array('view')),
                'CmCompanyProfileDelete' => array('label' => 'Delete Company Profile', 'url' => '#', 'linkOptions' => array('submit' => array('delete'), 'confirm' => 'Are you sure you want to delete this item?')),
                'CmCompanyProfileManage' => array('label' => 'Manage Company Profile', 'url' => array('admin')),
            ),
            'CmBranch' => array(
                'CmBranchCreate' => array('label' => 'Create a Branch', 'url' => array('create')),
                'CmBranchUpdate' => array('label' => 'Update Branch', 'url' => array('update')),
                'CmBranchView' => array('label' => 'View Branch', 'url' => array('view', 'id' => '')),
                'CmBranchDelete' => array('label' => 'Delete Branch', 'url' => '#', 'linkOptions' => array('submit' => array('delete'), 'confirm' => 'Are you sure you want to delete this branch?')),
                'CmBranchManage' => array('label' => 'Manage Branches', 'url' => array('admin')),
            ),
            'CmDesignation' => array(
                'CmDesignationCreate' => array('label' => 'Create a Designation', 'url' => array('create')),
                'CmDesignationUpdate' => array('label' => 'Update Designation', 'url' => array('update')),
                'CmDesignationView' => array('label' => 'View Designation', 'url' => array('view')),
                'CmDesignationDelete' => array('label' => 'Delete Designation', 'url' => '#', 'linkOptions' => array('submit' => array('delete'), 'confirm' => 'Are you sure you want to delete this designation?')),
                'CmDesignationManage' => array('label' => 'Manage Designations', 'url' => array('admin')),
            ),
            'CsSystemUserRole' => array(
                'CsSystemUserRoleCreate' => array('label' => 'Create a System User Role', 'url' => array('create')),
                'CsSystemUserRoleUpdate' => array('label' => 'Update System User Role', 'url' => array('update')),
                'CsSystemUserRoleView' => array('label' => 'View System User Role', 'url' => array('view')),
                'CsSystemUserRoleDelete' => array('label' => 'Delete CsSystemUserRole', 'url' => '#', 'linkOptions' => array('submit' => array('delete'), 'confirm' => 'Are you sure you want to delete this item?')),
                'CsSystemUserRoleManage' => array('label' => 'Manage System User Roles', 'url' => array('admin')),
            ),
            'PeEmployee' => array(
                'PeEmployeeCreate' => array('label' => 'Create a Employee', 'url' => array('create')),
                'PeEmployeeUpdate' => array('label' => 'Update Employee', 'url' => array('update')),
                'PeEmployeeView' => array('label' => 'View Employee', 'url' => array('view')),
                'PeEmployeeDelete' => array('label' => 'Delete Employee', 'url' => '#', 'linkOptions' => array('submit' => array('delete'), 'confirm' => 'Are you sure you want to delete this employee?')),
                'PeEmployeeManage' => array('label' => 'Manage Employees', 'url' => array('admin')),
            ),
            'CmDueDate' => array(
                'CmDueDateCreate' => array('label' => 'Create a Due Date', 'url' => array('create')),
                'CmDueDateUpdate' => array('label' => 'Update Due Date', 'url' => array('update')),
                'CmDueDateView' => array('label' => 'View Due Date', 'url' => array('view')),
                'CmDueDateDelete' => array('label' => 'Delete Due Date', 'url' => '#', 'linkOptions' => array('submit' => array('delete'), 'confirm' => 'Are you sure you want to delete this item?')),
                'CmDueDateManage' => array('label' => 'Manage Due Date', 'url' => array('admin')),
            ),
            'PeCustomer' => array(
                'PeCustomerCreate' => array('label' => 'Create a Customer', 'url' => array('create')),
                'PeCustomerUpdate' => array('label' => 'Update Customer', 'url' => array('update')),
                'PeCustomerView' => array('label' => 'View Customer', 'url' => array('view')),
                'PeCustomerDelete' => array('label' => 'Delete Customer', 'url' => '#', 'linkOptions' => array('submit' => array('delete'), 'confirm' => 'Are you sure you want to delete this item?')),
                'PeCustomerManage' => array('label' => 'Manage Customers', 'url' => array('PeCustomer/admin')),
                'PeCustomerSeperator' => array('label' => '', 'url'=>'#'),
            ),
            'HipAgreement' => array(
                'HipAgreementCreate' => array('label' => 'Create a Hire Agreement', 'url' => array('HipAgreement/create')),
                'HipAgreementUpdate' => array('label' => 'Update Hire Agreement', 'url' => array('HipAgreement/update')),
                'HipAgreementView' => array('label' => 'View Hire Agreement', 'url' => array('HipAgreement/view')),
                'HipAgreementDelete' => array('label' => 'Delete Hire Agreement', 'url' => '#', 'linkOptions' => array('submit' => array('HipAgreement/delete'), 'confirm' => 'Are you sure you want to delete this item?')),
                'HipAgreementManage' => array('label' => 'Manage Hire Agreements', 'url' => array('HipAgreement/admin')),
                'HipAgreementSeperator' => array('label' => '', 'url'=>'#'),
                'HipAgreementUpdateAll' => array('label' => 'Update Hire Agreement', 'url' => array('HipAgreement/updateAll')),
                'HipAgreementViewAll' => array('label' => 'View Hire Agreement', 'url' => array('HipAgreement/viewAll')),
                'HipAgreementManageAll' => array('label' => 'Manage Hire Agreements', 'url' => array('HipAgreement/adminAll')),
            ),
            'HipVehichleDetails'=>array(
                'HipVehichleDetailsCreate'=>array('label'=>'Create VehichleDetails', 'url'=>array('create')),
                'HipVehichleDetailsUpdate'=>array('label'=>'Update VehichleDetails', 'url'=>array('update')),
                'HipVehichleDetailsView'=>array('label'=>'View VehichleDetails', 'url'=>array('view')),
                'HipVehichleDetailsDelete'=>array('label'=>'Delete VehichleDetails', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete'),'confirm'=>'Are you sure you want to delete this item?')),
                'HipVehichleDetailsManage'=>array('label'=>'Manage VehichleDetails', 'url'=>array('admin')),
            ),
            'PeCusGuarantor' => array(
                'PeCusGuarantorCreate' => array('label' => 'Create a Guarantor', 'url' => array('PeCusGuarantor/create')),
                'PeCusGuarantorUpdate' => array('label' => 'Update Guarantor', 'url' => array('PeCusGuarantor/update')),
                'PeCusGuarantorView' => array('label' => 'View Guarantor', 'url' => array('PeCusGuarantor/view')),
                'PeCusGuarantorDelete' => array('label' => 'Delete Guarantor', 'url' => '#', 'linkOptions' => array('submit' => array('PeCusGuarantor/delete'), 'confirm' => 'Are you sure you want to delete this item?')),
                'PeCusGuarantorManage' => array('label' => 'Manage Guarantors', 'url' => array('PeCusGuarantor/admin')),
                'PeCusGuarantorSeperator' => array('label' => '', 'url'=>'#'),
            )
        );

        return $menu_links_all_array[$controller_menu];
    }

}
