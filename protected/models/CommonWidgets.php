<?php

/*
 * This class contains the widgets related functions
 */

final class CommonWidgets {

    /** cjui dialog options
     * @param array $options_array indexed options 
     */
    public static function getDialogOptions($options_array = array()) {
        $default_array = array(
            'title' => '',
            'position' => 'center',
            'width' => 'auto',
            'height' => '500',
            'autoOpen' => false,
            'resizable' => true,
            'stack' => false,
            'modal' => true,
        );
        
        return array_merge($default_array, $options_array);
    }

    public static function getformatCurrencyOptions($options_array = array()) {
        $default_array = array("negativeFormat" => '-%s%n', "decimalSymbol" => ',', "digitGroupSymbol" => '.',);
        if (count($options_array) > 0) {
            $default_array = array_merge($default_array, $options_array);
        }
        return $default_array;
    }

}
