<?php
/**
 * This is the model class for table "pe_cus_bank_details".
 *
 * The followings are the available columns in table 'pe_cus_bank_details':
 * @property string $bank_details_id
 * @property string $cus_id
 * @property string $bank_details_bank_name
 * @property string $bank_details_bank_ac_no
 * @property string $bank_details_bank_address
 * @property integer $bank_details_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeCustomer $cus
 */
class PeCusBankDetails extends CActiveRecord {

    public $modelScenario;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pe_cus_bank_details';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('modelScenario, bank_details_bank_name, bank_details_bank_ac_no, bank_details_bank_address, updated_by', 'required'),
            array('bank_details_status', 'numerical', 'integerOnly'=>true),
            array('cus_id, updated_by', 'length', 'max' => 10),
            array('bank_details_bank_ac_no', 'length', 'max' => 20),
            array('bank_details_bank_name', 'length', 'max' => 50),
            array('bank_details_bank_address', 'length', 'max' => 200),
            array('bank_details_id, cus_id, bank_details_bank_name, bank_details_bank_ac_no, bank_details_bank_address, bank_details_status, updated_by, updated_date', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cus' => array(self::BELONGS_TO, 'PeCustomer', 'cus_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'bank_details_id' => 'Bank Details',
            'cus_id' => 'Cus',
            'modelScenario' => 'Model Scenario',
            'bank_details_bank_name' => 'Bank Name',
            'bank_details_bank_ac_no' => 'Bank Account No',
            'bank_details_bank_address' => 'Bank Address',
            'bank_details_status' => 'Bank Details Status',
            'updated_by' => 'Updated By',
            'updated_date' => 'Updated Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('bank_details_id', $this->bank_details_id, true);
        $criteria->compare('cus_id', $this->cus_id, true);
        $criteria->compare('bank_details_bank_name', $this->bank_details_bank_name, true);
        $criteria->compare('bank_details_bank_ac_no', $this->bank_details_bank_ac_no);
        $criteria->compare('bank_details_bank_address', $this->bank_details_bank_address, true);
        $criteria->compare('bank_details_status',$this->bank_details_status);
        $criteria->compare('updated_by', $this->updated_by, true);
        $criteria->compare('updated_date', $this->updated_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PeCusBankDetails the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
