<?php

/**
 * This class wrapper for CNumberFormat
 * @param string $option formatting type null formats to number
 * format type currency, decimal, percentage,
 * @param int $integer_len lenth of the  Integer digits (Significand) valid for default option
 * @param int $fraction_len lenth of the  decimals digits or fractions (Mantissa)
 * @param int $value to be formatted with or without decimal point it'll padded with 0's
 * in default format number depending on the decimals digits value will be rounded or showed
 * eg :
 *      NumberFormatter::formatValue(null, 0, 0, 1232456.199) =  1,232,456.19900
 *      NumberFormatter::formatValue(null, 0, 2, 1232456.199) = 1,232,456.20
 *      NumberFormatter::formatValue(null, 0, 3, 1232456.199) = 1,232,456.199
 * 
 */
final class SysNumberFormatter {

    private static $pattern = '###,###,###,##0.00000';
    private static $pattern_string = '';
    private static $currency = "null";
    private static $integer_len = 0;
    private static $fraction_len = 0;
    private static $value = 0;
    private static $numformatter_obj = 0;
    public static $formatted_value = 0;

    static function getPatternString() {

        if (self::$integer_len === 3) {
            $integer_pos = 12;
        } elseif (self::$integer_len === 6) {
            $integer_pos = 8;
        } elseif (self::$integer_len === 9) {
            $integer_pos = 4;
        } else {
            $integer_pos = 0;
        }
        $dec_pos = strpos(self::$pattern, '.');
        /* integer string */
        $interger_pattern = substr(self::$pattern, 0, $dec_pos);
        $integer_string = substr($interger_pattern, $integer_pos);

        /* fraction string length with decimal point is 6 */
        $fraction_pattern = substr(self::$pattern, $dec_pos);
        $fraction_pattern_len = strlen($fraction_pattern);
        /* setting the length to select from end */
        $fraction_pos = -($fraction_pattern_len - (self::$fraction_len + 1));
        /* default select all the length */
        $fraction_string = substr($fraction_pattern, - $fraction_pattern_len);
        if ($fraction_pos !== 0) {
            $fraction_string = substr($fraction_pattern, -$fraction_pattern_len, $fraction_pos);
        }
        $pattern_string = '¤' . $integer_string . $fraction_string;
        if (empty(self::$currency)) {
            $pattern_string = $integer_string . $fraction_string;
        }
        self::$pattern_string = $pattern_string;
    }

    /* since the cative record class uses this function currency get overridden if not could set currency code from
     * here for the format value like $currency = Common::getCompanyCurrencyCode();
     * difficult to you number format with currency 
     */

    public static function formatValue($option = null, $integer_len = 12, $fraction_len = 0, $value, $currency = NULL) {
        self::$numformatter_obj = new CNumberFormatter('en');
        self::$integer_len = $integer_len;
        self::$fraction_len = $fraction_len;
        self::$currency = $currency;
        self::getPatternString();
        self::$value = null;
        self::$value = self::removeNumberFormat($value);

//        if ($fraction_len !== 0) {
//            self::$value = self::setNumberFormat($value, $fraction_len);
//        } else {
//            self::$value = self::removeNumberFormat($value);
//        }       
        switch ($option) {
            case "currency":
                self::formatCurrency(self::$value, self::$currency);
                break;
            case "decimal":
                self::formatDecimal(self::$value);
                break;
            case "percentage":
                self::formatPercentage(self::$value);
                break;
            default :
                self::formatAmount(self::$pattern_string, self::$value, self::$currency);
                break;
        }
        return self::$formatted_value;
    }

    private static function formatCurrency($value, $currency) {
        self::$formatted_value = self::$numformatter_obj->formatCurrency($value, $currency);
    }

    private static function formatDecimal($value) {
        self::$formatted_value = self::$numformatter_obj->formatDecimal($value);
    }

    private static function formatPercentage($value) {
        self::$formatted_value = self::$numformatter_obj->formatPercentage($value);
    }

    private static function formatAmount($pattern, $value, $currency) {
        self::$formatted_value = self::$numformatter_obj->format($pattern, $value, $currency);
    }

    /* only use for values with fractions */

    private static function setNumberFormat($number, $decimals = 5, $with_seperator = false) {
        $number = CPropertyValue::ensureFloat($number);
        if ($decimals === 0) {
            $decimals = 5;
        }
        $seperator = '';
        if ($with_seperator) {
            $seperator = ',';
        }

        $value = $number;
        if (substr_count($number, ',') === 0) {
            $value = number_format($number, $decimals, '.', $seperator);
        }
        return $value;
    }

    private static function removeNumberFormat($number) {
        $value = str_replace(array(',', ' '), '', $number);
        return $value;
    }

}
?>

