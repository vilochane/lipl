<?php

final class SystemMainMenu {

    /**
     * This function returns the main menu of the system
     * @return array main menu items array
     */
    public static function getSystemMainMenu() {
        $workingBranchId = CommonUser::getUserWorkingBranchId();
        $branchIds = CommonUser::getUserBranchIds();
        $workingBranchName = CommonUser::getUserWorkingBranchName();

        if (!empty(CommonUser::getUserWorkingBranchId())) {
            return array(
                array('label' => 'Company', 'url' => '#', 'visible' => !Yii::app()->user->isGuest,
                    'items' => array(
                        array('label' => 'Company Profile', 'url' => array('cmCompanyProfile/admin')),
                        array('label' => 'Branches', 'url' => array('cmBranch/admin')),
                        array('label' => 'Employees', 'url' => array('peEmployee/admin')),
                    )
                ),
                array('label' => 'Master', 'url' => '#', 'visible' => !Yii::app()->user->isGuest,
                    'items' => array(
                        array('label' => 'Due Dates', 'url' => array('cmDueDate/admin')),
                        array('label' => 'Designations', 'url' => array('cmDesignation/admin')),
                        array('label' => 'Divisional Secratariots', 'url' => array('CmDivisionalSecretariat/admin')),
                        array('label' => 'Professions', 'url' => array('CmProfession/admin')),
                        array('label' => 'Vehicle Types', 'url' => array('CmVehicleType/admin')),
                        array('label' => 'Vehicle Makes', 'url' => array('CmVehicleMake/admin')),
                        array('label' => 'Vehicle Models', 'url' => array('CmVehicleModel/admin')),
                    )
                ),
                array('label' => 'Customer', 'url' => array('peCustomer/admin')),
                array('label' => 'Agreements', 'url' => array('HipAgreement/adminAll')),
                array('label' => 'Payments', 'url' => '#', 'items'=>array(
                     array('label' => 'Pending Payments', 'url' => array('HipPayments/admin')),
                     array('label' => 'Rollback a Payment', 'url' => array('HipPayments/RollbackPaymentsMade')),
                     array('label' => 'Made Payments', 'url' => array('HipPayments/PaymentsMade')),
                    
                )),
                array('label' => 'Vehicles', 'url' => array('HipVehichleDetails/admin')),
                array('label' => 'Branch ' . $workingBranchName, 'url' => count($branchIds) > 1 ? array('/site/SelectUserBranch') : '#', 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
            );
        } else {
            return array(
                array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::app()->user->isGuest)
            );
        }
    }

}
