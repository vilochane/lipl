<?php

/**
 * This is the model class for table "map_employee_branch".
 *
 * The followings are the available columns in table 'map_employee_branch':
 * @property string $employee_branch_id
 * @property string $emp_id
 * @property string $branch_id
 * @property string $user_id
 * @property integer $employee_branch_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property SysUser $user
 * @property PeEmployee $emp
 * @property CmBranch $branch
 */
class MapEmployeeBranch extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'map_employee_branch';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, branch_id, updated_by', 'required'),
			array('employee_branch_status', 'numerical', 'integerOnly'=>true),
			array('emp_id, branch_id, user_id, updated_by', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('employee_branch_id, emp_id, branch_id, user_id, employee_branch_status, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'SysUser', 'user_id'),
			'emp' => array(self::BELONGS_TO, 'PeEmployee', 'emp_id'),
			'branch' => array(self::BELONGS_TO, 'CmBranch', 'branch_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'employee_branch_id' => 'Employee Branch',
			'emp_id' => 'Emp',
			'branch_id' => 'Branch',
			'user_id' => 'User',
			'employee_branch_status' => 'Employee Branch Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('employee_branch_id',$this->employee_branch_id,true);
		$criteria->compare('emp_id',$this->emp_id,true);
		$criteria->compare('branch_id',$this->branch_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('employee_branch_status',$this->employee_branch_status);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MapEmployeeBranch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
