<?php
/**
 * This is the model class for table "pe_employee".
 *
 * The followings are the available columns in table 'pe_employee':
 * @property string $emp_id
 * @property string $designation_id
 * @property string $emp_first_name
 * @property string $emp_middle_name
 * @property string $emp_last_name
 * @property integer $emp_gender
 * @property integer $emp_marital_status
 * @property string $emp_nic
 * @property integer $emp_slin
 * @property string $emp_dob
 * @property string $emp_address
 * @property string $emp_current_address
 * @property string $emp_telephone
 * @property string $emp_mobile
 * @property string $emp_email
 * @property integer $emp_etf_no
 * @property integer $emp_epf_no
 * @property integer $emp_system_user
 * @property string $emp_date_of_employment
 * @property string $emp_nic_scan_copy
 * @property string $emp_image
 * @property integer $emp_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property MapEmployeeBranch[] $mapEmployeeBranches
 * @property CmDesignation $designation
 * @property SysUser[] $sysUsers
 */
class PeEmployee extends CActiveRecord
{       
    public $branchIds;
    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pe_employee';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('designation_id, emp_first_name, emp_system_user, updated_by, branchIds', 'required'),
			array('emp_gender, emp_marital_status, emp_nic, emp_slin, emp_telephone, emp_mobile, emp_etf_no, emp_epf_no, emp_system_user, emp_status', 'numerical', 'integerOnly'=>true),
			array('designation_id, updated_by', 'length', 'max'=>10),
			array('emp_first_name, emp_middle_name, emp_last_name', 'length', 'max'=>50),
			array('emp_address, emp_current_address', 'length', 'max'=>200),
			array('emp_email', 'length', 'max'=>150),
                        array('emp_email', 'email'),
			array('emp_dob, emp_date_of_employment', 'safe'),
                        array('emp_nic', 'match', 'pattern'=>'/^([0-9]{9}[X|V])$/'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('emp_id, designation_id, emp_first_name, emp_middle_name, emp_last_name, emp_gender, emp_marital_status, emp_nic, emp_slin, emp_dob, emp_address, emp_current_address, emp_telephone, emp_mobile, emp_email, emp_etf_no, emp_epf_no, emp_system_user, emp_date_of_employment, emp_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'mapEmployeeBranches' => array(self::HAS_MANY, 'MapEmployeeBranch', 'emp_id'),
			'designation' => array(self::BELONGS_TO, 'CmDesignation', 'designation_id'),
			'sysUsers' => array(self::HAS_MANY, 'SysUser', 'emp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'emp_id' => 'Emp',
			'branchIds' => 'Branch(es)',
			'designation_id' => 'Designation',
			'emp_first_name' => 'First Name',
			'emp_middle_name' => 'Middle Name',
			'emp_last_name' => 'Last Name',
			'emp_gender' => 'Gender',
			'emp_marital_status' => 'Marital Status',
			'emp_nic' => 'NIC',
			'emp_slin' => 'SLIN',
			'emp_dob' => 'D.O.B.',
			'emp_address' => 'Address',
			'emp_current_address' => 'Current Address',
			'emp_telephone' => 'Telephone',
			'emp_mobile' => 'Mobile',
			'emp_email' => 'Email',
			'emp_etf_no' => 'Etf No',
			'emp_epf_no' => 'Epf No',
			'emp_system_user' => 'System User',
			'emp_date_of_employment' => 'Date Of Employment',
			'emp_nic_scan_copy' => 'Nic Scan Copy',
			'emp_image' => 'Image',
                        'emp_status' => 'Emp Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('emp_id',$this->emp_id,true);
		$criteria->compare('designation_id',$this->designation_id,true);
		$criteria->compare('emp_first_name',$this->emp_first_name,true);
		$criteria->compare('emp_middle_name',$this->emp_middle_name,true);
		$criteria->compare('emp_last_name',$this->emp_last_name,true);
		$criteria->compare('emp_gender',$this->emp_gender);
		$criteria->compare('emp_marital_status',$this->emp_marital_status);
		$criteria->compare('emp_nic',$this->emp_nic);
		$criteria->compare('emp_slin',$this->emp_slin);
		$criteria->compare('emp_dob',$this->emp_dob,true);
		$criteria->compare('emp_address',$this->emp_address,true);
		$criteria->compare('emp_current_address',$this->emp_current_address,true);
		$criteria->compare('emp_telephone',$this->emp_telephone);
		$criteria->compare('emp_mobile',$this->emp_mobile);
		$criteria->compare('emp_email',$this->emp_email,true);
		$criteria->compare('emp_etf_no',$this->emp_etf_no);
		$criteria->compare('emp_epf_no',$this->emp_epf_no);
		$criteria->compare('emp_system_user',$this->emp_system_user);
		$criteria->compare('emp_date_of_employment',$this->emp_date_of_employment,true);
		$criteria->compare('emp_nic_scan_copy',$this->emp_nic_scan_copy,true);
		$criteria->compare('emp_image',$this->emp_image,true);
                $criteria->compare('emp_status',$this->emp_status);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PeEmployee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
