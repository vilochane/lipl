<?php
/**
 * This is the model class for table "cm_emp_period".
 *
 * The followings are the available columns in table 'cm_emp_period':
 * @property string $emp_period_id
 * @property string $emp_period_name
 * @property integer $emp_period_status
 * @property string $updated_by
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property PeCusGuarantor[] $peCusGuarantors
 * @property PeCusGuarantor[] $peCusGuarantors1
 */
class CmEmpPeriod extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_emp_period';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_period_name, updated_by, updated_date', 'required'),
                        array('emp_period_status', 'numerical', 'integerOnly'=>true),
			array('emp_period_name', 'length', 'max'=>20),
			array('updated_by', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('emp_period_id, emp_period_name, emp_period_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'peCusGuarantors' => array(self::HAS_MANY, 'PeCusGuarantor', 'guarantor_emp_period_id'),
			'peCusGuarantors1' => array(self::HAS_MANY, 'PeCusGuarantor', 'guarantor_semp_period_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'emp_period_id' => 'Emp Period',
			'emp_period_name' => 'Emp Period Name',
                        'emp_period_status' => 'Emp Period Status',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('emp_period_id',$this->emp_period_id,true);
		$criteria->compare('emp_period_name',$this->emp_period_name,true);
                $criteria->compare('emp_period_status',$this->emp_period_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmEmpPeriod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
