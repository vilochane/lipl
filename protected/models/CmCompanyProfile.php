<?php

/**
 * This is the model class for table "cm_company_profile".
 *
 * The followings are the available columns in table 'cm_company_profile':
 * @property string $company_profile_id
 * @property string $company_profile_name
 * @property string $company_profile_reg_no
 * @property string $company_profile_address
 * @property integer $company_profile_telephone
 * @property integer $company_profile_telephone2
 * @property integer $company_profile_fax
 * @property integer $company_profile_fax2
 * @property string $company_profile_email
 * @property string $company_profile_email2
 * @property string $company_profile_web_site
 * @property string $company_profile_logo
 * @property string $updated_by
 * @property string $updated_date
 */
class CmCompanyProfile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cm_company_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_profile_name, company_profile_reg_no, company_profile_address, company_profile_telephone, updated_by', 'required'),
			array('company_profile_name', 'length', 'max'=>50),
			array('company_profile_reg_no, updated_by', 'length', 'max'=>10),
			array('company_profile_address', 'length', 'max'=>200),
			array('company_profile_email, company_profile_email2, company_profile_web_site', 'length', 'max'=>150),
			array('company_profile_logo', 'length', 'max'=>100),
                        array('company_profile_email, company_profile_email2', 'email'),
                        array('company_profile_web_site', 'url'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('company_profile_id, company_profile_name, company_profile_reg_no, company_profile_address, company_profile_telephone, company_profile_telephone2, company_profile_fax, company_profile_fax2, company_profile_email, company_profile_email2, company_profile_web_site, company_profile_logo, updated_by, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'company_profile_id' => 'Company',
			'company_profile_name' => 'Name',
			'company_profile_reg_no' => 'Registration No',
			'company_profile_address' => 'Address',
			'company_profile_telephone' => 'Telephone',
			'company_profile_telephone2' => 'Telephone Secondary',
			'company_profile_fax' => 'Fax',
			'company_profile_fax2' => 'Fax Secondary',
			'company_profile_email' => 'Email',
			'company_profile_email2' => 'Email Secondary',
			'company_profile_web_site' => 'Web Site',
			'company_profile_logo' => 'Logo',
			'updated_by' => 'Updated By',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('company_profile_id',$this->company_profile_id,true);
		$criteria->compare('company_profile_name',$this->company_profile_name,true);
		$criteria->compare('company_profile_reg_no',$this->company_profile_reg_no,true);
		$criteria->compare('company_profile_address',$this->company_profile_address,true);
		$criteria->compare('company_profile_telephone',$this->company_profile_telephone);
		$criteria->compare('company_profile_telephone2',$this->company_profile_telephone2);
		$criteria->compare('company_profile_fax',$this->company_profile_fax);
		$criteria->compare('company_profile_fax2',$this->company_profile_fax2);
		$criteria->compare('company_profile_email',$this->company_profile_email,true);
		$criteria->compare('company_profile_email2',$this->company_profile_email2,true);
		$criteria->compare('company_profile_web_site',$this->company_profile_web_site,true);
		$criteria->compare('company_profile_logo',$this->company_profile_logo,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CmCompanyProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
