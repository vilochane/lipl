<?php

/*
 * This calss contains the common functionality for the system
 * 
 */

class Common {

    public static function getPaymentStatus($status){
        $response = null;
        $statusArray = array(0=>'Pending', 1=>'Paid', 3=>'Closed by system');
        if(isset($statusArray[$status])){
            $response = $statusArray[$status];
        }
        return $response;
    }


    public static function getAgreementCode() {
        $prefix = 'L';
        $sequence = "000001";
        $seqAgreement = HipAgreement::model()->find(new CDbCriteria(array("order" => "agreement_no DESC", "limit" => "1")));
        if (!empty($seqAgreement)) {
            $prefix = $seqAgreement->prefix_name;
            $sequence = self::setSequenceValue($seqAgreement->agreement_no);
        }
        return array("prefix" => $prefix, "sequence" => $sequence);
    }

    public static function setSequenceValue($value, $increment = TRUE, $padLen = 6, $padString = '0', $padType = STR_PAD_LEFT) {
        if ($increment) {
            ++$value;
        }
        $seqValue = str_pad($value, $padLen, $padString, $padType);
        return $seqValue;
    }

    /**
     * this function returns images of the system
     * params $path = 'images directory path without starting and ending forward slashes'
     * $image_name = image name
     * $css = css class for the image   
     */
    public static function getImage($path = null, $image_name = null, $css_class = 'thumb') {
        /* trimming the path */
        $path = trim($path, '/');
//        $img_tag = preg_replace('#thumb-mini#', $css_class, Yii::app()->params['no_image_available']);
        $img_tag = '<span class="glyphicon glyphicon-picture" style="font-size: 20px;"></span>';
        if (!empty($path) && !empty($image_name) && file_exists($path . '/' . $image_name)) {
            $img_tag = CHtml::tag('img', array('src' => $path . '/' . $image_name, 'class' => $css_class));
        }

        return $img_tag;
    }

    /* this function returns the out put for development purpose */

    public static function displayResult($param, $die = true) {

        var_dump($param);
        if ($die) {
            die;
        }
    }

    /* date time or time stamp */

    public static function dateTimeOrTimeStamp($time_stamp = FALSE, $unix_time_time_stamp = FALSE, $date = TRUE) {
        $dateTimeObj = new DateTime();
        $dateTimeObj->setTimezone(new DateTimeZone("Asia/Colombo"));
        //Common::displayResult(DateTimeZone::listIdentifiers(DateTimeZone::PER_COUNTRY, "LK"));        
        $date_time = NULL;
        if ($time_stamp) {
            $date_time = $dateTimeObj->format('U');
        } elseif ($unix_time_time_stamp) {
            $date_time = $dateTimeObj->format('U = Y-m-d H:i:s');
        } elseif ($date) {
            $date_time = $dateTimeObj->format('Y-m-d');
        } else {
            $date_time = $dateTimeObj->format('Y-m-d H:i:s');
        }
        return $date_time;
    }

    /**
     * This function adds dates, months and years 
     */
   public static function addDate($givendate,$day = 0, $mth = 0, $yr = 0, $s = 0,$i = 0, $hr = 0) {
        $cd = strtotime($givendate);
        $newdate = date('Y-m-d', mktime(date('h', $cd) +$hr, date('i', $cd) + $i, date('s', $cd) + $s, date('m', $cd) + $mth, date('d', $cd) + $day, date('Y', $cd) + $yr));
        return $newdate;
    }
   public static function reduceDate($givendate,$day = 0, $mth = 0, $yr = 0, $s = 0,$i = 0, $hr = 0) {
        $cd = strtotime($givendate);
        $newdate = date('Y-m-d', mktime(date('h', $cd) -$hr, date('i', $cd) - $i, date('s', $cd) - $s, date('m', $cd) - $mth, date('d', $cd) - $day, date('Y', $cd) - $yr));
        return $newdate;
    }
    
    /**
     * This function will return the related child values for Cgrid view 
     */
    public static function getRelatedParentChildModelValues($model, $attributeValue, $parentRelation, $childRelation, $relatedAttribute) {
        $value = null;
        if(!empty($attributeValue) && isset($model->$parentRelation) && isset($model->$parentRelation->$childRelation)){  $value = $model->$parentRelation->$childRelation->$relatedAttribute;   }
        return $value;
        
    }
    public static function getRelatedModelValues($model, $attributeValue, $relation, $relatedAttribute) {
        $value = null;
        if(!empty($attributeValue) && isset($model->$relation)){  $value = $model->$relation->$relatedAttribute;   }
        return $value;
        
    }
    
    public static function formatNumber($value, $precsion = 3, $decPoint = '.', $thousandSep = ','){
        return number_format(self::cleanNumber($value), $precsion, $decPoint, $thousandSep);
    }
    public static function cleanNumber($value){
        return str_replace(',', '', $value);
    }

}
