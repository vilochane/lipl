<?php

class PeCusGuarantorController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = $this->decryptParam($id);        
        $modelBankDetails = new PeCusGuarantorBankDetails;
        $modelIncomeSources = new PeCusGuarantorIncomeSources;        
        $modelBankDetails->unsetAttributes();
        $modelIncomeSources->unsetAttributes();        
        if (isset($_POST['PeCusGuarantorBankDetails']))
            $modelBankDetails->attributes = $_POST['PeCusGuarantorBankDetails'];
        if (isset($_POST['PeCusGuarantorBankDetails']))
            $modelIncomeSources->attributes = $_POST['PeCusGuarantorBankDetails'];
        $modelBankDetails->guarantor_id = $modelIncomeSources->guarantor_id = $id;
        $this->render('view', array('model' => $this->loadModel($id), 'modelBankDetails' => $modelBankDetails, 'modelIncomeSources' => $modelIncomeSources));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($c) {
        $tabluarInput = new CommonTabularInput;
        $model = new PeCusGuarantor;
        $model->cus_id = $this->getUrlQuery('c');
        $model->branch_id = $this->getUserWorkingBranchId();
        $childModels = array('PeCusGuarantorBankDetails' => array(), "PeCusGuarantorIncomeSources" => array());
        $childModelsPostsArray = $incomeSourcePost = $bankDetailsPost = array();
        if (isset($_POST['PeCusGuarantor'])) {
            $model->attributes = $_POST['PeCusGuarantor'];
            $this->setLoggingData($model);
            if (isset($_POST['PeCusGuarantorBankDetails'])) {
                $bankDetailsPost = $_POST['PeCusGuarantorBankDetails'];
            }
            if (isset($_POST['PeCusGuarantorIncomeSources'])) {
                $incomeSourcePost = $_POST['PeCusGuarantorIncomeSources'];
            }
            $childModelsPostsArray = array('PeCusGuarantorBankDetails' => $bankDetailsPost, 'PeCusGuarantorIncomeSources' => $incomeSourcePost);
            $childModelParamsArray = array(
                'PeCusGuarantorBankDetails' => array("updated_by" => $this->getUserId(), "updated_date" => $this->dateTimeOrTimeStamp()),
                'PeCusGuarantorIncomeSources' => array("updated_by" => $this->getUserId(), "updated_date" => $this->dateTimeOrTimeStamp())
            );
			$model->save();
            $childModelsPkCol = array("PeCusGuarantorBankDetails" => "bank_details_id", "PeCusGuarantorIncomeSources" => "income_source_id");            
            if ($model->validate() && $tabluarInput->isChildModelsValid()) {
				$childModels = $tabluarInput->setModels($childModelsPostsArray, $childModelParamsArray, $childModelsPkCol, false, $model);
                Yii::app()->user->setFlash('success', $this->getCreateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->guarantor_id), 'c' => $this->getUrlQuery('c', false)));
            }
        }

        $this->render('create', array('model' => $model, 'childModels' => $childModels));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $tabluarInput = new CommonTabularInput;
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);
        $model->cus_id = $this->getUrlQuery('c');
        $model->branch_id = $this->getUserWorkingBranchId();
        $childModelsPostsArray = $incomeSourcePost = $bankDetailsPost = array();
        $childModels = array('PeCusGuarantorBankDetails' => array(), "PeCusGuarantorIncomeSources" => array());
        $bankDetails = PeCusGuarantorBankDetails::model()->findAll(new CDbCriteria(array("condition" => "guarantor_id = :guarantorId", "params" => array(":guarantorId" => $model->guarantor_id))));
        $incomeSources = PeCusGuarantorIncomeSources::model()->findAll(new CDbCriteria(array("condition" => "guarantor_id = :guarantorId", "params" => array(":guarantorId" => $model->guarantor_id))));
        if (count($bankDetails) > 0) {
            foreach ($bankDetails as $modelObj) {
                $childModels['PeCusGuarantorBankDetails'][] = $modelObj;
            }
        }
        if (count($incomeSources) > 0) {
            foreach ($incomeSources as $modelObj) {
                $childModels['PeCusGuarantorIncomeSources'][] = $modelObj;
            }
        }
        if (isset($_POST['PeCusGuarantor'])) {
            $model->attributes = $_POST['PeCusGuarantor'];
            $this->setLoggingData($model);
            if (isset($_POST['PeCusGuarantorBankDetails'])) {
                $bankDetailsPost = $_POST['PeCusGuarantorBankDetails'];
            }
            if (isset($_POST['PeCusGuarantorIncomeSources'])) {
                $incomeSourcePost = $_POST['PeCusGuarantorIncomeSources'];
            }
            $childModelsPostsArray = array('PeCusGuarantorBankDetails' => $bankDetailsPost, 'PeCusGuarantorIncomeSources' => $incomeSourcePost);
            $childModelParamsArray = array(
                'PeCusGuarantorBankDetails' => array("updated_by" => $this->getUserId(), "updated_date" => $this->dateTimeOrTimeStamp()),
                'PeCusGuarantorIncomeSources' => array("updated_by" => $this->getUserId(), "updated_date" => $this->dateTimeOrTimeStamp())
            );
            $childModelsPkCol = array("PeCusGuarantorBankDetails" => "bank_details_id", "PeCusGuarantorIncomeSources" => "income_source_id");
            $childModels = $tabluarInput->setModels($childModelsPostsArray, $childModelParamsArray, $childModelsPkCol, false, $model);
            if ($model->validate() && $tabluarInput->isChildModelsValid()) {
                Yii::app()->user->setFlash('success', $this->getUpdateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->guarantor_id), 'c' => $this->getUrlQuery('c', false)));
            }
        }

        $this->render('update', array('model' => $model, 'childModels' => $childModels));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $id = $this->decryptParam($id);
//        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->redirect(array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->layout = '//layouts/column1';
        $model = new PeCusGuarantor('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['PeCusGuarantor']))
            $model->attributes = $_GET['PeCusGuarantor'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PeCusGuarantor the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = PeCusGuarantor::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param PeCusGuarantor $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pe-cus-guarantor-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
