<?php

class HipVehichleDetailsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'AjaxCreate'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = $this->decryptParam($id);
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new HipVehichleDetails;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['HipVehichleDetails'])) {
            $model->attributes = $_POST['HipVehichleDetails'];
            $this->setLoggingData($model);
            if ($model->save()) {
                Yii::app()->user->setFlash('success', $this->getCreateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->vehicle_details_id)));
            }
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionAjaxCreate() {
        $this->preventJscriptLoading();
        $this->layout = '//layouts/column4';
        $select2Id = $this->getUrlQuery('s2', false);
        $dialogId = $this->getUrlQuery('di', false);
        $model = new HipVehichleDetails;
        $response = array('errorSummary'=>'', 'newOption'=>'', 'optionString'=>'');
        $this->performAjaxValidation($model);

        if (isset($_POST['HipVehichleDetails'])) {
            $model->attributes = $_POST['HipVehichleDetails'];
            $this->setLoggingData($model);
            if ($model->save()) {
              $response['newOption'] = $model->vehicle_details_id;
              $listOptions = CommonListData::getListDataVehicles();
              $response['optionString'] = CommonSystem::getAjaxOptionTags($listOptions, 'vehicle_type_id', 'vehicle_details_reg');
            }
            $response['errorSummary'] = CHtml::errorSummary($model);
            echo CJSON::encode($response);
            Yii::app()->end();
        }

        $this->render('_form-popup', array( 'model' => $model, 'select2Id' => $select2Id, 'dialogId'=> $dialogId));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['HipVehichleDetails'])) {
            $model->attributes = $_POST['HipVehichleDetails'];
            $this->setLoggingData($model);
            if ($model->save()) {
                Yii::app()->user->setFlash('success', $this->getUpdateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->vehicle_details_id)));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $id = $this->decryptParam($id);
//        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->redirect(array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->layout = '//layouts/column1';
        $model = new HipVehichleDetails('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['HipVehichleDetails']))
            $model->attributes = $_GET['HipVehichleDetails'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return HipVehichleDetails the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = HipVehichleDetails::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param HipVehichleDetails $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'hip-vehichle-details-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
