<?php

class PeCustomerController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = $this->decryptParam($id);
        $cusBankDetails = new PeCusBankDetails;
        $cusCreditDetails = new PeCusCreditRefDetails;
        $cusBankDetails->unsetAttributes();  // clear any default values
        if (isset($_GET['PeCusBankDetails']))
            $cusBankDetails->attributes = $_GET['PeCusBankDetails'];
        $cusCreditDetails->unsetAttributes();
        if (isset($_GET['PeCusCreditRefDetails']))
            $cusCreditDetails->attributes = $_GET['PeCusCreditRefDetails'];
        $cusBankDetails->cus_id = $cusCreditDetails->cus_id = $id;
        $this->render('view', array('model' => $this->loadModel($id), 'cusBankDetails' => $cusBankDetails, 'cusCreditDetails' => $cusCreditDetails
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $tabluarInput = new CommonTabularInput;
        $model = new PeCustomer;
        $childModels = array("PeCusBankDetails" => array(), "PeCusCreditRefDetails" => array());
        $childModelsPostsArray = $creditRefPost = $bankDetailsPost = array();
        $model->branch_id = $this->getUserWorkingBranchId();
        $nicImgFileObj = new HUploadedFile('images/cusImages/nicImages', $model, 'cus_nic_scan_copy');
        $cusImgFileObj = new HUploadedFile('images/cusImages/cusImages', $model, 'cus_image');
        
        if (isset($_POST['PeCustomer'])) {
            $model->attributes = $_POST['PeCustomer'];
            $this->setLoggingData($model);
            /* if any uploaded images */
            $nicImgFileObj->setFile();
            $cusImgFileObj->setFile();
            if (isset($_POST['PeCusBankDetails'])) {
                $bankDetailsPost = $_POST['PeCusBankDetails'];
            }
            if (isset($_POST["PeCusCreditRefDetails"])) {
                $creditRefPost = $_POST['PeCusCreditRefDetails'];
            }
            $childModelsPostsArray = array("PeCusBankDetails" => $bankDetailsPost, "PeCusCreditRefDetails" => $creditRefPost);
            $childModelParamsArray = array(
                "PeCusBankDetails" => array("updated_by" => $this->getUserId(), "updated_date" => $this->dateTimeOrTimeStamp()),
                "PeCusCreditRefDetails" => array("updated_by" => $this->getUserId(), "updated_date" => $this->dateTimeOrTimeStamp()));
            $childModelsPkCol = array("PeCusBankDetails" => "bank_details_id", "PeCusCreditRefDetails" => "credit_ref_details_id");
            $childModels = $tabluarInput->setModels($childModelsPostsArray, $childModelParamsArray, $childModelsPkCol, false, $model);            

            if ($model->validate() && $tabluarInput->isChildModelsValid()) {
                $nicImgFileObj->saveFile();
                $cusImgFileObj->saveFile();
                Yii::app()->user->setFlash('success', $this->getCreateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->cus_id)));
            }
        }
        $this->render('create', array('model' => $model, "childModels" => $childModels));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);
        $nicImgFileObj = new HUploadedFile('images/cusImages/nicImages', $model, 'cus_nic_scan_copy');
        $cusImgFileObj = new HUploadedFile('images/cusImages/cusImages', $model, 'cus_image');
        $tabluarInput = new CommonTabularInput;
        $childModelsPostsArray = $creditRefPost = $bankDetailsPost = array();
        $childModels = array("PeCusBankDetails" => array(), "PeCusCreditRefDetails" => array());
        $bankDetails = PeCusBankDetails::model()->findAll(new CDbCriteria(array("condition" => "cus_id = :cusId", "params" => array(":cusId" => $id))));
        $creditDetails = PeCusCreditRefDetails::model()->findAll(new CDbCriteria(array("condition" => "cus_id = :cusId", "params" => array(":cusId" => $id))));
        if (count($bankDetails) > 0) {
            foreach ($bankDetails as $modelObj) {
                $childModels["PeCusBankDetails"][] = $modelObj;
            }
        }
        if (count($creditDetails) > 0) {
            foreach ($creditDetails as $modelObj) {
                $childModels["PeCusCreditRefDetails"][] = $modelObj;
            }
        }
        // $this->performAjaxValidation($model);
        if (isset($_POST['PeCustomer'])) {
            $model->attributes = $_POST['PeCustomer'];
            $this->setLoggingData($model);
            /* if any uploaded images */
            $nicImgFileObj->setFile();
            $cusImgFileObj->setFile();
            if (isset($_POST['PeCusBankDetails'])) {
                $bankDetailsPost = $_POST['PeCusBankDetails'];
            }
            if (isset($_POST["PeCusCreditRefDetails"])) {
                $creditRefPost = $_POST['PeCusCreditRefDetails'];
            }
            $childModelsPostsArray = array("PeCusBankDetails" => $bankDetailsPost, "PeCusCreditRefDetails" => $creditRefPost);
            $childModelParamsArray = array(
                "PeCusBankDetails" => array("updated_by" => $this->getUserId(), "updated_date" => $this->dateTimeOrTimeStamp()),
                "PeCusCreditRefDetails" => array("updated_by" => $this->getUserId(), "updated_date" => $this->dateTimeOrTimeStamp()));
            $childModelsPkCol = array("PeCusBankDetails" => "bank_details_id", "PeCusCreditRefDetails" => "credit_ref_details_id");
            $childModels = $tabluarInput->setModels($childModelsPostsArray, $childModelParamsArray, $childModelsPkCol, false, $model);
            if ($model->validate() && $tabluarInput->isChildModelsValid()) {
                $nicImgFileObj->saveFile();
                $cusImgFileObj->saveFile();
                Yii::app()->user->setFlash('success', $this->getUpdateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->cus_id)));
            }
        }
        $this->render('update', array('model' => $model, "childModels" => $childModels));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $id = $this->decryptParam($id);
//        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->redirect(array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->layout = '//layouts/column1';
        $model = new PeCustomer('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['PeCustomer']))
            $model->attributes = $_GET['PeCustomer'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PeCustomer the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = PeCustomer::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param PeCustomer $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pe-customer-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
