<?php

class HipPaymentsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'AjaxUpdate', 'AjaxView', 'AjaxUpdate', 'RollbackPaymentsMade','PaymentsMade', 'reversePayment'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = $this->decryptParam($id);
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionAjaxView($id) {
        $id = $this->decryptParam($id);
        $this->preventJscriptLoading();
        $this->renderPartial('view-popup', array('model' => $this->loadModel($id), false, true));
    }

    public function actionReversePayment($id) {
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);
        $model->reversePayment();
        $this->redirect(array('RollbackPaymentsMade'));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new HipPayments;
        if (isset($_POST['HipPayments'])) {
            $model->attributes = $_POST['HipPayments'];
            $this->setLoggingData($model);
            if ($model->save()) {
                Yii::app()->user->setFlash('success', $this->getCreateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->payment_id)));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);
        $agreementModel = $model->agreement;   
		$model->dueDisplayVal = $model->payment_payable;
		//var_dump($model->payment_payable);		
        if (isset($_POST['HipPayments'])) {
            $model->attributes = $_POST['HipPayments'];
            $this->setLoggingData($model);
            $paymentReceivedDate = $model->payment_received_date;
            if (!empty($paymentReceivedDate)) {
                $this->updatePaymentRecord($agreementModel, $model, $paymentReceivedDate);
				$model->dueDisplayVal = $model->payment_payable;
            }
            $model->updateReceivedPayment();
        }
        if (isset($_POST['ajax_request']) && $_POST['ajax_request'] === 'hip-payments-form') {
            $cs = Yii::app()->clientScript;
            $cs->registerScriptFile(Yii::app()->request->baseUrl . "/js/jquery.noty.packaged.min.js", CClientScript::POS_HEAD);
            $this->renderPartial('_form', array('model' => $model), false, true);
            Yii::app()->end();
        }		
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionAjaxUpdate($id, $dialogId) {
        $this->preventJScriptLoading();
        $dialogId = $this->decryptParam($dialogId);
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);
        $model->payment_payable = $model->roundValue($model->payment_payable);
        if (isset($_POST['HipPayments'])) {
            $model->attributes = $_POST['HipPayments'];
            $this->setLoggingData($model);
            if ($model->updatePaymentRecord()) {
                $model->validate();
                $response["errorSummary"] = CHtml::errorSummary($model);
                $model->save();
                echo CJSON::encode($response);
                Yii::app()->end();
            }
        }
        $this->renderPartial('_form-popup', array('model' => $model, 'dialogId' => $dialogId), false, true);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $id = $this->decryptParam($id);
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->redirect(array('admin'));
    }

    public function actionRollbackPaymentsMade() { //closed agreements should not display
        $this->layout = '//layouts/column1';
        $model = new HipPayments('search');
        $model->unsetAttributes();  // clear any default values
        $model->payment_agreement_status = '0';
        $model->payment_status = '1';
        if (isset($_GET['HipPayments']))
            $model->attributes = $_GET['HipPayments'];

        $this->render('admin-rollback-payments-made', array(
            'model' => $model,
        ));
    }
    public function actionPaymentsMade() { //closed agreements should not display
        $this->layout = '//layouts/column1';
        $model = new HipPayments('search');
        $model->unsetAttributes();  // clear any default values
        $model->payment_agreement_status = '0';
        $model->payment_status = '1';
        if (isset($_GET['HipPayments']))
            $model->attributes = $_GET['HipPayments'];

        $this->render('admin-payments-made', array(
            'model' => $model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->layout = '//layouts/column1';
        /* fetching all the unclosed agreements if the start date is less than or equal today and next due date is  */
        $agreementModels = HipAgreement::notClosedAgreements();
        if (count($agreementModels)) {
            foreach ($agreementModels as $agreementModel) {
                $this->setPaymentRecord($agreementModel);
            }
        }
        $model = new HipPayments('search');
        $model->unsetAttributes();  // clear any default values
        $model->payment_status = '0';
        if (isset($_GET['HipPayments']))
            $model->attributes = $_GET['HipPayments'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    private function setPaymentRecord($agreementModel) {
        $agreementModel->isInitialRecord = false; /* setting the flag of agreement model */
        //must search payment records if not found it's a initial record
        $paymentModel = HipPayments::model()->find(array('order' => 'payment_id DESC', 'condition' => 'agreement_id =:agreementId AND payment_status = "0"', 'params' => array(':agreementId' => $agreementModel->agreement_id)));
        /* if no payment records found */
        //var_dump(($paymentModel->payment_status));
        if (!empty($paymentModel)) {
            $this->updatePaymentRecord($agreementModel, $paymentModel, date('Y-m-d'));
        } else { /* initial record */
            $agreementModel->isInitialRecord = true; /* setting the flag of agreement model */
            $this->createNewPaymentRecord($agreementModel, date('Y-m-d')); /* in here should search for not closed payment models for agreement */
        }
    }

    private function updatePaymentRecord($agreementModel, $paymentModel, $today) {
        $this->setPaymentModelPenaltyAttributes($paymentModel); /* setting penalty rate and grace period */
        $paymentModel->setModelValues($agreementModel, $today);
    }

    /**
     * creating new record Payment record 
     */
    private function createNewPaymentRecord($agreementModel, $today) {
        $paymentModel = new HipPayments();
        $this->setPaymentModelPenaltyAttributes($paymentModel); /* setting penalty rate and grace period */
        $paymentModel->setModelValues($agreementModel, $today);
    }

    private function setPaymentModelPenaltyAttributes($paymentModel) {
        $paymentModel->dailyPenaltyRate = ((5 / 100) / 31); /* 5% monthly penalty interest rate and grace period must be retrieved from db later */
        $paymentModel->gracePeriod = 5; /* grace period days */
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return HipPayments the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = HipPayments::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param HipPayments $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'hip-payments-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
