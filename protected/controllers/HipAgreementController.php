<?php

class HipAgreementController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'updateAll', 'viewAll', 'AdminAll'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = $this->decryptParam($id);
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionViewAll($id) {
        $id = $this->decryptParam($id);
        $this->render('view-all', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($c) {
        $cusId = $this->decryptParam($c);
        $model = new HipAgreement;
        $model->cus_id = $cusId;
        $this->setAgreementCode($model);
        if (isset($_POST['HipAgreement'])) {
            $model->attributes = $_POST['HipAgreement'];
            $this->setLoggingData($model);
            $model->agreement_code = $model->prefix_name . $model->agreement_no;            
            $model->setRental();
            $model->setRentalInterestAndInterestTotal();
            $model->setRentalTotal();
            $model->setRentalRate();
            $model->setRentalInterestRate();
            $model->setDueCapital();           
            /* this sets the current capital and current due*/
            $model->setCurrentDueCapitalAndInterest();
            $model->setCollectedPenaltyAmountTotals();
            $model->setCurrentDue();
            $model->setDueDates();
           // var_dump($model);
            if ($model->save()) {
                $this->setVehicleStatus($model->vehicle_details_id);
                Yii::app()->user->setFlash('success', $this->getCreateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->agreement_id), 'c' => $c));
            }
        }
        $this->render('create', array('model' => $model, 'c' => $c));
    }

    private function setAgreementCode($model) {
        $sequence = Common::getAgreementCode();
        $model->prefix_name = $sequence['prefix'];
        $model->agreement_no = $sequence['sequence'];
        $model->agreement_code = $sequence['prefix'] . $sequence['sequence'];
    }

    /**
     * This fucnton sets the vehiccle status to 1 "Hired"
     */
    private function setVehicleStatus($vehicleId) {
        $vehicleModel = HipVehichleDetails::model()->findByPk($vehicleId);
        if (!empty($vehicleModel)) {
            $vehicleModel->vehicle_details_status = '1';
            $vehicleModel->save();
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $c) {
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);
        if (isset($_POST['HipAgreement'])) {
            $model->attributes = $_POST['HipAgreement'];
            $this->setLoggingData($model);
                        $model->setDueCapital();           
            /* this sets the current capital and current due */
            $model->setCurrentDueCapitalAndInterest();
            $model->setCollectedPenaltyAmountTotals();
            $model->setCurrentDue();
            $model->setDueDates();
            if ($model->save()) {
                $this->setVehicleStatus($model->vehicle_details_id);
                Yii::app()->user->setFlash('success', $this->getUpdateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->agreement_id), 'c' => $c));
            }
        }

        $this->render('update', array('model' => $model, 'c' => $c));
    }

    public function actionUpdateAll($id) {
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);
        if (isset($_POST['HipAgreement'])) {
            $model->attributes = $_POST['HipAgreement'];
            $this->setLoggingData($model);
            if ($model->save()) {
                Yii::app()->user->setFlash('success', $this->getUpdateSuccessMessage());
                $this->redirect(array('viewAll', 'id' => $this->encryptParam($model->agreement_id)));
            }
        }

        $this->render('update-all', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $id = $this->decryptParam($id);
//        $this->loadModel($id)->delete();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->redirect(array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->layout = '//layouts/column1';
        $model = new HipAgreement('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['HipAgreement']))
            $model->attributes = $_GET['HipAgreement'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionAdminAll() {
        $this->layout = '//layouts/column1';
        $model = new HipAgreement('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['HipAgreement']))
            $model->attributes = $_GET['HipAgreement'];

        $this->render('admin-all', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return HipAgreement the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = HipAgreement::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param HipAgreement $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'hip-agreement-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
