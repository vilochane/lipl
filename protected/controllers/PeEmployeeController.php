<?php

class PeEmployeeController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $id = $this->decryptParam($id);
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new PeEmployee;
        $modelSystemUser = new SysUser;
        $model->emp_system_user = 0;
        $model->branchIds[] = $this->getUserWorkingBranchId();
        $nicImgFileObj = new HUploadedFile('images/empImages/nicImages', $model, 'emp_nic_scan_copy');
        $empImgFileObj = new HUploadedFile('images/empImages/empImages', $model, 'emp_image');
        $this->performAjaxValidation($model);
        if (isset($_POST['PeEmployee'])) {
            $model->attributes = $_POST['PeEmployee'];            
            $this->setLoggingData($model);
            /* if any uploaded images */
            $nicImgFileObj->setFile();
            $empImgFileObj->setFile();
            if ($model->emp_system_user === '1') {
                $modelSystemUser->attributes = $_POST['SysUser'];
                $this->setLoggingData($modelSystemUser);
                if ($model->validate() && $modelSystemUser->validate()) {
                    $model->save();
                    /* if any uploaded images */
                    $nicImgFileObj->saveFile();
                    $empImgFileObj->saveFile();
                    $modelSystemUser->emp_id = $model->emp_id;
                    if ($modelSystemUser->save()) {                        
                        $this->setEmployeeBranchMapping($model->branchIds, $model->emp_id, $modelSystemUser->user_id);
                        Yii::app()->user->setFlash('success', $this->getCreateSuccessMessage());
                        $this->redirect(array('view', 'id' => $this->encryptParam($model->emp_id)));
                    }
                }
            } elseif ($model->save()) {
                /* if any uploaded images */
                $nicImgFileObj->saveFile();
                $empImgFileObj->saveFile();
                $this->setEmployeeBranchMapping($model->branchIds, $model->emp_id, $modelSystemUser->user_id);
                Yii::app()->user->setFlash('success', $this->getCreateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->emp_id)));
            }
        }
        $this->render('create', array(
            'model' => $model, 'modelSystemUser' => $modelSystemUser
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $id = $this->decryptParam($id);
        $model = $this->loadModel($id);
        $nicImgFileObj = new HUploadedFile('images/empImages/nicImages', $model, 'emp_nic_scan_copy');
        $empImgFileObj = new HUploadedFile('images/empImages/empImages', $model, 'emp_image');
        $modelSystemUser = CommonUser::getEmployeeUserModel($id);
        $employeeBranches = CommonModelMapping::getEmployeeBranchesMapping($model->emp_id);
        foreach ($employeeBranches as $emp) {
            $model->branchIds[] = $emp->branch_id;
        }
        if (empty($modelSystemUser)) {
            $modelSystemUser = new SysUser;
        }
        // $this->performAjaxValidation($model);

        if (isset($_POST['PeEmployee'])) {
            $model->attributes = $_POST['PeEmployee'];
            $this->setLoggingData($model);            
            /* if any uploaded images */
            $nicImgFileObj->setFile();
            $empImgFileObj->setFile();
            if ($model->emp_system_user === '1') {
                $modelSystemUser->attributes = $_POST['SysUser'];
                $this->setLoggingData($modelSystemUser);
                if ($model->validate() && $modelSystemUser->validate()) {
                    $model->save();
                    $nicImgFileObj->saveFile();
                    $empImgFileObj->saveFile();
                    $modelSystemUser->emp_id = $model->emp_id;
                    if ($modelSystemUser->save()) {
                        $this->setEmployeeBranchMapping($model->branchIds, $model->emp_id, $modelSystemUser->user_id);
                        $this->setUserWorkingBranches($model->branchIds);
                        Yii::app()->user->setFlash('success', $this->getUpdateSuccessMessage());
                        $this->redirect(array('view', 'id' => $this->encryptParam($model->emp_id)));
                    }
                }
            } elseif ($model->save()) {
                $nicImgFileObj->saveFile();
                $empImgFileObj->saveFile();
                $this->setEmployeeBranchMapping($model->branchIds, $model->emp_id);
                $this->setUserWorkingBranches($model->branchIds);
                Yii::app()->user->setFlash('success', $this->getUpdateSuccessMessage());
                $this->redirect(array('view', 'id' => $this->encryptParam($model->emp_id)));
            }
        }
        $this->render('update', array(
            'model' => $model, 'modelSystemUser' => $modelSystemUser
        ));
    }

    /* mapping employee branches */

    public function setEmployeeBranchMapping($branchIdArray, $empId, $userId = null) {
        CommonModelMapping::setModelMapping('MapEmployeeBranch', $branchIdArray, 'emp_id', $empId, 'branch_id', 'employee_branch_status', array('user_id' => $userId));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $id = $this->decryptParam($id);
//        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $this->redirect(array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $this->layout = '//layouts/column1';
        $model = new PeEmployee('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['PeEmployee']))
            $model->attributes = $_GET['PeEmployee'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PeEmployee the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = PeEmployee::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param PeEmployee $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'pe-employee-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
