<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Lakliya Invesment',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),
    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'lak',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('127.0.0.1', '::1'),
        ),
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),

          'db'=>array(
          'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
          ),
         * 
         */
        // uncomment the following to use a MySQL database
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=lipl_sample',
            'emulatePrepare' => true,
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            // uncomment the following to show log messages on web pages
            /*
              array(
              'class'=>'CWebLogRoute',
              ),
             */
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        // this is used in contact page
        'adminEmail' => 'webmaster@example.com',
        'sString1' => '^&{<%2?$!2#@*1()*&^%&4~@!6#DHY&3)2{4',
        'sString2' => '&$*%#^&*(&*(7#$%342#52*(:"{4@&4>{]#29',
        'searchButton' => '<img class="operation-icons" src="./images/systemImages/search_file-50.png" title="Advanced search"/>',
        'addNewButton' => '<button type="button" class="btn btn-default btn-sm" title="Add a new record."><span class="glyphicon glyphicon-plus"></span></button>',
        'createANewRecord' => '<img class="operation-icons" src="./images/systemImages/add_file-50.png" title="Create a new record" />',
        'portal' => '<img class="operation-icons" src="./images/systemImages/portal-48.png" title="Go to portal" />',
        'removeNewRecord' => '<button type="button" class="btn btn-default btn-sm" title="Remove row."><span class="glyphicon glyphicon-minus"></span></button>',
        'deleteRecord' => '<button type="button" class="btn btn-default btn-sm" title="Delete record."><span class="glyphicon glyphicon-trash"></span></button>',
        'noImageAvailable' => '<img class="thumb-mini" src="./images/systemImages/no_image-20.png" title="No image available" />',
    ),
);
