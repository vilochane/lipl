
/** for form submission confirm */
function formSubmitConfirm() {
    noty({layout: "center", type: "warning", text: "Would like to submit the form?", buttons: [
            {addClass: "btn btn-primary", text: "Ok", onClick: function($noty) {
                    $noty.close();
                    console.log($("form"));
                    $("form")[0].submit(); // if there popup since the global selector those will too get submitted so to avoid only submitting buttons form
                }},
            {addClass: "btn btn-warning", text: "Cancel", onClick: function($noty) {
                    $noty.close();
                    noty({layout: "center", type: "information", timeout: 2500, text: "Operation cancelled"});
                }}
        ]});
}

/**
 * Confirmation dialog to detect the form caontent changed and want to leave to
 * another page
 * @returns none.
 */
function confirmationNoty(element) {
    var target = element.attr("href");
    noty({layout: "center", type: "warning", text: "Some changes not saved do you want to leave this page?", buttons: [
            {addClass: "btn btn-primary", text: "Ok", onClick: function($noty) {
                    $noty.close();
                    window.location.href = target;
                }},
            {addClass: "btn btn-warning", text: "Cancel", onClick: function($noty) {
                    $noty.close();
                    noty({layout: "center", type: "information", timeout: 2500, text: "Operation cancelled"});
                }}
        ]});
}
/**
 * reverse payment Confirmation dialog 
 * @returns none.
 */
function reversePaymentConfirmationNoty(element) {
    var target = element.attr("href");
    noty({layout: "center", type: "warning", text: "Are you sure you want to reverse this payment?", buttons: [
            {addClass: "btn btn-primary", text: "Ok", onClick: function($noty) {
                    $noty.close();
                    window.location.href = target;
                }},
            {addClass: "btn btn-warning", text: "Cancel", onClick: function($noty) {
                    $noty.close();
                    noty({layout: "center", type: "information", timeout: 2500, text: "Operation cancelled"});
                }}
        ]});
}


/**
 * Functions to add and remove overlay loader to the body
 */
function addOverlayLoader() {
    $('#overlay-loader').show();
}

function removeOverlayLoader() {
    $('#overlay-loader').hide();
}



function roundValue(value, presicion) {
    var rawValue = new String(value).replace(/,/g, '')
    value = parseFloat(rawValue);
    if ($.type(presicion) === 'undefined') {
        presicion = 3;
    }
    var formattedValue = value.toFixed(presicion);
    return parseFloat(formattedValue);
}

function getCurrentDate(addMonth) {
    var setMonth = 1;
    if (addMonth !== undefined) {
        setMonth = parseInt(addMonth) + setMonth;
    }
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + setMonth; //January is 0!
    var yyyy = today.getFullYear();
    if (mm === 13) {
        var mm = 1;
        var yyyy = today.getFullYear() + 1;
    }
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var date = yyyy + '-' + mm + '-' + dd;
    return date;
}

/**
 * Date set month
 */
function addMonth(dateText, instance, period) {
    var date = $.datepicker.parseDate(instance.datepicker('option', 'dateFormat'), dateText, instance.datepicker('option'));
    date.setMonth(date.getMonth() + parseInt(period));
    return date;
}
/**
 * Date function to set date
 */
function getDate(dateVal, dayVal) {
    var resDate = null;
    var isMatch = dateVal.match(/^([1|2]{1}[0-9]{3}-([0]{1}[1-9]{1}|[1]{1}[0-2]{1})-([0]{1}[1-9]{1}|[1|2]{1}[0-9]|[3]{1}[0-3]{1}))$/g);
    if (isMatch != null) {
        var date = new String(dateVal);
        /*removeing the date part */
        resDate = date.substring(-10, 8);
        resDate = resDate + dayVal;
    }
    return resDate;
}

/* disable date picker */
function disableDatePicker(elementId) {
    $('#' + elementId).datepicker("destroy");
    $('#' + elementId).attr('readonly', 'readonly');

//    $(".datepicker").datepicker();
//
//    Remove:
//            $(".datepicker").datepicker("destroy");
//
//    Enable:
//            $(".datepicker").datepicker('enable');
//
//    Disable:
//            $(".datepicker").datepicker('disable');

}

/* select2 functions */
function disableSelect2(elementId) {
    $('#' + elementId).select2('readonly', true);
}

function select2Val(element, val) {
    $('#' + element).select2().select2('val', val);
}
function select2Empty(element) {
    $('#' + element).select2().select2('val', '');
}

function select2Loading(element) {
    $('label[for = ' + element + ']').parent('span').addClass('loading');
}

function select2RemoveLoading(element) {
    $('label[for = ' + element + ']').parent('span').removeClass('loading');
}

/* This fucntion used for image preview */
function imagePreview(input, elementId) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            document.getElementById(elementId).innerHTML = ['<img class="thumb" src="', e.target.result, '"/>'].join('');
        };
        reader.readAsDataURL(input.files[0]);
    }
}

/** this function empties tabular input table row
 * for row count table rows are counted
 * because if a not empty row between empty row deleted that row index is used for if 
 * a new record added that intermidiate row value replaced by the new one
 * eg- 
 * value
 * empty value this removed and a new value added
 * value
 * new value third row index will be used and the third record will be replaced
 *@param {object} element remove anchor tag 
 */

function removeTableRow(element) {
    element.parent('td').parent('tr').html('');
}

/**
 *This function deletes tabualar input record 
 */

function deleteTableRow(element, ajaxDeleteUrl, deleteMessage) {
    var notyOptions = getNotyOptionsForAlert();
    var deleteUrl = ajaxDeleteUrl;
    var tr = element.parent('td').parent('tr');
    noty({type: notyOptions["prmaryNotyType"], layout: notyOptions["layout"], text: deleteMessage, dismissQueue: notyOptions["dismissQueue"],
        buttons: [
            {addClass: notyOptions["okButtonCss"], text: 'Delete', onClick: function($noty) {
                    /* restricting the records so reducing the current row count */
                    tr.html('');
                    $.post(deleteUrl);
                    $noty.close();
                    noty({layout: notyOptions["layout"], text: "Succesfully deleted", type: notyOptions["okButtonNotyType"], timeout: notyOptions["timeout"]});
                }},
            {addClass: notyOptions["cancelButtonCss"], text: 'Cancel', onClick: function($noty) {
                    $noty.close();
                    noty({layout: notyOptions["layout"], text: notyOptions["cancelMessage"], type: notyOptions["cancelButtonNotyType"], timeout: notyOptions["timeout"]});
                }}
        ]});

}

function initNotyAlert(type, meessage) {
    var notyOptions = getNotyOptionsForAlert();
    noty({type: type, layout: notyOptions["layout"], text: meessage, dismissQueue: notyOptions["dismissQueue"], timeout: notyOptions["timeout"]});

}

/* this function returns the default settings for javascript initialized noty*/
function getNotyOptionsForAlert() {
    var notyOptions = new Array();
    notyOptions["layout"] = "center";
    notyOptions["prmaryNotyType"] = "information";
    notyOptions["dismissQueue"] = true;
    notyOptions["okButtonCss"] = "btn btn-success";
    notyOptions["okButtonNotyType"] = "success";
    notyOptions["cancelButtonCss"] = "btn btn-info";
    notyOptions["cancelButtonNotyType"] = "information";
    notyOptions["cancelMessage"] = "Operation cancelled";
    notyOptions["timeout"] = 2000;
    return notyOptions;
}

/* this function disables the browser refresh */
function disableBrowserRefresh(e) {
    if ((e.which || e.keyCode) === 116) {
        e.preventDefault();
    }
}

/**
 * Set the focus to an element
 * */
function setFocus(elementId) {
    $("#" + elementId + "").focus();
}

